<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class tipoDocumento {
	
	public  $database;
	public  $error;
	private $codEmpresa;
	private $codTipoDocumento;
	private $dscTipoDocumento;
	private $codTipoProcesso;
	private $dscTipoProcesso;
	private $codTipoDispositivo;
	
	public function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getTipoDocumento(){
		if(TRUE == $this->codTipoDocumento && TRUE == $this->codEmpresa){
			
			$objConexao = $this->database;
			
			$strSql = "SELECT COD_TIPO_DOCUMENTO, DSC_TIPO_DOCUMENTO, COD_EMPRESA
					     FROM TIPO_DOCUMENTO 
					    WHERE COD_EMPRESA = :codEmpresa 
					      AND COD_TIPO_DOCUMENTO = :codTipoDocumento";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ':codTipoDocumento';
			$arrayParametros[0][1] = $this->codTipoDocumento;
			$arrayParametros[1][0] = ':codEmpresa';
			$arrayParametros[1][1] = $this->codEmpresa;
			
			$arrayDocumento = $objConexao->executeFetch($strSql, $arrayParametros);
			
			$this->codTipoDocumento = $arrayDocumento[0]["COD_TIPO_DOCUMENTO"];
			$this->dscTipoDocumento = $arrayDocumento[0]["DSC_TIPO_DOCUMENTO"];
			$this->codEmpresa 		= $arrayDocumento[0]["COD_EMPRESA"];
				
			return true;
		}else{
			return false;
		}
	}
	
	public function insTipoDocumento() {
		if(TRUE == $this->dscTipoDocumento && TRUE == $this->codEmpresa){
			$objConexao = $this->database;
			
			$strSql = "SELECT COD_TIPO_DOCUMENTO 
					     FROM TIPO_DOCUMENTO 
					    WHERE ROWNUM = 1 
					    ORDER BY COD_TIPO_DOCUMENTO DESC";
			
			$arrayDocumento = $objConexao->executeFetch($strSql);
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoDocumento";
			$arrayParametros[0][1] = $arrayDocumento[0]["COD_TIPO_DOCUMENTO"] + 1;
			$arrayParametros[1][0] = ":dscTipoDocumento";
			$arrayParametros[1][1] = $this->dscTipoDocumento;
			$arrayParametros[2][0] = ":codEmpresa";
			$arrayParametros[2][1] = $this->codEmpresa;
	
			$strSql = "INSERT INTO TIPO_DOCUMENTO 
					          (COD_TIPO_DOCUMENTO, DSC_TIPO_DOCUMENTO, COD_EMPRESA) 
					   VALUES (:codTipoDocumento, :dscTipoDocumento, :codEmpresa)";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$objConexao->rollbackTrans();
				return false;
			}
			
			return true;
		}else{
			return false;
		}
	}
	
	public function changeTipoDocumento() {
		
		if ($this->codTipoDocumento == TRUE && $this->dscTipoDocumento == TRUE && $this->codEmpresa == TRUE) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoDocumento";
			$arrayParametros[0][1] = $this->codTipoDocumento;
			$arrayParametros[1][0] = ":dscTipoDocumento";
			$arrayParametros[1][1] = $this->dscTipoDocumento;
			$arrayParametros[2][0] = ":codEmpresa";
			$arrayParametros[2][1] = $this->codEmpresa;
	
			$strSql = "UPDATE TIPO_DOCUMENTO 
					      SET DSC_TIPO_DOCUMENTO = :dscTipoDocumento 
					    WHERE COD_TIPO_DOCUMENTO = :codTipoDocumento
					      AND COD_EMPRESA        = :codEmpresa";
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$objConexao->rollbackTrans();
				return false;
			}
			
			return true;
		} else {
			return false;
		}
	}
	
	public function delTipoDocumento() {
	
		if (TRUE == $this->codTipoDocumento) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoDocumento";
			$arrayParametros[0][1] = $this->codTipoDocumento;
			$arrayParametros[1][0] = ":codEmpresa";
			$arrayParametros[1][1] = $this->codEmpresa;
	
			$strSql = "DELETE 
					     FROM TIPO_DOCUMENTO 
					    WHERE COD_TIPO_DOCUMENTO = :codTipoDocumento
					      AND COD_EMPRESA = :codEmpresa";
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$objConexao->rollbackTrans();
				return false;
			}
			
			return true;
		} else {
			return false;
		}
	}
	
	public function  getTipoDocumentoTipoProcesso($codTipoProcesso){
		$objConexao = $this->database;
		
		$strSql = "SELECT TD.COD_TIPO_DOCUMENTO, TD.DSC_TIPO_DOCUMENTO, TDI.COD_TIPO_DISPOSITIVO, 
				          TPTD.QTD_DPI, TPTD.PCT_QUALIDADE_PADRAO, TPTD.QTD_DPI,
				          TPTD.QTD_MAXIMA_PIXEL_ALTURA, TPTD.QTD_MAXIMA_PIXEL_LARGURA,
				          TPTD.QTD_TAMANHO_MAXIMO_KB
                     FROM TIPO_DOCUMENTO TD
                     JOIN TIPO_PROCESSO_TPDOC TPTD ON (TPTD.COD_TIPO_DOCUMENTO = TD.COD_TIPO_DOCUMENTO)
                    WHERE TPTD.COD_TIPO_PROCESSO = :codTipoProcesso
				      AND TD.COD_EMPRESA = :codEmpresa
                    ORDER BY TD.DSC_TIPO_DOCUMENTO";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ':codTipoProcesso';
		$arrayParametros[0][1] = $codTipoProcesso;
		$arrayParametros[1][0] = ':codEmpresa';
		$arrayParametros[1][1] = $this->codEmpresa;
		
		$arrayDocumento = $objConexao->executeFetch($strSql, $arrayParametros);
		
		if (!$arrayDocumento){
			return false;
		}
		
		return $arrayDocumento;
	}
	
	public function  getAllTipoDocumento(){
		$objConexao = $this->database;
	
		$strSql = "SELECT COD_TIPO_DOCUMENTO, DSC_TIPO_DOCUMENTO, COD_EMPRESA
				     FROM TIPO_DOCUMENTO
				    WHERE COD_EMPRESA = :codEmpresa
				    ORDER BY COD_TIPO_DOCUMENTO";
			
		$arrayParametros = array();
		$arrayParametros[0][0] = ':codEmpresa';
		$arrayParametros[0][1] = $this->codEmpresa;
	
		$arrayDocumento = $objConexao->executeFetch($strSql, $arrayParametros);
	
		if (!$arrayDocumento){
			return false;
		}
	
		return $arrayDocumento;
	}
	
	
	//
	//CASO SEJA PASSADO BRANCO NA PESQUISA TRAZ TODOS OS TIPO DE DOCUMENTOS
	/**
	 * BUSCA OS TIPOS DE DOCUMENTO POR  ESCRICAO DO DOCUMENTO
	 * @param integer codEmpresa  
	 * @param string dscTipoDocumento
	 * @return array $arrayDocumento 
	 * PS. Caso dscTipoDocumento seja null ele lista todos os tipos de documento da empresa
	 */	
	public function buscaTiposDocumentoNome(){
		$objConexao = $this->database;
		
		$strSql = "SELECT COD_TIPO_DOCUMENTO, TD.DSC_TIPO_DOCUMENTO, TD.COD_EMPRESA
                     FROM TIPO_DOCUMENTO TD
                    WHERE TD.COD_EMPRESA = :COD_EMPRESA 
                      AND UPPER(TD.DSC_TIPO_DOCUMENTO) LIKE NVL(UPPER('%'||:DSC_TIPO_DOCUMENTO||'%'), TD.DSC_TIPO_DOCUMENTO)
                    ORDER BY TD.DSC_TIPO_DOCUMENTO ASC";
			
		$arrayParametros = array();
		$arrayParametros[0][0] = ':COD_EMPRESA';
		$arrayParametros[0][1] = $this->codEmpresa;
		$arrayParametros[1][0] = ':DSC_TIPO_DOCUMENTO';
		$arrayParametros[1][1] = $this->dscTipoDocumento;
		
		$arrayDocumento = $objConexao->executeFetch($strSql, $arrayParametros);
		
		return $arrayDocumento;

	}
	
	/**
	 * BUSCA OS TIPOS DE DOCUMENTO POR TIPO DE PROCESSO
	 * @param integer codEmpresa
	 * @param integer codTipoProcesso
	 * @return array $arrayDocumento
	 */
	public function buscaTiposDocumentoTipoProcesso(){
		$objConexao = $this->database;
		$arrayParametros = array();
		$and = "";
		if($this->codTipoDispositivo){
			$and = " AND INSTR(TPTD.DSC_LISTA_DISPOSITIVOS, UPPER( :DSC_LISTA_DISPOSITIVO) ) > 0 ";
			$arrayParametros[2][0] = ':DSC_LISTA_DISPOSITIVO';
			$arrayParametros[2][1] = $this->codTipoDispositivo;
		}
		
		$strSql = "SELECT DISTINCT TD.COD_TIPO_DOCUMENTO, TD.DSC_TIPO_DOCUMENTO, TPTD.DSC_LISTA_DISPOSITIVOS, TPTD.NUM_SEQUENCIA
                     FROM TIPO_DOCUMENTO TD
                     JOIN TIPO_PROCESSO_TPDOC TPTD ON (TPTD.COD_TIPO_DOCUMENTO = TD.COD_TIPO_DOCUMENTO)
                    WHERE TPTD.COD_TIPO_PROCESSO = :COD_TIPO_PROCESSO
                      AND TD.COD_EMPRESA = :COD_EMPRESA ". $and ." ORDER BY TPTD.NUM_SEQUENCIA, TD.DSC_TIPO_DOCUMENTO";
		
		
		$arrayParametros[0][0] = ':COD_TIPO_PROCESSO';
		$arrayParametros[0][1] = $this->codTipoProcesso;
		$arrayParametros[1][0] = ':COD_EMPRESA';
		$arrayParametros[1][1] = $this->codEmpresa;
		
		$arrayDocumento = $objConexao->executeFetch($strSql, $arrayParametros);
		
		if (!$arrayDocumento){
			return false;
		}
		
		return $arrayDocumento;
	
	}
	
	public function buscaTiposDocumentoTipoProcessoMobile(){
		$objConexao = $this->database;
		//Verificar se o perfil do usuário é perfil do sistema
		$strSql = "SELECT COUNT(PU.IND_PERFIL_SISTEMA) AS IND_PERFIL_SISTEMA
                     FROM USUARIO_PERFIL_USUARIO UPU
                     JOIN PERFIL_USUARIO PU ON (PU.COD_PERFIL_USUARIO = UPU.COD_PERFIL_USUARIO)
                    WHERE UPU.COD_USUARIO = :COD_USUARIO
                      AND PU.IND_PERFIL_SISTEMA = 'S' ";

		// parametros
		$arrayParametros = array();
		$arrayParametros[0][0]	= ":COD_USUARIO";
		$arrayParametros[0][1]	= $this->codUsuario;
		
		$arrayPerfilSistema	= $objConexao->executeFetch($strSql, $arrayParametros);
		$indPerfilSistema_ = $arrayPerfilSistema[0]['IND_PERFIL_SISTEMA'];
		if($indPerfilSistema_ > 0){
			$indPerfilSistema = 'S';
		}else{
			$indPerfilSistema = 'N';
		}
		
		
		if ($indPerfilSistema == 'S'){
			$arrayParametros = array();
			
			$strSql = "SELECT DISTINCT TPTD.COD_TIPO_PROCESSO,  TD.COD_TIPO_DOCUMENTO, TD.DSC_TIPO_DOCUMENTO, TPTD.DSC_LISTA_DISPOSITIVOS, TPTD.NUM_SEQUENCIA
                     FROM TIPO_DOCUMENTO TD
                     JOIN TIPO_PROCESSO_TPDOC TPTD ON (TPTD.COD_TIPO_DOCUMENTO = TD.COD_TIPO_DOCUMENTO)
                    WHERE TPTD.COD_TIPO_PROCESSO IN (SELECT COD_TIPO_PROCESSO 
FROM TIPO_PROCESSO 
WHERE COD_EMPRESA = :COD_EMPRESA
)
                      AND TD.COD_EMPRESA = :COD_EMPRESA
					  AND TPTD.DSC_LISTA_DISPOSITIVOS LIKE '%M%'  ORDER BY TPTD.NUM_SEQUENCIA, TD.DSC_TIPO_DOCUMENTO";
			
			$arrayParametros[0][0] = ':COD_USUARIO';
			$arrayParametros[0][1] = $this->codUsuario;
			$arrayParametros[1][0] = ':COD_EMPRESA';
			$arrayParametros[1][1] = $this->codEmpresa;
		
			$arrayDocumento = $objConexao->executeFetch($strSql, $arrayParametros);
		
			if (!$arrayDocumento){
				return false;
			}
		
			return $arrayDocumento;
			
		}else{
			$arrayParametros = array();
				
			$strSql = "SELECT DISTINCT TPTD.COD_TIPO_PROCESSO,  TD.COD_TIPO_DOCUMENTO, TD.DSC_TIPO_DOCUMENTO, TPTD.DSC_LISTA_DISPOSITIVOS, TPTD.NUM_SEQUENCIA
	                     FROM TIPO_DOCUMENTO TD
	                     JOIN TIPO_PROCESSO_TPDOC TPTD ON (TPTD.COD_TIPO_DOCUMENTO = TD.COD_TIPO_DOCUMENTO)
	                    WHERE TPTD.COD_TIPO_PROCESSO IN (SELECT COD_TIPO_PROCESSO
						FROM TIPO_PROCESSO
						WHERE COD_EMPRESA = :COD_EMPRESA
						AND COD_TIPO_PROCESSO IN (SELECT PUTP.COD_TIPO_PROCESSO
								FROM PERFIL_USUARIO_TIPO_PROCESSO PUTP
							WHERE PUTP.COD_PERFIL_USUARIO IN ( SELECT LISTAGG(COD_PERFIL_USUARIO, ',') WITHIN GROUP (ORDER BY COD_PERFIL_USUARIO) AS COD_PERFIL_USUARIO
			             	FROM USUARIO_PERFIL_USUARIO PUP
							WHERE COD_USUARIO = :COD_USUARIO ) ))
	                      AND TD.COD_EMPRESA = :COD_EMPRESA
						  AND TPTD.DSC_LISTA_DISPOSITIVOS LIKE '%M%'  ORDER BY TPTD.NUM_SEQUENCIA, TD.DSC_TIPO_DOCUMENTO";
				
			$arrayParametros[0][0] = ':COD_USUARIO';
			$arrayParametros[0][1] = $this->codUsuario;
			$arrayParametros[1][0] = ':COD_EMPRESA';
			$arrayParametros[1][1] = $this->codEmpresa;
			
			$arrayDocumento = $objConexao->executeFetch($strSql, $arrayParametros);
			
			if (!$arrayDocumento){
				return false;
			}
			
			return $arrayDocumento;
		}
	
	}
	
	/**
	 * BUSCA AS CONFIGURAÇOES DO TIPO DE DOCUMENTO PARA O PROCESSO SELECIONADO
	 * % QUALIDADE PADRAO, QTD DE DPI, QUANTIDADE MAXIMA DE PIXEL ALTURA, TAMANHO MAXIMO EM KB, 
	 * QUANTIDADE DE BITS, 
	 * @param integer codEmpresa
	 * @param integer codTipoProcesso
	 * @return array $arrayDocumento
	 */
	public function buscaTipoDocumentoTipoProcessoConfiguracao(){
		$objConexao = $this->database;
	
		$strSql = "SELECT COD_TIPO_DOCUMENTO, COD_TIPO_PROCESSO, DSC_LISTA_DISPOSITIVOS, QTD_MAXIMA_PIXEL_ALTURA, QTD_MAXIMA_PIXEL_LARGURA,
	                      QTD_TAMANHO_MAXIMO_KB, PCT_QUALIDADE_PADRAO, QTD_DPI
	                 FROM TIPO_PROCESSO_TPDOC
	                WHERE COD_TIPO_PROCESSO = :COD_TIPO_PROCESSO
	                  AND COD_TIPO_DOCUMENTO = :COD_TIPO_DOCUMENTO";
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ':COD_TIPO_PROCESSO';
		$arrayParametros[0][1] = $this->codTipoProcesso;
		$arrayParametros[1][0] = ':COD_TIPO_DOCUMENTO';
		$arrayParametros[1][1] = $this->codTipoDocumento;
	
		$arrayDocumento = $objConexao->executeFetch($strSql, $arrayParametros);
	
		if (!$arrayDocumento){
			return false;
		}
	
		return $arrayDocumento;
	
	}
	
}