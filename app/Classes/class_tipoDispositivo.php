<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class tipoDispositivo {
	
	public  $database;
	private $codTipoDispositivo;
	private $dscTipoDispositivo;
	
	public function __construct($database) {
		$this->database = $database;
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getCodTipoDispositivo() {
		return $this->codTipoDispositivo;
	}
	
	public function setCodTipoDispositivo($codTipoDispositivo) {
		$this->codTipoDispositivo = $codTipoDispositivo;
	}
	
	public function getDscTipoDispositivo() {
		return $this->dscTipoDispositivo;
	}
	
	public function setDscTipoDispositivo($dscTipoDispositivo) {
		$this->dscTipoDispositivo = $dscTipoDispositivo;
	}
	
	
	
	public function getTipoDispositivo($codTipoDispositivo){
		$objConexao = $this->database;
		
		$strSql = "SELECT COD_TIPO_DISPOSITIVO, DSC_TIPO_DISPOSITIVO 
				     FROM TIPO_DISPOSITIVO 
				    WHERE COD_TIPO_DISPOSITIVO = :codTipoDispositivo";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ':codTipoDispositivo';
		$arrayParametros[0][1] = $codTipoDispositivo;
		
		$arrayDispositivo = $objConexao->executeFetch($strSql, $arrayParametros);
		
		$this->codTipoDispositivo = $arrayDispositivo[0]["COD_TIPO_DISPOSITIVO"];
		$this->dscTipoDispositivo = $arrayDispositivo[0]["DSC_TIPO_DISPOSITIVO"];
		
		return true;
		
	}
	
	public function insTipoDispositivo() {
		return true;
	}
	
	public function changeTipoDispositivo() {
		return true;
	}
	
	public function delTipoDispositivo($codTipoDispositivo) {
		return true;
	}
	
	public function getAllTipoDispositivo(){
		$objConexao = $this->database;
	
		$strSql = "SELECT COD_TIPO_DISPOSITIVO, DSC_TIPO_DISPOSITIVO 
				     FROM TIPO_DISPOSITIVO
				    ORDER BY COD_TIPO_DISPOSITIVO";
	
		$arrayDispositivo = $objConexao->executeFetch($strSql);
	
		return $arrayDispositivo;
	
	}
}