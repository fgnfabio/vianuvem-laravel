<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class tipoProcesso
{
	public    $database;
	public    $error;
	
	protected $arrayCampos;
	protected $arrayTipoDocDispositivo;
	protected $arrayDocumentoCampo;
	protected $arrayTipoProcesso;
	protected $codEmpresa;
	protected $codGrupo;
	protected $codTipoProcesso;
	protected $dscTipoProcesso;
	protected $codUsuario;
	protected $codFilial;
	protected $codEstabelecimento;
	protected $dscTipoLista;
	protected $codTipoLista;
	protected $indListaSistema;
	protected $indConfereDocumento;
	protected $dscLista;
	protected $codLista;
	protected $codPeso;
	protected $nomCampo;
	protected $labelCampo;
	protected $mascaraCampo;
	protected $tipoCampo;
	protected $qtdChar;
	protected $numOrdem;
	protected $codTipoIdentificador;
	protected $indObrigatorio;
	protected $codTipoDocumento;
	protected $indNotificaUpload;
	protected $indEnviaNotificacao;
	protected $qtdDiasEnvio;
	protected $indPublicado;
	protected $datIni;
	protected $datFim;
	protected $indDocObrigatorio;
	protected $conteudoCampo;
	protected $numSequencia;
	protected $indUtilizaProcesso;
	protected $qtdDiasFechamentoProcesso;
	protected $qtdDiasNotificaExpiracaoProcesso;
	protected $indNotificaAberturaProcesso;
	protected $indNotificaFechamentoProcesso;
	protected $indNotificaCancelamentoProcesso;
	protected $indNotificaPendenciaProcesso;
	 
	
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getCodTipoProcesso() {
		return $this->codTipoProcesso;
	}
	
	public function setCodTipoProcesso($codTipoProcesso) {
		$this->codTipoProcesso = $codTipoProcesso;
	}
	
	public function getDscTipoProcesso() {
		return $this->dscTipoProcesso;
	}
	
	public function getIndPublicado() {
		return $this->indPublicado;
	}
	
	public function setDscTipoProcesso($dscTipoProcesso) {
		$this->dscTipoProcesso = $dscTipoProcesso;
	}
	
	public function getCodGrupo() {
		return $this->codGrupo;
	}
	
	public function setCodGrupo($codGrupo) {
		$this->codGrupo = $codGrupo;
	}
	
	public function getCodEmpresa() {
		return $this->codEmpresa;
	}
	
	public function setCodEmpresa($codEmpresa) {
		$this->codEmpresa = $codEmpresa;
	}
	
	public function getArrayCampos() {
		return $this->arrayCampos;
	}
	
	public function setArrayCampos($arrayCampos) {
		$this->arrayCampos = $arrayCampos;
	}
	
	public function getArrayTipoDocDispositivo() {
		return $this->arrayTipoDocDispositivo;
	}
	
	public function setArrayTipoDocDispositivo($arrayTipoDocDispositivo) {
		$this->arrayTipoDocDispositivo = $arrayTipoDocDispositivo;
	}
	
	public function getTipoProcesso($codTipoProcesso){
		$objConexao = $this->database;
		
		
		if ($_SESSION['codEmpresa'] > 0){
			$codEmpresa = $_SESSION['codEmpresa'];
		}elseif($this->codEmpresa > 0){
			$codEmpresa = $this->codEmpresa;
		}else{
			$codEmpresa = 0;
		}
		
		$strSql = "SELECT COD_TIPO_PROCESSO, DSC_TIPO_PROCESSO, COD_GRUPO, COD_EMPRESA, 
				          IND_PUBLICADO, IND_CONFERE_DOCUMENTO, IND_UTILIZA_PROCESSO, 
				          QTD_DIAS_FECHAMENTO_PROCESSO, QTD_DIAS_NOTIFICA_EXPIRACAO,
				          IND_NOTIFICA_FECHAMENTO, IND_NOTIFICA_CANCELAMENTO, 
				          IND_NOTIFICA_ABERTURA, IND_NOTIFICA_PENDENCIA
				     FROM TIPO_PROCESSO 
				    WHERE COD_TIPO_PROCESSO = :codTipoProcesso
				      AND COD_EMPRESA = ".$codEmpresa;
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codTipoProcesso";
		$arrayParametros[0][1] = $codTipoProcesso;
		
		$arrayTipoProcesso = $objConexao->executeFetch($strSql, $arrayParametros);
		
		$this->codTipoProcesso                  = $arrayTipoProcesso[0]["COD_TIPO_PROCESSO"];
		$this->dscTipoProcesso                  = $arrayTipoProcesso[0]["DSC_TIPO_PROCESSO"];
		$this->codGrupo			                = $arrayTipoProcesso[0]["COD_GRUPO"];
		$this->codEmpresa		                = $arrayTipoProcesso[0]["COD_EMPRESA"];
		$this->indPublicado		                = $arrayTipoProcesso[0]["IND_PUBLICADO"];
		$this->indConfereDocumento              = $arrayTipoProcesso[0]["IND_CONFERE_DOCUMENTO"];
		$this->indUtilizaProcesso               = $arrayTipoProcesso[0]["IND_UTILIZA_PROCESSO"];
		$this->qtdDiasFechamentoProcesso        = $arrayTipoProcesso[0]["QTD_DIAS_FECHAMENTO_PROCESSO"];
		$this->qtdDiasNotificaExpiracaoProcesso = $arrayTipoProcesso[0]["QTD_DIAS_NOTIFICA_EXPIRACAO"];
		$this->indNotificaAberturaProcesso      = $arrayTipoProcesso[0]["IND_NOTIFICA_ABERTURA"];
		$this->indNotificaFechamentoProcesso    = $arrayTipoProcesso[0]["IND_NOTIFICA_FECHAMENTO"];
		$this->indNotificaCancelamentoProcesso  = $arrayTipoProcesso[0]["IND_NOTIFICA_CANCELAMENTO"];
		$this->indNotificaPendenciaProcesso     = $arrayTipoProcesso[0]["IND_NOTIFICA_PENDENCIA"];
		
		if (!$this->codTipoProcesso){
			return false;
		}
		
		$strSql = "SELECT TPC.NOM_CAMPO, TPC.DSC_LABEL_CAMPO, TPC.COD_TIPO_CAMPO, TPC.QTD_CARACTERES, TPC.NUM_ORDEM, 
				          TPC.IND_OBRIGATORIO, NVL(TPC.COD_TIPO_IDENTIFICADOR, 0) AS COD_TIPO_IDENTIFICADOR, TPC.DSC_MASCARA, 
				          TI.DSC_TIPO_IDENTIFICADOR, TI.DSC_CLASSE_VALIDACAO, TPC.COD_TIPO_LISTA, TPC.IND_ENVIA_NOTIFICACAO, TPC.QTD_DIAS_ENVIO
                     FROM TIPO_PROCESSO_CAMPO TPC
                     LEFT JOIN TIPO_IDENTIFICADOR TI ON (TI.COD_TIPO_IDENTIFICADOR = TPC.COD_TIPO_IDENTIFICADOR)
                    WHERE TPC.COD_TIPO_PROCESSO = :codTipoProcesso
				    ORDER BY TPC.NUM_ORDEM";
		
		$this->arrayCampos = $objConexao->executeFetch($strSql, $arrayParametros);
				
		$strSql = "SELECT TPTD.COD_TIPO_DOCUMENTO, TPTD.COD_TIPO_PROCESSO, TPTD.DSC_LISTA_DISPOSITIVOS,
		                  TPTD.PCT_QUALIDADE_PADRAO, TPTD.QTD_DPI, TPTD.QTD_MAXIMA_PIXEL_ALTURA,
		                  TPTD.QTD_MAXIMA_PIXEL_LARGURA, TPTD.QTD_TAMANHO_MAXIMO_KB,
		                  TD.DSC_TIPO_DOCUMENTO, TPTD.IND_NOTIFICA_UPLOAD, TPTD.IND_OBRIGATORIO, TPTD.NUM_SEQUENCIA
		             FROM TIPO_PROCESSO_TPDOC TPTD
		             JOIN TIPO_DOCUMENTO TD ON (TD.COD_TIPO_DOCUMENTO = TPTD.COD_TIPO_DOCUMENTO )
		            WHERE TPTD.COD_TIPO_PROCESSO = :codTipoProcesso
		            ORDER BY TPTD.NUM_SEQUENCIA, TD.DSC_TIPO_DOCUMENTO";
		
		$this->arrayTipoDocDispositivo = $objConexao->executeFetch($strSql, $arrayParametros);
		return true;
	}
		
	/**
	 * Pega todos os tipos de processo
	 * @return array
	 */
	public function getAllTipoProcesso(){
		$objConexao = $this->database;
		
		$strSql = "SELECT TP.COD_TIPO_PROCESSO, TP.DSC_TIPO_PROCESSO, TP.COD_GRUPO, TP.COD_EMPRESA
                     FROM TIPO_PROCESSO TP
                    ORDER BY TP.DSC_TIPO_PROCESSO";
		
		$arrayProcesso = $objConexao->executeFetch($strSql);
		
		return $arrayProcesso;
	}
	
	/**
	 * Pega o tipo de processo pelo estabelecimento
	 * @param integer $codEstabelecimento
	 * @return array
	 */
	public function getTipoProcessoEstabel($codEstabelecimento){
		$objConexao = $this->database;
	
		$strSql = "SELECT TP.COD_TIPO_PROCESSO, TP.DSC_TIPO_PROCESSO, TP.COD_GRUPO, TP.COD_EMPRESA
  					 FROM TIPO_PROCESSO TP
  					 JOIN ESTABELECIMENTO_TIPO_PROCESSO ETP ON (ETP.COD_TIPO_PROCESSO = TP.COD_TIPO_PROCESSO)
  					 JOIN ESTABELECIMENTO E ON (E.COD_ESTABELECIMENTO = ETP.COD_ESTABELECIMENTO)
 					WHERE E.COD_ESTABELECIMENTO = :codEstabelecimento
 					ORDER BY TP.DSC_TIPO_PROCESSO";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codEstabelecimento";
		$arrayParametros[0][1] = $codEstabelecimento;
	
		$arrayProcesso = $objConexao->executeFetch($strSql, $arrayParametros);
	
		return $arrayProcesso;
	}
	
	
	/**
	 * Pega a classe de validação do campo de um processo
	 * @param integer $codTipoProcesso
	 * @param string $nomCampo
	 * @return string
	 */
	public function getTipoProcessoClasseValidacao($codTipoProcesso, $nomCampo){
		$objConexao = $this->database;
		
		$strSql = "SELECT TI.DSC_CLASSE_VALIDACAO
                     FROM TIPO_PROCESSO TP
                     JOIN TIPO_PROCESSO_CAMPO TPC ON (TPC.COD_TIPO_PROCESSO = TP.COD_TIPO_PROCESSO)
                     JOIN TIPO_IDENTIFICADOR TI ON (TI.COD_TIPO_IDENTIFICADOR = TPC.COD_TIPO_IDENTIFICADOR)
                    WHERE TP.COD_TIPO_PROCESSO = :codTipoProcesso
                      AND TPC.NOM_CAMPO = :nomCampo";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codTipoProcesso";
		$arrayParametros[0][1] = $codTipoProcesso;
		$arrayParametros[1][0] = ":nomCampo";
		$arrayParametros[1][1] = $nomCampo;
		
		$arrayProcesso = $objConexao->executeFetch($strSql, $arrayParametros);
		
		return $arrayProcesso[0]["DSC_CLASSE_VALIDACAO"];
	}
	
	public function getNomeTipoProcesso($codTipoProcesso){
		$objConexao = $this->database;
	
		$strSql = "SELECT DSC_TIPO_PROCESSO
				     FROM TIPO_PROCESSO
				    WHERE COD_TIPO_PROCESSO = :codTipoProcesso";
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codTipoProcesso";
		$arrayParametros[0][1] = $codTipoProcesso;
	
		$arrayTipoProcesso = $objConexao->executeFetch($strSql, $arrayParametros);
	
		$this->dscTipoProcesso 	= $arrayTipoProcesso[0]["DSC_TIPO_PROCESSO"];
		return true;
	}
	
	public function getTipoProcessoUsuario($codUsuario, $codTipoUsuario){
		$objConexao = $this->database;
		
		$strSql = "SELECT PU.IND_PERFIL_SISTEMA, PF.COD_EMPRESA
                     FROM USUARIO U
                     JOIN PESSOA_FISICA PF ON (PF.COD_PESSOA = U.COD_USUARIO)
                     JOIN USUARIO_PERFIL_USUARIO UPU ON (UPU.COD_USUARIO = U.COD_USUARIO)
                     JOIN PERFIL_USUARIO PU ON (PU.COD_PERFIL_USUARIO = UPU.COD_PERFIL_USUARIO)
                    WHERE U.COD_USUARIO = :codUsuario";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codUsuario";
		$arrayParametros[0][1] = $codUsuario;
		
		$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
		
		if ($arrayUsuario[0]["IND_PERFIL_SISTEMA"] == "S"){
			$strSql = "SELECT COD_TIPO_PROCESSO
                         FROM TIPO_PROCESSO TP
                        WHERE TP.COD_EMPRESA = :COD_EMPRESA;";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ':COD_EMPRESA';
			$arrayParametros[0][1] = $arrayUsuario[0]["COD_EMPRESA"];
			
		}else{
			$strSql = "SELECT COD_TIPO_PROCESSO
				             FROM USUARIO_PERFIL_USUARIO UPU
				             JOIN PERFIL_USUARIO PU ON (PU.COD_PERFIL_USUARIO = UPU.COD_PERFIL_USUARIO )
				             JOIN PERFIL_USUARIO_TIPO_PROCESSO PUTP ON (PUTP.COD_PERFIL_USUARIO = PU.COD_PERFIL_USUARIO)
				            WHERE UPU.COD_USUARIO = :COD_USUARIO";
			$arrayParametros = array();
			$arrayParametros[0][0] = ':COD_USUARIO';
			$arrayParametros[0][1] = $codUsuario;
			
		}
		
		$this->arrayTipoProcesso = $objConexao->executeFetch($strSql, $arrayParametros);
		
		return true;
	}
	
	public function listaArvoreProcessos(){
		$objConexao = $this->database;
		
		//$strSql = "SELECT UPU.COD_PERFIL_USUARIO, PU.IND_PERFIL_SISTEMA
		//		     FROM USUARIO_PERFIL_USUARIO UPU
		//		     JOIN PERFIL_USUARIO PU ON (PU.COD_PERFIL_USUARIO = UPU.COD_PERFIL_USUARIO)
		//		    WHERE COD_USUARIO = :COD_USUARIO";
		
		
		
		
		
		// pega perfil do usuario
		$strSql = "SELECT LISTAGG(COD_PERFIL_USUARIO, ',') WITHIN GROUP (ORDER BY COD_PERFIL_USUARIO) AS COD_PERFIL_USUARIO
		             FROM USUARIO_PERFIL_USUARIO PUP
		            WHERE COD_USUARIO = :COD_USUARIO";
	
		// parametros
		$arrayParametros = array();
		$arrayParametros[0][0]	= ":COD_USUARIO";
		$arrayParametros[0][1]	= $this->codUsuario;
	
		$arrayPerfilUsuario	= $objConexao->executeFetch($strSql, $arrayParametros);
		$perfil = $arrayPerfilUsuario[0]['COD_PERFIL_USUARIO'];
		
		//VERIFICA SE ALGUM DOS PERFIS DO USUARIO É PERFIL DE SISTEMA
		$strSql = "SELECT COUNT(PU.IND_PERFIL_SISTEMA) AS IND_PERFIL_SISTEMA
                     FROM USUARIO_PERFIL_USUARIO UPU
                     JOIN PERFIL_USUARIO PU ON (PU.COD_PERFIL_USUARIO = UPU.COD_PERFIL_USUARIO)
                    WHERE UPU.COD_USUARIO = :COD_USUARIO
                      AND PU.IND_PERFIL_SISTEMA = 'S' ";
		
		
		// parametros
		$arrayParametros = array();
		$arrayParametros[0][0]	= ":COD_USUARIO";
		$arrayParametros[0][1]	= $this->codUsuario;
		
		$arrayPerfilSistema	= $objConexao->executeFetch($strSql, $arrayParametros);
		$indPerfilSistema_ = $arrayPerfilSistema[0]['IND_PERFIL_SISTEMA'];
		if($indPerfilSistema_ > 0){
			$indPerfilSistema = 'S';
		}else{
			$indPerfilSistema = 'N';
		}
		
		
		if ($indPerfilSistema == 'S'){
		
			$strSql = "  SELECT LEVEL - 1 AS COD_NIVEL, LPAD(' ', (LEVEL - 1) * 5, '-') || TMP.DSC_TIPO_PROCESSO AS DSC_TIPO_PROCESSO_NIVEL, TMP.COD_TIPO_PROCESSO, TMP.DSC_TIPO_PROCESSO, TMP.COD_TIPO_PROCESSO_PAI, TMP.NUM_SEQUENCIA, TMP.IND_FOLHA, TMP.QTD_TP
			    		       FROM (SELECT TPG.COD_GRUPO + 100000000 AS COD_TIPO_PROCESSO, TPG.DSC_GRUPO AS DSC_TIPO_PROCESSO,
			    		                    TO_NUMBER(DECODE(TPG.COD_GRUPO_PAI, NULL, NULL, TPG.COD_GRUPO_PAI + 100000000)) COD_TIPO_PROCESSO_PAI,
			    		                    'N' AS IND_FOLHA, TPG.NUM_SEQUENCIA, (SELECT COUNT(*) FROM TIPO_PROCESSO TPX WHERE TPX.COD_GRUPO = TPG.COD_GRUPO) AS QTD_TP
			    		               FROM TIPO_PROCESSO_GRUPO TPG
			    		              WHERE TPG.COD_EMPRESA = :COD_EMPRESA
			    		UNION
			    		SELECT TP.COD_TIPO_PROCESSO, TP.DSC_TIPO_PROCESSO, TP.COD_GRUPO + 100000000 AS COD_TIPO_PROCESSO_PAI, 'S' AS IND_FOLHA, NULL AS NUM_SEQUENCIA, NULL AS QTD_TP
			    		  FROM TIPO_PROCESSO TP
			    		 WHERE TP.COD_EMPRESA = :COD_EMPRESA
			    		ORDER BY COD_TIPO_PROCESSO_PAI, DSC_TIPO_PROCESSO
			    		                               ) TMP
			    		START WITH TMP.COD_TIPO_PROCESSO_PAI IS NULL
			    		CONNECT BY PRIOR TMP.COD_TIPO_PROCESSO = TMP.COD_TIPO_PROCESSO_PAI
			    		ORDER SIBLINGS BY TMP.NUM_SEQUENCIA, TMP.DSC_TIPO_PROCESSO";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":COD_EMPRESA";
			$arrayParametros[0][1] = $this->codEmpresa;
			//$arrayParametros[1][0] = ":COD_PERFIL_USUARIO";
			//$arrayParametros[1][1] = $perfil;
			
		}else{
			$strSql = "  SELECT LEVEL - 1 AS COD_NIVEL, LPAD(' ', (LEVEL - 1) * 5, '-') || TMP.DSC_TIPO_PROCESSO AS DSC_TIPO_PROCESSO_NIVEL, TMP.COD_TIPO_PROCESSO, TMP.DSC_TIPO_PROCESSO, TMP.COD_TIPO_PROCESSO_PAI, TMP.NUM_SEQUENCIA, TMP.IND_FOLHA, TMP.QTD_TP
			    		       FROM (SELECT TPG.COD_GRUPO + 100000000 AS COD_TIPO_PROCESSO, TPG.DSC_GRUPO AS DSC_TIPO_PROCESSO,
			    		                    TO_NUMBER(DECODE(TPG.COD_GRUPO_PAI, NULL, NULL, TPG.COD_GRUPO_PAI + 100000000)) COD_TIPO_PROCESSO_PAI,
			    		                    'N' AS IND_FOLHA, TPG.NUM_SEQUENCIA, (SELECT COUNT(*) FROM TIPO_PROCESSO TPX WHERE TPX.COD_GRUPO = TPG.COD_GRUPO) AS QTD_TP
			    		               FROM TIPO_PROCESSO_GRUPO TPG
			    		              WHERE TPG.COD_EMPRESA = :COD_EMPRESA
							    	  UNION
							    	 SELECT TP.COD_TIPO_PROCESSO, TP.DSC_TIPO_PROCESSO, TP.COD_GRUPO + 100000000 AS COD_TIPO_PROCESSO_PAI, 'S' AS IND_FOLHA, NULL AS NUM_SEQUENCIA, NULL AS QTD_TP
							      	   FROM TIPO_PROCESSO TP
							    	  WHERE TP.COD_EMPRESA = :COD_EMPRESA
							    	    AND TP.COD_TIPO_PROCESSO IN (SELECT PUTP.COD_TIPO_PROCESSO
							    		                                 FROM PERFIL_USUARIO_TIPO_PROCESSO PUTP
							    		                                WHERE PUTP.COD_PERFIL_USUARIO IN ( $perfil ) )
							    		  ORDER BY COD_TIPO_PROCESSO_PAI, DSC_TIPO_PROCESSO ) TMP
							   START WITH TMP.COD_TIPO_PROCESSO_PAI IS NULL
						     CONNECT BY PRIOR TMP.COD_TIPO_PROCESSO = TMP.COD_TIPO_PROCESSO_PAI
						       ORDER SIBLINGS BY TMP.NUM_SEQUENCIA, TMP.DSC_TIPO_PROCESSO";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":COD_EMPRESA";
			$arrayParametros[0][1] = $this->codEmpresa;
			
		}
		

		$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
		
		return $retorno;
	}
	
	public function listaArvoreMobileProcessos(){
		$objConexao = $this->database;
		
		//VERIFICA SE O PERFIL DO USUÁRIO TEM ALGUM PERFIL DO SISTEMA
		$strSql = "SELECT COUNT(PU.IND_PERFIL_SISTEMA) AS IND_PERFIL_SISTEMA
                     FROM USUARIO_PERFIL_USUARIO UPU
                     JOIN PERFIL_USUARIO PU ON (PU.COD_PERFIL_USUARIO = UPU.COD_PERFIL_USUARIO)
                    WHERE UPU.COD_USUARIO = :COD_USUARIO
                      AND PU.IND_PERFIL_SISTEMA = 'S'";
		$arrayParametros = array();
		$arrayParametros[0][0] = ":COD_USUARIO";
		$arrayParametros[0][1] = $this->codUsuario;
		
		$arrayUsuarioPerfil = $objConexao->executeFetch($strSql, $arrayParametros);
		$UsuarioPerfil = $arrayUsuarioPerfil[0]['IND_PERFIL_SISTEMA'];
		
		if ($UsuarioPerfil == 0){
			//NÃO TEM PERFIL DO SISTEMA		
			$and = "";
			$strSql = "SELECT LEVEL - 1 AS COD_NIVEL,
                  LPAD (' ', (LEVEL - 1) * 5, '-') || TMP.DSC_TIPO_PROCESSO
                     AS DSC_TIPO_PROCESSO_NIVEL,
                  TMP.COD_TIPO_PROCESSO,
                  TMP.DSC_TIPO_PROCESSO,
                  TMP.COD_TIPO_PROCESSO_PAI,
                  TMP.NUM_SEQUENCIA,
                  TMP.IND_FOLHA,
                  TMP.QTD_TP
             FROM (SELECT TPG.COD_GRUPO AS COD_TIPO_PROCESSO,
                          TPG.DSC_GRUPO AS DSC_TIPO_PROCESSO,
                          TO_NUMBER (
                             DECODE (TPG.COD_GRUPO_PAI, NULL, NULL, TPG.COD_GRUPO_PAI))
                             COD_TIPO_PROCESSO_PAI,
                          'N' AS IND_FOLHA,
                          TPG.NUM_SEQUENCIA,
                          (SELECT COUNT (*)
                             FROM TIPO_PROCESSO TPX
                            WHERE TPX.COD_GRUPO = TPG.COD_GRUPO)
                             AS QTD_TP
                     FROM TIPO_PROCESSO_GRUPO TPG
                    WHERE TPG.COD_EMPRESA = :COD_EMPRESA
                   UNION
                   SELECT TP.COD_TIPO_PROCESSO,
                          TP.DSC_TIPO_PROCESSO,
                          TP.COD_GRUPO AS COD_TIPO_PROCESSO_PAI,
                          'S' AS IND_FOLHA,
                          NULL AS NUM_SEQUENCIA,
                          NULL AS QTD_TP
                     FROM TIPO_PROCESSO TP
                    WHERE COD_TIPO_PROCESSO IN
                             (SELECT COD_TIPO_PROCESSO
                                FROM PERFIL_USUARIO_TIPO_PROCESSO
                               WHERE COD_PERFIL_USUARIO IN
                                        (SELECT LISTAGG (
                                                   COD_PERFIL_USUARIO,
                                                   ',')
                                                WITHIN GROUP (ORDER BY
                                                                 COD_PERFIL_USUARIO)
                                                   AS COD_PERFIL_USUARIO
                                           FROM USUARIO_PERFIL_USUARIO PUP
                                          WHERE COD_USUARIO = :COD_USUARIO))) TMP
								
       START WITH TMP.COD_TIPO_PROCESSO_PAI IS NULL
       CONNECT BY PRIOR TMP.COD_TIPO_PROCESSO = TMP.COD_TIPO_PROCESSO_PAI
		ORDER SIBLINGS BY TMP.NUM_SEQUENCIA, TMP.DSC_TIPO_PROCESSO";
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ":COD_EMPRESA";
			$arrayParametros[0][1] = $this->codEmpresa;
			$arrayParametros[1][0] = ":COD_USUARIO";
			$arrayParametros[1][1] = $this->codUsuario;
				
			$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
			
			return $retorno;
			
		}else{
		
			$strSql = "SELECT LEVEL - 1 AS COD_NIVEL, LPAD(' ', (LEVEL - 1) * 5, '-') || TMP.DSC_TIPO_PROCESSO AS DSC_TIPO_PROCESSO_NIVEL, TMP.COD_TIPO_PROCESSO, TMP.DSC_TIPO_PROCESSO, TMP.COD_TIPO_PROCESSO_PAI, TMP.NUM_SEQUENCIA, TMP.IND_FOLHA, TMP.QTD_TP
			    		       FROM (SELECT TPG.COD_GRUPO AS COD_TIPO_PROCESSO, TPG.DSC_GRUPO AS DSC_TIPO_PROCESSO,
			    		                    TO_NUMBER(DECODE(TPG.COD_GRUPO_PAI, NULL, NULL, TPG.COD_GRUPO_PAI)) COD_TIPO_PROCESSO_PAI,
			    		                    'N' AS IND_FOLHA, TPG.NUM_SEQUENCIA, (SELECT COUNT(*) FROM TIPO_PROCESSO TPX WHERE TPX.COD_GRUPO = TPG.COD_GRUPO) AS QTD_TP
			    		               FROM TIPO_PROCESSO_GRUPO TPG
									   JOIN TIPO_PROCESSO TP1 ON (TP1.COD_GRUPO = TPG.COD_GRUPO)
			    		              WHERE TPG.COD_EMPRESA = :COD_EMPRESA
									  
			    		UNION
			    		SELECT TP.COD_TIPO_PROCESSO, TP.DSC_TIPO_PROCESSO, TP.COD_GRUPO AS COD_TIPO_PROCESSO_PAI, 'S' AS IND_FOLHA, NULL AS NUM_SEQUENCIA, NULL AS QTD_TP
			    		  FROM TIPO_PROCESSO TP
						  
			    		 WHERE TP.COD_EMPRESA = :COD_EMPRESA
			    		ORDER BY COD_TIPO_PROCESSO_PAI, DSC_TIPO_PROCESSO
			    		                               ) TMP
			    		START WITH TMP.COD_TIPO_PROCESSO_PAI IS NULL
			    		CONNECT BY PRIOR TMP.COD_TIPO_PROCESSO = TMP.COD_TIPO_PROCESSO_PAI
			    		ORDER SIBLINGS BY TMP.NUM_SEQUENCIA, TMP.DSC_TIPO_PROCESSO";
					
				$arrayParametros = array();
				$arrayParametros[0][0] = ":COD_EMPRESA";
				$arrayParametros[0][1] = $this->codEmpresa;
					
			$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
		
			return $retorno;
		}
	}
	
	/**
	 * Retorna as listas disponiveis para a empresa
	 * @param integer codEmpresa
	 * @return array (COD_TIPO_LISTA, DSC_TIPO_LISTA, IND_TIPO_LISTA_SISTEMA, COD_EMPRESA)
	 */
	public function buscaTipoListaEmpresa(){
		$objConexao = $this->database;
	
		$strSql = " SELECT TL.COD_TIPO_LISTA, TL.DSC_TIPO_LISTA, TL.IND_TIPO_LISTA_SISTEMA, TL.COD_EMPRESA
                      FROM TIPO_LISTA TL
                     WHERE TL.COD_EMPRESA = :COD_EMPRESA 
				        OR TL.IND_TIPO_LISTA_SISTEMA = 'S' ";
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ":COD_EMPRESA";
		$arrayParametros[0][1] = $this->codEmpresa;
	
		$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
	
		return $retorno;
	}
	
	/**
	 * @param integer codTipoLista 
	 * @param integer codEmpresa
	 * @return array array (COD_LISTA, COD_LISTA_CLIENTE, COD_TIPO_LISTA, DSC_LISTA)
	 */
	public function buscaValoresCampoTipoListaLista(){
		$objConexao = $this->database;
	
		$strSql = " SELECT L.COD_LISTA, L.COD_LISTA_CLIENTE, L.COD_TIPO_LISTA, L.DSC_LISTA, L.COD_PESO
                      FROM LISTA L
                      JOIN TIPO_LISTA TL ON (TL.COD_TIPO_LISTA = L.COD_TIPO_LISTA)
                     WHERE L.COD_TIPO_LISTA = :COD_TIPO_LISTA
                       AND ( TL.COD_EMPRESA = :COD_EMPRESA OR TL.IND_TIPO_LISTA_SISTEMA = 'S') 
				ORDER BY L.COD_PESO, L.DSC_LISTA";
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ":COD_EMPRESA";
		$arrayParametros[0][1] = $this->codEmpresa;
		$arrayParametros[1][0] = ":COD_TIPO_LISTA";
		$arrayParametros[1][1] = $this->codTipoLista;
	
		$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
	
		return $retorno;
	}
	
/**
 * @param integer codTipoProcesso
 * @return array (retorno os dados dos campos do tipo de processo
 */
	public function buscaCamposTipoProcesso(){
		$objConexao = $this->database;
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codTipoProcesso";
		$arrayParametros[0][1] =  $this->codTipoProcesso;
		 
		$strSql = "SELECT TPC.NOM_CAMPO, TPC.DSC_LABEL_CAMPO, TPC.COD_TIPO_CAMPO, TPC.QTD_CARACTERES, TPC.NUM_ORDEM, TPC.IND_OBRIGATORIO,
        			      TI.COD_TIPO_IDENTIFICADOR, TI.DSC_TIPO_IDENTIFICADOR, TI.DSC_CLASSE_VALIDACAO, TPC.DSC_MASCARA, TPC.COD_TIPO_LISTA,
						  TPC.IND_UTILIZA_NUMERADOR, TPC.COD_NUMERADOR, NP.NUMERADOR_ATUAL
                     FROM TIPO_PROCESSO_CAMPO TPC
                LEFT JOIN TIPO_IDENTIFICADOR TI ON (TI.COD_TIPO_IDENTIFICADOR = TPC.COD_TIPO_IDENTIFICADOR)
				LEFT JOIN NUMERADOR_PARTICULAR NP ON (NP.COD_TIPO_PROCESSO = TPC.COD_TIPO_PROCESSO)
                    WHERE TPC.COD_TIPO_PROCESSO = :codTipoProcesso
                 ORDER BY TPC.NUM_ORDEM";
		 
		$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
	
		return $retorno;
	}
	
	/**
	* @param integer codTipoDocumento
	* @return array (retorno os dados dos campos do tipo de documento
	*/
	public function buscaCamposDocumentoTipoProcesso(){
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoProcesso";
			$arrayParametros[0][1] =  $this->codTipoProcesso;
			$arrayParametros[1][0] = ":codTipoDocumento";				$arrayParametros[1][1] =  $this->codTipoDocumento;
					
			$strSql = "SELECT TPCD.COD_TIPO_CAMPO, TPCD.COD_TIPO_DOCUMENTO, 
                              TPCD.COD_TIPO_IDENTIFICADOR, TPCD.COD_TIPO_LISTA,
                              TPCD.COD_TIPO_PROCESSO, TPCD.DSC_LABEL_CAMPO,
                              TPCD.DSC_MASCARA, TPCD.IND_OBRIGATORIO, TPCD.NOM_CAMPO,
                              TPCD.NUM_ORDEM, TPCD.QTD_CARACTERES, TI.DSC_CLASSE_VALIDACAO
                         FROM TIPO_PROCESSO_TPDOC_CAMPO TPCD
                    LEFT JOIN TIPO_IDENTIFICADOR TI ON (TI.COD_TIPO_IDENTIFICADOR = TPCD.COD_TIPO_IDENTIFICADOR)
                    WHERE TPCD.COD_TIPO_PROCESSO = :codTipoProcesso
                      AND TPCD.COD_TIPO_DOCUMENTO = :codTipoDocumento
                 ORDER BY TPCD.NUM_ORDEM";
					
			$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
				return $retorno;
	}
	
	/**
	 * @param integer codTipoProcesso
	 * @param integer codTipoDocumento
	 * @return array (retorno os dados dos campos do tipo de documento
	 */
	public function buscaCamposDocumentoTipoProcessoTipoDocumento(){
		$objConexao = $this->database;
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codTipoProcesso";
		$arrayParametros[0][1] =  $this->codTipoProcesso;
		$arrayParametros[1][0] = ":codTipoDocumento";				
		$arrayParametros[1][1] =  $this->codTipoDocumento;
			
		$strSql = " SELECT TPC.NOM_CAMPO, TPC.DSC_LABEL_CAMPO, 
                           TPC.COD_TIPO_CAMPO, TPC.QTD_CARACTERES,
                           TPC.NUM_ORDEM, TPC.IND_OBRIGATORIO,
                           TI.COD_TIPO_IDENTIFICADOR, TI.DSC_TIPO_IDENTIFICADOR,
                           TI.DSC_CLASSE_VALIDACAO, TPC.DSC_MASCARA,
                           TPC.COD_TIPO_LISTA
				      FROM TIPO_PROCESSO_CAMPO TPC
                      LEFT JOIN TIPO_IDENTIFICADOR TI ON (TI.COD_TIPO_IDENTIFICADOR = TPC.COD_TIPO_IDENTIFICADOR)
                     WHERE TPC.COD_TIPO_PROCESSO = :codTipoProcesso
                     UNION
                    SELECT TPCD.NOM_CAMPO, TPCD.DSC_LABEL_CAMPO,
                           TPCD.COD_TIPO_CAMPO, TPCD.QTD_CARACTERES,
                           TPCD.NUM_ORDEM, TPCD.IND_OBRIGATORIO,
                           TI.COD_TIPO_IDENTIFICADOR, TI.DSC_TIPO_IDENTIFICADOR,
                           TI.DSC_CLASSE_VALIDACAO, TPCD.DSC_MASCARA,
                           TPCD.COD_TIPO_LISTA 
				      FROM TIPO_PROCESSO_TPDOC_CAMPO TPCD
                 LEFT JOIN TIPO_IDENTIFICADOR TI ON (TI.COD_TIPO_IDENTIFICADOR = TPCD.COD_TIPO_IDENTIFICADOR)
                     WHERE TPCD.COD_TIPO_PROCESSO = :codTipoProcesso
                       AND TPCD.COD_TIPO_DOCUMENTO = :codTipoDocumento
                     ORDER BY NUM_ORDEM, NOM_CAMPO";
			
		$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
		return $retorno;
	}
	
	public function buscaCamposDocumentoTipoProcessoTipoDocumentoMobile(){
		$objConexao = $this->database;
		
		//Verificar se o perfil do usuário é perfil do sistema
		$strSql = "SELECT COUNT(PU.IND_PERFIL_SISTEMA) AS IND_PERFIL_SISTEMA
                     FROM USUARIO_PERFIL_USUARIO UPU
                     JOIN PERFIL_USUARIO PU ON (PU.COD_PERFIL_USUARIO = UPU.COD_PERFIL_USUARIO)
                    WHERE UPU.COD_USUARIO = :COD_USUARIO
                      AND PU.IND_PERFIL_SISTEMA = 'S' ";
		
		// parametros
		$arrayParametros = array();
		$arrayParametros[0][0]	= ":COD_USUARIO";
		$arrayParametros[0][1]	= $this->codUsuario;
		
		$arrayPerfilSistema	= $objConexao->executeFetch($strSql, $arrayParametros);
		$indPerfilSistema_ = $arrayPerfilSistema[0]['IND_PERFIL_SISTEMA'];
		if($indPerfilSistema_ > 0){
			$indPerfilSistema = 'S';
		}else{
			$indPerfilSistema = 'N';
		}
		
		if($indPerfilSistema == 'S'){
			/*$strSql = " SELECT TPC.COD_TIPO_PROCESSO, TPC.NOM_CAMPO, TPC.DSC_LABEL_CAMPO, 
	                           TPC.COD_TIPO_CAMPO, TPC.QTD_CARACTERES,
	                           TPC.NUM_ORDEM, TPC.IND_OBRIGATORIO,
	                           TI.COD_TIPO_IDENTIFICADOR, TI.DSC_TIPO_IDENTIFICADOR,
	                           TI.DSC_CLASSE_VALIDACAO, TPC.DSC_MASCARA,
	                           TPC.COD_TIPO_LISTA
					      FROM TIPO_PROCESSO_CAMPO TPC
	                      LEFT JOIN TIPO_IDENTIFICADOR TI ON (TI.COD_TIPO_IDENTIFICADOR = TPC.COD_TIPO_IDENTIFICADOR)
	                     WHERE TPC.COD_TIPO_PROCESSO IN (SELECT DISTINCT TPTD.COD_TIPO_PROCESSO
	                     FROM TIPO_DOCUMENTO TD
	                     JOIN TIPO_PROCESSO_TPDOC TPTD ON (TPTD.COD_TIPO_DOCUMENTO = TD.COD_TIPO_DOCUMENTO)
	                    WHERE TPTD.COD_TIPO_PROCESSO IN (SELECT COD_TIPO_PROCESSO 
						FROM TIPO_PROCESSO 
						WHERE COD_EMPRESA = :COD_EMPRESA
						)
	                      AND TD.COD_EMPRESA = :COD_EMPRESA
						  AND TPTD.DSC_LISTA_DISPOSITIVOS LIKE '%M%')
	                     UNION
	                    SELECT TPCD.COD_TIPO_PROCESSO, TPCD.NOM_CAMPO, TPCD.DSC_LABEL_CAMPO,
	                           TPCD.COD_TIPO_CAMPO, TPCD.QTD_CARACTERES,
	                           TPCD.NUM_ORDEM, TPCD.IND_OBRIGATORIO,
	                           TI.COD_TIPO_IDENTIFICADOR, TI.DSC_TIPO_IDENTIFICADOR,
	                           TI.DSC_CLASSE_VALIDACAO, TPCD.DSC_MASCARA,
	                           TPCD.COD_TIPO_LISTA 
					      FROM TIPO_PROCESSO_TPDOC_CAMPO TPCD
	                 LEFT JOIN TIPO_IDENTIFICADOR TI ON (TI.COD_TIPO_IDENTIFICADOR = TPCD.COD_TIPO_IDENTIFICADOR)
	                     WHERE TPCD.COD_TIPO_PROCESSO IN (SELECT DISTINCT TPTD.COD_TIPO_PROCESSO
	                     FROM TIPO_DOCUMENTO TD
	                     JOIN TIPO_PROCESSO_TPDOC TPTD ON (TPTD.COD_TIPO_DOCUMENTO = TD.COD_TIPO_DOCUMENTO)
	                    WHERE TPTD.COD_TIPO_PROCESSO IN (SELECT COD_TIPO_PROCESSO 
											FROM TIPO_PROCESSO 
											WHERE COD_EMPRESA = :COD_EMPRESA
											AND COD_TIPO_PROCESSO IN (SELECT TP.COD_TIPO_PROCESSO
						FROM TIPO_PROCESSO TP
						WHERE COD_TIPO_PROCESSO IN (SELECT COD_TIPO_PROCESSO FROM PERFIL_USUARIO_TIPO_PROCESSO WHERE COD_PERFIL_USUARIO IN (SELECT LISTAGG(COD_PERFIL_USUARIO, ',') WITHIN GROUP (ORDER BY COD_PERFIL_USUARIO) AS COD_PERFIL_USUARIO
											 FROM USUARIO_PERFIL_USUARIO PUP
											WHERE COD_USUARIO = :COD_USUARIO)))
											)
	                      AND TD.COD_EMPRESA = :COD_EMPRESA
						  AND TPTD.DSC_LISTA_DISPOSITIVOS LIKE '%M%')
	                       
	                     ORDER BY COD_TIPO_PROCESSO, NUM_ORDEM, NOM_CAMPO";*/
			$strSql = "SELECT TPC.COD_TIPO_PROCESSO, TPC.NOM_CAMPO, TPC.DSC_LABEL_CAMPO,
	                           TPC.COD_TIPO_CAMPO, TPC.QTD_CARACTERES,
	                           TPC.NUM_ORDEM, TPC.IND_OBRIGATORIO,
	                           TI.COD_TIPO_IDENTIFICADOR, TI.DSC_TIPO_IDENTIFICADOR,
	                           TI.DSC_CLASSE_VALIDACAO, TPC.DSC_MASCARA,
	                           TPC.COD_TIPO_LISTA
					      FROM TIPO_PROCESSO_CAMPO TPC
	                      LEFT JOIN TIPO_IDENTIFICADOR TI ON (TI.COD_TIPO_IDENTIFICADOR = TPC.COD_TIPO_IDENTIFICADOR)
	                     WHERE TPC.COD_TIPO_PROCESSO IN (SELECT DISTINCT TPTD.COD_TIPO_PROCESSO
	                     FROM TIPO_DOCUMENTO TD
	                     JOIN TIPO_PROCESSO_TPDOC TPTD ON (TPTD.COD_TIPO_DOCUMENTO = TD.COD_TIPO_DOCUMENTO)
	                    WHERE TPTD.COD_TIPO_PROCESSO IN (SELECT COD_TIPO_PROCESSO
						FROM TIPO_PROCESSO
						WHERE COD_EMPRESA = :COD_EMPRESA
						AND COD_TIPO_PROCESSO IN (SELECT TP.COD_TIPO_PROCESSO
						FROM TIPO_PROCESSO TP
						WHERE TD.COD_EMPRESA = :COD_EMPRESA
						  AND TPTD.DSC_LISTA_DISPOSITIVOS LIKE '%M%')))
	                     UNION
	                    SELECT TPCD.COD_TIPO_PROCESSO, TPCD.NOM_CAMPO, TPCD.DSC_LABEL_CAMPO,
	                           TPCD.COD_TIPO_CAMPO, TPCD.QTD_CARACTERES,
	                           TPCD.NUM_ORDEM, TPCD.IND_OBRIGATORIO,
	                           TI.COD_TIPO_IDENTIFICADOR, TI.DSC_TIPO_IDENTIFICADOR,
	                           TI.DSC_CLASSE_VALIDACAO, TPCD.DSC_MASCARA,
	                           TPCD.COD_TIPO_LISTA
					      FROM TIPO_PROCESSO_TPDOC_CAMPO TPCD
	                 LEFT JOIN TIPO_IDENTIFICADOR TI ON (TI.COD_TIPO_IDENTIFICADOR = TPCD.COD_TIPO_IDENTIFICADOR)
	                     WHERE TPCD.COD_TIPO_PROCESSO IN (SELECT DISTINCT TPTD.COD_TIPO_PROCESSO
	                     FROM TIPO_DOCUMENTO TD
	                     JOIN TIPO_PROCESSO_TPDOC TPTD ON (TPTD.COD_TIPO_DOCUMENTO = TD.COD_TIPO_DOCUMENTO)
	                    WHERE TPTD.COD_TIPO_PROCESSO IN (SELECT COD_TIPO_PROCESSO
											FROM TIPO_PROCESSO
											WHERE COD_EMPRESA = :COD_EMPRESA
											)
	                      AND TD.COD_EMPRESA = :COD_EMPRESA
						  AND TPTD.DSC_LISTA_DISPOSITIVOS LIKE '%M%')
	                     ORDER BY COD_TIPO_PROCESSO, NUM_ORDEM, NOM_CAMPO";
			$arrayParametros = array();
			$arrayParametros[0][0] = ":COD_EMPRESA";
			$arrayParametros[0][1] =  $this->codEmpresa;
			$arrayParametros[1][0] = "COD_USUARIO";
			$arrayParametros[1][1] = $this->codUsuario;
			
			$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
			return $retorno;
			
		}else{
			$strSql = " SELECT TPC.COD_TIPO_PROCESSO, TPC.NOM_CAMPO, TPC.DSC_LABEL_CAMPO,
	                           TPC.COD_TIPO_CAMPO, TPC.QTD_CARACTERES,
	                           TPC.NUM_ORDEM, TPC.IND_OBRIGATORIO,
	                           TI.COD_TIPO_IDENTIFICADOR, TI.DSC_TIPO_IDENTIFICADOR,
	                           TI.DSC_CLASSE_VALIDACAO, TPC.DSC_MASCARA,
	                           TPC.COD_TIPO_LISTA
					      FROM TIPO_PROCESSO_CAMPO TPC
	                      LEFT JOIN TIPO_IDENTIFICADOR TI ON (TI.COD_TIPO_IDENTIFICADOR = TPC.COD_TIPO_IDENTIFICADOR)
	                     WHERE TPC.COD_TIPO_PROCESSO IN (SELECT DISTINCT TPTD.COD_TIPO_PROCESSO
	                     FROM TIPO_DOCUMENTO TD
	                     JOIN TIPO_PROCESSO_TPDOC TPTD ON (TPTD.COD_TIPO_DOCUMENTO = TD.COD_TIPO_DOCUMENTO)
	                    WHERE TPTD.COD_TIPO_PROCESSO IN (SELECT COD_TIPO_PROCESSO
						FROM TIPO_PROCESSO
						WHERE COD_EMPRESA = :COD_EMPRESA
						AND COD_TIPO_PROCESSO IN (SELECT TP.COD_TIPO_PROCESSO
						FROM TIPO_PROCESSO TP
						WHERE COD_TIPO_PROCESSO IN (SELECT COD_TIPO_PROCESSO FROM PERFIL_USUARIO_TIPO_PROCESSO WHERE COD_PERFIL_USUARIO IN (SELECT LISTAGG(COD_PERFIL_USUARIO, ',') WITHIN GROUP (ORDER BY COD_PERFIL_USUARIO) AS COD_PERFIL_USUARIO
											 FROM USUARIO_PERFIL_USUARIO PUP
											WHERE COD_USUARIO = :COD_USUARIO)))
						)
	                      AND TD.COD_EMPRESA = :COD_EMPRESA
						  AND TPTD.DSC_LISTA_DISPOSITIVOS LIKE '%M%')
	                     UNION
	                    SELECT TPCD.COD_TIPO_PROCESSO, TPCD.NOM_CAMPO, TPCD.DSC_LABEL_CAMPO,
	                           TPCD.COD_TIPO_CAMPO, TPCD.QTD_CARACTERES,
	                           TPCD.NUM_ORDEM, TPCD.IND_OBRIGATORIO,
	                           TI.COD_TIPO_IDENTIFICADOR, TI.DSC_TIPO_IDENTIFICADOR,
	                           TI.DSC_CLASSE_VALIDACAO, TPCD.DSC_MASCARA,
	                           TPCD.COD_TIPO_LISTA
					      FROM TIPO_PROCESSO_TPDOC_CAMPO TPCD
	                 LEFT JOIN TIPO_IDENTIFICADOR TI ON (TI.COD_TIPO_IDENTIFICADOR = TPCD.COD_TIPO_IDENTIFICADOR)
	                     WHERE TPCD.COD_TIPO_PROCESSO IN (SELECT DISTINCT TPTD.COD_TIPO_PROCESSO
	                     FROM TIPO_DOCUMENTO TD
	                     JOIN TIPO_PROCESSO_TPDOC TPTD ON (TPTD.COD_TIPO_DOCUMENTO = TD.COD_TIPO_DOCUMENTO)
	                    WHERE TPTD.COD_TIPO_PROCESSO IN (SELECT COD_TIPO_PROCESSO
											FROM TIPO_PROCESSO
											WHERE COD_EMPRESA = :COD_EMPRESA
											)
	                      AND TD.COD_EMPRESA = :COD_EMPRESA
						  AND TPTD.DSC_LISTA_DISPOSITIVOS LIKE '%M%')
			
	                     ORDER BY COD_TIPO_PROCESSO, NUM_ORDEM, NOM_CAMPO";
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ":COD_EMPRESA";
			$arrayParametros[0][1] =  $this->codEmpresa;
			$arrayParametros[1][0] = "COD_USUARIO";
			$arrayParametros[1][1] = $this->codUsuario;
					
			$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
			return $retorno;
			
		}
	}
	
	
	
	
	/**
	 * @param string dscTipoProcesso
	 * @return integer codTipoProcesso
	 */
	public function insereTipoProcesso() {
	
		if (TRUE == $this->dscTipoProcesso) {
			$objConexao = $this->database;
							
			// pega novo id
			$strSql = "SELECT SQ_TPPRO_01.NEXTVAL AS COD_TIPO_PROCESSO FROM DUAL";
			$proximoProcesso = $objConexao->executeFetch($strSql);
				
			if (!$proximoProcesso){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao gerar ID";
				$objConexao->rollbackTrans();
				return false;
			}
	
			$codTipoProcesso = $proximoProcesso[0]["COD_TIPO_PROCESSO"];
	
			if ($this->codGrupo == 0){
				$this->codGrupo = null;
			}
				
			// insere processo
			$strSql = "INSERT INTO TIPO_PROCESSO
					          (COD_TIPO_PROCESSO, DSC_TIPO_PROCESSO, COD_GRUPO, 
					           COD_EMPRESA, IND_PUBLICADO, IND_CONFERE_DOCUMENTO, 
					           IND_UTILIZA_PROCESSO, QTD_DIAS_FECHAMENTO_PROCESSO, 
					           QTD_DIAS_NOTIFICA_EXPIRACAO, IND_NOTIFICA_FECHAMENTO, 
					           IND_NOTIFICA_CANCELAMENTO, IND_NOTIFICA_ABERTURA, 
					           IND_NOTIFICA_PENDENCIA)
					   VALUES (:codTipoProcesso, :dscTipoProcesso, :codGrupo, 
					           :codEmpresa, 'N', :indConfereDocumento, 
					           :indUtilizaProcesso, :qtdDiasFechamentoProcesso,
					           :qtdDiasNotificacaoExpiracao, :indNotificaFechamento,
		                       :indNotificaCancelamento, :indNotificaAbertura,
					           :indNotificaPendencia)";
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoProcesso";
			$arrayParametros[0][1] = $codTipoProcesso;
			$arrayParametros[1][0] = ":dscTipoProcesso";
			$arrayParametros[1][1] = $this->dscTipoProcesso;
			$arrayParametros[2][0] = ":codGrupo";
			$arrayParametros[2][1] = $this->codGrupo;
			$arrayParametros[3][0] = ":codEmpresa";
			$arrayParametros[3][1] = $_SESSION['codEmpresa'];
			$arrayParametros[4][0] = ":indConfereDocumento";
			$arrayParametros[4][1] = $this->indConfereDocumento;		
			$arrayParametros[5][0] = ":indUtilizaProcesso";
			$arrayParametros[5][1] = $this->indUtilizaProcesso;
			$arrayParametros[6][0] = ":qtdDiasFechamentoProcesso";
			$arrayParametros[6][1] = $this->qtdDiasFechamentoProcesso;
			$arrayParametros[7][0] = ":qtdDiasNotificacaoExpiracao";
			$arrayParametros[7][1] = $this->qtdDiasNotificaExpiracaoProcesso;
			$arrayParametros[8][0] = ":indNotificaFechamento";
			$arrayParametros[8][1] = $this->indNotificaFechamentoProcesso;
			$arrayParametros[9][0] = ":indNotificaCancelamento";
			$arrayParametros[9][1] = $this->indNotificaCancelamentoProcesso;
			$arrayParametros[10][0] = ":indNotificaAbertura";
			$arrayParametros[10][1] = $this->indNotificaAberturaProcesso;
			$arrayParametros[11][0] = ":indNotificaPendencia";
			$arrayParametros[11][1] = $this->indNotificaAberturaProcesso;
			
			
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao inserir processo";
				return false;
			}
		
			return $codTipoProcesso;
			
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "variável não setada";
			return false;
		}
	}
	
	/**DELETA O TIPO DE PROCESSO
	 * @return integer codTipoProcesso
	 */
	public function delTipoProcesso(){
		if(TRUE == $this->codTipoProcesso) {
		
			$objConexao = $this->database;
		
			$strSql = "BEGIN PRC_EXCLUIR_TIPO_PROCESSO(:COD_TIPO_PROCESSO); END;";
	        
	        $arrayParametros = array();
	       	$arrayParametros[0][0] = ":COD_TIPO_PROCESSO";
	       	$arrayParametros[0][1] = $this->codTipoProcesso;
	       	
	        $retorno = $objConexao->executeProcedure($strSql, $arrayParametros);
	        	
	        if($retorno){
	       		return true;
	       	}else{
	       		$returnedError = $objConexao->__get('error');
				$this->error['code'] 	= $returnedError['code'];
				$this->error['message'] = "Erro ao excluir tipo de processo \n".$returnedError['message'];
				return false;
	       	}
		
		}else{
			$this->error['code'] 	= "";
			$this->error['message'] = "variável não setada";
			return false;
			
		}
	
	}
	
	/**
	 * @param string dscTipoProcesso
	 * @param string dscTipoProcesso
	 * @return integer codTipoProcesso
	 */
	public function alterarTipoProcesso() {
	
		if (TRUE == $this->dscTipoProcesso) {
			$objConexao = $this->database;
				
			if ($this->codGrupo == 0){
				$this->codGrupo = null;
			}
			
			// UPDATE NO PROCESSO
			$strSql = "UPDATE TIPO_PROCESSO 
					      SET DSC_TIPO_PROCESSO            = :dscTipoProcesso, 
					          COD_GRUPO                    = :codGrupo,
					          IND_PUBLICADO                = 'N',
							  IND_CONFERE_DOCUMENTO        = :indConfereDocumento,
					          IND_UTILIZA_PROCESSO         = :indUtilizaProcesso, 
				              QTD_DIAS_FECHAMENTO_PROCESSO = :qtdDiasFechamentoProcesso, 
					          QTD_DIAS_NOTIFICA_EXPIRACAO  = :qtdDiasNotificacaoExpiracao,
				              IND_NOTIFICA_FECHAMENTO      = :indNotificaFechamento, 
					          IND_NOTIFICA_CANCELAMENTO    = :indNotificaCancelamento, 
				              IND_NOTIFICA_ABERTURA        = :indNotificaAbertura, 
					          IND_NOTIFICA_PENDENCIA       = :indNotificaPendencia					
					    WHERE COD_TIPO_PROCESSO = :codTipoProcesso";
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoProcesso";
			$arrayParametros[0][1] = $this->codTipoProcesso;
			$arrayParametros[1][0] = ":dscTipoProcesso";
			$arrayParametros[1][1] = $this->dscTipoProcesso;
			$arrayParametros[2][0] = ":codGrupo";
			$arrayParametros[2][1] = $this->codGrupo;
			$arrayParametros[3][0] = ":indConfereDocumento";
			$arrayParametros[3][1] = $this->indConfereDocumento;
			$arrayParametros[4][0] = ":indUtilizaProcesso";
			$arrayParametros[4][1] = $this->indUtilizaProcesso;
			$arrayParametros[5][0] = ":qtdDiasFechamentoProcesso";
			$arrayParametros[5][1] = $this->qtdDiasFechamentoProcesso;
			$arrayParametros[6][0] = ":qtdDiasNotificacaoExpiracao";
			$arrayParametros[6][1] = $this->qtdDiasNotificaExpiracaoProcesso;
			$arrayParametros[7][0] = ":indNotificaFechamento";
			$arrayParametros[7][1] = $this->indNotificaFechamentoProcesso;
			$arrayParametros[8][0] = ":indNotificaCancelamento";
			$arrayParametros[8][1] = $this->indNotificaCancelamentoProcesso;
			$arrayParametros[9][0] = ":indNotificaAbertura";
			$arrayParametros[9][1] = $this->indNotificaAberturaProcesso;
			$arrayParametros[10][0] = ":indNotificaPendencia";
			$arrayParametros[10][1] = $this->indNotificaAberturaProcesso;
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar processo";
				return false;
			}
	
			return true;
				
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "variável não setada";
			return false;
		}
	}
	
	
	/** INSERE CAMPO AO TIPO DE PROCESSO
	 * @param integer codTipoProcesso
	 * @param string nomCampo
	 * @param string mascaraCampo  
	 * @param string tipoCampo
	 * @param string codLista
	 * @param string qtdChar
	 * @param integer numOrdem 
	 * @param integer codTipoIdentificador
	 * @param string indObrigatorio
	 * @return boolean
	 */
	public function insereTipoProcessoCampo(){
		if (TRUE == $this->codTipoProcesso && TRUE == $this->nomCampo && TRUE == $this->labelCampo ) {
		
			$objConexao = $this->database;
			
			if ($this->codTipoIdentificador == "0"){
				$this->codTipoIdentificador = null;
			}
			
			// insere processo_campo		
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoProcesso";
			$arrayParametros[0][1] = $this->codTipoProcesso;
			$arrayParametros[1][0] = ":nomCampo";
			$arrayParametros[1][1] = $this->nomCampo;
			$arrayParametros[2][0] = ":labelCampo";
			$arrayParametros[2][1] = $this->labelCampo;
			$arrayParametros[3][0] = ":mascaraCampo";
			$arrayParametros[3][1] = $this->mascaraCampo;
			$arrayParametros[4][0] = ":tipoCampo";
			$arrayParametros[4][1] = $this->tipoCampo;
			$arrayParametros[5][0] = ":codLista";
			$arrayParametros[5][1] = $this->codLista;
			$arrayParametros[6][0] = ":qtdChar";
			$arrayParametros[6][1] = $this->qtdChar;
			$arrayParametros[7][0] = ":numOrdem";
			$arrayParametros[7][1] = $this->numOrdem;
			$arrayParametros[8][0] = ":codTipoIdentificador";
			$arrayParametros[8][1] = $this->codTipoIdentificador;
			$arrayParametros[9][0] = ":indObrigatorio";
			$arrayParametros[9][1] = $this->indObrigatorio;
			$arrayParametros[10][0] = ":indEnviaNotificacao";
			$arrayParametros[10][1] = $this->indEnviaNotificacao;
			$arrayParametros[11][0] = ":qtdDiasEnvio";
			$arrayParametros[11][1] = $this->qtdDiasEnvio;
			
			$strSql = "INSERT INTO TIPO_PROCESSO_CAMPO
							          (COD_TIPO_PROCESSO, NOM_CAMPO, DSC_LABEL_CAMPO, COD_TIPO_CAMPO, QTD_CARACTERES, NUM_ORDEM, IND_OBRIGATORIO, COD_TIPO_IDENTIFICADOR, DSC_MASCARA, COD_TIPO_LISTA, IND_ENVIA_NOTIFICACAO, QTD_DIAS_ENVIO)
							   VALUES (:codTipoProcesso, :nomCampo, :labelCampo, :tipoCampo, :qtdchar, :numOrdem, :indObrigatorio, :codTipoIdentificador, :mascaraCampo, :codLista, :indEnviaNotificacao, :qtdDiasEnvio)";
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$returnedError = $objConexao->__get('error');
				$this->error['code'] 	= $returnedError['code'];
				$this->error['message'] = "Erro ao inserir campos do processo\n".$returnedError['message'];
			
				return false;
			}
			
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados obrigatórios não foram informados";
			return false;
		}
	}
	
	/** DELETA O CAMPO DO TIPO DE PROCESSO
	 * @param integer codTipoProcesso
	 * @param string nomCampo
	 * @return boolean
	 */
	public function deletaTipoProcessoCampo(){
		if (TRUE == $this->codTipoProcesso && TRUE == $this->nomCampo) {
	
			$objConexao = $this->database;
				
			// insere processo_campo
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoProcesso";
			$arrayParametros[0][1] = $this->codTipoProcesso;
			$arrayParametros[1][0] = ":nomCampo";
			$arrayParametros[1][1] = $this->nomCampo;
				
			$strSql = "DELETE TIPO_PROCESSO_CAMPO
						WHERE COD_TIPO_PROCESSO = :codTipoProcesso
					     AND NOM_CAMPO = :nomCampo";
				
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Não foi possivel deletar o campo.";
				return false;
				break;
			}
				
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados obrigatórios não foram informados!";
			return false;
		}
	}
	
	/** ALTERA CAMPO AO TIPO DE PROCESSO
	 * @param integer codTipoProcesso
	 * @param string nomCampo
	 * @param string mascaraCampo
	 * @param string tipoCampo
	 * @param string codLista
	 * @param string qtdChar
	 * @param integer numOrdem
	 * @param integer codTipoIdentificador
	 * @param string indObrigatorio
	 * @return boolean
	 */
	public function alteraTipoProcessoCampo(){
		if (TRUE == $this->codTipoProcesso && TRUE == $this->nomCampo && TRUE == $this->labelCampo ) {
	
			$objConexao = $this->database;
				
			if ($this->codTipoIdentificador == "0"){
				$this->codTipoIdentificador = null;
			}
				
			// insere processo_campo
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoProcesso";
			$arrayParametros[0][1] = $this->codTipoProcesso;
			$arrayParametros[1][0] = ":nomCampo";
			$arrayParametros[1][1] = $this->nomCampo;
			$arrayParametros[2][0] = ":labelCampo";
			$arrayParametros[2][1] = $this->labelCampo;
			$arrayParametros[3][0] = ":mascaraCampo";
			$arrayParametros[3][1] = $this->mascaraCampo;
			$arrayParametros[4][0] = ":tipoCampo";
			$arrayParametros[4][1] = $this->tipoCampo;
			$arrayParametros[5][0] = ":codLista";
			$arrayParametros[5][1] = $this->codLista;
			$arrayParametros[6][0] = ":qtdChar";
			$arrayParametros[6][1] = $this->qtdChar;
			$arrayParametros[7][0] = ":numOrdem";
			$arrayParametros[7][1] = $this->numOrdem;
			$arrayParametros[8][0] = ":codTipoIdentificador";
			$arrayParametros[8][1] = $this->codTipoIdentificador;
			$arrayParametros[9][0] = ":indObrigatorio";
			$arrayParametros[9][1] = $this->indObrigatorio;
			$arrayParametros[10][0] = ":indEnviaNotificacao";
			$arrayParametros[10][1] = $this->indEnviaNotificacao;
			$arrayParametros[11][0] = ":qtdDiasEnvio";
			$arrayParametros[11][1] = $this->qtdDiasEnvio;
				
			$strSql = "UPDATE TIPO_PROCESSO_CAMPO
						  SET DSC_LABEL_CAMPO         = :labelCampo,
					          COD_TIPO_CAMPO          = :tipoCampo,
					          QTD_CARACTERES          = :qtdchar,
					          NUM_ORDEM               = :numOrdem,
					          IND_OBRIGATORIO         = :indObrigatorio,
					          COD_TIPO_IDENTIFICADOR  = :codTipoIdentificador,
					          DSC_MASCARA             = :mascaraCampo,
					          COD_TIPO_LISTA          = :codLista,
					          IND_ENVIA_NOTIFICACAO   = :indEnviaNotificacao,
					          QTD_DIAS_ENVIO          = :qtdDiasEnvio
					   WHERE COD_TIPO_PROCESSO = :codTipoProcesso
					     AND NOM_CAMPO = :nomCampo";
				
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$returnedError = $objConexao->__get('error');
				$this->error['code'] 	= $returnedError['code'];
				$this->error['message'] = "Erro ao editar campos do processo\n".$returnedError['message'];
					
				return false;
			}
				
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados obrigatórios não foram informados";
			return false;
		}
	}
	
	/** INSERE DOCUMENTO AO TIPO DE PROCESSO
	 * @param integer codTipoProcesso
	 * @param integer codTipoDocumento
     * @param string dscListaDispositivo
     * @param integer qtdMaximaPixelLargura
     * @param integer qtdMaximaPixelAltura
     * @param integer qtdTamanhoMaximoKb
     * @param integer pctQualidadePadrao
     * @param integer qtdDpi
     * @param string indNotificaUpload
	 * @return boolean
	 */
	public function insereTipoProcessoDocumento(){
		if (TRUE == $this->codTipoProcesso && TRUE == $this->codTipoDocumento ) {
	
			$objConexao = $this->database;
											
			// insere processo_campo
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoProcesso";
			$arrayParametros[0][1] = $this->codTipoProcesso;
			$arrayParametros[1][0] = ":codTipoDocumento";
			$arrayParametros[1][1] = $this->codTipoDocumento;
			$arrayParametros[2][0] = ":dscListaDispositivo";
			$arrayParametros[2][1] = $this->dscListaDispositivo;
			$arrayParametros[3][0] = ":qtdMaximaPixelLargura";
			$arrayParametros[3][1] = $this->qtdMaximaPixelLargura;
			$arrayParametros[4][0] = ":qtdMaximaPixelAltura";
			$arrayParametros[4][1] = $this->qtdMaximaPixelAltura;
			$arrayParametros[5][0] = ":qtdTamanhoMaximoKb";
			$arrayParametros[5][1] = $this->qtdTamanhoMaximoKb;
			$arrayParametros[6][0] = ":pctQualidadePadrao";
			$arrayParametros[6][1] = $this->pctQualidadePadrao;
			$arrayParametros[7][0] = ":qtdDpi";
			$arrayParametros[7][1] = $this->qtdDpi;
			$arrayParametros[8][0] = ":indNotificaUpload";
			$arrayParametros[8][1] = $this->indNotificaUpload;
			$arrayParametros[9][0] = ":indDocObrigatorio";
			$arrayParametros[9][1] = $this->indDocObrigatorio;
			$arrayParametros[10][0] = ":numSequencia";
			$arrayParametros[10][1] = $this->numSequencia;
		
			$strSql = "INSERT INTO TIPO_PROCESSO_TPDOC
						          (COD_TIPO_PROCESSO, COD_TIPO_DOCUMENTO, DSC_LISTA_DISPOSITIVOS, QTD_MAXIMA_PIXEL_LARGURA, 
					               QTD_MAXIMA_PIXEL_ALTURA, QTD_TAMANHO_MAXIMO_KB, PCT_QUALIDADE_PADRAO, QTD_DPI, 
					               IND_NOTIFICA_UPLOAD, IND_OBRIGATORIO, NUM_SEQUENCIA)
						   VALUES (:codTipoProcesso, :codTipoDocumento, :dscListaDispositivo, :qtdMaximaPixelLargura,
					               :qtdMaximaPixelAltura, :qtdTamanhoMaximoKb , :pctQualidadePadrao, :qtdDpi, 
					               :indNotificaUpload, :indDocObrigatorio, :numSequencia)";
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$returnedError = $objConexao->__get('error');
				$this->error['code'] 	= $returnedError['code'];
				$this->error['message'] = "Erro ao inserir documentos do processo\n".$returnedError['message'];
				return false;
			}
				
			return true;
		} else {
			
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados obrigatórios não foram informados";
			return false;
		}
	}
	
	/** ALTERA DOCUMENTO DO TIPO DE PROCESSO
	 * @param integer codTipoProcesso
	 * @param integer codTipoDocumento
	 * @param string dscListaDispositivo
	 * @param integer qtdMaximaPixelLargura
	 * @param integer qtdMaximaPixelAltura
	 * @param integer qtdTamanhoMaximoKb
	 * @param integer pctQualidadePadrao
	 * @param integer qtdDpi
	 * @param string indNotificaUpload
	 * @return boolean
	 */
	public function alteraTipoProcessoDocumento(){
		if (TRUE == $this->codTipoProcesso && TRUE == $this->codTipoDocumento ) {
	
			$objConexao = $this->database;
				
			// insere processo_campo
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoProcesso";
			$arrayParametros[0][1] = $this->codTipoProcesso;
			$arrayParametros[1][0] = ":codTipoDocumento";
			$arrayParametros[1][1] = $this->codTipoDocumento;
			$arrayParametros[2][0] = ":dscListaDispositivo";
			$arrayParametros[2][1] = $this->dscListaDispositivo;
			$arrayParametros[3][0] = ":qtdMaximaPixelLargura";
			$arrayParametros[3][1] = $this->qtdMaximaPixelLargura;
			$arrayParametros[4][0] = ":qtdMaximaPixelAltura";
			$arrayParametros[4][1] = $this->qtdMaximaPixelAltura;
			$arrayParametros[5][0] = ":qtdTamanhoMaximoKb";
			$arrayParametros[5][1] = $this->qtdTamanhoMaximoKb;
			$arrayParametros[6][0] = ":pctQualidadePadrao";
			$arrayParametros[6][1] = $this->pctQualidadePadrao;
			$arrayParametros[7][0] = ":qtdDpi";
			$arrayParametros[7][1] = $this->qtdDpi;
			$arrayParametros[8][0] = ":indNotificaUpload";
			$arrayParametros[8][1] = $this->indNotificaUpload;
			$arrayParametros[9][0] = ":indDocObrigatorio";
			$arrayParametros[9][1] = $this->indDocObrigatorio;
			$arrayParametros[10][0] = ":numSequencia";
			$arrayParametros[10][1] = $this->numSequencia;
			
			$strSql = "UPDATE TIPO_PROCESSO_TPDOC
						  SET DSC_LISTA_DISPOSITIVOS   = :dscListaDispositivo,
					          QTD_MAXIMA_PIXEL_LARGURA = :qtdMaximaPixelLargura,
					          QTD_MAXIMA_PIXEL_ALTURA  = :qtdMaximaPixelAltura,
					          QTD_TAMANHO_MAXIMO_KB    = :qtdTamanhoMaximoKb,
					          PCT_QUALIDADE_PADRAO     = :pctQualidadePadrao,
					          QTD_DPI                  = :qtdDpi,
					          IND_NOTIFICA_UPLOAD      = :indNotificaUpload,
							  IND_OBRIGATORIO          = :indDocObrigatorio,
							  NUM_SEQUENCIA            = :numSequencia
					    WHERE COD_TIPO_PROCESSO        = :codTipoProcesso
					      AND COD_TIPO_DOCUMENTO       = :codTipoDocumento   
					";
				
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$returnedError = $objConexao->__get('error');
				$this->error['code'] 	= $returnedError['code'];
				$this->error['message'] = "Erro ao alterar documentos do processo\n".$returnedError['message'];
				return false;
			}
	
			return true;
		} else {
				
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados obrigatórios não foram informados";
			return false;
		}
	}
	
	
	
	/** DELETA O DOCUMENTO DO TIPO DE PROCESSO
	 * @param integer codTipoProcesso
	 * @param integer codTipoDocumento
	 * @return boolean
	 */
	public function deletaTipoProcessoDocumento(){
	if(TRUE == $this->codTipoProcesso && TRUE == $this->codTipoDocumento ) {
		
			$objConexao = $this->database;
		
			$strSql = "BEGIN PRC_EXCLUIR_TIPO_PROC_TPDOC(:COD_TIPO_PROCESSO, :COD_TIPO_DOCUMENTO); END;";
	        
	        $arrayParametros = array();
	       	$arrayParametros[0][0] = ":COD_TIPO_PROCESSO";
	       	$arrayParametros[0][1] = $this->codTipoProcesso;
	       	$arrayParametros[1][0] = ":COD_TIPO_DOCUMENTO";
	       	$arrayParametros[1][1] = $this->codTipoDocumento;
	       	
	        $retorno = $objConexao->executeProcedure($strSql, $arrayParametros);
	        	
	        if($retorno){
	       		return true;
	       	}else{
	       		$returnedError = $objConexao->__get('error');
				$this->error['code'] 	= $returnedError['code'];
				$this->error['message'] = "Erro ao excluir tipo de documento \n".$returnedError['message'];
				return false;
	       	}
		
		}else{
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários para exclusão não encontrados";
			return false;
			
		}
	}
	
	
	/** INSERE CAMPO AO TIPO DE DOCUMENTO
	 * @param integer codTipoProcesso
	 * @param integer codTipoDocumento
	 * @param string nomCampo
	 * @param string mascaraCampo
	 * @param string tipoCampo
	 * @param string codLista
	 * @param string qtdChar
	 * @param integer numOrdem
	 * @param integer codTipoIdentificador
	 * @param string indObrigatorio
	 * @return boolean
	 */
	public function insereTipoProcessoCampoDocumento(){
		if (TRUE == $this->codTipoProcesso) {
	
			$objConexao = $this->database;
				
			if ($this->codTipoIdentificador == "0"){
				$this->codTipoIdentificador = null;
			}
				
			// insere processo_campo
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoProcesso";
			$arrayParametros[0][1] = $this->codTipoProcesso;
			$arrayParametros[1][0] = ":codTipoDocumento";
			$arrayParametros[1][1] = $this->codTipoDocumento;
			$arrayParametros[2][0] = ":nomCampo";
			$arrayParametros[2][1] = $this->nomCampo;
			$arrayParametros[3][0] = ":labelCampo";
			$arrayParametros[3][1] = $this->labelCampo;
			$arrayParametros[4][0] = ":mascaraCampo";
			$arrayParametros[4][1] = $this->mascaraCampo;
			$arrayParametros[5][0] = ":tipoCampo";
			$arrayParametros[5][1] = $this->tipoCampo;
			$arrayParametros[6][0] = ":codLista";
			$arrayParametros[6][1] = $this->codLista;
			$arrayParametros[7][0] = ":qtdChar";
			$arrayParametros[7][1] = $this->qtdChar;
			$arrayParametros[8][0] = ":numOrdem";
			$arrayParametros[8][1] = $this->numOrdem;
			$arrayParametros[9][0] = ":codTipoIdentificador";
			$arrayParametros[9][1] = $this->codTipoIdentificador;
			$arrayParametros[10][0] = ":indObrigatorio";
			$arrayParametros[10][1] = $this->indObrigatorio;
			$arrayParametros[11][0] = ":indEnviaNotificacao";
			$arrayParametros[11][1] = $this->indEnviaNotificacao;
			$arrayParametros[12][0] = ":qtdDiasEnvio";
			$arrayParametros[12][1] = $this->qtdDiasEnvio;
				
			$strSql = "INSERT INTO TIPO_PROCESSO_TPDOC_CAMPO
							          (COD_TIPO_PROCESSO, COD_TIPO_DOCUMENTO,NOM_CAMPO, DSC_LABEL_CAMPO, COD_TIPO_CAMPO, QTD_CARACTERES, NUM_ORDEM, IND_OBRIGATORIO, COD_TIPO_IDENTIFICADOR, DSC_MASCARA, COD_TIPO_LISTA, IND_ENVIA_NOTIFICACAO, QTD_DIAS_ENVIO)
							   VALUES (:codTipoProcesso, :codTipoDocumento,:nomCampo, :labelCampo, :tipoCampo, :qtdchar, :numOrdem, :indObrigatorio, :codTipoIdentificador, :mascaraCampo, :codLista, :indEnviaNotificacao, :qtdDiasEnvio)";
				
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$returnedError = $objConexao->__get('error');
				$this->error['code'] 	= $returnedError['code'];
				$this->error['message'] = "Erro ao inserir campos do tipo de documento\n".$returnedError['message'];
				return false;
			}
				
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "variável não setada";
			return false;
		}
	}
	
	/** ALTERA CAMPO DO TIPO DE DOCUMENTO
	 * @param integer codTipoProcesso
	 * @param integer codTipoDocumento
	 * @param string nomCampo
	 * @param string mascaraCampo
	 * @param string tipoCampo
	 * @param string codLista
	 * @param string qtdChar
	 * @param integer numOrdem
	 * @param integer codTipoIdentificador
	 * @param string indObrigatorio
	 * @return boolean
	 */
	public function alteraTipoProcessoCampoDocumento(){
		if (TRUE == $this->codTipoProcesso) {
	
			$objConexao = $this->database;
	
			if ($this->codTipoIdentificador == "0"){
				$this->codTipoIdentificador = null;
			}
	
			// insere processo_campo
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoProcesso";
			$arrayParametros[0][1] = $this->codTipoProcesso;
			$arrayParametros[1][0] = ":codTipoDocumento";
			$arrayParametros[1][1] = $this->codTipoDocumento;
			$arrayParametros[2][0] = ":nomCampo";
			$arrayParametros[2][1] = $this->nomCampo;
			$arrayParametros[3][0] = ":labelCampo";
			$arrayParametros[3][1] = $this->labelCampo;
			$arrayParametros[4][0] = ":mascaraCampo";
			$arrayParametros[4][1] = $this->mascaraCampo;
			$arrayParametros[5][0] = ":tipoCampo";
			$arrayParametros[5][1] = $this->tipoCampo;
			$arrayParametros[6][0] = ":codLista";
			$arrayParametros[6][1] = $this->codLista;
			$arrayParametros[7][0] = ":qtdChar";
			$arrayParametros[7][1] = $this->qtdChar;
			$arrayParametros[8][0] = ":numOrdem";
			$arrayParametros[8][1] = $this->numOrdem;
			$arrayParametros[9][0] = ":codTipoIdentificador";
			$arrayParametros[9][1] = $this->codTipoIdentificador;
			$arrayParametros[10][0] = ":indObrigatorio";
			$arrayParametros[10][1] = $this->indObrigatorio;
			$arrayParametros[11][0] = ":indEnviaNotificacao";
			$arrayParametros[11][1] = $this->indEnviaNotificacao;
			$arrayParametros[12][0] = ":qtdDiasEnvio";
			$arrayParametros[12][1] = $this->qtdDiasEnvio;
	
			$strSql = "UPDATE TIPO_PROCESSO_TPDOC_CAMPO
						  SET DSC_LABEL_CAMPO        = :labelCampo,
					          COD_TIPO_CAMPO         = :tipoCampo,
					          QTD_CARACTERES         = :qtdchar,
					          NUM_ORDEM              = :numOrdem,
					          IND_OBRIGATORIO        = :indObrigatorio,
					          COD_TIPO_IDENTIFICADOR = :codTipoIdentificador,
					          DSC_MASCARA            = :mascaraCampo, 
					          COD_TIPO_LISTA         = :codLista,
							  IND_ENVIA_NOTIFICACAO  = :indEnviaNotificacao,
					          QTD_DIAS_ENVIO         = :qtdDiasEnvio					
					  WHERE COD_TIPO_PROCESSO        = :codTipoProcesso
					        AND COD_TIPO_DOCUMENTO   = :codTipoDocumento
					        AND NOM_CAMPO            = :nomCampo";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$returnedError = $objConexao->__get('error');
				$this->error['code'] 	= $returnedError['code'];
				$this->error['message'] = "Erro ao alterar campos do tipo de documento\n".$returnedError['message'];
				return false;
			}
	
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "variável não setada";
			return false;
		}
	}
	
	
	/** DELETA CAMPO AO TIPO DE PROCESSO
	 * @param integer codTipoProcesso
	 * @param integer codTipoDocumento
	 * @param string nomCampo
	 * @param string mascaraCampo
	 * @param string tipoCampo
	 * @param string codLista
	 * @param string qtdChar
	 * @param integer numOrdem
	 * @param integer codTipoIdentificador
	 * @param string indObrigatorio
	 * @return boolean
	 */
	public function deletaTipoProcessoCampoDocumento(){
		if (TRUE == $this->codTipoProcesso && TRUE == $this->codTipoDocumento && TRUE == $this->nomCampo ) {
	
			$objConexao = $this->database;
		
			// insere processo_campo
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoProcesso";
			$arrayParametros[0][1] = $this->codTipoProcesso;
			$arrayParametros[1][0] = ":codTipoDocumento";
			$arrayParametros[1][1] = $this->codTipoDocumento;
			$arrayParametros[2][0] = ":nomCampo";
			$arrayParametros[2][1] = $this->nomCampo;
	
			$strSql = "DELETE TIPO_PROCESSO_TPDOC_CAMPO
						WHERE COD_TIPO_PROCESSO = :codTipoProcesso
					      AND COD_TIPO_DOCUMENTO = :codTipoDocumento
					      AND NOM_CAMPO = :nomCampo";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao excluir campos do tipo de documento";
				return false;
				break;
			}
	
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários para a exclusão não foram encontrados.";
			return false;
		}
	}
	
	/** BUSCA CAMPO DO TIPO DE DOCUMENTO
	 * @param integer codTipoProcesso
	 * @param integer codTipoDocumento
	 * @return boolean
	 */
	public function buscaTipoProcessoCampoDocumento(){
		$objConexao = $this->database;
	
	
		// insere processo_campo
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codTipoProcesso";
		$arrayParametros[0][1] = $this->codTipoProcesso;
		$arrayParametros[1][0] = ":codTipoDocumento";
		$arrayParametros[1][1] = $this->codTipoDocumento;
			
		$strSql = "SELECT TPC.COD_TIPO_PROCESSO, TPC.COD_TIPO_DOCUMENTO, TPC.NOM_CAMPO, 
				          TPC.DSC_LABEL_CAMPO, TPC.COD_TIPO_CAMPO, TPC.QTD_CARACTERES, 
				          TPC.NUM_ORDEM, IND_OBRIGATORIO, TPC.COD_TIPO_IDENTIFICADOR, 
				          TPC.DSC_MASCARA, TPC.COD_TIPO_LISTA, TI.DSC_TIPO_IDENTIFICADOR,
				          TPC.IND_ENVIA_NOTIFICACAO, TPC.QTD_DIAS_ENVIO
				     FROM TIPO_PROCESSO_TPDOC_CAMPO TPC
				     LEFT JOIN TIPO_IDENTIFICADOR TI ON (TI.COD_TIPO_IDENTIFICADOR = TPC.COD_TIPO_IDENTIFICADOR)
				    WHERE COD_TIPO_PROCESSO = :codTipoProcesso
				      AND COD_TIPO_DOCUMENTO =:codTipoDocumento";

		$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
		
		return $retorno;

	}
	
	/** LISTA A ARVORE DE PROCESSOS DA EMPRESA
	 * @param integer codEmpresa
	 * @return array com os dados da arvore de processos
	 */
	public function listaArvoreProcessosEmpresa(){
		$objConexao = $this->database;
	
		$strSql = " SELECT LPAD(' ', (LEVEL - 1) * 5, '-') || TMP.DSC_GRUPO AS DSC_TIPO_PROCESSO_NIVEL, TMP.DSC_GRUPO AS DSC_TIPO_PROCESSO, TMP.COD_GRUPO, TMP.COD_TIPO_PROCESSO, TMP.COD_GRUPO_PAI, TMP.IND_CONFERE_DOCUMENTO
                          FROM (SELECT TPG.COD_GRUPO, TPG.DSC_GRUPO, TPG.COD_GRUPO_PAI, TPG.NUM_SEQUENCIA, NULL AS COD_TIPO_PROCESSO, NULL AS IND_CONFERE_DOCUMENTO
                                  FROM TIPO_PROCESSO_GRUPO TPG
                                 WHERE TPG.COD_EMPRESA = :COD_EMPRESA
                                 UNION
                                SELECT NULL AS COD_GRUPO, TP.DSC_TIPO_PROCESSO AS DSC_GRUPO, TP.COD_GRUPO AS COD_GRUPO_PAI, 0 AS NUM_SEQUENCIA, TP.COD_TIPO_PROCESSO, TP.IND_CONFERE_DOCUMENTO
                                  FROM TIPO_PROCESSO TP
                                 WHERE TP.COD_EMPRESA = :COD_EMPRESA
                                ) TMP
                         START WITH TMP.COD_GRUPO_PAI IS NULL
                        CONNECT BY PRIOR TMP.COD_GRUPO = TMP.COD_GRUPO_PAI
                         ORDER SIBLINGS BY TMP.NUM_SEQUENCIA";
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ":COD_EMPRESA";
		$arrayParametros[0][1] = $this->codEmpresa;
			
		$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
	
		return $retorno;
	}
	
	/** Publica o tipo de processo na tabela AUX_TIPO_PROCESSO
	 * @param integer codTipoProcesso
	 */
	public function publicaTipoProcesso(){
		$objConexao = $this->database;
	
		$strSql = "BEGIN PRC_PUBLICA_TIPO_PROCESSO(:COD_TIPO_PROCESSO, :COD_USUARIO); END;";
        
        	$arrayParametros = array();
        	$arrayParametros[0][0] = ":COD_TIPO_PROCESSO";
        	$arrayParametros[0][1] = $this->codTipoProcesso;
        	$arrayParametros[1][0] = ":COD_USUARIO";
        	$arrayParametros[1][1] = $this->codUsuario;
        	
        	$retorno = $objConexao->executeProcedure($strSql, $arrayParametros);
        	
        	if($retorno){
        		return true;
        	}else{
        		$returnedError = $objConexao->__get('error');
				$this->error['code'] 	= $returnedError['code'];
				$this->error['message'] = "Erro ao publicar processo\n".$returnedError['message'];
				return false;
        	}
	}
	
	/**
	 * @param string dscTipoLista
	 * @return integer codTipoLista
	 */
	public function insereTipoLista() {
	
		if (TRUE == $this->dscTipoLista) {
			$objConexao = $this->database;
				
			// pega novo id
			$strSql = "SELECT MAX(COD_TIPO_LISTA)+1 AS COD_TIPO_LISTA FROM TIPO_LISTA";
			$proximoCodTipoLista = $objConexao->executeFetch($strSql);
	
			if (!$proximoCodTipoLista){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao gerar codigo para a lista";
				$objConexao->rollbackTrans();
				return false;
			}
	
			$codTipoLista = $proximoCodTipoLista[0]["COD_TIPO_LISTA"];
	
			// insere LISTA
			$strSql = "INSERT INTO TIPO_LISTA
					          (COD_TIPO_LISTA, DSC_TIPO_LISTA, COD_EMPRESA, IND_TIPO_LISTA_SISTEMA)
					   VALUES (:codTipoLista, :dscTipoLista, :codEmpresa, :indListaSistema)";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoLista";
			$arrayParametros[0][1] = $codTipoLista;
			$arrayParametros[1][0] = ":dscTipoLista";
			$arrayParametros[1][1] = $this->dscTipoLista;
			
			
			if($this->indListaSistema == "S"){
				$arrayParametros[2][0] = ":codEmpresa";
				$arrayParametros[2][1] = null;
				$arrayParametros[3][0] = ":indListaSistema";
				$arrayParametros[3][1] = $this->indListaSistema;
				
			}else{
				$arrayParametros[2][0] = ":codEmpresa";
				$arrayParametros[2][1] = $_SESSION['codEmpresa'];
				$arrayParametros[3][0] = ":indListaSistema";
				$arrayParametros[3][1] = $this->indListaSistema;
			}
			
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao inserir lista";
				return false;
			}
	
			return $codTipoLista;
				
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "variável não setada";
			return false;
		}
	}
	
	/**
	 * @param string dscTipoLista
	 * @return integer codTipoLista
	 */
	public function alterarTipoLista() {
	
		if (TRUE == $this->dscTipoLista && TRUE == $this->codTipoLista ) {
			$objConexao = $this->database;
	
		
	
			// insere LISTA
			$strSql = "UPDATE TIPO_LISTA 
					      SET DSC_TIPO_LISTA = :dscTipoLista,
					          IND_TIPO_LISTA_SISTEMA = :indListaSistema,
					          COD_EMPRESA = :codEmpresa
					    WHERE COD_TIPO_LISTA = :codTipoLista";
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoLista";
			$arrayParametros[0][1] = $this->codTipoLista;
			$arrayParametros[1][0] = ":dscTipoLista";
			$arrayParametros[1][1] = $this->dscTipoLista;
				
				
			if($this->indListaSistema == "S"){
				$arrayParametros[2][0] = ":codEmpresa";
				$arrayParametros[2][1] = null;
				$arrayParametros[3][0] = ":indListaSistema";
				$arrayParametros[3][1] = $this->indListaSistema;
	
			}else{
				$arrayParametros[2][0] = ":codEmpresa";
				$arrayParametros[2][1] = $_SESSION['codEmpresa'];
				$arrayParametros[3][0] = ":indListaSistema";
				$arrayParametros[3][1] = $this->indListaSistema;
			}
				
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar lista";
				return false;
			}
	
			return true;
	
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "variável não setada";
			return false;
		}
	}
	
	
	/**
	 * @param string dscLista
	 * @param integer codTipoLista
	 */
	public function insereItemLista() {
	
		if (TRUE == $this->dscLista && TRUE == $this->codTipoLista) {
			$objConexao = $this->database;
	
			// pega novo id
			$strSql = "SELECT MAX(COD_LISTA)+1 AS COD_LISTA FROM LISTA";
			$proximoCodLista = $objConexao->executeFetch($strSql);
	
			if (!$proximoCodLista){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao gerar codigo para a lista";
				$objConexao->rollbackTrans();
				return false;
			}
	
			$codLista = $proximoCodLista[0]["COD_LISTA"];
	
			// insere LISTA
			$strSql = "INSERT INTO LISTA
					          (COD_TIPO_LISTA, DSC_LISTA, COD_LISTA, COD_PESO)
					   VALUES (:codTipoLista, :dscLista, :codLista, :codPeso)";
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoLista";
			$arrayParametros[0][1] = $this->codTipoLista;
			$arrayParametros[1][0] = ":dscLista";
			$arrayParametros[1][1] = $this->dscLista;
			$arrayParametros[2][0] = ":codLista";
			$arrayParametros[2][1] = $codLista;
			$arrayParametros[3][0] = ":codPeso";
			$arrayParametros[3][1] = $this->codPeso;

			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao inserir lista";
				return false;
			}
	
			return $this->codTipoLista;
	
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "variável não setada";
			return false;
		}
	}
	
	/**
	 * @param string dscLista
	 * @param integer codTipoLista
	 */
	public function alteraItemLista() {
	
		if (TRUE == $this->dscLista && TRUE == $this->codLista) {
			$objConexao = $this->database;
		
			// insere LISTA
			$strSql = "UPDATE LISTA 
					     SET DSC_LISTA = :dscLista,
					         COD_PESO  = :codPeso
					   WHERE COD_LISTA = :codLista";
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codLista";
			$arrayParametros[0][1] = $this->codLista;
			$arrayParametros[1][0] = ":dscLista";
			$arrayParametros[1][1] = $this->dscLista;
			$arrayParametros[2][0] = ":codPeso";
			$arrayParametros[2][1] = $this->codPeso;
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar lista";
				return false;
			}
	
			return $this->codTipoLista;
	
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "variável não setada";
			return false;
		}
	}
	
	/** DELETA ITEM DA LISTA
	 * @param integer codLista
	 * @return boolean
	 */
	public function delItemLista(){
		if (TRUE == $this->codLista ) {
	
			$objConexao = $this->database;
	
			// insere processo_campo
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codLista";
			$arrayParametros[0][1] = $this->codLista;
			
	
			$strSql = "DELETE LISTA
						WHERE COD_LISTA = :codLista";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Falha ao excluir item. O item pode estar sendo usado em algum documento capturado";
				return false;
				break;
			}
	
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários para a exclusão não foram encontrados.";
			return false;
		}
	}
	
	/** DELETA LISTA
	 * @param integer codLista
	 * @return boolean
	 */
	public function delTipoLista(){
		if (TRUE == $this->codTipoLista ) {
	
			$objConexao = $this->database;
	
			// insere processo_campo
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoLista";
			$arrayParametros[0][1] = $this->codTipoLista;
				
	
			$strSql = "DELETE TIPO_LISTA
						WHERE COD_TIPO_LISTA = :codTipoLista";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Falha ao excluir item. Existem item associados a esta lista";
				return false;
				break;
			}
	
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários para a exclusão não foram encontrados.";
			return false;
		}
	}
	
	/** BUSCA OS DADOS DO CABEÇAÇHO DO MAPA
	 * @param integer codTipoProcesso
	 * @param integer codTipoDocumento
	 * @return boolean
	 */
	public function buscaCabecalhoMapa(){
		$objConexao = $this->database;
	
	
		// insere processo_campo
		$arrayParametros = array();
		$arrayParametros[0][0] = ":COD_TIPO_PROCESSO";
		$arrayParametros[0][1] = $this->codTipoProcesso;
		//arrayParametros[1][0] = ":NUM_ORDEM";
		//$arrayParametros[1][1] = $this->numOrdem;
			
		$strSql = "SELECT TD.DSC_TIPO_DOCUMENTO, TD.COD_TIPO_DOCUMENTO, TPT.IND_OBRIGATORIO, TPT.NUM_SEQUENCIA
                     FROM TIPO_PROCESSO_TPDOC TPT
                     JOIN TIPO_DOCUMENTO TD ON (TD.COD_TIPO_DOCUMENTO = TPT.COD_TIPO_DOCUMENTO)
                    WHERE COD_TIPO_PROCESSO = :COD_TIPO_PROCESSO
                    ORDER BY TPT.NUM_SEQUENCIA, TD.COD_TIPO_DOCUMENTO";
	
		$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
	
		return $retorno;
	
	}
	
	/** BUSCA OS INDEXADORES DO MAPA
	 * @param integer codTipoProcesso
   */
	public function buscaIndexadoresMapa(){
		$objConexao = $this->database;
	
	
		// insere processo_campo
		$arrayParametros = array();
		$arrayParametros[0][0] = ":COD_TIPO_PROCESSO";
		$arrayParametros[0][1] = $this->codTipoProcesso;
		
			
		$strSql = " SELECT TPC.NUM_ORDEM, TPC.DSC_LABEL_CAMPO, TPC.NOM_CAMPO
                      FROM TIPO_PROCESSO_CAMPO TPC
                     WHERE TPC.COD_TIPO_PROCESSO = :COD_TIPO_PROCESSO";
	
		$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
	
		return $retorno;
	
	}
	

	/** BUSCA OS DADOS DO DO MAPA
	 * @param integer codTipoProcesso
	 * @param integer codTipoDocumento
	 * @return boolean
	 */
	public function buscaDadosMapa(){
		$objConexao = $this->database;
		
		//VERIFICA SE O PROCESSO CONFERE DOCUMENTO
		$strSql = "SELECT IND_CONFERE_DOCUMENTO
				     FROM TIPO_PROCESSO
				    WHERE COD_TIPO_PROCESSO = :codTipoProcesso";
				     
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codTipoProcesso";
		$arrayParametros[0][1] = $this->codTipoProcesso;
		
		$arrayTipoProcesso = $objConexao->executeFetch($strSql, $arrayParametros);
		
		$this->indConfereDocumento = $arrayTipoProcesso[0]["IND_CONFERE_DOCUMENTO"];
	
		
		
		// BUSCA OS DADOS DO MAPA
		$arrayParametros = array();
		$arrayParametros[0][0] = ":COD_TIPO_PROCESSO";
		$arrayParametros[0][1] = $this->codTipoProcesso;
		$arrayParametros[1][0] = ":NUM_ORDEM";
		$arrayParametros[1][1] = $this->numOrdem;

		$arrayParametros[2][0] = ":DAT_INI";
		$arrayParametros[2][1] = $this->datIni;
		$arrayParametros[3][0] = ":DAT_FIM";
		$arrayParametros[3][1] = $this->datFim;
		
		$where = "";
		$_where = "";		
		if ($this->indConfereDocumento == "S"){
			$where = " AND D.COD_SITUACAO = 'C' ";
		}
		

		// Estabelecimento
		if(@$this->codEstabelecimento){
			$where .= " AND D.COD_ESTABELECIMENTO = :COD_ESTABELECIMENTO ";
			$arrayParametros[4][0] = ":COD_ESTABELECIMENTO";
			$arrayParametros[4][1] = $this->codEstabelecimento;
			
			$_where = "  ";
		
		}elseif(@$this->codGrupoEstabelecimento){
			$where .= " AND D.COD_ESTABELECIMENTO IN (SELECT EX.COD_ESTABELECIMENTO
	    				                                       FROM ESTABELECIMENTO EX
	    				                                      WHERE EX.COD_GRUPO_ESTABELECIMENTO = :COD_GRUPO_ESTABELECIMENTO) ";
			$arrayParametros[4][0] = ":COD_GRUPO_ESTABELECIMENTO";
			$arrayParametros[4][1] = $this->codGrupoEstabelecimento;
			
		}elseif(@$this->codFilial){
			$where .= " AND D.COD_ESTABELECIMENTO IN (SELECT EX.COD_ESTABELECIMENTO
                                                              FROM ESTABELECIMENTO EX
                                                             WHERE EX.COD_GRUPO_ESTABELECIMENTO IN (SELECT GE.COD_GRUPO_ESTABELECIMENTO
                                                                                                      FROM GRUPO_ESTABELECIMENTO GE
                                                                                                     WHERE GE.COD_FILIAL = :COD_FILIAL)) ";
			$arrayParametros[4][0] = ":COD_FILIAL";
			$arrayParametros[4][1] = $this->codFilial;
			
		}
		
		$strSql = "SELECT *
                     FROM (
					       SELECT DECODE(APC.COD_TIPO_LISTA, NULL, D.INDEXADOR".$this->numOrdem.",(SELECT LST.DSC_LISTA FROM LISTA LST WHERE LST.COD_LISTA = D.INDEXADOR".$this->numOrdem.") ) AS INDEXADOR, 
					              TD.COD_TIPO_DOCUMENTO
					         FROM DOCUMENTO D 
					         JOIN TIPO_DOCUMENTO TD ON (TD.COD_TIPO_DOCUMENTO = D.COD_TIPO_DOCUMENTO)
					         JOIN AUX_TIPO_PROCESSO APC ON (APC.COD_TIPO_PROCESSO = D.COD_TIPO_PROCESSO AND APC.COD_TIPO_DOCUMENTO = D.COD_TIPO_DOCUMENTO AND APC.NUM_ORDEM = :NUM_ORDEM)        
					        WHERE D.COD_TIPO_PROCESSO = :COD_TIPO_PROCESSO
					          AND TRUNC(D.DTH_CAPTACAO) BETWEEN :DAT_INI AND :DAT_FIM
					          AND D.INDEXADOR".$this->numOrdem." IS NOT NULL
					          $where
					       )
					 PIVOT(
					   COUNT(COD_TIPO_DOCUMENTO)
					    FOR COD_TIPO_DOCUMENTO IN ( $this->dscLista )
					 )
					 ORDER BY 1";
	
		$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
	
		return $retorno;

	}
	
	/** BUSCA OS DADOS DO  MAPA ORGANIZACIONAL
	 * @param integer codTipoProcesso
	 * @param integer codTipoDocumento
	 * @return boolean
	 */
	public function buscaDadosMapaOrganizacional(){
		$objConexao = $this->database;
	
		//VERIFICA SE O PROCESSO CONFERE DOCUMENTO
		$strSql = "SELECT IND_CONFERE_DOCUMENTO
				     FROM TIPO_PROCESSO
				    WHERE COD_TIPO_PROCESSO = :codTipoProcesso";
		 
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codTipoProcesso";
		$arrayParametros[0][1] = $this->codTipoProcesso;
	
		$arrayTipoProcesso = $objConexao->executeFetch($strSql, $arrayParametros);
	
		$this->indConfereDocumento = $arrayTipoProcesso[0]["IND_CONFERE_DOCUMENTO"];
	
	
	
		// BUSCA OS DADOS DO MAPA
		$arrayParametros = array();
		$arrayParametros[0][0] = ":COD_TIPO_PROCESSO";
		$arrayParametros[0][1] = $this->codTipoProcesso;
		//$arrayParametros[1][0] = ":NUM_ORDEM";
		//$arrayParametros[1][1] = $this->numOrdem;
	
		$arrayParametros[1][0] = ":DAT_INI";
		$arrayParametros[1][1] = $this->datIni;
		$arrayParametros[2][0] = ":DAT_FIM";
		$arrayParametros[2][1] = $this->datFim;
		
		$arrayParametros[3][0] = ":COD_EMPRESA";
		$arrayParametros[3][1] = $this->codEmpresa;
	
		$where = "";
		$_where = "";
		if ($this->indConfereDocumento == "S"){
			$where = " AND D.COD_SITUACAO = 'C' ";
		}
	
	
		// Estabelecimento
		if(@$this->codEstabelecimento){
			$_where = " WHERE INDEXADOR = ( SELECT NVL(PJX.NOM_FANTASIA, PFX.DSC_APELIDO)
                         FROM PESSOA  PX  
                         LEFT OUTER JOIN PESSOA_JURIDICA PJX ON (PJX.COD_PESSOA = PX.COD_PESSOA)
                         LEFT OUTER JOIN PESSOA_FISICA  PFX ON (PFX.COD_PESSOA = PX.COD_PESSOA) 
                        WHERE PX.COD_PESSOA = :COD_ESTABELECIMENTO )";
			
			$where .= " AND D.COD_ESTABELECIMENTO = :COD_ESTABELECIMENTO ";
			$arrayParametros[4][0] = ":COD_ESTABELECIMENTO";
			$arrayParametros[4][1] = $this->codEstabelecimento;
	
		}elseif(@$this->codGrupoEstabelecimento){
			$_where = " WHERE INDEXADOR IN ( SELECT NVL(PJX.NOM_FANTASIA, PFX.DSC_APELIDO)
			             FROM ESTABELECIMENTO EX
			             LEFT OUTER JOIN PESSOA_JURIDICA PJX ON (PJX.COD_PESSOA = EX.COD_ESTABELECIMENTO)
			             LEFT OUTER JOIN PESSOA_FISICA  PFX ON (PFX.COD_PESSOA = EX.COD_ESTABELECIMENTO)
			            WHERE EX.COD_GRUPO_ESTABELECIMENTO  = :COD_GRUPO_ESTABELECIMENTO )";
			
			
			$where .= " AND D.COD_ESTABELECIMENTO IN (SELECT EX.COD_ESTABELECIMENTO
	    				                                       FROM ESTABELECIMENTO EX
	    				                                      WHERE EX.COD_GRUPO_ESTABELECIMENTO = :COD_GRUPO_ESTABELECIMENTO) ";
			$arrayParametros[4][0] = ":COD_GRUPO_ESTABELECIMENTO";
			$arrayParametros[4][1] = $this->codGrupoEstabelecimento;
				
		}elseif(@$this->codFilial){
			$_where = " WHERE INDEXADOR IN ( SELECT NVL(PJX.NOM_FANTASIA, PFX.DSC_APELIDO)
                         FROM ESTABELECIMENTO EX
                         LEFT OUTER JOIN PESSOA_JURIDICA PJX ON (PJX.COD_PESSOA = EX.COD_ESTABELECIMENTO)
                         LEFT OUTER JOIN PESSOA_FISICA  PFX ON (PFX.COD_PESSOA = EX.COD_ESTABELECIMENTO) 
                        WHERE EX.COD_GRUPO_ESTABELECIMENTO IN (SELECT GEX.COD_GRUPO_ESTABELECIMENTO
                                                              FROM GRUPO_ESTABELECIMENTO GEX
                                                             WHERE GEX.COD_FILIAL = :COD_FILIAL)) ";
			
			$where .= " AND D.COD_ESTABELECIMENTO IN (SELECT EX.COD_ESTABELECIMENTO
                                                              FROM ESTABELECIMENTO EX
                                                             WHERE EX.COD_GRUPO_ESTABELECIMENTO IN (SELECT GE.COD_GRUPO_ESTABELECIMENTO
                                                                                                      FROM GRUPO_ESTABELECIMENTO GE
                                                                                                     WHERE GE.COD_FILIAL = :COD_FILIAL)) ";
			$arrayParametros[4][0] = ":COD_FILIAL";
			$arrayParametros[4][1] = $this->codFilial;
				
		}
	/* SELECT NVL(PJ.NOM_FANTASIA, PF.DSC_APELIDO) AS INDEXADOR,
					              TD.COD_TIPO_DOCUMENTO
					         FROM DOCUMENTO D
					         JOIN TIPO_DOCUMENTO TD ON (TD.COD_TIPO_DOCUMENTO = D.COD_TIPO_DOCUMENTO)
					         JOIN PESSOA  P   ON (P.COD_PESSOA = D.COD_ESTABELECIMENTO)
  						    LEFT OUTER JOIN PESSOA_JURIDICA PJ ON (PJ.COD_PESSOA = P.COD_PESSOA)
            		        LEFT OUTER JOIN PESSOA_FISICA  PF ON (PF.COD_PESSOA = P.COD_PESSOA)
							WHERE D.COD_TIPO_PROCESSO = :COD_TIPO_PROCESSO
					          AND TRUNC(D.DTH_CAPTACAO) BETWEEN :DAT_INI AND :DAT_FIM */
		
		$strSql = "SELECT *
                     FROM (SELECT NVL(PJ.NOM_FANTASIA, PF.DSC_APELIDO) AS INDEXADOR, D.COD_TIPO_DOCUMENTO
                             FROM ESTABELECIMENTO E
                             JOIN PESSOA  P  ON (P.COD_PESSOA = E.COD_ESTABELECIMENTO)
                             LEFT OUTER JOIN PESSOA_JURIDICA PJ ON (PJ.COD_PESSOA = P.COD_PESSOA)
                             LEFT OUTER JOIN PESSOA_FISICA  PF ON (PF.COD_PESSOA = P.COD_PESSOA)       
                             LEFT OUTER JOIN DOCUMENTO D ON ( D.COD_ESTABELECIMENTO = E.COD_ESTABELECIMENTO AND D.COD_TIPO_PROCESSO = :COD_TIPO_PROCESSO AND (TRUNC(D.DTH_CAPTACAO) BETWEEN :DAT_INI AND :DAT_FIM) $where )
                            WHERE ( PJ.COD_EMPRESA = :COD_EMPRESA OR  PF.COD_EMPRESA = :COD_EMPRESA )
					         )
					  PIVOT( COUNT(COD_TIPO_DOCUMENTO)
						          FOR COD_TIPO_DOCUMENTO IN ( $this->dscLista )
					  )
					  
					  $_where
					  
					  ORDER BY 1";
	
			          $retorno = $objConexao->executeFetch($strSql, $arrayParametros);

			          return $retorno;
	
	}
	
	/** BUSCA OS DADOS DAS IAMGENS DO MAPA INDEXADOR
	 * @param integer codTipoProcesso
	 * @param integer codTipoDocumento
	 * @return boolean
	 */
	public function buscaDadosDocumentoMapa(){
		$objConexao = $this->database;
	
		//VERIFICA SE O PROCESSO CONFERE DOCUMENTO
		$strSql = "SELECT IND_CONFERE_DOCUMENTO
				     FROM TIPO_PROCESSO
				    WHERE COD_TIPO_PROCESSO = :codTipoProcesso";
		 
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codTipoProcesso";
		$arrayParametros[0][1] = $this->codTipoProcesso;
	
		$arrayTipoProcesso = $objConexao->executeFetch($strSql, $arrayParametros);
	
		$this->indConfereDocumento = $arrayTipoProcesso[0]["IND_CONFERE_DOCUMENTO"];
		
		
		
		//VERIFICA SE O CAMPO É UMA LISTA
		$strSql = "SELECT COD_TIPO_LISTA
                     FROM AUX_TIPO_PROCESSO ATP 
                    WHERE ATP.COD_TIPO_PROCESSO = :COD_TIPO_PROCESSO 
                      AND ATP.COD_TIPO_DOCUMENTO =  :COD_TIPO_DOCUMENTO
                      AND ATP.NUM_ORDEM = :NUM_ORDEM";
			
		$arrayParametros = array();
		$arrayParametros[0][0] = ":COD_TIPO_PROCESSO";
		$arrayParametros[0][1] = $this->codTipoProcesso;
		$arrayParametros[1][0] = ":COD_TIPO_DOCUMENTO";
		$arrayParametros[1][1] = $this->codTipoDocumento;
		$arrayParametros[2][0] = ":NUM_ORDEM";
		$arrayParametros[2][1] = $this->numOrdem;
		
		$arrayTipoLista = $objConexao->executeFetch($strSql, $arrayParametros);
		$this->codTipoLista = $arrayTipoLista[0]["COD_TIPO_LISTA"];
		
		
		// BUSCA OS DADOS DO MAPA
		$arrayParametros = array();
		$arrayParametros[0][0] = ":COD_TIPO_PROCESSO";
		$arrayParametros[0][1] = $this->codTipoProcesso;
		
		$arrayParametros[1][0] = ":NUM_ORDEM";
		$arrayParametros[1][1] = $this->numOrdem;
	
		$arrayParametros[2][0] = ":DAT_INI";
		$arrayParametros[2][1] = $this->datIni;
		
		$arrayParametros[3][0] = ":DAT_FIM";
		$arrayParametros[3][1] = $this->datFim;
		
		$arrayParametros[4][0] = ":COD_TIPO_DOCUMENTO";
		$arrayParametros[4][1] = $this->codTipoDocumento;
		
		$arrayParametros[5][0] = ":DSC_CONTEUDO_CAMPO";
		$arrayParametros[5][1] = $this->conteudoCampo;
		
		
		
		$where = "";
	
		if ($this->indConfereDocumento == "S"){
			$where = " AND D.COD_SITUACAO = 'C' ";
		}
	    
		if ($this->codTipoLista){
			$where .= " AND D.INDEXADOR".$this->numOrdem." = (SELECT L.COD_LISTA FROM LISTA L WHERE L.COD_TIPO_LISTA = ".$this->codTipoLista." AND DSC_LISTA = :DSC_CONTEUDO_CAMPO) ";
		}else{
			$where .= " AND D.INDEXADOR".$this->numOrdem." = :DSC_CONTEUDO_CAMPO ";
		}
	
		// Estabelecimento
		if(@$this->codEstabelecimento){
			$where .= " AND D.COD_ESTABELECIMENTO = :COD_ESTABELECIMENTO ";
			$arrayParametros[6][0] = ":COD_ESTABELECIMENTO";
			$arrayParametros[6][1] = $this->codEstabelecimento;
	
		}elseif(@$this->codGrupoEstabelecimento){
			$where .= " AND D.COD_ESTABELECIMENTO IN (SELECT EX.COD_ESTABELECIMENTO
	    				                                       FROM ESTABELECIMENTO EX
	    				                                      WHERE EX.COD_GRUPO_ESTABELECIMENTO = :COD_GRUPO_ESTABELECIMENTO) ";
			$arrayParametros[6][0] = ":COD_GRUPO_ESTABELECIMENTO";
			$arrayParametros[6][1] = $this->codGrupoEstabelecimento;
				
		}elseif(@$this->codFilial){
			$where .= " AND D.COD_ESTABELECIMENTO IN (SELECT EX.COD_ESTABELECIMENTO
                                                              FROM ESTABELECIMENTO EX
                                                             WHERE EX.COD_GRUPO_ESTABELECIMENTO IN (SELECT GE.COD_GRUPO_ESTABELECIMENTO
                                                                                                      FROM GRUPO_ESTABELECIMENTO GE
                                                                                                     WHERE GE.COD_FILIAL = :COD_FILIAL)) ";
			$arrayParametros[6][0] = ":COD_FILIAL";
			$arrayParametros[6][1] = $this->codFilial;
				
		}
	
		$strSql = "SELECT D.COD_DOCUMENTO, MIM.DSC_MIME_TYPE , FNC_LISTA_INDEXADORES(D.COD_TIPO_PROCESSO, D.COD_TIPO_DOCUMENTO, D.INDEXADOR1, D.INDEXADOR2, D.INDEXADOR3, D.INDEXADOR4, D.INDEXADOR5, D.INDEXADOR6) AS DSC_INDEXADORES  
   					 FROM DOCUMENTO D
     			     JOIN TIPO_DOCUMENTO TD ON (TD.COD_TIPO_DOCUMENTO = D.COD_TIPO_DOCUMENTO)
     			     JOIN MIME_TYPE   MIM ON (MIM.COD_MIME_TYPE = D.COD_MIME_TYPE)
    				WHERE D.COD_TIPO_PROCESSO = :COD_TIPO_PROCESSO
      				  AND TRUNC(D.DTH_CAPTACAO) BETWEEN :DAT_INI AND :DAT_FIM
                      AND D.COD_TIPO_DOCUMENTO = :COD_TIPO_DOCUMENTO
                      $where
                    ORDER BY 1 desc";
	
			$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
			return $retorno;
	
		}
		
		/** BUSCA OS DADOS DAS IAMGENS DO MAPA INDEXADOR
		 * @param integer codTipoProcesso
		 * @param integer codTipoDocumento
		 * @return boolean
		 */
		public function buscaDadosDocumentoMapaOrganizacional(){
			$objConexao = $this->database;
		
			//VERIFICA SE O PROCESSO CONFERE DOCUMENTO
			$strSql = "SELECT IND_CONFERE_DOCUMENTO
				     FROM TIPO_PROCESSO
				    WHERE COD_TIPO_PROCESSO = :codTipoProcesso";
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoProcesso";
			$arrayParametros[0][1] = $this->codTipoProcesso;
		
			$arrayTipoProcesso = $objConexao->executeFetch($strSql, $arrayParametros);
		
			$this->indConfereDocumento = $arrayTipoProcesso[0]["IND_CONFERE_DOCUMENTO"];
		
			// BUSCA OS DADOS DO MAPA
			$arrayParametros = array();
			$arrayParametros[0][0] = ":COD_TIPO_PROCESSO";
			$arrayParametros[0][1] = $this->codTipoProcesso;
			
			$arrayParametros[1][0] = ":DAT_INI";
			$arrayParametros[1][1] = $this->datIni;
		
			$arrayParametros[2][0] = ":DAT_FIM";
			$arrayParametros[2][1] = $this->datFim;
		
			$arrayParametros[3][0] = ":COD_TIPO_DOCUMENTO";
			$arrayParametros[3][1] = $this->codTipoDocumento;
		
			$arrayParametros[4][0] = ":DSC_CONTEUDO_CAMPO";
			$arrayParametros[4][1] = $this->conteudoCampo;
		
		
		
			$where = "";
		
			if ($this->indConfereDocumento == "S"){
				$where = " AND D.COD_SITUACAO = 'C' ";
			}
		
		
			// Estabelecimento
			if(@$this->codEstabelecimento){
				$where .= " AND D.COD_ESTABELECIMENTO = :COD_ESTABELECIMENTO ";
				$arrayParametros[5][0] = ":COD_ESTABELECIMENTO";
				$arrayParametros[5][1] = $this->codEstabelecimento;
		
			}elseif(@$this->codGrupoEstabelecimento){
				$where .= " AND D.COD_ESTABELECIMENTO IN (SELECT EX.COD_ESTABELECIMENTO
	    				                                       FROM ESTABELECIMENTO EX
	    				                                      WHERE EX.COD_GRUPO_ESTABELECIMENTO = :COD_GRUPO_ESTABELECIMENTO) ";
				$arrayParametros[5][0] = ":COD_GRUPO_ESTABELECIMENTO";
				$arrayParametros[5][1] = $this->codGrupoEstabelecimento;
		
			}elseif(@$this->codFilial){
				$where .= " AND D.COD_ESTABELECIMENTO IN (SELECT EX.COD_ESTABELECIMENTO
                                                              FROM ESTABELECIMENTO EX
                                                             WHERE EX.COD_GRUPO_ESTABELECIMENTO IN (SELECT GE.COD_GRUPO_ESTABELECIMENTO
                                                                                                      FROM GRUPO_ESTABELECIMENTO GE
                                                                                                     WHERE GE.COD_FILIAL = :COD_FILIAL)) ";
				$arrayParametros[5][0] = ":COD_FILIAL";
				$arrayParametros[5][1] = $this->codFilial;
		
			}
		
			$strSql = "SELECT D.COD_DOCUMENTO, MIM.DSC_MIME_TYPE , FNC_LISTA_INDEXADORES(D.COD_TIPO_PROCESSO, D.COD_TIPO_DOCUMENTO, D.INDEXADOR1, D.INDEXADOR2, D.INDEXADOR3, D.INDEXADOR4, D.INDEXADOR5, D.INDEXADOR6) AS DSC_INDEXADORES  
                         FROM DOCUMENTO D
                         JOIN TIPO_DOCUMENTO TD ON (TD.COD_TIPO_DOCUMENTO = D.COD_TIPO_DOCUMENTO)
                         JOIN MIME_TYPE   MIM ON (MIM.COD_MIME_TYPE = D.COD_MIME_TYPE)
                         JOIN PESSOA  P   ON (P.COD_PESSOA = D.COD_ESTABELECIMENTO)
                         LEFT OUTER JOIN PESSOA_JURIDICA PJ ON (PJ.COD_PESSOA = P.COD_PESSOA)
                         LEFT OUTER JOIN PESSOA_FISICA  PF ON (PF.COD_PESSOA = P.COD_PESSOA)
                        WHERE D.COD_TIPO_PROCESSO = :COD_TIPO_PROCESSO
                         AND TRUNC(D.DTH_CAPTACAO) BETWEEN :DAT_INI AND :DAT_FIM
                         AND (PJ.NOM_FANTASIA = :DSC_CONTEUDO_CAMPO OR PF.DSC_APELIDO = :DSC_CONTEUDO_CAMPO)
                         AND D.COD_TIPO_DOCUMENTO = :COD_TIPO_DOCUMENTO
                         $where
                        ORDER BY 1 desc";
		
			$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
			return $retorno;
		
		}
}