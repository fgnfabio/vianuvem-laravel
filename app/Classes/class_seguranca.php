<?php

namespace App\Classes;

session_start();

/**
 * @author: Marcos Menezes
 */

Class seguranca {
	
	public  	$database;
	public  	$error;
	public  	$codToken;
	protected 	$dadosWebservice;
		
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//

	public function verificaUrl(){
		if (strripos($_SERVER['SERVER_NAME'], 'vianuvem') === false){
			return false;
		}else{
			return true;
		}
	}
	
	// verifica se é www e redireciona se nao for
	//public function verificaWww(){
	//	if (strripos($_SERVER['SERVER_NAME'], 'www.') === false){
	//		return false;
	//	}else{
	//		return true;
	//	}
	//}
	
	// verifica se o usuário está logado
	public function verificaSessao(){
		
		if( isset( $_SESSION['codUsuario']) ) {
			return true;
		} else {
			return false;
		}
	}
	
	// verifica se o usuario tem acesso
	public function verificaAcessoRecurso($cod_recurso){
	
		$recursos = explode(',', $_SESSION['codPerfilUsuarioRecurso']);
	
		if (in_array($cod_recurso, $recursos)){
			return true;
		}else{
			return false;
		}
	}
	
	// verifica se o usuario pode acessar a pagina
	public function verificaAcessoPagina(){
		$url_pagina = "home";
	
		// pega parametros da pagina que chamou
		$parametros = explode('&', $_SERVER['QUERY_STRING']);
	
		// pega o GO
		foreach ($parametros as $param){
			$qstring = explode('=', $param);
				
			if (strtoupper($qstring[0]) == 'GO'){
				$url_pagina = $qstring[1];
			}
		}
	
		$objConexao = $this->database;
	
		$strSql = "SELECT NVL(COD_RECURSO, 0) AS COD_RECURSO
                     FROM ITEM_MENU
                    WHERE UPPER(DSC_URL_DESTINO) = UPPER(:dscUrlDestino)";
	
		// parametros
		$arrayParametros = array();
		$arrayParametros[0][0]	= ":dscUrlDestino";
		$arrayParametros[0][1]	= $url_pagina;
	
		$arrayDados	= $objConexao->executeFetch($strSql, $arrayParametros);
	
		// página não inserida em recursos pode ser acessada
		if (count($arrayDados) == 0){
			return true;
	
		}elseif (TRUE == $this->verificaAcessoRecurso($arrayDados[0]["COD_RECURSO"])){
			return true;
				
		}else{
			return false;
		}
	
	}
	
	public function encriptaSenha($senha){
		return md5(md5($senha));
	}
	
	// autentica o usuario
	/**
	 * 
	 * @param string $dscIdentificacaoUsuario
	 * @param string $dscSenhaAcesso
	 * @param string $checaTipoUsuario
	 * @param boolean $sandbox
	 * @return boolean
	 */
	public function autentica($dscIdentificacaoUsuario, $dscSenhaAcesso, $checaTipoUsuario = 'X', $sandbox = false) {
		// instancia conexao
		$objConexao = $this->database;
	
		// pega os dados basicos
		/*$strSql = "
				SELECT U.COD_USUARIO, U.DSC_IDENTIFICACAO_USUARIO, P.NOM_PESSOA, U.COD_PAPEL_USUARIO, U.COD_TIPO_USUARIO, IND_LOGIN_SITE
				  FROM USUARIO U
				  JOIN PESSOA P ON (P.COD_PESSOA = U.COD_USUARIO)
			     WHERE U.IND_ATIVO = 'S'
				   AND U.DSC_IDENTIFICACAO_USUARIO = :dscIdentificacaoUsuario
				   AND U.DSC_SENHA_ACESSO = :dscSenhaAcesso";*/
		
		$strSql = "
				SELECT U.COD_USUARIO, E.COD_PESSOA, U.DSC_IDENTIFICACAO_USUARIO, P.NOM_PESSOA, U.COD_PAPEL_USUARIO, U.COD_TIPO_USUARIO, IND_LOGIN_SITE
                  FROM USUARIO U
                  JOIN PESSOA P ON (P.COD_PESSOA = U.COD_USUARIO)
                  JOIN PESSOA_EMAIL E ON (E.COD_PESSOA = U.COD_USUARIO)
                 WHERE U.IND_ATIVO = 'S'
                   AND (U.DSC_IDENTIFICACAO_USUARIO = :dscIdentificacaoUsuario OR E.DSC_EMAIL = :dscIdentificacaoUsuario)
				   AND U.DSC_SENHA_ACESSO = :dscSenhaAcesso";
		
		// parametros
		$arrayParametros = array();
		$arrayParametros[0][0]	= ":dscIdentificacaoUsuario";
		$arrayParametros[0][1]	= $dscIdentificacaoUsuario;
		$arrayParametros[1][0]	= ":dscSenhaAcesso";
		$arrayParametros[1][1]	= $dscSenhaAcesso;
	
		$arrayUsuario	= $objConexao->executeFetch($strSql, $arrayParametros);
		// se nao acha usuario retorna false (array vazio de retorno da execucao)
		if (is_array($arrayUsuario) && count($arrayUsuario) == 0 || is_null($arrayUsuario)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Nenhum usuário encontrado";
			return false;
		}
		
		//verifica se o tipo de usuário deve ser testado
		if ($checaTipoUsuario != 'X' && $checaTipoUsuario != $arrayUsuario[0]["COD_TIPO_USUARIO"]){
			$this->error['code'] 	= "";
			$this->error['message'] = "Usuário não possui permissão para acessar";
			return false;
		}
		
		// verifica se pode se logar pelo site (apenas pela tela inicial de login)
		if ($arrayUsuario[0]["IND_LOGIN_SITE"] != 'S' && $sandbox == false){
			$this->error['code'] 	= "";
			$this->error['message'] = "Acesso negado! Por favor, faça seu login pelo site da sua empresa!";
			return false;
		}
	
		// pega a lista de perfis do usuario
		//$strSql = "SELECT UPU.COD_PERFIL_USUARIO, PU.IND_PERFIL_SISTEMA
		//		     FROM USUARIO_PERFIL_USUARIO UPU
		//		     JOIN PERFIL_USUARIO PU ON (PU.COD_PERFIL_USUARIO = UPU.COD_PERFIL_USUARIO)
		//		    WHERE COD_USUARIO = :codUsuario";
		
		$strSql = "SELECT LISTAGG(COD_PERFIL_USUARIO, ',') WITHIN GROUP (ORDER BY COD_PERFIL_USUARIO) AS COD_PERFIL_USUARIO
                     FROM USUARIO_PERFIL_USUARIO PUP
                     WHERE COD_USUARIO = :codUsuario";
		
		// parametros
		$arrayParametros = array();
		$arrayParametros[0][0]	= ":codUsuario";
		$arrayParametros[0][1]	= $arrayUsuario[0]["COD_USUARIO"];
	
		$arrayPerfilUsuario	= $objConexao->executeFetch($strSql, $arrayParametros);
		$perfil = $arrayPerfilUsuario[0]['COD_PERFIL_USUARIO'];
		
		//VERIFICA SE ALGUM DOS PERFIS DO USUARIO É PERFIL DE SISTEMA
		$strSql = "SELECT COUNT(PU.IND_PERFIL_SISTEMA) AS IND_PERFIL_SISTEMA
                     FROM USUARIO_PERFIL_USUARIO UPU
                     JOIN PERFIL_USUARIO PU ON (PU.COD_PERFIL_USUARIO = UPU.COD_PERFIL_USUARIO)
                    WHERE UPU.COD_USUARIO = :codUsuario
                      AND PU.IND_PERFIL_SISTEMA = 'S' ";
		
		// parametros
		$arrayParametros = array();
		$arrayParametros[0][0]	= ":codUsuario";
		$arrayParametros[0][1]	= $arrayUsuario[0]["COD_USUARIO"];
		
		$arrayPerfilSistema	= $objConexao->executeFetch($strSql, $arrayParametros);
		$indPerfilSistema_ = $arrayPerfilSistema[0]['IND_PERFIL_SISTEMA'];
		$indPerfilSistema = "N";
		
		if($indPerfilSistema_ > 0){
			$indPerfilSistema = 'S';
		}
		
		// pra não dar pau no select abaixo
		if (!$perfil){
			$perfil = "0";
		}
					
		// pega recursos que pode acessar
		$strSql = "SELECT DISTINCT COD_RECURSO
				     FROM PERFIL_USUARIO_RECURSO
				    WHERE COD_PERFIL_USUARIO IN ( $perfil )";
	
		$arrayPerfilUsuarioRecurso	= $objConexao->executeFetch($strSql);
		$recurso = "";
		if (is_array($arrayPerfilUsuarioRecurso)){
			for ($x=0; $x < count($arrayPerfilUsuarioRecurso); $x++){
				$recurso .= $arrayPerfilUsuarioRecurso[$x]["COD_RECURSO"].",";
			}
			$recurso = substr($recurso, 0, strlen($recurso)-1);
		}
	
		// pega empresa, filial grupo e estabelecimento do usuaário
		
		// parametros
		$arrayParametros = array();
		$arrayParametros[0][0]	= ":codUsuario";
		$arrayParametros[0][1]	= $arrayUsuario[0]["COD_USUARIO"];
		
		// se for usuario estabel
		if ($arrayUsuario[0]["COD_TIPO_USUARIO"] == 'E'){
			$strSql =  "SELECT DISTINCT(E.COD_EMPRESA) AS COD_EMPRESA, E.NOM_EMPRESA, F.COD_FILIAL, PF.NOM_PESSOA AS NOM_FILIAL,
						       GE.COD_GRUPO_ESTABELECIMENTO, GE.NOM_GRUPO_ESTABELECIMENTO,
						       ES.COD_ESTABELECIMENTO, PE.NOM_PESSOA AS NOM_ESTABELECIMENTO,
					           E.DSC_LABEL_FILIAL, E.DSC_LABEL_GRUPO_ESTABEL, E.DSC_LABEL_ESTABELECIMENTO,
					           E.IND_UTILIZA_CALENDARIO
						  FROM USUARIO_ESTABELECIMENTO UETP
						  JOIN ESTABELECIMENTO ES ON (ES.COD_ESTABELECIMENTO = UETP.COD_ESTABELECIMENTO)
						  JOIN PESSOA PE ON (PE.COD_PESSOA = ES.COD_ESTABELECIMENTO)
						  JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_GRUPO_ESTABELECIMENTO = ES.COD_GRUPO_ESTABELECIMENTO)
						  JOIN FILIAL F ON (F.COD_FILIAL = GE.COD_FILIAL)
						  JOIN PESSOA PF ON (PF.COD_PESSOA = F.COD_FILIAL)
						  JOIN EMPRESA E ON (E.COD_EMPRESA = F.COD_EMPRESA)
						 WHERE UETP.COD_USUARIO = :codUsuario";
			
			$arrayEmpresa				= $objConexao->executeFetch($strSql, $arrayParametros);
			$codEmpresa 				= $arrayEmpresa[0]["COD_EMPRESA"];
			$nomEmpresa 				= $arrayEmpresa[0]["NOM_EMPRESA"];
			$codFilial 					= $arrayEmpresa[0]["COD_FILIAL"];
			$nomFilial					= $arrayEmpresa[0]["NOM_FILIAL"];
			$codGrupoEstabel 			= $arrayEmpresa[0]["COD_GRUPO_ESTABELECIMENTO"];
			$nomGrupoEstabel			= $arrayEmpresa[0]["NOM_GRUPO_ESTABELECIMENTO"];
			$codEstabel					= $arrayEmpresa[0]["COD_ESTABELECIMENTO"];
			$nomEstabel					= $arrayEmpresa[0]["NOM_ESTABELECIMENTO"];
			$dscLabelFilial             = $arrayEmpresa[0]["DSC_LABEL_FILIAL"];
			$dscLabelGrupoEstabel       = $arrayEmpresa[0]["DSC_LABEL_GRUPO_ESTABEL"];
			$dscLabelEstabelecimento    = $arrayEmpresa[0]["DSC_LABEL_ESTABELECIMENTO"];
			$indUtilizaCalendario        = $arrayEmpresa[0]["IND_UTILIZA_CALENDARIO"];
			
		// se for usuario grupo
		}elseif ($arrayUsuario[0]["COD_TIPO_USUARIO"] == 'G'){
			$strSql =  "SELECT E.COD_EMPRESA, E.NOM_EMPRESA, F.COD_FILIAL, PF.NOM_PESSOA AS NOM_FILIAL,
						       GE.COD_GRUPO_ESTABELECIMENTO, GE.NOM_GRUPO_ESTABELECIMENTO,
						       NULL AS COD_ESTABELECIMENTO, NULL AS NOM_ESTABELECIMENTO,
					           E.DSC_LABEL_FILIAL, E.DSC_LABEL_GRUPO_ESTABEL, E.DSC_LABEL_ESTABELECIMENTO,
							   E.IND_UTILIZA_CALENDARIO
						  FROM USUARIO_GRUPO_ESTABELECIMENTO UGE
						  JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_GRUPO_ESTABELECIMENTO = UGE.COD_GRUPO_ESTABELECIMENTO)
						  JOIN FILIAL F ON (F.COD_FILIAL = GE.COD_FILIAL)
						  JOIN EMPRESA E ON (E.COD_EMPRESA = F.COD_EMPRESA)
						  JOIN PESSOA PF ON (PF.COD_PESSOA = F.COD_FILIAL)
						 WHERE UGE.COD_USUARIO = :codUsuario";
			
			$arrayEmpresa				= $objConexao->executeFetch($strSql, $arrayParametros);
			$codEmpresa 				= $arrayEmpresa[0]["COD_EMPRESA"];
			$nomEmpresa 				= $arrayEmpresa[0]["NOM_EMPRESA"];
			$codFilial 					= $arrayEmpresa[0]["COD_FILIAL"];
			$nomFilial					= $arrayEmpresa[0]["NOM_FILIAL"];
			$dscLabelFilial             = $arrayEmpresa[0]["DSC_LABEL_FILIAL"];
			$dscLabelGrupoEstabel       = $arrayEmpresa[0]["DSC_LABEL_GRUPO_ESTABEL"];
			$dscLabelEstabelecimento    = $arrayEmpresa[0]["DSC_LABEL_ESTABELECIMENTO"];
			$indUtilizaCalendario       = $arrayEmpresa[0]["IND_UTILIZA_CALENDARIO"];
			$codGrupoEstabel = "";
			$nomGrupoEstabel = "";
			foreach ($arrayEmpresa as $novoArray){
				$codGrupoEstabel .= $novoArray['COD_GRUPO_ESTABELECIMENTO'].',';
				$nomGrupoEstabel .= $novoArray['NOM_GRUPO_ESTABELECIMENTO'].',';
			}
			$codGrupoEstabel = substr($codGrupoEstabel, 0, strlen($codGrupoEstabel)-1);
			$nomGrupoEstabel = substr($nomGrupoEstabel, 0, strlen($nomGrupoEstabel)-1);
			
			$codEstabel					= $codGrupoEstabel;//$arrayEmpresa[0]["COD_ESTABELECIMENTO"];
			$nomEstabel					= $nomGrupoEstabel;//$arrayEmpresa[0]["NOM_ESTABELECIMENTO"];
			
		// se for usuario filial
		}elseif ($arrayUsuario[0]["COD_TIPO_USUARIO"] == 'F'){
			$strSql = " SELECT E.COD_EMPRESA, E.NOM_EMPRESA, F.COD_FILIAL, PF.NOM_PESSOA AS NOM_FILIAL,
					           NULL AS COD_GRUPO_ESTABELECIMENTO, NULL AS NOM_GRUPO_ESTABELECIMENTO,
					           NULL AS COD_ESTABELECIMENTO, NULL AS NOM_ESTABELECIMENTO,
					           E.DSC_LABEL_FILIAL, E.DSC_LABEL_GRUPO_ESTABEL, E.DSC_LABEL_ESTABELECIMENTO,
					           E.IND_UTILIZA_CALENDARIO
						  FROM USUARIO_FILIAL UF
						  JOIN FILIAL F ON (F.COD_FILIAL = UF.COD_FILIAL)
						  JOIN EMPRESA E ON (E.COD_EMPRESA = F.COD_EMPRESA)
						  JOIN PESSOA PF ON (PF.COD_PESSOA = F.COD_FILIAL)
						 WHERE UF.COD_USUARIO = :codUsuario";
			
			$arrayEmpresa				= $objConexao->executeFetch($strSql, $arrayParametros);
			$codEmpresa 				= $arrayEmpresa[0]["COD_EMPRESA"];
			$nomEmpresa 				= $arrayEmpresa[0]["NOM_EMPRESA"];
			
			$codFilial = "";
			$nomFilial = "";
			foreach ($arrayEmpresa as $novoArray){
				$codFilial .= $novoArray['COD_FILIAL'].',';
				$nomFilial .= $novoArray['NOM_FILIAL'].',';
			}
			$codFilial = substr($codFilial, 0, strlen($codFilial)-1);
			$nomFilial = substr($nomFilial, 0, strlen($nomFilial)-1);
			
			//$codFilial 					= $codFilial;//$arrayEmpresa[0]["COD_FILIAL"];
			//$nomFilial					= $nomFilial;//$arrayEmpresa[0]["NOM_FILIAL"];
			$codGrupoEstabel 			= $arrayEmpresa[0]["COD_GRUPO_ESTABELECIMENTO"];
			$nomGrupoEstabel			= $arrayEmpresa[0]["NOM_GRUPO_ESTABELECIMENTO"];
			$codEstabel					= $arrayEmpresa[0]["COD_ESTABELECIMENTO"];
			$nomEstabel					= $arrayEmpresa[0]["NOM_ESTABELECIMENTO"];
			$dscLabelFilial             = $arrayEmpresa[0]["DSC_LABEL_FILIAL"];
			$dscLabelGrupoEstabel       = $arrayEmpresa[0]["DSC_LABEL_GRUPO_ESTABEL"];
			$dscLabelEstabelecimento    = $arrayEmpresa[0]["DSC_LABEL_ESTABELECIMENTO"];
			$indUtilizaCalendario       = $arrayEmpresa[0]["IND_UTILIZA_CALENDARIO"];
			
		// se for usuario admin
		}elseif ($arrayUsuario[0]["COD_TIPO_USUARIO"] == 'A'){
			$strSql =  "SELECT E.COD_EMPRESA, E.NOM_EMPRESA, NULL AS COD_FILIAL, NULL AS NOM_FILIAL,
						       NULL AS COD_GRUPO_ESTABELECIMENTO, NULL AS NOM_GRUPO_ESTABELECIMENTO,
						       NULL AS COD_ESTABELECIMENTO, NULL AS NOM_ESTABELECIMENTO,
					           E.DSC_LABEL_FILIAL, E.DSC_LABEL_GRUPO_ESTABEL, E.DSC_LABEL_ESTABELECIMENTO, E.IND_UTILIZA_CALENDARIO
						  FROM PESSOA_FISICA PF
						  JOIN EMPRESA E ON (E.COD_EMPRESA = PF.COD_EMPRESA)
						 WHERE PF.COD_PESSOA = :codUsuario";
				
			$arrayEmpresa				= $objConexao->executeFetch($strSql, $arrayParametros);
			$codEmpresa 				= $arrayEmpresa[0]["COD_EMPRESA"];
			$nomEmpresa 				= $arrayEmpresa[0]["NOM_EMPRESA"];
			$codFilial 					= $arrayEmpresa[0]["COD_FILIAL"];
			$nomFilial					= $arrayEmpresa[0]["NOM_FILIAL"];
			$codGrupoEstabel 			= $arrayEmpresa[0]["COD_GRUPO_ESTABELECIMENTO"];
			$nomGrupoEstabel			= $arrayEmpresa[0]["NOM_GRUPO_ESTABELECIMENTO"];
			$codEstabel					= $arrayEmpresa[0]["COD_ESTABELECIMENTO"];
			$nomEstabel					= $arrayEmpresa[0]["NOM_ESTABELECIMENTO"];
			$dscLabelFilial             = $arrayEmpresa[0]["DSC_LABEL_FILIAL"];
			$dscLabelGrupoEstabel       = $arrayEmpresa[0]["DSC_LABEL_GRUPO_ESTABEL"];
			$dscLabelEstabelecimento    = $arrayEmpresa[0]["DSC_LABEL_ESTABELECIMENTO"];
			$indUtilizaCalendario       = $arrayEmpresa[0]["IND_UTILIZA_CALENDARIO"];
		}
	
		// verifica se o usuário não tem ip bloqueado
		// bloqueio por estabelecimento
		if ($arrayUsuario[0]["COD_TIPO_USUARIO"] == 'E'){
			include_once 'class_estabelecimentoIp.php';
			include_once 'class_grupoEstabelecimentoIp.php';
			$objEstabelIp 		= new estabelecimentoIp($this->database);
			$objGrupoEstabelIp 	= new grupoEstabelecimentoIp($this->database);
				
			if (!$objEstabelIp->verificaIp($codEstabel, $_SERVER['REMOTE_ADDR'])){
				$this->error['code'] 	= "";
				$this->error['message'] = "IP em uso bloqueado pelo sistema!";
				return false;
			}
				
			if (!$objGrupoEstabelIp->verificaIp($codGrupoEstabel, $_SERVER['REMOTE_ADDR'])){
				$this->error['code'] 	= "";
				$this->error['message'] = "IP em uso bloqueado pelo sistema!";
				return false;
			}
	
		// bloqueio por grupo
		}elseif ($arrayUsuario[0]["COD_TIPO_USUARIO"] == 'G'){
			include_once 'class_grupoEstabelecimentoIp.php';
			$objGrupoEstabelIp = new grupoEstabelecimentoIp($this->database);
	
			if (!$objGrupoEstabelIp->verificaIp($codGrupoEstabel, $_SERVER['REMOTE_ADDR'])){
				$this->error['code'] 	= "";
				$this->error['message'] = "IP em uso bloqueado pelo sistema!";
				return false;
			}
		}

		// joga dados na sessao

        $_SESSION['codUsuario'] 				= $arrayUsuario[0]["COD_USUARIO"];
        $_SESSION['nomPessoa'] 					= $arrayUsuario[0]["NOM_PESSOA"];
		$_SESSION['dscIdentificacaoUsuario']	= $arrayUsuario[0]["DSC_IDENTIFICACAO_USUARIO"];
	
		// se administrador gera nova variavel de sessao para usar em auditorias (devido adm poder trocar empresa)
		if ($arrayUsuario[0]["COD_TIPO_USUARIO"] == 'A'){
			$_SESSION['codEmpresaReal']			= $codEmpresa;
		}
		
		$_SESSION['codEmpresa']					= $codEmpresa;
		$_SESSION['nomEmpresa']					= $nomEmpresa;
		$_SESSION['codFilial'] 					= $codFilial;
		$_SESSION['nomFilial']					= $nomFilial;
		$_SESSION['codGrupoEstabelecimento']	= $codGrupoEstabel;
		$_SESSION['nomGrupoEstabelecimento']	= $nomGrupoEstabel;
		$_SESSION['codEstabelecimento'] 		= $codEstabel;
		$_SESSION['nomEstabelecimento']			= $nomEstabel;
		
		
		$_SESSION['dscLabelFilial']			  = $dscLabelFilial;
		$_SESSION['dscLabelGrupoEstabel']     = $dscLabelGrupoEstabel;
		$_SESSION['dscLabelEstabelecimento'] = $dscLabelEstabelecimento;
		
		$_SESSION['indUtilizaCalendario'] =     $indUtilizaCalendario;
	
		$_SESSION['codTipoUsuario']				= $arrayUsuario[0]["COD_TIPO_USUARIO"];
		$_SESSION['codPapelUsuario']			= $arrayUsuario[0]["COD_PAPEL_USUARIO"];
		$_SESSION['codPerfilUsuario'] 			= $perfil;
		$_SESSION['codPerfilUsuarioRecurso'] 	= $recurso;
		$_SESSION['indPerfilSistema'] 	        = $indPerfilSistema;

        $this->dadosWebservice = $_SESSION;
		
		return true;
	}
	
}