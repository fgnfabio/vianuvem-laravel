<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class filial
{
	public    $database;
	public    $error;
	protected $codEmpresa;
	protected $codFilial;
	protected $codFilialOld;
	protected $codUsuario;
	
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getCodEmpresa() {
		return $this->codEmpresa;
	}
	
	public function setCodEmpresa($codEmpresa) {
		$this->codEmpresa = $codEmpresa;
	}
	
	public function getCodFilial() {
		return $this->codFilial;
	}
	
	public function setCodFilial($codFilial) {
		$this->codFilial = $codFilial;
	}
	
	public function setCodUsuario($codUsuario) {
		$this->codUsuario = $codUsuario;
	}
	
	public function getFilial($codFilial){
		
		if (TRUE == $codFilial) {
			$objConexao = $this->database;
		
			//Seleciona informacoes do usuario
			$strSql = "SELECT COD_EMPRESA, COD_FILIAL 
					     FROM FILIAL 
					    WHERE COD_FILIAL = :codFilial";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codFilial";
			$arrayParametros[0][1] = $codFilial;
		
			$arrayFilial = $objConexao->executeFetch($strSql, $arrayParametros);
		
			$this->codEmpresa = $arrayFilial[0]["COD_EMPRESA"];
			$this->codFilial = $arrayFilial[0]["COD_FILIAL"];
		}
		return true;
	}
	
	function insFilial() {	
		$objConexao = $this->database;
		
		$strSql = "INSERT INTO FILIAL
				          (COD_FILIAL, COD_EMPRESA) 
				   VALUES (:codFilial,:codEmpresa)";

		$arrayParametros = array();
		$arrayParametros[0][0] = ":codFilial";
		$arrayParametros[0][1] = $this->codFilial;
		$arrayParametros[1][0] = ":codEmpresa";
		$arrayParametros[1][1] = $this->codEmpresa;

		if (!$objConexao->execute($strSql, $arrayParametros)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro ao inserir filial";
			return false;
		}
		
		return true;
	}
	
	function changeFilial() {
		if (TRUE == $this->codEmpresa && TRUE == $this->codFilial && TRUE == $this->codFilialOld) {
			$objConexao = $this->database;
			
			$strSql = "UPDATE FILIAL 
					      SET COD_FILIAL = :codfilial
					    WHERE COD_FILIAL = :codfilialOld
					      AND COD_EMPRESA = :codEmpresa";
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codFilial";
			$arrayParametros[0][1] = $this->codFilial;
			$arrayParametros[1][0] = ":codfilialOld";
			$arrayParametros[1][1] = $this->codFilialOld;
			$arrayParametros[2][0] = ":codEmpresa";
			$arrayParametros[2][1] = $this->codEmpresa;
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar filial";
				return false;
			}
			
			$objConexao->commitTrans();
	
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para alterar filial";
			return false;
		}
	}
	
	function delFilial($codFilial) {
	
		if ($codFilial == TRUE) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codFilial";
			$arrayParametros[0][1] = $codFilial;
			
			$strSql = "DELETE 
					     FROM FILIAL 
					    WHERE COD_FILIAL =:codFilial";
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar filial";
				return false;
			}
			
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para deletar filial";
			return false;
		}
	}
	
	public function totalFilial(){
		$objConexao = $this->database;
		
		$strSql = "SELECT COUNT(COD_FILIAL) AS TOTAL 
				     FROM FILIAL";
		$arrayDados = $objConexao->executeFetch($strSql);
		return $arrayDados[0]["TOTAL"];
	}
	
	public function totalFilialEmpresa(){
		$objConexao = $this->database;
		
		$strSql = "SELECT COUNT(COD_FILIAL) AS TOTAL 
				     FROM FILIAL
				    WHERE COD_EMPRESA = ".$_SESSION['codEmpresa'];
		$arrayDados = $objConexao->executeFetch($strSql);
		return $arrayDados[0]["TOTAL"];
	}
	
	public function listaFilialUsuario(){
		$objConexao = $this->database;
	
		// chama procedure para autenticar e fazer inserções necessarias
        $strSql = "BEGIN PKG_USUARIO.PRC_LISTA_FILIAL(:COD_USUARIO, :COD_EMPRESA_LOGADA, :refc); END;";
        
       	$arrayParametros = array();
       	$arrayParametros[0][0] = ":COD_USUARIO";
       	$arrayParametros[0][1] = $this->codUsuario;
       	$arrayParametros[1][0] = ":COD_EMPRESA_LOGADA";
       	$arrayParametros[1][1] = $this->codEmpresa;
        
       	$retorno = $objConexao->executeProcedureRefCursor($strSql, $arrayParametros);
       	return $retorno;
	}
}
?>