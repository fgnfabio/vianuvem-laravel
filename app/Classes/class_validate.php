<?php

namespace App\Classes;

/**
 * Classe para validação de identificadores
 * Todas as functions que forem aparecer no combo para seleção de validação devem ter seus nomes iniciados com "valida"
 * 
 * @author: Marcos Menezes
 * 
 */

Class validate {

	public  $error;

    function __construct() {
    	$this->error['code'] 	= "";
    	$this->error['message'] = "";
        return true;
    }
    
    public function __get($propriedade) {
    	return $this->$propriedade;
    }
    
    public function __set($propriedade, $valor) {
    	$this->$propriedade = $valor;
    }
    
    //**********************************************************************************************//
    
    public function validaCPF($cpf){
    	
    	$cpf = str_replace('.', '', $cpf);
    	$cpf = str_replace('-', '', $cpf);
    	$cpf = str_replace('/', '', $cpf);
    	
    	if (!is_numeric($cpf)) {
    		return false;
    		
    	} elseif ( ($cpf == '11111111111') || ($cpf == '22222222222') ||
    			($cpf == '33333333333') || ($cpf == '44444444444') ||
    			($cpf == '55555555555') || ($cpf == '66666666666') ||
    			($cpf == '77777777777') || ($cpf == '88888888888') ||
    			($cpf == '99999999999') || ($cpf == '00000000000') ) {
    		
    		return false;
    			
    	} else {
    		// PEGA O DIGITO VERIFIACADOR
    		$dv_informado = substr($cpf, 9,2);
    				
    		for ($i=0; $i<=8; $i++) {
    			$digito[$i] = substr($cpf, $i,1);
    		}
    				
    		// CALCULA O VALOR DO DIGITO VERIFICADOR
    		$posicao = 10;
    		$soma = 0;
    				
    		for ($i=0; $i<=8; $i++) {
    			$soma = $soma + $digito[$i] * $posicao;
    			$posicao = $posicao - 1;
    		}
    				
    		$digito[9] = $soma % 11;
    			
    		if ($digito[9] < 2) {
    			$digito[9] = 0;
    			
    		} else {
    			$digito[9] = 11 - $digito[9];
    		}
    				
    		// CALCULA O VALOR DO DIGITO VERIFICADOR
    		$posicao = 11;
    		$soma = 0;
    				
    		for ($i=0; $i<=9; $i++) {
    			$soma = $soma + $digito[$i] * $posicao;
    			$posicao = $posicao - 1;
    		}
    				
    		$digito[10] = $soma % 11;
    			
    		if ($digito[10] < 2) {
    			$digito[10] = 0;
    			
    		} else {
    			$digito[10] = 11 - $digito[10];
    		}
    				
    		// VERIFICA SE O DV CALCULADO É IGUAL AO INFORMADO
    		$dv = $digito[9] * 10 + $digito[10];
    		if ($dv != $dv_informado) {
    			return false;
    		}
    	}
    	
    	return true;
    }
    
    public function validaCNPJ($cnpj){
    	
    	$cnpj = str_replace('.', '', $cnpj);
    	$cnpj = str_replace('-', '', $cnpj);
    	$cnpj = str_replace('/', '', $cnpj);
    	
    	if (!is_numeric($cnpj)) {
    		return false;
    		
    	}elseif (strlen($cnpj) <> 14){
    		return false;
    	
    	} elseif ( ($cnpj == "00000000000000") || ($cnpj == "11111111111111") ||
    				($cnpj == "22222222222222") || ($cnpj == "33333333333333") ||
    				($cnpj == "44444444444444") || ($cnpj == "55555555555555") ||
    				($cnpj == "66666666666666") || ($cnpj == "77777777777777") ||
    				($cnpj == "88888888888888") || ($cnpj == "99999999999999") ) {
    		
    		return false;
    	} else {
    	
	    	$soma1 = ($cnpj[0] * 5) +
	    	($cnpj[1] * 4) +
	    	($cnpj[2] * 3) +
	    	($cnpj[3] * 2) +
	    	($cnpj[4] * 9) +
	    	($cnpj[5] * 8) +
	    	($cnpj[6] * 7) +
	    	($cnpj[7] * 6) +
	    	($cnpj[8] * 5) +
	    	($cnpj[9] * 4) +
	    	($cnpj[10] * 3) +
	    	($cnpj[11] * 2);
	    	
	    	$resto = $soma1 % 11;
	    	$digito1 = $resto < 2 ? 0 : 11 - $resto;
	    	
	    	$soma2 = ($cnpj[0] * 6) +
	    	($cnpj[1] * 5) +
	    	($cnpj[2] * 4) +
	    	($cnpj[3] * 3) +
	    	($cnpj[4] * 2) +
	    	($cnpj[5] * 9) +
	    	($cnpj[6] * 8) +
	    	($cnpj[7] * 7) +
	    	($cnpj[8] * 6) +
	    	($cnpj[9] * 5) +
	    	($cnpj[10] * 4) +
	    	($cnpj[11] * 3) +
	    	($cnpj[12] * 2);
	    		
	    	$resto = $soma2 % 11;
	    	$digito2 = $resto < 2 ? 0 : 11 - $resto;
	    	
	    	return (($cnpj[12] == $digito1) && ($cnpj[13] == $digito2));
    	
    	}
    	
    	return true;
    }
	
    public function validaCPFCNPJ($numDocumento){
    	$retorno = false; 
    	$numDocumento = str_replace('.', '', $numDocumento);
    	$numDocumento = str_replace('-', '', $numDocumento);
    	$numDocumento = str_replace('/', '', $numDocumento);
    	 
    	if (strlen($numDocumento) == 11){
    		$retorno = $this->validaCPF($numDocumento);
    	}else{
    		$retorno = $this->validaCNPJ($numDocumento);
    	}
    	 
    	return $retorno;
    }
    
    public function validaAnoMes($anoMes){
    	$retorno = false;
    	$anoMes = str_replace('/', '', $anoMes);
    	
    	if (strlen($anoMes) != 6){
    		$retorno = false;
    	}else{
    		if( is_numeric ( trim($anoMes) ) ){
    			$ano = substr($anoMes, 0, 4);
    			$mes = substr($anoMes, -2);
    			if ($mes > 12){
    				$retorno = false;
    			}elseif($mes < 1){
    				$retorno = false;
    			}elseif($ano < 1900 || $ano > 2049){
    				$retorno = false;
    			}else{
    				return true;
    			}
    		}else{
    			$retorno = false;
    		}
    		
    	}
    	return $retorno;
    }
}

