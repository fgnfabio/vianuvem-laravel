<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class grupoEstabelecimentoIp
{
	public    $database;
	public    $error;
	
	protected $codGrupoEstabelecimento;
	protected $datInicial;
	protected $datInicialAntiga;
	protected $datFinal;
	protected $dscMascaraIp;
	
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getCodGrupoEstabelecimento() {
		return $this->codGrupoEstabelecimento;
	}
	
	public function setCodGrupoEstabelecimento($codGrupoEstabelecimento) {
		$this->codGrupoEstabelecimento = $codGrupoEstabelecimento;
	}
	
	public function getDatInicial() {
		return $this->datInicial;
	}
	
	public function setDatInicial($datInicial) {
		$this->datInicial = $datInicial;
	}
	
	public function setDatInicialAntiga($datInicialAntiga) {
		$this->datInicialAntiga = $datInicialAntiga;
	}
	
	public function getDatFinal() {
		return $this->datFinal;
	}
	
	public function setDatFinal($datFinal) {
		$this->datFinal = $datFinal;
	}
	
	public function getDscMascaraIp() {
		return $this->dscMascaraIp;
	}
	
	public function setdscMascaraIp($dscMascaraIp) {
		$this->dscMascaraIp = $dscMascaraIp;
	}
	
	
	public function getGrupoEstabelecimentoIp($codGrupoEstabelecimento){
		
		if(TRUE == $codGrupoEstabelecimento){
			$objConexao = $this->database;

			$strSql = "SELECT GEI.DAT_INICIAL, GEI.DAT_FINAL, GEI.DSC_MASCARA_IP
  						 FROM GRUPO_ESTABELECIMENTO_IP GEI
 						WHERE GEI.COD_GRUPO_ESTABELECIMENTO = :codGrupoEstabelecimento";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codGrupoEstabelecimento";
			$arrayParametros[0][1] = $codGrupoEstabelecimento;
		
			$arrayGrupoEstabelecimento = $objConexao->executeFetch($strSql, $arrayParametros);
		
			$this->codGrupoEstabelecimento 	= $codGrupoEstabelecimento;
			$this->datInicial 				= $arrayGrupoEstabelecimento[0]["DAT_INICIAL"];
			$this->datFinal					= $arrayGrupoEstabelecimento[0]["DAT_FINAL"];
			$this->dscMascaraIp				= $arrayGrupoEstabelecimento[0]["DSC_MASCARA_IP"];
		}
		
		return true;
	}
	
	public function getGrupoEstabelecimentoIpVigente($codGrupoEstabelecimento){
		if(TRUE == $codGrupoEstabelecimento){
			$objConexao = $this->database;
		
			
			$strSql = "SELECT GEI.DAT_INICIAL, GEI.DAT_FINAL, GEI.DSC_MASCARA_IP
  						 FROM GRUPO_ESTABELECIMENTO_IP GEI
 						WHERE GEI.COD_GRUPO_ESTABELECIMENTO = :codGrupoEstabelecimento
					      AND TRUNC(SYSDATE) BETWEEN GEI.DAT_INICIAL AND GEI.DAT_FINAL";
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codGrupoEstabelecimento";
			$arrayParametros[0][1] = $codGrupoEstabelecimento;
		
			$arrayGrupoEstabelecimento = $objConexao->executeFetch($strSql, $arrayParametros);
		
			$this->codGrupoEstabelecimento 	= $codGrupoEstabelecimento;
			$this->datInicial 				= $arrayGrupoEstabelecimento[0]["DAT_INICIAL"];
			$this->datFinal					= $arrayGrupoEstabelecimento[0]["DAT_FINAL"];
			$this->dscMascaraIp				= $arrayGrupoEstabelecimento[0]["DSC_MASCARA_IP"];
		}
		
		return true;
	}
	
	public function insGrupoEstabelecimentoIp() {
		$objConexao = $this->database;
		
		// cria estabelecimentoIp
		if (TRUE == $this->codGrupoEstabelecimento) {
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codGrupoEstabelecimento";
			$arrayParametros[0][1] = $this->codGrupoEstabelecimento;
			$arrayParametros[1][0] = ":datInicial";
			$arrayParametros[1][1] = $this->datInicial;
			$arrayParametros[2][0] = ":datFinal";
			$arrayParametros[2][1] = $this->datFinal;
			$arrayParametros[3][0] = ":dscMascaraIp";
			$arrayParametros[3][1] = $this->dscMascaraIp;
	
			$strSql = "INSERT INTO GRUPO_ESTABELECIMENTO_IP 
					          (COD_GRUPO_ESTABELECIMENTO, DAT_INICIAL, DAT_FINAL, DSC_MASCARA_IP)
					   VALUES (:codGrupoEstabelecimento, :datInicial, :datFinal, :dscMascaraIp)";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao inserir IP de acesso";
				return false;
			}
			
			return true;
		}
	}
	
	public function changeGrupoEstabelecimentoIp() {
		$objConexao = $this->database;
		
		if (TRUE == $this->codGrupoEstabelecimento && TRUE == $this->datInicialAntiga){
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codGrupoEstabelecimento";
			$arrayParametros[0][1] = $this->codGrupoEstabelecimento;
			$arrayParametros[1][0] = ":datInicial";
			$arrayParametros[1][1] = $this->datInicial;
			$arrayParametros[2][0] = ":datFinal";
			$arrayParametros[2][1] = $this->datFinal;
			$arrayParametros[3][0] = ":dscMascaraIp";
			$arrayParametros[3][1] = $this->dscMascaraIp;
			$arrayParametros[4][0] = ":datInicialAntiga";
			$arrayParametros[4][1] = $this->datInicialAntiga;
	
			// altera estabelecimento
			$strSql = "UPDATE GRUPO_ESTABELECIMENTO_IP
					      SET DAT_INICIAL = :datInicial, 
					          DAT_FINAL = :datFinal, 
					          DSC_MASCARA_IP = :dscMascaraIp
					    WHERE COD_GRUPO_ESTABELECIMENTO = :codGrupoEstabelecimento
					      AND DAT_INICIAL = :datInicialAntiga";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar IP de acesso";
				return false;
			}
			
			return true;
			
		}else{
			return false;
		}
	}
	
	
	
	public function delGrupoEstabelecimentoIp($codGrupoEstabelecimento, $datInicial){
		
		if (TRUE == $codGrupoEstabelecimento && TRUE == $datInicial) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codGrupoEstabelecimento";
			$arrayParametros[0][1] = $codGrupoEstabelecimento;
			$arrayParametros[1][0] = ":datInicial";
			$arrayParametros[1][1] = $datInicial;
	
			$strSql = "DELETE
					     FROM GRUPO_ESTABELECIMENTO_IP
					    WHERE COD_GRUPO_ESTABELECIMENTO =:codGrupoEstabelecimento
					      AND DAT_INICIAL = :datInicial";
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar IP de acesso";
				return false;
			}
			
			return true;
		} else {
			return false;
		}
	
	}
	
	public function verificaIp($codGrupoEstabelecimento, $ip){
		
		if (TRUE == $codGrupoEstabelecimento && TRUE == $ip) {
			$objConexao = $this->database;
			
			$this->getGrupoEstabelecimentoIpVigente($codGrupoEstabelecimento);
			
			if (TRUE == $this->dscMascaraIp){
				$libera = false;
				$msk = explode(".", $this->dscMascaraIp);
				$arrayIp = explode(".", $ip);
				$total = count($msk);
				
				
				// primeira parte do IP
				if ($total >= 1){
					// coringa libera tudo
					if ($msk[0] == '*'){
						return true;
					}else{
						if ($msk[0] != $arrayIp[0]){
							return false;
						}
					}
				}
				
				// segunda parte do IP
				if ($total >= 2){
					// coringa libera tudo
					if ($msk[1] == '*'){
						return true;
					}else{
						if ($msk[1] != $arrayIp[1]){
							return false;
						}
					}
				}
				
				// terceira parte do IP
				if ($total >= 3){
					// coringa libera tudo
					if ($msk[2] == '*'){
						return true;
					}else{
						if ($msk[2] != $arrayIp[2]){
							return false;
						}
					}
				}
				
				// quarta parte do IP
				if ($total >= 4){
					// coringa libera tudo
					if ($msk[3] == '*'){
						return true;
					}else{
						if ($msk[3] != $arrayIp[3]){
							return false;
						}
					}
				}
				
				return true;
				
			}else{
				// se nao achou uma mascara é que não existe bloqueio vigente para o grupo
				return true;
			}

		} else {
			return false;
		}
	}
}