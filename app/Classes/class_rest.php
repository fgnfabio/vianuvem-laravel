<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 * 
 * Utilizar no CRUD (Create, Read, Update e Delete) respectivamente POST, GET, PUT e DELETE
 * Retornos de informações podem ser no formato padrão XML ou em JSON 
 */

Class rest {
	
	function __construct() {
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	/**
	 * Retorna a descrição do código do header
	 * 
	 * @param integer $code
	 * @return string|boolean
	 */
	public function descriptionHeaderCode($code){
		switch ($code){
			case '200':
				return '200 OK';
				break;
			case '201':
				return '201 CREATED';
				break;
			case '304':
				return '304 NOT MODIFIED';
				break;
			case '400':
				return '400 BAD REQUEST';
				break;
			case '401':
				return '401 UNAUTHORIZED';
				break;
			case '403':
				return '403 FORBIDDEN';
				break;
			case '404':
				return '404 NOT FOUND';
				break;
			case '405':
				return '405 METHOD NOT ALLOWED';
				break;
			case '409':
				return '409 CONFLICT';
				break;
			case '410':
				return '410 GONE';
				break;
			case '412':
				return '412 PRECONDITION FAILED';
				break;
			case '415':
				return '415 UNSUPORTED MEDIA TYPE';
				break;
			case '500':
				return '500 INTERNAL SERVER ERROR';
				break;
			case '503':
				return '503 SERVICE UNAVAILABLE';
				break;
			default:
				return false;
				break;
		}
	}
	
	/**
	 * Adiciona no header o tipo de conteudo retornado
	 * 
	 * @param string $tipo
	 * @return boolean
	 */
	public function includeHeader($tipo = 'XML'){
		if (!$tipo){
			return false;
		}
		
		if (strtoupper($tipo) == "XML"){
			header('Content-type: application/xml; charset=UTF8');
			
		}elseif(strtoupper($tipo) == "JSON"){
			header('Content-type: application/json; charset=UTF8');
			
		}elseif(strtoupper($tipo) == "PNG"){
			header('Content-type: image/png');
					
		}elseif(strtoupper($tipo) == "GIF"){
			header('Content-type: image/gif');
					
		}elseif(strtoupper($tipo) == "JPG"){
			header('Content-type: image/jpg');
					
		}elseif(strtoupper($tipo) == "JPEG"){
			header('Content-type: image/jpeg');
					
		}
		
		return true;
	}
	
	/**
	 * Adiciona no header um status
	 * 
	 * @param integer $code
	 * @return boolean
	 */
	public function includeHeaderCode($code){
		switch ($code){
			case '200':
				header('HTTP/1.1 200 OK');
				return true;
				break;
			case '201':
				header('HTTP/1.1 201 CREATED');
				return true;
				break;
			case '304':
				header('HTTP/1.1 304 NOT MODIFIED');
				return true;
				break;
			case '400':
				header('HTTP/1.1 400 BAD REQUEST');
				return true;
				break;
			case '401':
				header('HTTP/1.1 401 UNAUTHORIZED');
				return true;
				break;
			case '403':
				header('HTTP/1.1 403 FORBIDDEN');
				return true;
				break;
			case '404':
				header('HTTP/1.1 404 NOT FOUND');
				return true;
				break;
			case '405':
				header('HTTP/1.1 405 METHOD NOT ALLOWED');
				return true;
				break;
			case '409':
				header('HTTP/1.1 409 CONFLICT');
				return true;
				break;
			case '410':
				header('HTTP/1.1 410 GONE');
				return true;
				break;
			case '412':
				header('HTTP/1.1 412 PRECONDITION FAILED');
				return true;
				break;
			case '415':
				header('HTTP/1.1 415 UNSUPORTED MEDIA TYPE');
				return true;
				break;
			case '500':
				header('HTTP/1.1 500 INTERNAL SERVER ERROR');
				return true;
				break;
			case '503':
				header('HTTP/1.1 503 SERVICE UNAVAILABLE');
				return true;
				break;
			default:
				return false;
				break;
		}
	}
	
	/**
	 * Inclui headers e retorna xml
	 * 
	 * @param string $arquivo
	 * @param string $bloco
	 * @param integer $code
	 * @param string $mensagem
	 * @return string xml
	 */
	
	public function retornaXMLError($arquivo, $bloco=null, $code, $mensagem, $antigo=false){
		
		$arquivo = strtoupper($arquivo);
		$bloco  = strtoupper($bloco);
		
		// gera novo XML
		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$arquivo.' generator="Via Nuvem" version="2.0"></'.$arquivo.'>');
		
		// cria um novo bloco contendo os dados de retorno
		if ($bloco){
			$funcChild = $xml->addChild($bloco);
			$response = $funcChild->addChild('RESPONSE');
			$response->addChild('MESSAGE', $mensagem);
			$funcChild->addChild('STATUS', $this->descriptionHeaderCode($code));
			$funcChild->addChild('CODE', $code);
			
		// cria o retorno
		}else{
			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', $mensagem);
			$xml->addChild('STATUS', $this->descriptionHeaderCode($code));
			$xml->addChild('CODE', $code);
		}
		
		//adiciona o header na resposta
		$this->includeHeader('XML');
		$this->includeHeaderCode($code);
		
		// se for o metodo antigo, retorna objeto pro zend
		if ($antigo){
			return $xml;
		}else{
			// retorna o resultado como XML
			return $xml->asXML();
		}
	}
	
	/**
	 * Inclui headers e retorna json
	 * 
	 * @param string $arquivo
	 * @param string $bloco
	 * @param integer $code
	 * @param string $mensagem
	 * @return string json
	 */
	public function retornaJsonError($arquivo, $bloco=null, $code, $mensagem){
	
		$arquivo = strtoupper($arquivo);
		$bloco  = strtoupper($bloco);
	
		// gera array que sera convertido em json
		if ($bloco){
			$arrayInicial = 
				array($arquivo => 
					array($bloco =>
						array("RESPONSE" => 
							array("MESSAGE" => $mensagem), 
								  "STATUS" => $this->descriptionHeaderCode($code),
								  "CODE" => $code
						)
					)
				);
		}else{
			$arrayInicial = 
				array($arquivo => 
					array("RESPONSE" => 
						array("MESSAGE" => $mensagem), 
							  "STATUS" => $this->descriptionHeaderCode($code),
							  "CODE" => $code
					)
				);
		}
		
		//adiciona o header na resposta
		$this->includeHeader('JSON');
		$this->includeHeaderCode($code);
	
		// retorna o resultado como JSON
		return json_encode($arrayInicial);
	}
	
	public function retornaPNGError($arquivo, $bloco=null, $code = '200', $mensagem, $antigo=false){
		
		//adiciona o header na resposta
		$this->includeHeader('PNG');
		$this->includeHeaderCode($code);
		header('Cache-Control: max-age=0');
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
				
		$im = @imagecreate(700, 100)	or die("Cannot Initialize new GD image stream");
		$background_color = imagecolorallocate($im, 0, 0, 0);
		$text_color = imagecolorallocate($im, 255, 0, 0);
		
		imagestring($im, 6, 30, 10,  $arquivo, $text_color);
		imagestring($im, 4, 30, 30,  $mensagem, $text_color);
		imagepng($im);
		imagedestroy($im);
		die();
		
	}
	
	/**
	 * Retorna o método utilizado no header de requisição
	 * 
	 * @return string
	 */
	public function getMethod(){
		// em caso de put e delete alguns browsers/aplicações não suportam enviar no cabeçalho, 
		// então enviam em POST sobreencrevendo o cabeçalho
		
		if (isset($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'])){
			return strtoupper($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE']);
		}else{
			return strtoupper($_SERVER['REQUEST_METHOD']);
		}		
	}
	
	/**
	 * testa informações recebidas
	 * 
	 * @param string $requestMethod (GET|PUT|POST|DELETE)
	 * @param string $nomeBloco
	 * @param array $arrayAtributos
	 * @param string $tipoRetorno
	 * @return boolean
	 */
	public function verificaDados($requestMethod, $nomeBloco, $arrayAtributos, $tipoRetorno = 'XML'){
		
		// verifica se veio no metodo especificado
		if ($this->getMethod() != strtoupper($requestMethod)){
			// Retorna erro
			if (strtoupper($tipoRetorno) == "XML" ){
				echo $this->retornaXMLError($nomeBloco, null, '405', 'Método de requisição não permitido');
				
			}elseif (strtoupper($tipoRetorno) == "JSON" ){
				echo $this->retornaJsonError($nomeBloco, null, '405', 'Método de requisição não permitido');
				
			}elseif (strtoupper($tipoRetorno) == "PNG" ){
				$this->retornaPNGError($nomeBloco, null, '405', 'Método de requisição não permitido');
			}
			return false;
		}
		
		// verifica se vieram os atributos obrigatórios
		$msg_attr = "";
		foreach ($arrayAtributos as $atributo){
			
			if (@!$_REQUEST[$atributo]){
				$msg_attr .= $atributo.", ";
			}
		}
		
		if ($msg_attr){
			$msg_attr = substr($msg_attr, 0, strlen($msg_attr) -2);
			if (strtoupper($tipoRetorno) == "XML" ){
				echo $this->retornaXMLError($nomeBloco, null, '400', 'Requisição inválida: '.$msg_attr.' não informado(s)');
				
			}elseif (strtoupper($tipoRetorno) == "JSON" ){
				echo $this->retornaJsonError($nomeBloco, null, '400', 'Requisição inválida: '.$msg_attr.' não informado(s)');
				
			}elseif (strtoupper($tipoRetorno) == "PNG" ){
				$this->retornaPNGError($nomeBloco, null, '405', 'Requisição inválida: '.$msg_attr.' não informado(s)');
				
			}
			return false;
		}
		
		return true;
	}
	
}