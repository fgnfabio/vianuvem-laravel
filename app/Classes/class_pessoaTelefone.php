<?php

namespace App\Classes;

/**
 * @author: Rogerio
 * num_sequencia
 * 1 - telefone residencial
 * 2 - telefone comercial
 * 3 - telefone celular
 * 4 - telefone principal (pj)
 */

Class pessoaTelefone
{
	public    $database;
	public    $error;
	private   $codPessoa;
	protected $codTipoTelefone;
	protected $numDDD;
	protected $numTelefone;
	public    $numSequencia = array("9" => "1", "10" => "2",  "11" => "3", "12" => "4" );
	
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getCodPessoa() {
		return $this->codPessoa;
	}
	
	public function setCodPessoa($codPessoa) {
		$this->codPessoa = $codPessoa;
		return $this;
	}
	
	public function getCodTipoTelefone() {
		return $this->codTipoTelefone;
	}
	
	public function setCodTipoTelefone($codTipoTelefone) {
		$this->codTipoTelefone = $codTipoTelefone;
		return $this;
	}
	
	public function getNumDDD() {
		return $this->numDDD;
	}
	
	public function setNumDDD($numDDD) {
		$this->numDDD = $numDDD;
		return $this;
	}
	
	
	public function getNumTelefone() {
		return $this->numTelefone;
	}
	
	public function setNumTelefone($numTelefone) {
		$this->numTelefone = $numTelefone;
		return $this;
	}
	
	public function getPessoaTelefone($codPessoa, $codTipoTelefone){
		if($codPessoa && $codTipoTelefone){
			$objConexao = $this->database;
		
			$strSql = "SELECT COD_DDD, NUM_TELEFONE, COD_PESSOA, COD_TIPO_TELEFONE, NUM_SEQUENCIA
                         FROM PESSOA_TELEFONE
                        WHERE COD_PESSOA = :codPessoa
					      AND COD_TIPO_TELEFONE = :codTipoTelefone";
		
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $codPessoa;
			$arrayParametros[1][0] = ":codTipoTelefone";
			$arrayParametros[1][1] = $codTipoTelefone;
		
			$arrayTelefone = $objConexao->executeFetch($strSql, $arrayParametros);
		
			$this->codPessoa = $arrayTelefone[0]["COD_PESSOA"];
			$this->codTipoTelefone = $arrayTelefone[0]["COD_TIPO_TELEFONE"];
			$this->numDDD = $arrayTelefone[0]["COD_DDD"];
			$this->numTelefone = $arrayTelefone[0]["NUM_TELEFONE"];
			$this->numSequencia = $arrayTelefone[0]["NUM_SEQUENCIA"];
			
		}
		return true;
		
	}
	
	
	function insPessoaTelefone() {
	
		if (TRUE == $this->codPessoa && TRUE == $this->codTipoTelefone && TRUE == $this->numTelefone ) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $this->codPessoa;
			$arrayParametros[1][0] = ":codTipoTelefone";
			$arrayParametros[1][1] = $this->codTipoTelefone;
			$arrayParametros[2][0] = ":numDDD";
			$arrayParametros[2][1] = $this->numDDD;
			$arrayParametros[3][0] = ":numTelefone";
			$arrayParametros[3][1] = $this->numTelefone;
			$arrayParametros[4][0] = ":numSequencia";
			$arrayParametros[4][1] = $this->numSequencia[$this->codTipoTelefone];
			
	
			$strSql = "INSERT INTO PESSOA_TELEFONE
					          (COD_PESSOA, COD_TIPO_TELEFONE, COD_DDD, NUM_TELEFONE, NUM_SEQUENCIA)
                       VALUES (:codPessoa, :codTipoTelefone, :numDDD, :numTelefone, :numSequencia)";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao inserir pessoa telefone";
				return false;
			}
	
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para inserir pessoa telefone";
			return false;
		}
	}
	
	function changePessoaTelefone() {
		
		if (TRUE == $this->getCodPessoa()) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $this->codPessoa;
			$arrayParametros[1][0] = ":codTipoEndereco";
			$arrayParametros[1][1] = $this->codTipoTelefone;
			$arrayParametros[2][0] = ":numDDD";
			$arrayParametros[2][1] = $this->numDDD;
			$arrayParametros[3][0] = ":numTelefone";
			$arrayParametros[3][1] = $this->numTelefone;
			$arrayParametros[4][0] = ":numSequencia";
			$arrayParametros[4][1] = $this->numSequencia[$this->codTipoTelefone];
			
	
			$strSql = "UPDATE PESSOA_TELEFONE 
					      SET COD_TIPO_TELEFONE = :codTipoTelefone, 
					 		  NUM_DDD = :numDDD, 
							  NUM_TELEFONE = :numTelefone
					    WHERE COD_PESSOA = :codPessoa 
					      AND COD_TIPO_TELEFONE = :codTipoTelefone
					      AND NUM_SEQUENCIA = :numSequencia";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar pessoa telefone";
				return false;
			}
	
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para alterar pessoa telefone";
			return false;
		}
	}
	
	function delPessoaTelefone($codPessoa, $codTipoTelefone){
		
		if (TRUE == $codPessoa && TRUE == $codTipoTelefone) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $codPessoa;
			$arrayParametros[1][0] = ":codTipoTelefone";
			$arrayParametros[1][1] = $codTipoTelefone;
			$arrayParametros[2][0] = ":numSequencia";
			$arrayParametros[2][1] = $this->numSequencia[$codTipoTelefone];
	
			$strSql = "DELETE 
					     FROM PESSOA_TELEFONE 
					    WHERE COD_PESSOA =:codPessoa
					      AND COD_TIPO_TELEFONE = :codTipoTelefone
					      AND NUM_SEQUENCIA = :numSequencia";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar pessoa telefone";
				return false;
			}
	
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para deletar pessoa telefone";
			return false;
		}
	
	}
}