<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class util {
	
	public  	$database;
	public  	$error;

	/**
	 * Inicializa a classe
	 * @param object $database
	 * @return boolean
	 */
	function __construct() {
		//$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	/**
	 * Retira os caracteres informados
	 * @param string $frase
	 * @param array $arrayCaracteres
	 * @return string
	 */
	public function retiraCaracteres($frase, $arrayCaracteres){
		return str_replace($arrayCaracteres, "",  $frase);
	}
	
	/**
	 * Retira os caracteres informados
	 * @param string $frase
	 * @param array $arrayCaracteres
	 * @return string
	 */
	public function dscTipoDispositivo($codTipoDispositivo){
		$dscTipoDispositivo = array("S" => "Scanner", 
				                    "W" => "WebCam", 
				                    "D" => "FileDisk",
				                    "I" => "Impressora Virtual",
				                    "M" => "Mobile");
		
		return $dscTipoDispositivo[$codTipoDispositivo]; 

	}
	
}