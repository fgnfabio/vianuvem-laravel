<?php

namespace App\Classes;

//set_time_limit(0);
//ini_set('post_max_size', '64M');
//ini_set('upload_max_filesize', '64M');
//ini_set('memory_limit', '96M');
/**
 * @author: Marcos Menezes
 */

Class webservice {

	public  	$database;
	public  	$error;
	public		$tipoRetorno;
	public      $nomeBloco;
	public		$retornoXmlJson;
	
	// variaveis em ordem alfabética
	private     $arquivo;
	private     $arrayCampos;
	private 	$codEmpresa;
	private 	$codDocumento;
	private		$codEstabelecimento;
	private 	$codIdentificacao;
	private     $codMacAdress;
	private		$codTipoDispositivo;
	private		$codTipoDocumento;
	private 	$codTipoIdentificador;
	private		$codTipoProcesso;
	private		$codToken;
	private		$codUsuario;
	private		$datInclusao;
	private		$datInicio;
	private 	$datFim;
	private		$dscLogin;
	private     $dscSenha;
	private	    $nomEmpresa;
	private		$numCnpj;
	private     $serviceToken;
	private     $codFilial;
	private     $codGrupoEstabelecimento;
	private     $codTipoLista;
	
	private     $ext;
	private     $mimeType;
	private     $dscClasseValidacao;
	private     $conteudoCampo;
	public     $processoArray;
		
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
    //**********************************************************************************************//
    
	/**
	 * Autentica a empresa e devolve um token único
	 * @return boolean
	 */
	public function autenticarEmpresa(){
		
		if (!$this->codEmpresa || !$this->codToken){//  || !$this->dscLogin
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
		
		require_once '../classes/class_empresa.php';
		$objEmpresa = new empresa($this->database);
		
		// retira zeros da esquerda do codEmpresa
		$this->codEmpresa = intval($this->codEmpresa);		
		
		// se informou o loogin descobre quem é o usuario
		if ($this->dscLogin){
			
			require_once '../classes/class_usuario.php';
			$objUsuario = new usuario($this->database);
			
			// pega o codigo do usuario pelo nome de login
			$objUsuario->__set('dscIdentificacaoUsuario', $this->dscLogin);
			$this->codUsuario = $objUsuario->getCodUsuarioByLogin();
			
			// se nao achou usuario tenta pelo padrao de usuario do autologin
			if (!$this->codUsuario){
				$objUsuario->__set('dscIdentificacaoUsuario', $this->dscLogin."_".str_pad($this->codEmpresa, 6, "0", STR_PAD_LEFT));
				$this->codUsuario = $objUsuario->getCodUsuarioByLogin();
				
				// se não achar pelo padrão do autologin encerra
				if (!$this->codUsuario){
					$this->error['code'] 	= "";
					$this->error['message'] = "Usuário não encontrado";
					return false;
				}
			}
			
			// verifica se o usuario informado é da mesma empresa informada
			if ($this->codEmpresa != $objEmpresa->getCodEmpresaByUsuario($this->codUsuario)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Usuário inválido";
				return false;
			}
		}else{
			$this->codUsuario = null;
		}
		
		// valida token de primeiro contato e gera token de acesso
		if (!$objEmpresa->validaTokenBoasVindas($this->codEmpresa, $this->codToken, $this->codUsuario)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token inválido";
			return false;
		}
		
		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
			$usuario = $xml->addChild("EMPRESA");
			$usuario->addChild("COD_EMPRESA", $objEmpresa->__get('codEmpresa'));
			$usuario->addChild("NOM_EMPRESA", $objEmpresa->__get('nomEmpresa'));
			$usuario->addChild("COD_TOKEN_ACESSO", $objEmpresa->__get('codTokenEmpresa'));
			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
	
			$this->retornoXmlJson = $xml->asXML();
		}else{
			$arrayInicial =	array(
				$this->nomeBloco => array(
						"EMPRESA" => array(
								"COD_EMPRESA" => $objEmpresa->__get('codEmpresa'), 
							   	"NOM_EMPRESA" => $objEmpresa->__get('nomEmpresa'),
							   	"COD_TOKEN_ACESSO" => $objEmpresa->__get('codTokenEmpresa')
						),
						"RESPONSE" => array(
								"MESSAGE" => 'OK'
						),
						"STATUS" => "200 OK",
						"CODE" => "200"
				)
			);
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
		return true;	
	}
	
	//**********************************************************************************************//
	
	/**
	 * Autentica usuários do webservice para sistemas internos (impressora virtual, mobil, etc)
	 * @return boolean
	 */
	public function autenticarUsuarioWebService(){
	
		if (!$this->dscLogin || !$this->dscSenha || !$this->serviceToken ){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
	
		$tokenGerado = $this->geraServiceToken();
		if (!$this->validaServiceToken($tokenGerado, $this->serviceToken)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token de serviço inválido";
			return false;
		}
		
		require_once '../classes/class_seguranca.php';
		$objSeguranca = new seguranca($this->database);

		if(!$objSeguranca->autentica($this->dscLogin, $this->dscSenha)){
			$this->error['code'] 	= "";
			$this->error['message'] = $objSeguranca->error['message'];
			return false;
		}
		
		// dados retornados da autenticacao
		$dados = $objSeguranca->__get('dadosWebservice');

		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
			
			$usuario = $xml->addChild("USUARIO");
			$usuario->addChild("COD_EMPRESA", $dados['codEmpresa']);
			$usuario->addChild("NOM_EMPRESA", $dados['nomEmpresa']);
			$usuario->addChild("COD_USUARIO", $dados['codUsuario']);
			$usuario->addChild("NOM_USUARIO", $dados['nomPessoa']);
			$usuario->addChild("DSC_LOGIN", $dados['dscIdentificacaoUsuario']);
			$usuario->addChild("COD_TIPO_USUARIO", $dados['codTipoUsuario']);
			$usuario->addChild("COD_ESTABELECIMENTO", $dados['codEstabelecimento']);
			$usuario->addChild("NOM_ESTABELECIMENTO", $dados['nomEstabelecimento']);
			$usuario->addChild("DSC_LABEL_FILIAL", $dados['dscLabelFilial']);
			$usuario->addChild("DSC_LABEL_GRUPO_ESTABELECIMENTO", $dados['dscLabelGrupoEstabel']);
			$usuario->addChild("DSC_LABEL_ESTABELECIMENTO", $dados['dscLabelEstabelecimento']);
								
			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
			
			$this->retornoXmlJson = $xml->asXML();
		}else{
						
			$arrayInicial =	array(
					$this->nomeBloco => array(
							
							"USUARIO" => array(
									"COD_EMPRESA" => $dados['codEmpresa'],
									"NOM_EMPRESA"  =>  $dados['nomEmpresa'],
									"COD_USUARIO" => $dados['codUsuario'],
									"NOM_USUARIO" => $dados['nomPessoa'],
									"DSC_LOGIN" => $dados['dscIdentificacaoUsuario'],
									"COD_TIPO_USUARIO" => $dados['codTipoUsuario'],
									"COD_ESTABELECIMENTO"  => $dados['codEstabelecimento'],
									"NOM_ESTABELECIMENTO"  => $dados['nomEstabelecimento'],
									"DSC_LABEL_FILIAL" => $dados['dscLabelFilial'],
									"DSC_LABEL_GRUPO_ESTABELECIMENTO" => $dados['dscLabelGrupoEstabel'],
									"DSC_LABEL_ESTABELECIMENTO" => $dados['dscLabelEstabelecimento']
							),
							"RESPONSE" => array(
									"MESSAGE" => 'OK'
							),
							"STATUS" => "200 OK",
							"CODE" => "200"
					)
			);
			$this->retornoXmlJson = json_encode($arrayInicial);
		}

		return true;
	}
	
	//**********************************************************************************************//
	/**
	 * Lista as filiais para a empresa e usuario solicitado por sistemas internos (impressora virtual, mobil, etc)
	 * @return boolean
	 */
	 public function listaFiliaisUsuarioWebService(){
	 	
	 	if (!$this->codEmpresa || !$this->serviceToken ){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = "Variáveis necessárias não informadas";
	 		return false;
	 	}
	 	
	 	$tokenGerado = $this->geraServiceToken();
	 	if (!$this->validaServiceToken($tokenGerado, $this->serviceToken)){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = "Token de serviço inválido";
	 		return false;
	 	}
	 	
	 	require_once '../classes/class_seguranca.php';
	 	$objSeguranca = new seguranca($this->database);
	 	
	 	if(!$objSeguranca->autentica($this->dscLogin, $this->dscSenha)){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = $objSeguranca->error['message'];
	 		return false;
	 	}
	 	
	 	$dados = $objSeguranca->__get('dadosWebservice');
	 	
	 	if($dados['codUsuario'] != $this->codUsuario){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = "Código do Usuário recebido nao confere com o login informado";
	 		return false;
	 		 
	 	}
	 	
	 	//busca as filiais que o usuario tem acesso
		require_once '../classes/class_filial.php';
		$objFilial = new filial($this->database);
		$objFilial->__set('codUsuario',  $this->codUsuario );
		$objFilial->__set('codEmpresa', $this->codEmpresa );
		$arrayFiliais = $objFilial->listaFilialUsuario();

		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
			
			$filiais = $xml->addChild("FILIAIS");
			 foreach ($arrayFiliais as $key => $value){
				$filial = $filiais->addChild("FILIAL");
				$_filial = trim($value['COD_FILIAL']);
				$_nomFilial = trim($value['NOM_FILIAL']);
				$filial->addChild("COD_EMPRESA", $this->codEmpresa);
				$filial->addChild("COD_FILIAL", $_filial);
				$filial->addChild("NOM_FILIAL", $_nomFilial);
			}
						
			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
			
			$this->retornoXmlJson = $xml->asXML();
		}else{
			//gera a lista de filiais
			$filiais = "";
			foreach ($arrayFiliais as $indice => $filial){
				$_filial = trim($filial['COD_FILIAL']);
				$_nomFilial = trim($filial['NOM_FILIAL']);
				$filiais[] = array("FILIAL" =>	array ("COD_EMPRESA" => $this->codEmpresa,
													   "COD_FILIAL" => $_filial,
					                                   "NOM_FILIAL" => $_nomFilial)
				);
			}
	
			$arrayInicial =	array(
					$this->nomeBloco => array(
							
							"FILIALIS" => $filiais
							),
								
							"RESPONSE" => array(
									"MESSAGE" => 'OK'
							),
							"STATUS" => "200 OK",
							"CODE" => "200"
			);
			
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
	 	return true;
	 }
	 
	 //Carrega todas as informações para o funcionamento do App ViaNuvem
	 public function Mobile(){
	 	if (!$this->dscLogin || !$this->dscSenha || !$this->serviceToken ){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = "Variáveis necessárias não informadas";
	 		return false;
	 	}
	 	
	 	$tokenGerado = $this->geraServiceToken();
	 	if (!$this->validaServiceToken($tokenGerado, $this->serviceToken)){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = "Token de serviço inválido";
	 		return false;
	 	}
	 	
	 	require_once '../classes/class_seguranca.php';
	 	$objSeguranca = new seguranca($this->database);
	 	
	 	if(!$objSeguranca->autentica($this->dscLogin, $this->dscSenha)){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = $objSeguranca->error['message'];
	 		return false;
	 	}
	 	
	 	// dados retornados da autenticacao
	 	$dados = $objSeguranca->__get('dadosWebservice');
	 	
	 	if($dados['codTipoUsuario'] == 'A'){
	 		$this->error['code'] = "";
	 		$this->error['message'] = "Tipo de usuário não permitido";
	 		return false;
	 	}
	 	
	 	//Lista as filiais que o usuario tem acesso
	 	require_once '../classes/class_filial.php';
	 	$objFilial = new filial($this->database);
	 	$objFilial->__set('codUsuario', $dados['codUsuario']);
	 	$objFilial->__set('codEmpresa', $dados['codEmpresa']);
	 	$arrayFiliais = $objFilial->listaFilialUsuario();
	 	
	 	//Lista processos que o usuario tem permissao
	 	require_once '../classes/class_tipoProcesso.php';
	 	$objTipoProcesso = new tipoProcesso($this->database);
	 	$objTipoProcesso->__set('codUsuario',  $dados['codUsuario']);
	 	$objTipoProcesso->__set('codEmpresa', $dados['codEmpresa']);
	 	//$objTipoProcesso->__set('codEstabelecimento', $this->codEstabelecimento );
	 	$arrayTipoProcesso = $objTipoProcesso->listaArvoreMobileProcessos();
	 	
	 	//Lista tipos de documento que o usuario tem permissao
	 	require_once '../classes/class_tipoDocumento.php';
	 	$objDocumento = new tipoDocumento($this->database);
	 	//$objDocumento->__set('codTipoProcesso',  $this->codTipoProcesso );
	 	$objDocumento->__set('codEmpresa', $dados['codEmpresa']);
	 	$objDocumento->__set('codUsuario', $dados['codUsuario']);
	 	$arrayDocumentos = $objDocumento->buscaTiposDocumentoTipoProcessoMobile();
	 	
	 	//Lista todos os indexadores ordenados por tipo de processo
	 	require_once '../classes/class_tipoProcesso.php';
	 	$objTipoProcesso = new tipoProcesso($this->database);
	 	//$objTipoProcesso->__set('codTipoProcesso', $this->codTipoProcesso );
	 	$objTipoProcesso->__set('codUsuario', $dados['codUsuario']);
	 	$objTipoProcesso->__set('codEmpresa', $dados['codEmpresa']);
	 	$arrayCampos = $objTipoProcesso->buscaCamposDocumentoTipoProcessoTipoDocumentoMobile();
	 		
	 	if ($this->tipoRetorno == "XML"){
	 		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="2.0"></'.$this->nomeBloco.'>');
	 		
	 		//TUPLA USUARIO
	 		$usuario = $xml->addChild("USUARIO");
	 		$usuario->addChild("COD_EMPRESA", $dados['codEmpresa']);
	 		$usuario->addChild("NOM_EMPRESA", $dados['nomEmpresa']);
	 		$usuario->addChild("COD_USUARIO", $dados['codUsuario']);
	 		$usuario->addChild("NOM_USUARIO", $dados['nomPessoa']);
	 		$usuario->addChild("DSC_LOGIN", $dados['dscIdentificacaoUsuario']);
	 		$usuario->addChild("COD_TIPO_USUARIO", $dados['codTipoUsuario']);
	 		$usuario->addChild("DSC_LABEL_FILIAL", $dados['dscLabelFilial']);
	 		$usuario->addChild("DSC_LABEL_GRUPO_ESTABELECIMENTO", $dados['dscLabelGrupoEstabel']);
	 		$usuario->addChild("DSC_LABEL_ESTABELECIMENTO", $dados['dscLabelEstabelecimento']);
	 		
	 		//TUPLA FILIAIS
	 		$filiais = $xml->addChild("FILIAIS");
	 		foreach ($arrayFiliais as $key => $value){
	 			$filial = $filiais->addChild("FILIAL");
	 			$_filial = trim($value['COD_FILIAL']);
	 			$_nomFilial = trim($value['NOM_FILIAL']);
	 			$filial->addChild("COD_FILIAL", $_filial);
	 			$filial->addChild("NOM_FILIAL", $_nomFilial);
	 			
		 			//Lista os grupos de estabelecimento que o usuário tem acesso
		 			require_once '../classes/class_grupoEstabelecimento.php';
		 			$objGrupoEstabel = new grupoEstabelecimento($this->database);
		 			$objGrupoEstabel->__set('codUsuario',  $dados['codUsuario']);
		 			$objGrupoEstabel->__set('codFilial', $value['COD_FILIAL']);
		 			$arrayGrupos = $objGrupoEstabel->listaGrupoEstabelecimentoUsuario();
		 			
		 			//Lista os estabelecimentos que o usuario tem acesso
		 			require_once '../classes/class_estabelecimento.php';
		 			$objEstabel = new estabelecimento($this->database);
		 			$objEstabel->__set('codUsuario',  $dados['codUsuario']);
		 			$objEstabel->__set('codFilial', $value['COD_FILIAL']);
		 			$arrayEstabel = $objEstabel->listaEstabelecimentosUsuarioMobile();
	 		}
	 		
	 		//TUPLA GRUPO DE ESTABELECIMENTOS
	 		$grupoEstabelecimentos = $xml->addChild("GRUPOS_ESTABELECIMENTOS");
	 		foreach ($arrayGrupos as $key => $value){
	 			$grupoEstabelecimento = $grupoEstabelecimentos->addChild("GRUPO_ESTABELECIMENTO");
	 			$_grupo = trim($value['COD_GRUPO_ESTABELECIMENTO']);
	 			$_nomGrupo = trim($value['NOM_GRUPO_ESTABELECIMENTO']);
	 			//$grupoEstabelecimento->addChild("COD_FILIAL", $this->codFilial);
	 			$grupoEstabelecimento->addChild("COD_GRUPO_ESTABELECIMENTO", $_grupo);
	 			$grupoEstabelecimento->addChild("NOM_GRUPO_ESTABELECIMENTO", $_nomGrupo);
	 			
	 		}
	 		
	 		//TUPLA ESTABELECIMENTOS
	 		$grupoEstabelecimentos = $xml->addChild("ESTABELECIMENTOS");
	 		foreach ($arrayEstabel as $key => $value){
	 			$grupoEstabelecimento = $grupoEstabelecimentos->addChild("ESTABELECIMENTO");
	 			$_estabelecimento = trim($value['COD_ESTABELECIMENTO']);
	 			$_nomEstabelecimento = trim($value['NOM_ESTABELECIMENTO']);
	 			$_codGrupoEstabelecimento = trim($value['COD_GRUPO_ESTABELECIMENTO']);
	 			$grupoEstabelecimento->addChild("COD_GRUPO_ESTABELECIMENTO", $_codGrupoEstabelecimento);
	 			$grupoEstabelecimento->addChild("COD_ESTABELECIMENTO", $_estabelecimento);
	 			$grupoEstabelecimento->addChild("NOM_ESTABELECIMENTO", $_nomEstabelecimento);
	 		}
	 		
	 		//TUPLA TIPOS DE PROCESSO
	 		$tiposProcesso = $xml->addChild("TIPOS_PROCESSOS");
	 		foreach ($arrayTipoProcesso as $key => $value){
	 			 
	 			$tipoProcesso = $tiposProcesso->addChild("TIPO_PROCESSO");
	 			if($value['IND_FOLHA'] == "N"){
	 				$_nivel = trim($value['COD_NIVEL']);
	 				$_grupo = trim($value['COD_TIPO_PROCESSO']);
	 				$_processo = trim($value['COD_TIPO_PROCESSO']);
	 				$_nomProcesso = trim($value['DSC_TIPO_PROCESSO']);
	 				$_codGrupoPai = trim($value['COD_TIPO_PROCESSO_PAI']);
	 				$_grupoAgrupador = 'G';
	 			}else{
	 				$_nivel = trim($value['COD_NIVEL']);
	 				$_grupo = trim($value['COD_GRUPO']);
	 				$_processo = trim($value['COD_TIPO_PROCESSO']);
	 				$_nomProcesso = trim($value['DSC_TIPO_PROCESSO']);
	 				$_codGrupoPai = trim($value['COD_TIPO_PROCESSO_PAI']);
	 				$_grupoAgrupador = 'F';
	 			}
	 			//$tipoProcesso->addChild("COD_EMPRESA", $this->codEmpresa);
	 			//$tipoProcesso->addChild("COD_ESTABELECIMENTO", $this->codEstabelecimento);
	 			$tipoProcesso->addChild("COD_TIPO_PROCESSO", $_processo);
	 			$tipoProcesso->addChild("NIVEL", $_nivel);
	 			$tipoProcesso->addChild("AGRUPAMENTO_TIPO", $_grupoAgrupador);
	 			$tipoProcesso->addChild("DSC_TIPO_PROCESSO", $_nomProcesso);
	 			//$tipoProcesso->addChild("COD_GRUPO", $_grupo);
	 			$tipoProcesso->addChild("COD_GRUPO_PAI", $_codGrupoPai);
	 			 
	 		}
	 		
	 		//TUPLA TIPO DOCUMENTOS DE TODOS OS PROCESSOS
	 		$documentos = $xml->addChild("TIPOS_DOCUMENTOS");
	 		foreach ($arrayDocumentos as $key => $value){
	 			$documento = $documentos->addChild("DOCUMENTO");
	 			$_codTipoProcesso = trim($value['COD_TIPO_PROCESSO']);
	 			$_dscTipoDocumento = trim($value['DSC_TIPO_DOCUMENTO']);
	 			$documento->addChild("COD_TIPO_PROCESSO", $_codTipoProcesso);
	 			$documento->addChild("COD_TIPO_DOCUMENTO", $value['COD_TIPO_DOCUMENTO']);
	 			$documento->addChild("DSC_TIPO_DOCUMENTO", $_dscTipoDocumento);
	 			$documento->addChild("LISTA_DISPOSITIVOS", $value['DSC_LISTA_DISPOSITIVOS']);
	 		}
	 		
	 		//TUPLA INDEXADORES
	 		$campoBloco = $xml->addChild("CAMPOS");
	 		foreach ($arrayCampos as $indiceCampo => $arrCampo){
	 			$campo = $campoBloco->addChild("CAMPO");
	 			$campo->addChild("COD_TIPO_PROCESSO", $arrCampo['COD_TIPO_PROCESSO']);
	 			$campo->addChild("NOM_CAMPO", $arrCampo['NOM_CAMPO']);
	 			$campo->addChild("DSC_LABEL_CAMPO", $arrCampo['DSC_LABEL_CAMPO']);
	 			$campo->addChild("COD_TIPO_CAMPO", $arrCampo['COD_TIPO_CAMPO']);
	 			$campo->addChild("QTD_CARACTERES", $arrCampo['QTD_CARACTERES']);
	 			$campo->addChild("NUM_ORDEM", $arrCampo['NUM_ORDEM']);
	 			$campo->addChild("IND_OBRIGATORIO", $arrCampo['IND_OBRIGATORIO']);
	 			$campo->addChild("COD_TIPO_IDENTIFICADOR", $arrCampo['COD_TIPO_IDENTIFICADOR']);
	 			$campo->addChild("DSC_MASCARA", $arrCampo['DSC_MASCARA']);
	 			$campo->addChild("DSC_TIPO_IDENTIFICADOR", $arrCampo['DSC_TIPO_IDENTIFICADOR']);
	 			$campo->addChild("DSC_CLASSE_VALIDACAO", $arrCampo['DSC_CLASSE_VALIDACAO']);
	 			$campo->addChild("COD_TIPO_LISTA", $arrCampo['COD_TIPO_LISTA']);
	 			$lista = $campo->addChild("LISTA");
	 		
	 			$arrayLista = "";
	 			if ($arrCampo['COD_TIPO_CAMPO'] == "L"){
	 				$objTipoProcesso->__set('codTipoLista', $arrCampo['COD_TIPO_LISTA'] );
	 				$arrayLista = $objTipoProcesso->buscaValoresCampoTipoListaLista();
	 			}
	 			 
	 			if(is_array($arrayLista) ){
	 				foreach ($arrayLista as $indiceLista => $arrLista){
	 					$lista->addChild("COD_LISTA", $arrLista['COD_LISTA'] );
	 					$lista->addChild("DSC_LISTA", $arrLista['DSC_LISTA'] );
	 					$lista->addChild("COD_LISTA_CLIENTE", $arrLista['COD_LISTA_CLIENTE'] );
	 				}
	 			}
	 		}
	 		
	 		$response = $xml->addChild('RESPONSE');
	 		$response->addChild('MESSAGE', 'OK');
	 		$xml->addChild('STATUS', '200 OK');
	 		$xml->addChild('CODE', '200');
	 			
	 		$this->retornoXmlJson = $xml->asXML();
	 	}else{
	 		//gera a lista de filiais
	 		$filiais = "";
	 		foreach ($arrayFiliais as $indice => $filial){
	 			$_filial = trim($filial['COD_FILIAL']);
	 			$_nomFilial = trim($filial['NOM_FILIAL']);
	 			$filiais[] = array("FILIAL" =>	array ("COD_EMPRESA" => $dados['codEmpresa'],
	 					"COD_FILIAL" => $_filial,
	 					"NOM_FILIAL" => $_nomFilial)
	 			);
	 		
	 			//Lista os grupos de estabelecimento que o usuário tem acesso
	 			require_once '../classes/class_grupoEstabelecimento.php';
	 			$objGrupoEstabel = new grupoEstabelecimento($this->database);
	 			$objGrupoEstabel->__set('codUsuario',  $dados['codUsuario']);
	 			$objGrupoEstabel->__set('codFilial', $_filial);
	 			$arrayGrupos = $objGrupoEstabel->listaGrupoEstabelecimentoUsuario();
	 			 
	 			//Lista os estabelecimentos que o usuario tem acesso
	 			require_once '../classes/class_estabelecimento.php';
	 			$objEstabel = new estabelecimento($this->database);
	 			$objEstabel->__set('codUsuario',  $dados['codUsuario']);
	 			$objEstabel->__set('codFilial', $_filial);
	 			$arrayEstabel = $objEstabel->listaEstabelecimentosUsuarioMobile();
	 		
	 		}
	 		
	 		//gera a lista de filiais
	 		$grupos = "";
	 		foreach ($arrayGrupos as $indice => $grupo){
	 			$_grupo = trim($grupo['COD_GRUPO_ESTABELECIMENTO']);
	 			$_nomGrupo = trim($grupo['NOM_GRUPO_ESTABELECIMENTO']);
	 			$grupos[] = array("GRUPO_ESTABELECIMENTO" =>	array ("COD_FILIAL" => $_filial,
	 					"COD_GRUPO_ESTABELECIMENTO" => $_grupo,
	 					"NOM_GRUPO_ESTABELECIMENTO" => $_nomGrupo)
	 			);
	 		}
	 		
	 		//gera a lista de estabelecimentos
	 		$estabelecimentos = "";
	 		foreach ($arrayEstabel as $indice => $estabel){
	 			$_estabelecimento = trim($estabel['COD_ESTABELECIMENTO']);
	 			$_nomEstabelecimento = trim($estabel['NOM_ESTABELECIMENTO']);
	 			$estabelecimentos[] = array("ESTABELECIMENTO" =>	array ("COD_GRUPO_ESTABELECIMENTO" => $_grupo,
	 					"COD_ESTABELECIMENTO" => $_estabelecimento,
	 					"NOM_ESTABELECIMENTO" => $_nomEstabelecimento)
	 			);
	 		}
	 		
	 			 		
	 		$processos = "";
	 		foreach ($arrayTipoProcesso as $indice => $proc){
 	 			
 	 			
 	 			//$arrayListaFilho[] = "";
	 			
 	 			$_nivel = trim($proc['COD_NIVEL']);
	 			$_nomProcesso = trim($proc['DSC_TIPO_PROCESSO']);
 	 			$_codGrupoPai = trim($proc['COD_TIPO_PROCESSO_PAI']);
	 			
 	 			if($proc['IND_FOLHA'] == "N"){
 	 				$_processo = NULL;
 	 				$_grupo = trim($proc['COD_TIPO_PROCESSO']);
	 				$_grupoAgrupador = 'G';
	 				
	 				
 	 			}else{
 	 				$_processo = trim($proc['COD_TIPO_PROCESSO']);
 	 				$_grupo = trim($proc['COD_GRUPO']);
	 				$_grupoAgrupador = 'F';
 	 			}
	 		
 	 			$processos[] = array("TIPO_PROCESSO" =>	array (
 	 					"COD_TIPO_PROCESSO" => $_processo,
 	 					"NIVEL" 			=> $_nivel,
 	 					"AGRUPAMENTO_TIPO"	=> $_grupoAgrupador,
	 					"DSC_TIPO_PROCESSO" => $_nomProcesso,
 	 					"COD_GRUPO_PAI"     => $_codGrupoPai
 	 					)
	 					
 	 			);
 	 			
	 		}
	 		
	 		//LISTA DE TIPOS DE DOCUMENTO
	 		$documentos = "";
	 		foreach ($arrayDocumentos as $indice => $documento){
	 			$_dscTipoDocumento = trim($documento['DSC_TIPO_DOCUMENTO']);
	 			$documentos[] = array("DOCUMENTO" =>	array ("COD_TIPO_PROCESSO" => $documento['COD_TIPO_PROCESSO'],
	 					"COD_TIPO_DOCUMENTO" => $documento['COD_TIPO_DOCUMENTO'],
	 					"DSC_TIPO_DOCUMENTO" => $_dscTipoDocumento,
	 					"LISTA_DISPOSITIVOS" => $documento['DSC_LISTA_DISPOSITIVOS'])
	 			);
	 		}
	 		
	 		//LISTA CAMPOS
	 		$campos = "";
	 		foreach ($arrayCampos as $indice => $valor){
	 			$arrayLista[] = "";
	 			if ($valor['COD_TIPO_CAMPO'] <> ""){
	 				$objTipoProcesso->__set('codTipoLista', $valor['COD_TIPO_LISTA'] );
	 				$arrayLista = $objTipoProcesso->buscaValoresCampoTipoListaLista();
	 			}
	 		
	 			$campos[] = array("CAMPO" =>	array (
	 					"COD_TIPO_PROCESSO"		  => $valor['COD_TIPO_PROCESSO'],
	 					"NOM_CAMPO"               => $valor['NOM_CAMPO'],
	 					"DSC_LABEL_CAMPO"         => $valor['DSC_LABEL_CAMPO'],
	 					"COD_TIPO_CAMPO"          => $valor['COD_TIPO_CAMPO'],
	 					"QTD_CARACTERES"          => $valor['QTD_CARACTERES'],
	 					"NUM_ORDEM"               => $valor['NUM_ORDEM'],
	 					"IND_OBRIGATORIO"         => $valor['IND_OBRIGATORIO'],
	 					"DSC_MASCARA"             => $valor['DSC_MASCARA'],
	 					"COD_TIPO_LISTA"          => $valor['COD_TIPO_LISTA'],
	 					"LISTA"					  => $arrayLista
	 					
	 			)
	 			);
	 		}
	 		
	 		$arrayInicial =	array(
	 				$this->nomeBloco => array(
	 							
	 						"USUARIO" => array(
	 								"COD_EMPRESA" => $dados['codEmpresa'],
	 								"NOM_EMPRESA"  =>  $dados['nomEmpresa'],
	 								"COD_USUARIO" => $dados['codUsuario'],
	 								"NOM_USUARIO" => $dados['nomPessoa'],
	 								"DSC_LOGIN" => $dados['dscIdentificacaoUsuario'],
	 								"COD_TIPO_USUARIO" => $dados['codTipoUsuario'],
	 								"COD_ESTABELECIMENTO"  => $dados['codEstabelecimento'],
	 								"NOM_ESTABELECIMENTO"  => $dados['nomEstabelecimento'],
	 								"DSC_LABEL_FILIAL" => $dados['dscLabelFilial'],
	 								"DSC_LABEL_GRUPO_ESTABELECIMENTO" => $dados['dscLabelGrupoEstabel'],
	 								"DSC_LABEL_ESTABELECIMENTO" => $dados['dscLabelEstabelecimento']
	 						),
	 						
	 						"FILIAIS" => ($filiais		
	 						),
	 						
	 						"GRUPOS_ESTABELECIMENTOS" => ($grupos
	 						),
	 						
	 						"ESTABELECIMENTOS" => ($estabelecimentos
	 						),
	 						
	 						"TIPOS_PROCESSOS" => ($processos
	 						),
	 						
	 						"TIPOS_DOCUMENTOS" => ($documentos
	 						),
	 						
	 						"CAMPOS" => ($campos
	 						),
	
	 						"RESPONSE" => array(
	 								"MESSAGE" => 'OK'
	 						),
	 						"STATUS" => "200 OK",
	 						"CODE" => "200"
	 				)
	 		);
	 		$this->retornoXmlJson = json_encode($arrayInicial);
	 	}
	 	
	 	return true;

	 }
	 
	 /**
	  * Lista os grupos de estabelecimento disponiveis para o usuario solicitado por sistemas internos (impressora virtual, mobil, etc)
	  * @return boolean
	  */
	 public function listaGrupoEstabelecimentoUsuarioWebService(){
	 	 
	 	if (!$this->codFilial || !$this->serviceToken ){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = "Variáveis necessárias não informadas";
	 		return false;
	 	}
	 	 
	 	$tokenGerado = $this->geraServiceToken();
	 	if (!$this->validaServiceToken($tokenGerado, $this->serviceToken)){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = "Token de serviço inválido";
	 		return false;
	 	}
	 	 
	 	require_once '../classes/class_seguranca.php';
	 	$objSeguranca = new seguranca($this->database);
	 	 
	 	if(!$objSeguranca->autentica($this->dscLogin, $this->dscSenha)){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = $objSeguranca->error['message'];
	 		return false;
	 	}
	 	 
	 	$dados = $objSeguranca->__get('dadosWebservice');
	 	 
	 	if($dados['codUsuario'] != $this->codUsuario){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = "Código do Usuário recebido nao confere com o login informado";
	 		return false;
	 			
	 	}
	 	
	 	//busca as filiais que o usuario tem acesso
	 	require_once '../classes/class_grupoEstabelecimento.php';
	 	$objGrupoEstabel = new grupoEstabelecimento($this->database);
	 	$objGrupoEstabel->__set('codUsuario',  $this->codUsuario );
	 	$objGrupoEstabel->__set('codFilial', $this->codFilial );
	 	$arrayGrupos = $objGrupoEstabel->listaGrupoEstabelecimentoUsuario();
	 
	 	if ($this->tipoRetorno == "XML"){
	 		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
	 			
	 		$grupoEstabelecimentos = $xml->addChild("GRUPOS_ESTABELECIMENTOS");
	 		foreach ($arrayGrupos as $key => $value){
	 			$grupoEstabelecimento = $grupoEstabelecimentos->addChild("GRUPO_ESTABELECIMENTO");
	 			$_grupo = trim($value['COD_GRUPO_ESTABELECIMENTO']);
	 			$_nomGrupo = trim($value['NOM_GRUPO_ESTABELECIMENTO']);
	 			$grupoEstabelecimento->addChild("COD_FILIAL", $this->codFilial);
	 			$grupoEstabelecimento->addChild("COD_GRUPO_ESTABELECIMENTO", $_grupo);
	 			$grupoEstabelecimento->addChild("NOM_GRUPO_ESTABELECIMENTO", $_nomGrupo);
	 		}
	 
	 		$response = $xml->addChild('RESPONSE');
	 		$response->addChild('MESSAGE', 'OK');
	 		$xml->addChild('STATUS', '200 OK');
	 		$xml->addChild('CODE', '200');
	 			
	 		$this->retornoXmlJson = $xml->asXML();
	 	}else{
	 		//gera a lista de filiais
	 		$grupos = "";
	 		foreach ($arrayGrupos as $indice => $grupo){
	 			$_grupo = trim($grupo['COD_GRUPO_ESTABELECIMENTO']);
	 			$_nomGrupo = trim($grupo['NOM_GRUPO_ESTABELECIMENTO']);
	 			$grupos[] = array("GRUPO_ESTABELECIMENTO" =>	array ("COD_FILIAL" => $this->codFilial,
	 					"COD_GRUPO_ESTABELECIMENTO" => $_grupo,
	 					"NOM_GRUPO_ESTABELECIMENTO" => $_nomGrupo)
	 			);
	 		}
	 
	 		$arrayInicial =	array(
	 				$this->nomeBloco => array(
	 							
	 						"GRUPOS_ESTABELECIMENTOS" => $grupos
	 				),
	 
	 				"RESPONSE" => array(
	 						"MESSAGE" => 'OK'
	 				),
	 				"STATUS" => "200 OK",
	 				"CODE" => "200"
	 		);
	 			
	 		$this->retornoXmlJson = json_encode($arrayInicial);
	 	}
	 	return true;
	}
	
	/**
	 * Lista os estabelecimentos para o grupoEstabelecimento e usuario solicitado por sistemas internos (impressora virtual, mobil, etc)
	 * @return boolean
	 */
	public function listaEstabelecimentoUsuarioWebService(){
	
		if (!$this->codGrupoEstabelecimento || !$this->serviceToken ){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
	
		$tokenGerado = $this->geraServiceToken();
		if (!$this->validaServiceToken($tokenGerado, $this->serviceToken)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token de serviço inválido";
			return false;
		}
	
		require_once '../classes/class_seguranca.php';
		$objSeguranca = new seguranca($this->database);
	
		if(!$objSeguranca->autentica($this->dscLogin, $this->dscSenha)){
			$this->error['code'] 	= "";
			$this->error['message'] = $objSeguranca->error['message'];
			return false;
		}
		
		$dados = $objSeguranca->__get('dadosWebservice');
		 
		if($dados['codUsuario'] != $this->codUsuario){
			$this->error['code'] 	= "";
			$this->error['message'] = "Código do Usuário recebido nao confere com o login informado";
			return false;
				
		}
	
		//busca as filiais que o usuario tem acesso
		require_once '../classes/class_estabelecimento.php';
		$objEstabel = new estabelecimento($this->database);
		$objEstabel->__set('codUsuario',  $this->codUsuario );
		$objEstabel->__set('codGrupoEstabelecimento', $this->codGrupoEstabelecimento );
		$arrayEstabel = $objEstabel->listaEstabelecimentosUsuario();
	
		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
		 	
			$grupoEstabelecimentos = $xml->addChild("ESTABELECIMENTOS");
			foreach ($arrayEstabel as $key => $value){
				$grupoEstabelecimento = $grupoEstabelecimentos->addChild("ESTABELECIMENTO");
				$_estabelecimento = trim($value['COD_ESTABELECIMENTO']);
				$_nomEstabelecimento = trim($value['NOM_ESTABELECIMENTO']);
				$grupoEstabelecimento->addChild("COD_GRUPO_ESTABELECIMENTO", $this->codGrupoEstabelecimento);
				$grupoEstabelecimento->addChild("COD_ESTABELECIMENTO", $_estabelecimento);
				$grupoEstabelecimento->addChild("NOM_ESTABELECIMENTO", $_nomEstabelecimento);
			}
	
			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
		 	
			$this->retornoXmlJson = $xml->asXML();
		}else{
			//gera a lista de estabelecimentos
			$estabelecimentos = "";
			foreach ($arrayEstabel as $indice => $estabel){
				$_estabelecimento = trim($estabel['COD_ESTABELECIMENTO']);
				$_nomEstabelecimento = trim($estabel['NOM_ESTABELECIMENTO']);
				$estabelecimentos[] = array("ESTABELECIMENTO" =>	array ("COD_GRUPO_ESTABELECIMENTO" => $this->codGrupoEstabelecimento,
						"COD_ESTABELECIMENTO" => $_estabelecimento,
						"NOM_ESTABELECIMENTO" => $_nomEstabelecimento)
				);
			}
	
			$arrayInicial =	array(
					$this->nomeBloco => array(
								
							"ESTABELECIMENTOS" => $estabelecimentos
					),
	
					"RESPONSE" => array(
							"MESSAGE" => 'OK'
					),
					"STATUS" => "200 OK",
					"CODE" => "200"
			);
		 	
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
		return true;
	}
	
	/**
	 * Lista os processos usuario solicitado por sistemas internos (impressora virtual etc)
	 * @return boolean
	 */
	public function listaTipoProcessoUsuarioWebService(){
		 
		if (!$this->codEmpresa || !$this->codEstabelecimento || !$this->codUsuario || !$this->dscSenha || !$this->dscLogin  ||  !$this->serviceToken ){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
		 
		$tokenGerado = $this->geraServiceToken();
		if (!$this->validaServiceToken($tokenGerado, $this->serviceToken)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token de serviço inválido";
			return false;
		}
		 
		require_once '../classes/class_seguranca.php';
		$objSeguranca = new seguranca($this->database);
		 
		if(!$objSeguranca->autentica($this->dscLogin, $this->dscSenha)){
			$this->error['code'] 	= "";
			$this->error['message'] = $objSeguranca->error['message'];
			return false;
		}
		
		$dados = $objSeguranca->__get('dadosWebservice');
		 
		if($dados['codUsuario'] != $this->codUsuario){
			$this->error['code'] 	= "";
			$this->error['message'] = "Código do Usuário recebido nao confere com o login informado";
			return false;
		}
		 
		//busca os processos que o usuario tem acesso
		require_once '../classes/class_tipoProcesso.php';
		$objTipoProcesso = new tipoProcesso($this->database);
		$objTipoProcesso->__set('codUsuario',  $this->codUsuario );
		$objTipoProcesso->__set('codEmpresa', $this->codEmpresa );
		$objTipoProcesso->__set('codEstabelecimento', $this->codEstabelecimento );
		$arrayTipoProcesso = $objTipoProcesso->listaArvoreProcessos();
	
		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="2.0"></'.$this->nomeBloco.'>');
				
			$tiposProcesso = $xml->addChild("TIPOS_PROCESSOS");
			foreach ($arrayTipoProcesso as $key => $value){
				
				$tipoProcesso = $tiposProcesso->addChild("TIPO_PROCESSO");
				if($value['IND_FOLHA'] == "N"){
					$_grupo = trim($value['COD_TIPO_PROCESSO']);
					$_processo = "";
					$_nomProcesso = trim($value['DSC_TIPO_PROCESSO']);
					$_codGrupoPai = trim($value['COD_TIPO_PROCESSO_PAI']);
				}else{
					$_grupo = trim($value['COD_GRUPO']);
					$_processo = trim($value['COD_TIPO_PROCESSO']);
					$_nomProcesso = trim($value['DSC_TIPO_PROCESSO']);
					$_codGrupoPai = trim($value['COD_TIPO_PROCESSO_PAI']);
				}
				$tipoProcesso->addChild("COD_EMPRESA", $this->codEmpresa);
				$tipoProcesso->addChild("COD_ESTABELECIMENTO", $this->codEstabelecimento);
				$tipoProcesso->addChild("COD_TIPO_PROCESSO", $_processo);
				$tipoProcesso->addChild("DSC_TIPO_PROCESSO", $_nomProcesso);
				$tipoProcesso->addChild("COD_GRUPO", $_grupo);
				$tipoProcesso->addChild("COD_GRUPO_PAI", $_codGrupoPai);
				
			}
	
			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
				
			$this->retornoXmlJson = $xml->asXML();
		}else{
			$processos = "";
			foreach ($arrayTipoProcesso as $indice => $proc){
				if($proc['IND_FOLHA'] == "N"){
					$_grupo = trim($proc['COD_TIPO_PROCESSO']);
					$_processo = NULL;
					$_nomProcesso = trim($proc['DSC_TIPO_PROCESSO']);
					$_codGrupoPai = trim($proc['COD_TIPO_PROCESSO_PAI']);
				}else{
					$_grupo = trim($proc['COD_GRUPO']);
					$_processo = trim($proc['COD_TIPO_PROCESSO']);
					$_nomProcesso = trim($proc['DSC_TIPO_PROCESSO']);
					$_codGrupoPai = trim($proc['COD_TIPO_PROCESSO_PAI']);
				}

				//$_grupo = trim($proc['COD_GRUPO']);
				//$_processo = trim($proc['COD_TIPO_PROCESSO']);
				//$_nomProcesso = trim($proc['DSC_TIPO_PROCESSO']);
				//$_grupoPai = trim($proc['COD_GRUPO_PAI']);
				$processos[] = array("TIPO_PROCESSO" =>	array (
						"COD_EMPRESA" => $this->codEmpresa,
						"COD_ESTABELECIMENTO" => $this->codEstabelecimento,
						"COD_TIPO_PROCESSO" => $_processo,
						"DSC_TIPO_PROCESSO" => $_nomProcesso,
						"COD_GRUPO" => $_grupo,              
						"COD_GRUPO_PAI"     => $_codGrupoPai)
				);
			}
	
			$arrayInicial =	array(
					$this->nomeBloco => array(
								
							"TIPOS_PROCESSOS" => $processos
					),
	
					"RESPONSE" => array(
							"MESSAGE" => 'OK'
					),
					"STATUS" => "200 OK",
					"CODE" => "200"
			);
				
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
		return true;
	}
	
	/**
	 * Lista os campos do tipo processo  (impressora virtual etc)
	 * @return boolean
	 */
	public function listaCamposTipoProcessoWebService(){
			
		if (!$this->codUsuario || !$this->dscSenha || !$this->dscLogin  ||  !$this->serviceToken || !$this->codTipoProcesso || !$this->codEmpresa ){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
			
		$tokenGerado = $this->geraServiceToken();
		if (!$this->validaServiceToken($tokenGerado, $this->serviceToken)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token de serviço inválido";
			return false;
		}
			
		require_once '../classes/class_seguranca.php';
		$objSeguranca = new seguranca($this->database);
			
		if(!$objSeguranca->autentica($this->dscLogin, $this->dscSenha)){
			$this->error['code'] 	= "";
			$this->error['message'] = $objSeguranca->error['message'];
			return false;
		}
	
		$dados = $objSeguranca->__get('dadosWebservice');
			
		if($dados['codUsuario'] != $this->codUsuario){
			$this->error['code'] 	= "";
			$this->error['message'] = "Código do Usuário recebido nao confere com o login informado";
			return false;
		}
			
		//busca os processos que o usuario tem acesso
		require_once '../classes/class_tipoProcesso.php';
		$objTipoProcesso = new tipoProcesso($this->database);
		$objTipoProcesso->__set('codTipoProcesso', $this->codTipoProcesso );
		$objTipoProcesso->__set('codTipoDocumento', $this->codTipoDocumento );
		$objTipoProcesso->__set('codEmpresa', $this->codEmpresa );
		$arrayCampos = $objTipoProcesso->buscaCamposDocumentoTipoProcessoTipoDocumento();
	
		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
	
			$campoBloco = $xml->addChild("CAMPOS");
			foreach ($arrayCampos as $indiceCampo => $arrCampo){
				$campo = $campoBloco->addChild("CAMPO");
				$campo->addChild("NOM_CAMPO", $arrCampo['NOM_CAMPO']);
				$campo->addChild("DSC_LABEL_CAMPO", $arrCampo['DSC_LABEL_CAMPO']);
				$campo->addChild("COD_TIPO_CAMPO", $arrCampo['COD_TIPO_CAMPO']);
				$campo->addChild("QTD_CARACTERES", $arrCampo['QTD_CARACTERES']);
				$campo->addChild("NUM_ORDEM", $arrCampo['NUM_ORDEM']);
				$campo->addChild("IND_OBRIGATORIO", $arrCampo['IND_OBRIGATORIO']);
				$campo->addChild("COD_TIPO_IDENTIFICADOR", $arrCampo['COD_TIPO_IDENTIFICADOR']);
				$campo->addChild("DSC_MASCARA", $arrCampo['DSC_MASCARA']);
				$campo->addChild("DSC_TIPO_IDENTIFICADOR", $arrCampo['DSC_TIPO_IDENTIFICADOR']);
				$campo->addChild("DSC_CLASSE_VALIDACAO", $arrCampo['DSC_CLASSE_VALIDACAO']);
				$campo->addChild("COD_TIPO_LISTA", $arrCampo['COD_TIPO_LISTA']);
				$lista = $campo->addChild("LISTA");
			
				$arrayLista = "";
				if ($arrCampo['COD_TIPO_CAMPO'] == "L"){
					$objTipoProcesso->__set('codTipoLista', $arrCampo['COD_TIPO_LISTA'] );
					$arrayLista = $objTipoProcesso->buscaValoresCampoTipoListaLista();
				}
				
				if(is_array($arrayLista) ){
					foreach ($arrayLista as $indiceLista => $arrLista){
						$lista->addChild("COD_LISTA", $arrLista['COD_LISTA'] );
						$lista->addChild("DSC_LISTA", $arrLista['DSC_LISTA'] );
						$lista->addChild("COD_LISTA_CLIENTE", $arrLista['COD_LISTA_CLIENTE'] );
					}
				}
			}
	
			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
	
			$this->retornoXmlJson = $xml->asXML();
		}else{
			$campos = "";
			foreach ($arrayCampos as $indice => $valor){
				$arrayLista[] = "";
				if ($valor['COD_TIPO_CAMPO'] == "L"){
					$objTipoProcesso->__set('codTipoLista', $valor['COD_TIPO_LISTA'] );
					$arrayLista = $objTipoProcesso->buscaValoresCampoTipoListaLista();
				}
								
			$campos[] = array("CAMPO" =>	array (
						"NOM_CAMPO"               => $valor['NOM_CAMPO'],
						"DSC_LABEL_CAMPO"         => $valor['DSC_LABEL_CAMPO'],
						"COD_TIPO_CAMPO"          => $valor['COD_TIPO_CAMPO'],
						"QTD_CARACTERES"          => $valor['QTD_CARACTERES'],
						"NUM_ORDEM"               =>  $valor['NUM_ORDEM'],
						"IND_OBRIGATORIO"         =>  $valor['IND_OBRIGATORIO'],
						"COD_TIPO_IDENTIFICADOR"  =>  $valor['COD_TIPO_IDENTIFICADOR'],
						"DSC_MASCARA"             =>  $valor['DSC_MASCARA'],
						"DSC_TIPO_IDENTIFICADOR"  =>  $valor['DSC_TIPO_IDENTIFICADOR'],
						"DSC_CLASSE_VALIDACAO"    =>  $valor['DSC_CLASSE_VALIDACAO'],
						"COD_TIPO_LISTA"          =>  $valor['COD_TIPO_LISTA'],
						"LISTA"                   =>  $arrayLista
					)
				);
			}
	
			$arrayInicial =	array(
					$this->nomeBloco => array(
								
							"CAMPOS" => $campos
					),
	
					"RESPONSE" => array(
							"MESSAGE" => 'OK'
					),
					"STATUS" => "200 OK",
					"CODE" => "200"
			);
				
			$this->retornoXmlJson = json_encode($arrayInicial);

		}
		return true;
	}
	
	/**
	 * Lista os documentos do tipo processo  (impressora virtual etc)
	 * @return boolean
	 */
	public function listaDocumentosTipoProcessoWebService(){
				 	
	 	if (!$this->codEmpresa || !$this->serviceToken || !$this->codTipoProcesso){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = "Variáveis necessárias não informadas";
	 		return false;
	 	}
	 	
	 	$tokenGerado = $this->geraServiceToken();
	 	if (!$this->validaServiceToken($tokenGerado, $this->serviceToken)){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = "Token de serviço inválido";
	 		return false;
	 	}
	 	
	 	require_once '../classes/class_seguranca.php';
	 	$objSeguranca = new seguranca($this->database);
	 	
	 	if(!$objSeguranca->autentica($this->dscLogin, $this->dscSenha)){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = $objSeguranca->error['message'];
	 		return false;
	 	}
	 	
	 	$dados = $objSeguranca->__get('dadosWebservice');
	 	
	 	if($dados['codUsuario'] != $this->codUsuario){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = "Código do Usuário recebido nao confere com o login informado";
	 		return false;
	 		 
	 	}
	 	
	 	//busca as documentos que o usuario tem acesso
		require_once '../classes/class_tipoDocumento.php';
		$objDocumento = new tipoDocumento($this->database);
		$objDocumento->__set('codTipoProcesso',  $this->codTipoProcesso );
		$objDocumento->__set('codEmpresa', $this->codEmpresa );
		$objDocumento->__set('codTipoDispositivo', $this->codTipoDispositivo );
		$arrayDocumentos = $objDocumento->buscaTiposDocumentoTipoProcesso();

		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
			
			$documentos = $xml->addChild("TIPOS_DOCUMENTOS");
			 foreach ($arrayDocumentos as $key => $value){
				$documento = $documentos->addChild("DOCUMENTO");
				$_dscTipoDocumento = trim($value['DSC_TIPO_DOCUMENTO']);
			
				$documento->addChild("COD_TIPO_DOCUMENTO", $value['COD_TIPO_DOCUMENTO']);
				$documento->addChild("DSC_TIPO_DOCUMENTO", $_dscTipoDocumento);
				$documento->addChild("LISTA_DISPOSITIVOS", $value['DSC_LISTA_DISPOSITIVOS']);
			}
						
			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
			
			$this->retornoXmlJson = $xml->asXML();
		}else{
			//gera a lista de filiais
			$documentos = "";
			foreach ($arrayDocumentos as $indice => $documento){
				$_dscTipoDocumento = trim($documento['DSC_TIPO_DOCUMENTO']);
				$documentos[] = array("DOCUMENTO" =>	array ("COD_TIPO_DOCUMENTO" => $documento['COD_TIPO_DOCUMENTO'],
					                                           "DSC_TIPO_DOCUMENTO" => $_dscTipoDocumento,
																"LISTA_DISPOSITIVOS" => $documento['DSC_LISTA_DISPOSITIVOS'])
				);
			}
	
			$arrayInicial =	array(
					$this->nomeBloco => array(
							
							"DOCUMENTOS" => $documentos
							),
								
							"RESPONSE" => array(
									"MESSAGE" => 'OK'
							),
							"STATUS" => "200 OK",
							"CODE" => "200"
			);
			
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
	 	return true;
	 }
	
	/**
	 * retorna informações do usuario
	 * @return boolean
	 */
	public function retornaUsuario(){
		if (!$this->codToken || !$this->dscLogin){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
	
		require_once '../classes/class_empresa.php';
		$objEmpresa = new empresa($this->database);
	
		// valida token de acesso
		if (!$objEmpresa->validaTokenAcesso($this->codToken)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token inválido";
			return false;
		}
		
		require_once '../classes/class_usuario.php';
		$objUsuario = new usuario($this->database);
		
		// pega dados do usuario
		$objUsuario->__set('dscIdentificacaoUsuario', $this->dscLogin);
		$arrayDados = $objUsuario->getUsuario($objUsuario->getCodUsuarioByLogin());
		
		// verifica se a empresa do usuarioé igual a do token
		if ($objEmpresa->__get('codEmpresa') != $objUsuario->__get('codEmpresa')){
			$this->error['code'] 	= "";
			$this->error['message'] = "Acesso inválido";
			return false;
		}
		
		// verifica se é array e corrige dados de exibição da filial
		if (is_array($objUsuario->__get('codFilial') ) ){
			$codFilial = "";
			foreach ($objUsuario->__get('codFilial') as $codigo){
				$codFilial .= $codigo['COD_FILIAL'].",";
			}
			$codFilial = substr($codFilial, 0, strlen($codFilial)-1);
		}else{
			$codFilial = $objUsuario->__get('codFilial');
		}
		
		
		// verifica se é array e corrige dados de exibição do grupo de estabelecimento
		if (is_array($objUsuario->__get('codGrupoEstabelecimento') ) ){
			$codGrupo = "";
			foreach ($objUsuario->__get('codGrupoEstabelecimento') as $codigo){
				$codGrupo .= $codigo['COD_GRUPO_ESTABELECIMENTO'].",";
			}
			$codGrupo = substr($codGrupo, 0, strlen($codGrupo)-1);
		}else{
			$codGrupo = $objUsuario->__get('codGrupoEstabelecimento');
		}
		
		// para pegar dados do processo
		require_once '../classes/class_tipoProcesso.php';
		$objTipoProcesso = new tipoProcesso($this->database);
	
		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
				
			$dados = $xml->addChild("DADOS");
			$dados->addChild("COD_USUARIO", $objUsuario->getCodUsuarioByLogin());
			$dados->addChild("IND_ATIVO", $objUsuario->__get('indAtivo'));
			$dados->addChild("COD_TIPO_USUARIO", $objUsuario->__get('codTipoUsuario'));
			$dados->addChild("NOM_PESSOA", $objUsuario->__get('nomPessoa'));
			

			$empresa = $xml->addChild("EMPRESA");
			$empresa->addChild("COD_EMPRESA", $objUsuario->__get('codEmpresa'));
			$empresa->addChild("NOM_EMPRESA", $objUsuario->__get('nomEmpresa'));
			
			$filial = $xml->addChild("FILIAL");
			$filial->addChild("COD_FILIAL", $codFilial);
			
			$grupo = $xml->addChild("GRUPO");
			$grupo->addChild("COD_GRUPO_ESTABELECIMENTO", $codGrupo);
			
			$estabelecimento = $xml->addChild("ESTABELECIMENTO");
			$estabelecimento->addChild("COD_ESTABELECIMENTO", $objUsuario->__get('codEstabelecimento'));
			
			$processo = $objUsuario->__get('codTipoProcesso');
			
			for($i = 0; $i < count($objUsuario->__get('codTipoProcesso')); $i++){
				$tipoProcesso[$i] = $xml->addChild("TIPO_PROCESSO");
				$tipoProcesso[$i]->addChild("COD_TIPO_PROCESSO", $processo[$i]["COD_TIPO_PROCESSO"]);
				$objTipoProcesso->getNomeTipoProcesso($processo[$i]["COD_TIPO_PROCESSO"]);
				$tipoProcesso[$i]->addChild("DSC_TIPO_PROCESSO", $objTipoProcesso->__get('dscTipoProcesso'));
			}			
				
			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
	
			$this->retornoXmlJson = $xml->asXML();
	
		}else{		
			$processo = $objUsuario->__get('codTipoProcesso');
			
			for($i = 0; $i < count($objUsuario->__get('codTipoProcesso')); $i++){
			
				$objTipoProcesso->getNomeTipoProcesso($processo[$i]["COD_TIPO_PROCESSO"]);
				
				$tipoProcesso[] =
				array(
						"COD_TIPO_PROCESSO" => $processo[$i]["COD_TIPO_PROCESSO"],
						"DSC_TIPO_PROCESSO" => $objTipoProcesso->__get('dscTipoProcesso')
				);
			}
				
			$arrayInicial =	array(
					$this->nomeBloco => array(
							"DADOS" => array(
								"COD_USUARIO" 			=> $objUsuario->getCodUsuarioByLogin(),
								"IND_ATIVO" 			=> $objUsuario->__get('indAtivo'), 
								"COD_TIPO_USUARIO" 		=> $objUsuario->__get('codTipoUsuario'), 
								"NOM_PESSOA" 			=> $objUsuario->__get('nomPessoa')
							),
							"EMPRESA" => array(
								"COD_EMPRESA" 			=> $objUsuario->__get('codEmpresa'), 
								"NOM_EMPRESA" 			=> $objUsuario->__get('nomEmpresa')
							),
							"FILIAL" => array(
								"COD_FILIAL" 			=> $codFilial
							),
							"GRUPO" => array(
								"COD_GRUPO_ESTABELECIMENTO" => $codGrupo
							),
							"ESTABELECIMENTO" => array(
								"COD_ESTABELECIMENTO" 	=> $objUsuario->__get('codEstabelecimento')
							),
							"TIPO_PROCESSO" 			=> $tipoProcesso,
							
							"RESPONSE" => array(
									"MESSAGE" 			=> "OK"
							),
							"STATUS" 					=> "200 OK",
							"CODE" 						=> "200"
					)
			);
				
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
		return true;
	}
		
	//**********************************************************************************************//
	
	/**
	 * retorna lista de documento
	 * @return boolean
	 */
	public function retornaTipoDocumento(){
		if (!$this->codToken){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
	
		require_once '../classes/class_empresa.php';
		$objEmpresa = new empresa($this->database);
	
		// valida token de acesso
		if (!$objEmpresa->validaTokenAcesso($this->codToken)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token inválido";
			return false;
		}
	
		require_once '../classes/class_tipoDocumento.php';
		$objTipoDocumento = new tipoDocumento($this->database);
	
		$objTipoDocumento->__set('codEmpresa', $objEmpresa->__get('codEmpresa'));
		if ($this->codTipoDocumento){
			$objTipoDocumento->__set('codTipoDocumento', $this->codTipoDocumento);
			
			$arrayDocumento = $objTipoDocumento->getTipoDocumento();
		}else{
			$arrayDocumento = $objTipoDocumento->getAllTipoDocumento();
		}
	
		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
				
			if (is_array($arrayDocumento)){
				for($i = 0; $i < count($arrayDocumento); $i++){
					$tipoDocumento[$i] = $xml->addChild("TIPO_DOCUMENTO");
					$tipoDocumento[$i]->addChild("COD_TIPO_DOCUMENTO", $arrayDocumento[$i]["COD_TIPO_DOCUMENTO"]);
					$tipoDocumento[$i]->addChild("DSC_TIPO_DOCUMENTO", $arrayDocumento[$i]["DSC_TIPO_DOCUMENTO"]);
				}
			}else{
				if ($objTipoDocumento->__get('codTipoDocumento')){
					$i = 1;
					$tipoDocumento = $xml->addChild("TIPO_DOCUMENTO");
					$tipoDocumento->addChild("COD_TIPO_DOCUMENTO", $objTipoDocumento->__get('codTipoDocumento'));
					$tipoDocumento->addChild("DSC_TIPO_DOCUMENTO", $objTipoDocumento->__get('dscTipoDocumento'));
				}else{
					$i = 0;
				}
			}
				
			$response = $xml->addChild('RESPONSE');
			if ($i > 0){
				$response->addChild('MESSAGE', 'OK - '.$i.' tipo(s) de documento encontrado(s)');
			}else{
				$response->addChild('MESSAGE', 'Nenhum tipo de documento encontrado');
			}
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
	
			$this->retornoXmlJson = $xml->asXML();
	
		}else{
				
			if (is_array($arrayDocumento)){
				for($i = 0; $i < count($arrayDocumento); $i++){
						
					$tipoDocumento[] =
					array(
							"COD_TIPO_DOCUMENTO" => $arrayDocumento[$i]["COD_TIPO_DOCUMENTO"],
							"DSC_TIPO_DOCUMENTO" => $arrayDocumento[$i]["DSC_TIPO_DOCUMENTO"]
					);
				}
			}else{
				if ($objTipoDocumento->__get('codTipoDocumento')){
					$i = 1;
					$tipoDocumento =
					array(
							"COD_TIPO_DOCUMENTO" => $objTipoDocumento->__get('codTipoDocumento'),
							"DSC_TIPO_DOCUMENTO" => $objTipoDocumento->__get('dscTipoDocumento')
					);
				}else{
					$i = 0;
					$tipoDocumento = null;
				}
			}
	
			if ($i > 0){
				$message = 'OK - '.$i.' tipo(s) de documento encontrado(s)';
			}else{
				$message = 'Nenhum tipo de documento encontrado';
			}
				
				
			$arrayInicial =	array(
					$this->nomeBloco => array(
							"TIPO_DOCUMENTO" => $tipoDocumento,
							"RESPONSE" => array(
									"MESSAGE" => $message
							),
							"STATUS" => "200 OK",
							"CODE" => "200"
					)
			);
				
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
		return true;
	}
	
	//**********************************************************************************************//
	
	/**
	 * retorna lista de tipos de processo
	 * @return boolean
	 */
	public function retornaTipoProcesso(){
		if (!$this->codToken || !$this->codTipoProcesso){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
	
		require_once '../classes/class_empresa.php';
		$objEmpresa = new empresa($this->database);
	    
		include_once('../classes/class_util.php');
		$objUtil = new util();
		
		// valida token de acesso
		if (!$objEmpresa->validaTokenAcesso($this->codToken)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token inválido";
			return false;
		}
	
		require_once '../classes/class_tipoProcesso.php';
		$objTipoProcesso = new tipoProcesso($this->database);
	
		$objTipoProcesso->__set('codEmpresa', $objEmpresa->__get('codEmpresa'));
		$objTipoProcesso->getTipoProcesso($this->codTipoProcesso);
		$arrayCampos = $objTipoProcesso->getArrayCampos();
		$arrayTipoDocDispositivo = $objTipoProcesso->getArrayTipoDocDispositivo();
		
		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
				
			if ($objTipoProcesso->__get('codTipoProcesso')){
				$xml->addChild("COD_TIPO_PROCESSO", $objTipoProcesso->__get('codTipoProcesso'));
				$xml->addChild("DSC_TIPO_PROCESSO", $objTipoProcesso->__get('dscTipoProcesso'));
			}
			
			$campos = $xml->addChild('CAMPOS');
			for($i = 0; $i < count($arrayCampos); $i++){
				$campo[$i] = $campos->addChild("CAMPO");
				$campo[$i]->addChild("NOM_CAMPO", 				$arrayCampos[$i]["NOM_CAMPO"]);
				$campo[$i]->addChild("DSC_LABEL_CAMPO", 		$arrayCampos[$i]["DSC_LABEL_CAMPO"]);
				$campo[$i]->addChild("COD_TIPO_CAMPO", 			$arrayCampos[$i]["COD_TIPO_CAMPO"]);
				$campo[$i]->addChild("QTD_CARACTERES", 			$arrayCampos[$i]["QTD_CARACTERES"]);
				$campo[$i]->addChild("NUM_ORDEM", 				$arrayCampos[$i]["NUM_ORDEM"]);
				$campo[$i]->addChild("IND_OBRIGATORIO", 		$arrayCampos[$i]["IND_OBRIGATORIO"]);
				$campo[$i]->addChild("COD_TIPO_IDENTIFICADOR", 	$arrayCampos[$i]["COD_TIPO_IDENTIFICADOR"]);
				$campo[$i]->addChild("DSC_TIPO_IDENTIFICADOR", 	$arrayCampos[$i]["DSC_TIPO_IDENTIFICADOR"]);
				$campo[$i]->addChild("DSC_CLASSE_VALIDACAO", 	$arrayCampos[$i]["DSC_CLASSE_VALIDACAO"]);
			}
				
			$dispositivos = $xml->addChild("DOCUMENTOS_DISPOSITIVOS");
			for($i = 0; $i < count($arrayTipoDocDispositivo); $i++){
				$dispositivo[$i] = $dispositivos->addChild("DOCUMENTO_DISPOSITIVO");
				$dispositivo[$i]->addChild("COD_TIPO_DOCUMENTO", 	$arrayTipoDocDispositivo[$i]["COD_TIPO_DOCUMENTO"]);
				$dispositivo[$i]->addChild("DSC_TIPO_DOCUMENTO", 	$arrayTipoDocDispositivo[$i]["DSC_TIPO_DOCUMENTO"]);
				$dispositivo[$i]->addChild("COD_TIPO_DISPOSITIVO", 	$arrayTipoDocDispositivo[$i]["COD_TIPO_DISPOSITIVO"]);
				$dispositivo[$i]->addChild("DSC_TIPO_DISPOSITIVO", 	$objUtil->dscTipoDispositivo($arrayTipoDocDispositivo[$i]["COD_TIPO_DISPOSITIVO"]));
				//$dispositivo[$i]->addChild("QTD_DPI", 				$arrayTipoDocDispositivo[$i]["QTD_DPI"]);
				//$dispositivo[$i]->addChild("QTD_QUALIDADE", 		$arrayTipoDocDispositivo[$i]["QTD_BITS"]);
			}

			$response = $xml->addChild('RESPONSE');
			if ($objTipoProcesso->__get('codTipoProcesso')){
				$response->addChild('MESSAGE', 'OK - 1 tipo de processo encontrado');
			}else{
				$response->addChild('MESSAGE', 'Nenhum tipo de processo encontrado');
			}
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
	
			$this->retornoXmlJson = $xml->asXML();
	
		}else{
				
			for($i = 0; $i < count($arrayCampos); $i++){
				$campo[] = 
				array(
					"CAMPO" =>
						array(
							"NOM_CAMPO" 				=> $arrayCampos[$i]["NOM_CAMPO"],
							"DSC_LABEL_CAMPO" 			=> $arrayCampos[$i]["DSC_LABEL_CAMPO"],
							"COD_TIPO_CAMPO" 			=> $arrayCampos[$i]["COD_TIPO_CAMPO"],
							"QTD_CARACTERES" 			=> $arrayCampos[$i]["QTD_CARACTERES"],
							"NUM_ORDEM" 				=> $arrayCampos[$i]["NUM_ORDEM"],
							"IND_OBRIGATORIO" 			=> $arrayCampos[$i]["IND_OBRIGATORIO"],
							"COD_TIPO_IDENTIFICADOR" 	=> $arrayCampos[$i]["COD_TIPO_IDENTIFICADOR"],
							"DSC_TIPO_IDENTIFICADOR" 	=> $arrayCampos[$i]["DSC_TIPO_IDENTIFICADOR"],
							"DSC_CLASSE_VALIDACAO" 		=> $arrayCampos[$i]["DSC_CLASSE_VALIDACAO"]
					)
				);
			}
			if ($i == 0){
				$campo = null;
			}
			
			for($i = 0; $i < count($arrayTipoDocDispositivo); $i++){
				$dispositivo[] = 
				array(
						"DOCUMENTO_DISPOSITIVO" =>
						array(
							"COD_TIPO_DOCUMENTO" 	=> $arrayTipoDocDispositivo[$i]["COD_TIPO_DOCUMENTO"],
							"DSC_TIPO_DOCUMENTO" 	=> $arrayTipoDocDispositivo[$i]["DSC_TIPO_DOCUMENTO"],
							"COD_TIPO_DISPOSITIVO" 	=> $arrayTipoDocDispositivo[$i]["COD_TIPO_DISPOSITIVO"],
							"DSC_TIPO_DISPOSITIVO" 	=> $objUtil->dscTipoDispositivo($arrayTipoDocDispositivo[$i]["COD_TIPO_DISPOSITIVO"]),
							"QTD_DPI" 				=> $arrayTipoDocDispositivo[$i]["QTD_DPI"],
							"QTD_QUALIDADE" 		=> $arrayTipoDocDispositivo[$i]["QTD_BITS"]
						)
				);
			}
			if ($i == 0){
				$dispositivo = null;
			}
			
			if ($objTipoProcesso->__get('codTipoProcesso')){
				$message = 'OK - 1 tipo de processo encontrado';
			}else{
				$message = 'Nenhum tipo de processo encontrado';
			}
				
				
			$arrayInicial =	array(
					$this->nomeBloco => array(
							"COD_TIPO_PROCESSO" => $objTipoProcesso->__get('codTipoProcesso'),
							"DSC_TIPO_PROCESSO" => $objTipoProcesso->__get('dscTipoProcesso'),
							"CAMPOS" => $campo,
							"DOCUMENTOS_DISPOSITIVOS" => $dispositivo,
							"RESPONSE" => array(
									"MESSAGE" => $message
							),
							"STATUS" => "200 OK",
							"CODE" => "200"
					)
			);
				
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
		return true;
	}
	
	//**********************************************************************************************//
	
	/**
	 * retorna lista de tipos de identificador
	 * @return boolean
	 */
	public function retornaTipoIdentificador(){
		if (!$this->codToken){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
		
		require_once '../classes/class_empresa.php';
		$objEmpresa = new empresa($this->database);
		
		// valida token de acesso
		if (!$objEmpresa->validaTokenAcesso($this->codToken)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token inválido";
			return false;
		}
		
		require_once '../classes/class_tipoIdentificador.php';
		$objTipoIdentificador = new tipoIdentificador($this->database);
		
		if ($this->codTipoIdentificador){
			$arrayIdentificador = $objTipoIdentificador->getTipoIdentificador($this->codTipoIdentificador);
		}else{
			$arrayIdentificador = $objTipoIdentificador->getAllTipoIdentificador();
		}
		
		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
			
			if (is_array($arrayIdentificador)){
				for($i = 0; $i < count($arrayIdentificador); $i++){
					$tipoIdentificador[$i] = $xml->addChild("TIPO_IDENTIFICADOR");
					$tipoIdentificador[$i]->addChild("COD_TIPO_IDENTIFICADOR", 	$arrayIdentificador[$i]["COD_TIPO_IDENTIFICADOR"]);
					$tipoIdentificador[$i]->addChild("DSC_TIPO_IDENTIFICADOR", 	$arrayIdentificador[$i]["DSC_TIPO_IDENTIFICADOR"]);
					$tipoIdentificador[$i]->addChild("DSC_CLASSE_VALIDACAO", 	$arrayIdentificador[$i]["DSC_CLASSE_VALIDACAO"]);
				}
			}else{
				if ($objTipoIdentificador->__get('codTipoIdentificador')){
					$i = 1;
					$tipoIdentificador = $xml->addChild("TIPO_IDENTIFICADOR");
					$tipoIdentificador->addChild("COD_TIPO_IDENTIFICADOR", 	$objTipoIdentificador->__get('codTipoIdentificador'));
					$tipoIdentificador->addChild("DSC_TIPO_IDENTIFICADOR", 	$objTipoIdentificador->__get('dscTipoIdentificador'));
					$tipoIdentificador->addChild("DSC_CLASSE_VALIDACAO", 	$objTipoIdentificador->__get('dscClasseValidacao'));
				}else{
					$i = 0;	
				}
			}
			
			$response = $xml->addChild('RESPONSE');
			if ($i > 0){
				$response->addChild('MESSAGE', 'OK - '.$i.' tipo(s) de identificador encontrado(s)');
			}else{
				$response->addChild('MESSAGE', 'Nenhum tipo de identificador encontrado');
			}
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
		
			$this->retornoXmlJson = $xml->asXML();

		}else{
			
			if (is_array($arrayIdentificador)){
				for($i = 0; $i < count($arrayIdentificador); $i++){
					
					$tipoIdentificador[] = 
						array(
							"COD_TIPO_IDENTIFICADOR" 	=> $arrayIdentificador[$i]["COD_TIPO_IDENTIFICADOR"],
							"DSC_TIPO_IDENTIFICADOR" 	=> $arrayIdentificador[$i]["DSC_TIPO_IDENTIFICADOR"],
							"DSC_CLASSE_VALIDACAO"		=> $arrayIdentificador[$i]["DSC_CLASSE_VALIDACAO"]
						);
				}
			}else{
				if ($objTipoIdentificador->__get('codTipoIdentificador')){
					$i = 1;
					$tipoIdentificador =
					array(
							"COD_TIPO_IDENTIFICADOR" 	=> $objTipoIdentificador->__get('codTipoIdentificador'),
							"DSC_TIPO_IDENTIFICADOR" 	=> $objTipoIdentificador->__get('dscTipoIdentificador'),
							"DSC_CLASSE_VALIDACAO" 		=> $objTipoIdentificador->__get('dscClasseValidacao')
					);
				}else{
					$i = 0;
					$tipoIdentificador = null;
				}
			}

			if ($i > 0){
				$message = 'OK - '.$i.' tipo(s) de identificador encontrado(s)';
			}else{
				$message = 'Nenhum tipo de identificador encontrado';
			}
			
			
			$arrayInicial =	array(
					$this->nomeBloco => array(
							"TIPO_IDENTIFICADOR" => $tipoIdentificador,
							"RESPONSE" => array(
									"MESSAGE" => $message
							),
							"STATUS" => "200 OK",
							"CODE" => "200"
					)
			);
			
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
		return true;
	}
	
	//**********************************************************************************************//
	
	/**
	 * valida o tipo de identificador
	 * @return boolean
	 */
	public function validarTipoIdentificador(){
		if (!$this->codToken || !$this->codTipoIdentificador || !$this->codIdentificacao){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
	
		require_once '../classes/class_empresa.php';
		$objEmpresa = new empresa($this->database);
	
		// valida token de acesso
		if (!$objEmpresa->validaTokenAcesso($this->codToken)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token inválido";
			return false;
		}
	
		require_once '../classes/class_tipoIdentificador.php';
		$objTipoIdentificador = new tipoIdentificador($this->database);
		$validou = $objTipoIdentificador->validaTipoIdentificador($this->codTipoIdentificador, $this->codIdentificacao);
		
		if ($validou){
			$validou = "TRUE";
		}else{
			$validou = "FALSE";
		}
		
		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
			
			$validacao = $xml->addChild("VALIDACAO");
			$validacao->addChild("COD_TIPO_IDENTIFICADOR", 	$this->codTipoIdentificador);
			$validacao->addChild("COD_IDENTIFICACAO", 		$this->codIdentificacao);
			$validacao->addChild("IND_VALIDACAO", 			$validou);
		

			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
	
			$this->retornoXmlJson = $xml->asXML();
	
		}else{
				
			$arrayInicial =	array(
					$this->nomeBloco => array(
							"VALIDACAO" => array(
									"COD_TIPO_IDENTIFICADOR" 	=> $this->codTipoIdentificador,
									"COD_IDENTIFICACAO"			=> $this->codIdentificacao,
									"IND_VALIDACAO"				=> $validou
							),
							"RESPONSE" => array(
									"MESSAGE" => "OK"
							),
							"STATUS" => "200 OK",
							"CODE" => "200"
					)
			);
				
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
		return true;
	}
	
	//**********************************************************************************************//
	
	/**
	 * retorna dados do documento
	 * @return boolean
	 */
	public function retornaDocumento(){
		$documento = "";
		$i = 0;
		// pesquisa por codigo de documento
		if ($this->codDocumento){
			
			if (!$this->codToken){
				$this->error['code'] 	= "";
				$this->error['message'] = "Variáveis necessárias não informadas";
				return false;
			}
			
		// senão procura por tipo processo e por tipo identificador
		}else{
			if (!$this->codToken || !$this->codTipoProcesso || !$this->codTipoIdentificador || !$this->datInicio){
				$this->error['code'] 	= "";
				$this->error['message'] = "Variáveis necessárias não informadas";
				return false;
			}
		}
	
		require_once '../classes/class_empresa.php';
		$objEmpresa = new empresa($this->database);
	
		// valida token de acesso
		if (!$objEmpresa->validaTokenAcesso($this->codToken)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token inválido";
			return false;
		}
		
		// verifica se o token possui usuario
		if (!$objEmpresa->__get('codUsuario')){
			$this->error['code'] 	= "";
			$this->error['message'] = "Usuário inválido";
			return false;
		}
		
		// pega o tipo de usuario para passar pra query
		require_once '../classes/class_usuario.php';
		$objUsuario = new usuario($this->database);
		
		$objUsuario->getUsuario($objEmpresa->__get('codUsuario'));
		
		if (!$objUsuario->__get('codUsuario') || !$objUsuario->__get('codTipoUsuario')){
			$this->error['code'] 	= "";
			$this->error['message'] = "Ocorreu um erro ao obter dados do usuário";
			return false;
		}	
	
		require_once '../classes/class_documento.php';
		$objDocumento = new documento($this->database);
		require_once '../classes/class_util.php';
		$objUtil = new util();
		
		// se pesquisa o documento especifico
		if ($this->codDocumento){
			
			if ($objDocumento->verificaDocumentoEmpresa($this->codDocumento, $objEmpresa->__get('codEmpresa'))){
				$objDocumento->__set('codEmpresa', $objEmpresa->__get('codEmpresa'));
				$objDocumento->__set('codDocumento', $this->codDocumento);
				$objDocumento->__set('codUsuario', $objUsuario->__get('codUsuario'));
				$objDocumento->__set('codTipoUsuario', $objUsuario->__get('codTipoUsuario'));
				$arrayDocumento = $objDocumento->getDocumentoFiltros();
			}else{
				$this->error['code'] 	= "";
				$this->error['message'] = "Documento não encontrado";
				return false;
			}
			
		// senão procura por tipo processo e por tipo identificador
		}else{
			// obrigatorios
			$objDocumento->__set('codEmpresa', $objEmpresa->__get('codEmpresa'));
			$objDocumento->__set('codTipoProcesso', $this->codTipoProcesso);
			$objDocumento->__set('codTipoIdentificador', $this->codTipoIdentificador);
			$objDocumento->__set('dthDataIni', $this->datInicio);
			$objDocumento->__set('codUsuario', $objUsuario->__get('codUsuario'));
			$objDocumento->__set('codTipoUsuario', $objUsuario->__get('codTipoUsuario'));
			// opcionais
			$objDocumento->__set('codTipoDocumento', $this->codTipoDocumento);
			$objDocumento->__set('codEstabelecimento', $this->codEstabelecimento);
			$objDocumento->__set('codIdentificacao', $this->codIdentificacao);
			$objDocumento->__set('dthDataFim', $this->datFim);
			
			// pega dados
			$arrayDocumento = $objDocumento->getDocumentoFiltros();
		}
		
	
		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');

			for($i = 0; $i < count($arrayDocumento); $i++){
				$documento[$i] = $xml->addChild("DOCUMENTO");
				$documento[$i]->addChild("COD_DOCUMENTO", 				$arrayDocumento[$i]["COD_DOCUMENTO"]);
				$documento[$i]->addChild("COD_TIPO_DOCUMENTO", 			$arrayDocumento[$i]["COD_TIPO_DOCUMENTO"]);
				$documento[$i]->addChild("DSC_TIPO_DOCUMENTO", 			$arrayDocumento[$i]["DSC_TIPO_DOCUMENTO"]);
				$documento[$i]->addChild("COD_TIPO_DISPOSITIVO", 		$arrayDocumento[$i]["COD_TIPO_DISPOSITIVO"]);
				$documento[$i]->addChild("DSC_TIPO_DISPOSITIVO", 		$objUtil->dscTipoDispositivo($arrayDocumento[$i]["COD_TIPO_DISPOSITIVO"]));
				
				$documento[$i]->addChild("COD_USUARIO", 				$arrayDocumento[$i]["COD_USUARIO"]);
				$documento[$i]->addChild("DSC_IDENTIFICACAO_USUARIO", 	$arrayDocumento[$i]["DSC_IDENTIFICACAO_USUARIO"]);
				$documento[$i]->addChild("COD_MIME_TYPE", 				$arrayDocumento[$i]["COD_MIME_TYPE"]);
				$documento[$i]->addChild("DSC_MIME_TYPE", 				$arrayDocumento[$i]["DSC_MIME_TYPE"]);
				$documento[$i]->addChild("COD_ESTABELECIMENTO", 		$arrayDocumento[$i]["COD_ESTABELECIMENTO"]);
				$documento[$i]->addChild("NOM_ESTABELECIMENTO", 		$arrayDocumento[$i]["NOM_ESTABELECIMENTO"]);
				$documento[$i]->addChild("NUM_CNPJ_ESTABELECIMENTO",	$arrayDocumento[$i]["NUM_CNPJ"]);
				
				$documento[$i]->addChild("DTH_CAPTACAO", 				$arrayDocumento[$i]["DTH_CAPTACAO"]);
				$documento[$i]->addChild("COD_PROCESSO", 				$arrayDocumento[$i]["COD_PROCESSO"]);
				$documento[$i]->addChild("COD_TIPO_PROCESSO", 			$arrayDocumento[$i]["COD_TIPO_PROCESSO"]);
				$documento[$i]->addChild("DSC_TIPO_PROCESSO", 			$arrayDocumento[$i]["DSC_TIPO_PROCESSO"]);
				$documento[$i]->addChild("COD_TIPO_IDENTIFICADOR", 		$arrayDocumento[$i]["COD_TIPO_IDENTIFICADOR"]);
				$documento[$i]->addChild("DSC_TIPO_IDENTIFICADOR", 		$arrayDocumento[$i]["DSC_TIPO_IDENTIFICADOR"]);
				
				$documento[$i]->addChild("COD_IDENTIFICACAO", 			$arrayDocumento[$i]["COD_IDENTIFICACAO"]);

			}

			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK - '.$i.' documento(s) encontrado(s)');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
	
			$this->retornoXmlJson = $xml->asXML();
	
		}else{
			
			for($i = 0; $i < count($arrayDocumento); $i++){
					
				$documento[] =
				array(
						"COD_DOCUMENTO" 				=> $arrayDocumento[$i]["COD_DOCUMENTO"],
						"COD_TIPO_DOCUMENTO" 			=> $arrayDocumento[$i]["COD_TIPO_DOCUMENTO"],
						"DSC_TIPO_DOCUMENTO" 			=> $arrayDocumento[$i]["DSC_TIPO_DOCUMENTO"],
						"COD_TIPO_DISPOSITIVO" 			=> $arrayDocumento[$i]["COD_TIPO_DISPOSITIVO"],
						"DSC_TIPO_DISPOSITIVO" 			=>  $objUtil->dscTipoDispositivo($arrayDocumento[$i]["COD_TIPO_DISPOSITIVO"]),
						
						"COD_USUARIO" 					=> $arrayDocumento[$i]["COD_USUARIO"],
						"DSC_IDENTIFICACAO_USUARIO" 	=> $arrayDocumento[$i]["DSC_IDENTIFICACAO_USUARIO"],
						"COD_MIME_TYPE" 				=> $arrayDocumento[$i]["COD_MIME_TYPE"],
						"DSC_MIME_TYPE" 				=> $arrayDocumento[$i]["DSC_MIME_TYPE"],
						"COD_ESTABELECIMENTO" 			=> $arrayDocumento[$i]["COD_ESTABELECIMENTO"],
						"NOM_ESTABELECIMENTO" 			=> $arrayDocumento[$i]["NOM_ESTABELECIMENTO"],
						"NUM_CNPJ_ESTABELECIMENTO" 		=> $arrayDocumento[$i]["NUM_CNPJ"],
						
						"DTH_CAPTACAO" 					=> $arrayDocumento[$i]["DTH_CAPTACAO"],
						"COD_PROCESSO" 					=> $arrayDocumento[$i]["COD_PROCESSO"],
						"COD_TIPO_PROCESSO" 			=> $arrayDocumento[$i]["COD_TIPO_PROCESSO"],
						"DSC_TIPO_PROCESSO" 			=> $arrayDocumento[$i]["DSC_TIPO_PROCESSO"],
						"COD_TIPO_IDENTIFICADOR" 		=> $arrayDocumento[$i]["COD_TIPO_IDENTIFICADOR"],
						"DSC_TIPO_IDENTIFICADOR" 		=> $arrayDocumento[$i]["DSC_TIPO_IDENTIFICADOR"],
						
						"COD_IDENTIFICACAO" 			=> $arrayDocumento[$i]["COD_IDENTIFICACAO"],
				);
			}
			
			$arrayInicial =	array(
					$this->nomeBloco => array(
							"DOCUMENTO" => $documento,
							"RESPONSE" => array(
									"MESSAGE" => "OK - ".$i." documento(s) encontrado(s)"
							),
							"STATUS" => "200 OK",
							"CODE" => "200"
					)
			);
	
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
		return true;
	}
	
	//**********************************************************************************************//
	
	
	
	
	
	
	/**
	 * retorna dados do documento
	 * @return boolean
	 */
	public function _retornaDocumento(){
		$documento = "";
		$i = 0;
		// pesquisa por codigo de documento
		if ($this->codDocumento){
				
			if (!$this->codToken){
				$this->error['code'] 	= "";
				$this->error['message'] = "Variáveis necessárias não informadas";
				return false;
			}
				
			// senão procura por tipo processo e por tipo identificador
		}else{
			if (!$this->codToken || !$this->codTipoProcesso || !$this->datInicio){
				$this->error['code'] 	= "";
				$this->error['message'] = "Variáveis necessárias não informadas";
				return false;
			}
		}
	
		require_once '../classes/class_empresa.php';
		$objEmpresa = new empresa($this->database);
	
		// valida token de acesso
		if (!$objEmpresa->validaTokenAcesso($this->codToken)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token inválido";
			return false;
		}
	
		// verifica se o token possui usuario
		if (!$objEmpresa->__get('codUsuario')){
			$this->error['code'] 	= "";
			$this->error['message'] = "Usuário inválido";
			return false;
		}
	
		// pega o tipo de usuario para passar pra query
		require_once '../classes/class_usuario.php';
		$objUsuario = new usuario($this->database);
	
		$objUsuario->getUsuario($objEmpresa->__get('codUsuario'));
	
		if (!$objUsuario->__get('codUsuario') || !$objUsuario->__get('codTipoUsuario')){
			$this->error['code'] 	= "";
			$this->error['message'] = "Ocorreu um erro ao obter dados do usuário";
			return false;
		}
	
		require_once '../classes/class_documento.php';
		$objDocumento = new documento($this->database);
		require_once '../classes/class_util.php';
		$objUtil = new util();
	
		// se pesquisa o documento especifico
		if ($this->codDocumento){
				
			if ($objDocumento->verificaDocumentoEmpresa($this->codDocumento, $objEmpresa->__get('codEmpresa'))){
				$objDocumento->__set('codEmpresa', $objEmpresa->__get('codEmpresa'));
				$objDocumento->__set('codDocumento', $this->codDocumento);
				$objDocumento->__set('codUsuario', $objUsuario->__get('codUsuario'));
				$objDocumento->__set('codTipoUsuario', $objUsuario->__get('codTipoUsuario'));
				$arrayDocumento = $objDocumento->_getDocumentoFiltros();
			}else{
				$this->error['code'] 	= "";
				$this->error['message'] = "Documento não encontrado";
				return false;
			}
				
			// senão procura por tipo processo e por tipo identificador
		}else{
			// obrigatorios
			$objDocumento->__set('codEmpresa', $objEmpresa->__get('codEmpresa'));
			$objDocumento->__set('codTipoProcesso', $this->codTipoProcesso);
			//$objDocumento->__set('codTipoIdentificador', $this->codTipoIdentificador);
			$objDocumento->__set('dthDataIni', $this->datInicio);
			$objDocumento->__set('codUsuario', $objUsuario->__get('codUsuario'));
			$objDocumento->__set('codTipoUsuario', $objUsuario->__get('codTipoUsuario'));
			// opcionais
			$objDocumento->__set('codTipoDocumento', $this->codTipoDocumento);
			$objDocumento->__set('codEstabelecimento', $this->codEstabelecimento);
			$objDocumento->__set('codIdentificacao', $this->codIdentificacao);
			$objDocumento->__set('dthDataFim', $this->datFim);
			$objDocumento->__set('arrayCampos', $this->arrayCampos);
				
			// pega dados
			$arrayDocumento = $objDocumento->_getDocumentoFiltros();
		}
	
	
		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
	
			for($i = 0; $i < count($arrayDocumento); $i++){
				$documento[$i] = $xml->addChild("DOCUMENTO");
				$documento[$i]->addChild("COD_DOCUMENTO", 				$arrayDocumento[$i]["COD_DOCUMENTO"]);
				$documento[$i]->addChild("COD_TIPO_DOCUMENTO", 			$arrayDocumento[$i]["COD_TIPO_DOCUMENTO"]);
				$documento[$i]->addChild("DSC_TIPO_DOCUMENTO", 			$arrayDocumento[$i]["DSC_TIPO_DOCUMENTO"]);
				$documento[$i]->addChild("COD_SITUACAO", 			    $arrayDocumento[$i]["COD_SITUACAO"]);
				$documento[$i]->addChild("COD_TIPO_DISPOSITIVO", 		$arrayDocumento[$i]["COD_TIPO_DISPOSITIVO"]);
				$documento[$i]->addChild("DSC_TIPO_DISPOSITIVO", 		$objUtil->dscTipoDispositivo($arrayDocumento[$i]["COD_TIPO_DISPOSITIVO"]));
	
				$documento[$i]->addChild("COD_USUARIO", 				$arrayDocumento[$i]["COD_USUARIO"]);
				$documento[$i]->addChild("DSC_IDENTIFICACAO_USUARIO", 	$arrayDocumento[$i]["DSC_IDENTIFICACAO_USUARIO"]);
				$documento[$i]->addChild("COD_MIME_TYPE", 				$arrayDocumento[$i]["COD_MIME_TYPE"]);
				$documento[$i]->addChild("DSC_MIME_TYPE", 				$arrayDocumento[$i]["DSC_MIME_TYPE"]);
				$documento[$i]->addChild("COD_ESTABELECIMENTO", 		$arrayDocumento[$i]["COD_ESTABELECIMENTO"]);
				$documento[$i]->addChild("NOM_ESTABELECIMENTO", 		$arrayDocumento[$i]["NOM_ESTABELECIMENTO"]);
				$documento[$i]->addChild("NUM_CNPJ_ESTABELECIMENTO",	$arrayDocumento[$i]["NUM_CNPJ"]);
	
				$documento[$i]->addChild("DTH_CAPTACAO", 				$arrayDocumento[$i]["DTH_CAPTACAO"]);
				$documento[$i]->addChild("COD_PROCESSO", 				$arrayDocumento[$i]["COD_PROCESSO"]);
				$documento[$i]->addChild("COD_TIPO_PROCESSO", 			$arrayDocumento[$i]["COD_TIPO_PROCESSO"]);
				$documento[$i]->addChild("DSC_TIPO_PROCESSO", 			$arrayDocumento[$i]["DSC_TIPO_PROCESSO"]);
				$documento[$i]->addChild("COD_TIPO_IDENTIFICADOR", 		$arrayDocumento[$i]["COD_TIPO_IDENTIFICADOR"]);
				$documento[$i]->addChild("DSC_TIPO_IDENTIFICADOR", 		$arrayDocumento[$i]["DSC_TIPO_IDENTIFICADOR"]);
	
				$documento[$i]->addChild("COD_IDENTIFICACAO", 			$arrayDocumento[$i]["COD_IDENTIFICACAO"]);
	
			}
	
			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK - '.$i.' documento(s) encontrado(s)');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
	
			$this->retornoXmlJson = $xml->asXML();
	
		}else{
				
			for($i = 0; $i < count($arrayDocumento); $i++){
					
				$documento[] =
				array(
						"COD_DOCUMENTO" 				=> $arrayDocumento[$i]["COD_DOCUMENTO"],
						"COD_TIPO_DOCUMENTO" 			=> $arrayDocumento[$i]["COD_TIPO_DOCUMENTO"],
						"DSC_TIPO_DOCUMENTO" 			=> $arrayDocumento[$i]["DSC_TIPO_DOCUMENTO"],
						"COD_SITUACAO" 			   		=> $arrayDocumento[$i]["COD_SITUACAO"],
						"COD_TIPO_DISPOSITIVO" 			=> $arrayDocumento[$i]["COD_TIPO_DISPOSITIVO"],
						"DSC_TIPO_DISPOSITIVO" 			=>  $objUtil->dscTipoDispositivo($arrayDocumento[$i]["COD_TIPO_DISPOSITIVO"]),
	
						"COD_USUARIO" 					=> $arrayDocumento[$i]["COD_USUARIO"],
						"DSC_IDENTIFICACAO_USUARIO" 	=> $arrayDocumento[$i]["DSC_IDENTIFICACAO_USUARIO"],
						"COD_MIME_TYPE" 				=> $arrayDocumento[$i]["COD_MIME_TYPE"],
						"DSC_MIME_TYPE" 				=> $arrayDocumento[$i]["DSC_MIME_TYPE"],
						"COD_ESTABELECIMENTO" 			=> $arrayDocumento[$i]["COD_ESTABELECIMENTO"],
						"NOM_ESTABELECIMENTO" 			=> $arrayDocumento[$i]["NOM_ESTABELECIMENTO"],
						"NUM_CNPJ_ESTABELECIMENTO" 		=> $arrayDocumento[$i]["NUM_CNPJ"],
	
						"DTH_CAPTACAO" 					=> $arrayDocumento[$i]["DTH_CAPTACAO"],
						"COD_PROCESSO" 					=> $arrayDocumento[$i]["COD_PROCESSO"],
						"COD_TIPO_PROCESSO" 			=> $arrayDocumento[$i]["COD_TIPO_PROCESSO"],
						"DSC_TIPO_PROCESSO" 			=> $arrayDocumento[$i]["DSC_TIPO_PROCESSO"],
						"COD_TIPO_IDENTIFICADOR" 		=> $arrayDocumento[$i]["COD_TIPO_IDENTIFICADOR"],
						"DSC_TIPO_IDENTIFICADOR" 		=> $arrayDocumento[$i]["DSC_TIPO_IDENTIFICADOR"],
	
						"COD_IDENTIFICACAO" 			=> $arrayDocumento[$i]["COD_IDENTIFICACAO"],
				);
			}
				
			$arrayInicial =	array(
					$this->nomeBloco => array(
							"DOCUMENTO" => $documento,
							"RESPONSE" => array(
									"MESSAGE" => "OK - ".$i." documento(s) encontrado(s)"
							),
							"STATUS" => "200 OK",
							"CODE" => "200"
					)
			);
	
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Verifica se um documento existe
	 * @return boolean
	 */
	public function existeDocumento(){
		if (!$this->codToken || !$this->codTipoIdentificador || !$this->codIdentificacao || !$this->codEstabelecimento){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
		
		require_once '../classes/class_empresa.php';
		$objEmpresa = new empresa($this->database);
		
		// valida token de acesso
		if (!$objEmpresa->validaTokenAcesso($this->codToken)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token inválido";
			return false;
		}
		
		require_once '../classes/class_documento.php';
		$objDocumento = new documento($this->database);
		
		
		$existe = $objDocumento->existeDocumento($this->codTipoIdentificador, $this->codIdentificacao, $this->codEstabelecimento, $this->codTipoDocumento, $this->datInclusao);
		
		if ($existe){
			$existe = "TRUE";
		}else{
			$existe = "FALSE";
		}
		
		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
				
			$validacao = $xml->addChild("DOCUMENTO");
			$validacao->addChild("COD_TIPO_IDENTIFICADOR", 	$this->codTipoIdentificador);
			$validacao->addChild("COD_IDENTIFICACAO", 		$this->codIdentificacao);
			$validacao->addChild("COD_ESTABELECIMENTO", 	$this->codEstabelecimento);
			$validacao->addChild("COD_TIPO_DOCUMENTO", 		$this->codTipoDocumento);
			$validacao->addChild("DAT_INCLUSAO", 			$this->datInclusao);
			$validacao->addChild("IND_EXISTE", 				$existe);
		
		
			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
		
			$this->retornoXmlJson = $xml->asXML();
		
		}else{
		
			$arrayInicial =	array(
					$this->nomeBloco => array(
							"DOCUMENTO" => array(
									"COD_TIPO_IDENTIFICADOR" 	=> $this->codTipoIdentificador,
									"COD_IDENTIFICACAO"			=> $this->codIdentificacao,
									"COD_ESTABELECIMENTO"		=> $this->codEstabelecimento,
									"COD_TIPO_DOCUMENTO"		=> $this->codTipoDocumento,
									"DAT_INCLUSAO"				=> $this->datInclusao,
									"IND_EXISTE"				=> $existe
							),
							"RESPONSE" => array(
									"MESSAGE" => "OK"
							),
							"STATUS" => "200 OK",
							"CODE" => "200"
					)
			);
		
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
		return true;
		
		
	}
	
	//**********************************************************************************************//
	
	/**
	 * Exibe um documento
	 * @return boolean
	 */
	public function visualizaDocumento(){
		if (!$this->codToken || !$this->codDocumento){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
		
		require_once '../classes/class_empresa.php';
		$objEmpresa = new empresa($this->database);
		
		// valida token de acesso
		if (!$objEmpresa->validaTokenAcesso($this->codToken)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token inválido";
			return false;
		}
		
		// verifica se o token possui usuario
		if (!$objEmpresa->__get('codUsuario')){
			$this->error['code'] 	= "";
			$this->error['message'] = "Usuário inválido";
			return false;
		}
		
		require_once '../classes/class_documento.php';
		$objDocumento = new documento($this->database);
		
		require_once '../classes/class_documentoAndamento.php';
		$objAndamento   = new documentoAndamento($this->database);
		
		// gera o header do documento
		$mimeType = $objDocumento->getDscMimeType($this->codDocumento);
		
		// se não tem mime type nao achou documento
		if (!$mimeType){
			$this->error['code'] 	= "";
			$this->error['message'] = "Documento não encontrado";
			return false;
		}
		
		header("Content-Type:".$mimeType);
		header('Cache-Control: max-age=0');
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		
		$objDocumento->__set('codDocumento', $this->codDocumento);
		
		// andamento
		$objAndamento->__set('codDocumento', $this->codDocumento);
		$objAndamento->__set('codAndamento', '6'); // sigla (Visualização do documento via webservice)
		$objAndamento->__set('codUsuario', $objEmpresa->__get('codUsuario'));
		@$objAndamento->insAndamento();
		
		// exibe documento
		echo $objDocumento->getBlbArquivo();
		
		// ao alterar a classe de DB pra stringficar o blob/clob, utilizo o echo acima em vez de fpassthru
		//fpassthru($objDocumento->getBlbArquivo());
		
		return true;
		
	}
	
	//**********************************************************************************************//
	
	/**
	 * retorna dados do processo
	 * @return boolean
	 */
	public function criaProcesso(){
	
		if (!$this->codUsuario || !$this->codTipoProcesso || !$this->codEstabelecimento){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
		
		// vai gerar um novo processo
		require_once '../classes/class_processo.php';
		$objProcesso = new processo($this->database);
		
		$objProcesso->__set('codTipoProcesso', $this->codTipoProcesso);
		$objProcesso->__set('codEstabelecimento', $this->codEstabelecimento);
		$objProcesso->__set('codUsuario', $this->codUsuario);
		
		if (!empty($_SERVER['HTTP_CLIENT_IP'])){
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		$objProcesso->__set('codIpEstacao', $ip);
		$objProcesso->__set('codMacAdress', $this->codMacAdress);		
		$objProcesso->__set('arrayCampos', $this->arrayCampos);
		
		if(!$objProcesso->insProcesso()){
			$this->error['code'] 	= "";
			$this->error['message'] = "Documento não encontrado";
			return false;
		}
		
		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
	
			$processo = $xml->addChild("PROCESSO");
			$processo->addChild("COD_PROCESSO", $objProcesso->__get('codProcesso'));	
			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK - processo criado com sucesso');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
	
			$this->retornoXmlJson = $xml->asXML();
	
		}else{
				
			$arrayInicial =	array(
					$this->nomeBloco => array(
							"PROCESSO" => array(
									"COD_PROCESSO" => $objProcesso->__get('codProcesso')
							),
							"RESPONSE" => array(
									"MESSAGE" => "OK - processo criado com sucesso"
							),
							"STATUS" => "200 OK",
							"CODE" => "200"
					)
			);
	
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
		return true;
	}
	
	public function insereDocumento(){
		
		if (!$this->codTipoDocumento || !$this->codTipoProcesso|| !$this->codEstabelecimento || !$this->codUsuario || !$this->codTipoDispositivo || !$this->arquivo){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
		
		// vai unserir um novo documento
		require_once '../classes/class_documento.php';
		$objDocumento = new documento($this->database);
		
		require_once '../classes/class_empresa.php';
		$objEmpresa = new empresa($this->database);
				
		$uploaded_file 	= $this->arquivo;
		
		if (!empty($_SERVER['HTTP_CLIENT_IP'])){
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
				
		//$objDocumento->__set('codProcesso', $this->codProcesso);
		
		$objDocumento->__set('codTipoProcesso', $this->codTipoProcesso);
		$objDocumento->__set('codTipoDocumento', $this->codTipoDocumento);
		$objDocumento->__set('codEstabelecimento', $this->codEstabelecimento);
		$objDocumento->__set('codUsuario', $this->codUsuario);
		$objDocumento->__set('codTipoDispositivo', $this->codTipoDispositivo);
		$objDocumento->__set('codIpEstacao', $_SERVER['REMOTE_ADDR']);
		$objDocumento->__set('arrayCampos', $this->arrayCampos);
		
		$codEmpresa = $objEmpresa->getCodEmpresaByEstabelecimento($this->codEstabelecimento);
		$objDocumento->__set('codEmpresa', $codEmpresa);

		
		//CONTROLA O PROCESSO
		require_once '../classes/class_processo.php';
		$objProcesso = new processo($this->database);
		
		require_once '../classes/class_tipoProcesso.php';
		$objTipoProcesso = new tipoProcesso($this->database);
		$objTipoProcesso->__set('codEmpresa', $codEmpresa);
		
		$objTipoProcesso->getTipoProcesso($this->codTipoProcesso);
		$indUtilizaProcesso =  $objTipoProcesso->__get('indUtilizaProcesso');
		
		$objProcesso->__set('codTipoProcesso', null);
		if ($indUtilizaProcesso == "S"){
			$objProcesso->__set('codTipoProcesso', $this->codTipoProcesso);
			$objProcesso->__set('codUsuario', $this->codUsuario);
			$objProcesso->__set('codEmpresa', $codEmpresa);
			$objProcesso->__set('indexador1', $this->arrayCampos[1]);
			$objProcesso->__set('indexador2', $this->arrayCampos[2]);
			$objProcesso->__set('indexador3', $this->arrayCampos[3]);
			$objProcesso->__set('indexador4', $this->arrayCampos[4]);
			$objProcesso->__set('indexador5', $this->arrayCampos[5]);
			$objProcesso->__set('indexador6', $this->arrayCampos[6]);
			
			if(TRUE == $objProcesso->insProcesso()){
				$codProcesso = $objProcesso->__get('codProcesso');
				$objDocumento->__set('codProcesso', $codProcesso);
				
			}
			else
			{
				unlink($uploaded_file);
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao encontrar processo";
				return false;
			}
		}
			
		$objDocumento->setBlbArquivo($this->arquivo);
		
		// pega o mime type
		//$finfo = new finfo(FILEINFO_MIME_TYPE);
		//$dscType = $finfo->buffer(file_get_contents($uploaded_file));
		
		// se for mime type application/x-empty deu erro e veio vazio
		if ($this->mimeType == "application/x-empty" || $this->mimeType == ""){
			unlink($uploaded_file);
			
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro: Documento vazio recebido";
			return false;
		}
		
		$codMimeType = $objDocumento->getCodMimeByDscMime($this->mimeType);
		
		// se precisar cadastrar o novo mime type
		if ($codMimeType == ""){
			unlink($uploaded_file);
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro: Documento vazio recebido";
			return false;
		}
		
		$objDocumento->setCodMimeType($codMimeType);

		
		
		if (!$objDocumento->insDocumento()){
			unlink($uploaded_file);
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro:".$objDocumento->error['message'];
			return false;
		}
		unlink($uploaded_file);
		
		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
		
			$documento = $xml->addChild("DOCUMENTO");
			$documento->addChild("COD_DOCUMENTO", $objDocumento->__get('codDocumento'));
			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK - documento inserido com sucesso');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
		
			$this->retornoXmlJson = $xml->asXML();
		
		}else{
		
			$arrayInicial =	array(
					$this->nomeBloco => array(
							"DOCUMENTO" => array(
									"COD_DOCUMENTO" => $objDocumento->__get('codDocumento')
							),
							"RESPONSE" => array(
									"MESSAGE" => "OK - documento inserido com sucesso"
							),
							"STATUS" => "200 OK",
							"CODE" => "200"
					)
			);
		
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
		return true;
	}
	
	
	// Function para iniciar processo sem inserir documento
	public function iniciaProcesso(){
	
		if (!$this->codTipoDocumento || !$this->codTipoProcesso|| !$this->codEstabelecimento || !$this->codUsuario || !$this->codTipoDispositivo || !$this->arrayCampos){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
	
		// vai inserir um novo documento
		require_once '../classes/class_documento.php';
		$objDocumento = new documento($this->database);
	
		require_once '../classes/class_empresa.php';
		$objEmpresa = new empresa($this->database);
	
		if (!empty($_SERVER['HTTP_CLIENT_IP'])){
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
	
		$objDocumento->__set('codTipoProcesso', $this->codTipoProcesso);
		$objDocumento->__set('codTipoDocumento', $this->codTipoDocumento);
		$objDocumento->__set('codEstabelecimento', $this->codEstabelecimento);
		$objDocumento->__set('codUsuario', $this->codUsuario);
		$objDocumento->__set('codTipoDispositivo', $this->codTipoDispositivo);
		$objDocumento->__set('codIpEstacao', $_SERVER['REMOTE_ADDR']);
		$objDocumento->__set('arrayCampos', $this->arrayCampos);
	
		$codEmpresa = $objEmpresa->getCodEmpresaByEstabelecimento($this->codEstabelecimento);
		$objDocumento->__set('codEmpresa', $codEmpresa);
	
		//CONTROLA O PROCESSO
		require_once '../classes/class_processo.php';
		$objProcesso = new processo($this->database);
	
		require_once '../classes/class_tipoProcesso.php';
		$objTipoProcesso = new tipoProcesso($this->database);
		$objTipoProcesso->__set('codEmpresa', $codEmpresa);
	
		$objTipoProcesso->getTipoProcesso($this->codTipoProcesso);
		$indUtilizaProcesso =  $objTipoProcesso->__get('indUtilizaProcesso');
	
		$objProcesso->__set('codTipoProcesso', null);
		if ($indUtilizaProcesso == "S"){
			$objProcesso->__set('codTipoProcesso', $this->codTipoProcesso);
			$objProcesso->__set('codUsuario', $this->codUsuario);
			$objProcesso->__set('codEmpresa', $codEmpresa);
			$objProcesso->__set('indexador1', $this->arrayCampos[1]);
			$objProcesso->__set('indexador2', $this->arrayCampos[2]);
			$objProcesso->__set('indexador3', $this->arrayCampos[3]);
			$objProcesso->__set('indexador4', $this->arrayCampos[4]);
			$objProcesso->__set('indexador5', $this->arrayCampos[5]);
			$objProcesso->__set('indexador6', $this->arrayCampos[6]);
				
			if(TRUE == $objProcesso->insProcesso()){
				$codProcesso = $objProcesso->__get('codProcesso');
				$objDocumento->__set('codProcesso', $codProcesso);
	
			}
			else
			{
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao encontrar processo";
				return false;
			}
		}
			

		if ($this->tipoRetorno == "XML"){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="2.0"></'.$this->nomeBloco.'>');
	
			$documento = $xml->addChild("CONTROLE_PROCESSO");
			$documento->addChild("COD_PROCESSO", $objDocumento->__get('codProcesso'));
			$response = $xml->addChild('RESPONSE');
			$response->addChild('MESSAGE', 'OK - processo iniciado com sucesso');
			$xml->addChild('STATUS', '200 OK');
			$xml->addChild('CODE', '200');
	
			$this->retornoXmlJson = $xml->asXML();
	
		}else{
	
			$arrayInicial =	array(
					$this->nomeBloco => array(
							"DOCUMENTO" => array(
									"COD_DOCUMENTO" => $objDocumento->__get('codTipoProcesso')
							),
							"RESPONSE" => array(
									"MESSAGE" => "OK - processo iniciado com sucesso"
							),
							"STATUS" => "200 OK",
							"CODE" => "200"
					)
			);
	
			$this->retornoXmlJson = json_encode($arrayInicial);
		}
		return true;
	}
	
	public function geraServiceToken(){
		date_default_timezone_set("Brazil/East");
		$hoje = date('d/m/Y');
		$frase = "serviceToken";
		return sha1(sha1($hoje . $frase));
	}
	
	public function validaServiceToken($tokenGerado, $tokenRecebido){
		if($tokenGerado != $tokenRecebido){
			$this->error['code'] 	= "";
			$this->error['message'] = "Token de serviço inválido";
			return false;
		}
		return true;
	}
	
	public function validaConteudoCampo(){
	
		if (!$this->codUsuario || !$this->dscSenha || !$this->dscLogin  ||  !$this->serviceToken || !$this->dscClasseValidacao || !$this->conteudoCampo ){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
	
		$tokenGerado = $this->geraServiceToken();
	 	if (!$this->validaServiceToken($tokenGerado, $this->serviceToken)){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = "Token de serviço inválido";
	 		return false;
	 	}
	 	 
	 	require_once '../classes/class_seguranca.php';
	 	$objSeguranca = new seguranca($this->database);
	 	 
	 	if(!$objSeguranca->autentica($this->dscLogin, $this->dscSenha)){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = $objSeguranca->error['message'];
	 		return false;
	 	}
	 	 
	 	$dados = $objSeguranca->__get('dadosWebservice');
	 	 
	 	if($dados['codUsuario'] != $this->codUsuario){
	 		$this->error['code'] 	= "";
	 		$this->error['message'] = "Código do Usuário recebido nao confere com o login informado";
	 		return false;
	 			
	 	}
	 	
	    require_once('../classes/class_validate.php');
	 	$objValidate	= new validate();	
	 	$classeValidacao = $this->dscClasseValidacao;
	 	$resultado = $objValidate->$classeValidacao($this->conteudoCampo);
	 	if ($resultado){
	 		$resultado = "TRUE";	
	 	}else{
	 		$resultado = "FALSE";
	 	}
	 	if ($this->tipoRetorno == "XML"){
	 		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$this->nomeBloco.' generator="Via Nuvem" version="1.0"></'.$this->nomeBloco.'>');
	 			
	 		$validacao = $xml->addChild("RESULTADO_VALIDACAO", $resultado);

	 		$response = $xml->addChild('RESPONSE');
	 		$response->addChild('MESSAGE', 'OK');
	 		$xml->addChild('STATUS', '200 OK');
	 		$xml->addChild('CODE', '200');
	 			
	 		$this->retornoXmlJson = $xml->asXML();
	 	}else{
	 		$validacao = "";
	 		$validacao[] = array ("RESULTADO_VALIDACAO" => $resultado);

	 		$arrayInicial =	array(
	 				$this->nomeBloco => array($validacao
	 				),
	 	
	 				"RESPONSE" => array(
	 						"MESSAGE" => 'OK'
	 				),
	 				"STATUS" => "200 OK",
	 				"CODE" => "200"
	 		);
	 			
	 		$this->retornoXmlJson = json_encode($arrayInicial);
	 	}
	 	return true;
	}

}