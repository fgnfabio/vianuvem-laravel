<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class pessoa
{
	public    $database;
	public    $error;
	protected $codPessoa;
	protected $nomPessoa;
	private   $codTipoPessoa;
	protected $dscEmail;
	protected $codUsuario;
	
	
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getCodPessoa() {
		return $this->codPessoa;
	}
	
	public function setCodPessoa($codPessoa) {
		$this->codPessoa = $codPessoa;
	}
	
	public function getNomPessoa() {
		return $this->nomPessoa;
	}
	
	public function setNomPessoa($nomPessoa) {
		$this->nomPessoa = $nomPessoa;
	}
	
	public function getCodTipoPessoa() {
		return $this->codTipoPessoa;
	}
	
	public function setCodTipoPessoa($codTipoPessoa) {
		$this->codTipoPessoa = $codTipoPessoa;
	}
	
	public function getDscEmail() {
		return $this->dscEmail;
	}
	
	public function setDscEmail($dscEmail) {
		$this->dscEmail = $dscEmail;
	}
	
	public function getCodUsuario() {
		return $this->codUsuario;
	}
	
	public function setCodUsuario($codUsuario) {
		$this->codUsuario = $codUsuario;
	}
	
	// pega uma pessoa
	public function getPessoa($codPessoa){
		$objConexao = $this->database;
		
		$strSql = "SELECT P.COD_PESSOA, NOM_PESSOA, COD_TIPO_PESSOA, DSC_EMAIL FROM PESSOA P 
				     LEFT JOIN PESSOA_EMAIL PE ON (PE.COD_PESSOA = P.COD_PESSOA) 
				    WHERE P.COD_PESSOA =:codPessoa";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codPessoa";
		$arrayParametros[0][1] = $codPessoa;
	
		$arrayPessoa = $objConexao->executeFetch($strSql, $arrayParametros);
	
		$this->codPessoa 		= $arrayPessoa[0]["COD_PESSOA"];
		$this->nomPessoa 		= $arrayPessoa[0]["NOM_PESSOA"];
		$this->codTipoPessoa 	= $arrayPessoa[0]["COD_TIPO_PESSOA"];
		$this->dscEmail 		= $arrayPessoa[0]["DSC_EMAIL"];
	
		return true;
	}
	
	
	// cadastra uma pessoa
	function insPessoa() {
		if(TRUE == $this->nomPessoa){
			
			$objConexao = $this->database;
	
			$strSql = "SELECT SQ_PESSO_01.NEXTVAL AS COD_PESSOA FROM DUAL";
				
			$arrayPessoa = $objConexao->executeFetch($strSql);
			$this->codPessoa = $arrayPessoa[0]["COD_PESSOA"];
			
			if (!$this->codPessoa){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao criar ID de pessoa";
				return false;
			}
		
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $this->codPessoa;
			$arrayParametros[1][0] = ":nomPessoa";
			$arrayParametros[1][1] = $this->nomPessoa;
			$arrayParametros[2][0] = ":codTipoPessoa";
			$arrayParametros[2][1] = $this->codTipoPessoa;
			$arrayParametros[3][0] = ":codUsuario";
			$arrayParametros[3][1] = $this->codUsuario;
		
			$strSql = "INSERT INTO PESSOA 
					          (COD_PESSOA, NOM_PESSOA, COD_TIPO_PESSOA, COD_USUARIO, DTH_INCLUSAO) 
					   VALUES (:codPessoa, :nomPessoa, :codTipoPessoa, :codUsuario, SYSDATE)";
		
			if (!$objConexao->execute($strSql, $arrayParametros)) {
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao inserir pessoa";
				return false;
			}
		
			$arrayParametrosEmail = array();
			$arrayParametrosEmail[0][0] = ":codPessoa";
			$arrayParametrosEmail[0][1] = $this->codPessoa;
			$arrayParametrosEmail[1][0] = ":dscEmail";
			$arrayParametrosEmail[1][1] = $this->dscEmail;
		
			$strSql = "INSERT INTO PESSOA_EMAIL 
					          (COD_PESSOA, NUM_SEQUENCIA, DSC_EMAIL, IND_EMAIL_PRINCIPAL) 
					   VALUES (:codPessoa, 1, :dscEmail, 'S')";
		
			if (!$objConexao->execute($strSql, $arrayParametrosEmail)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao inserir pessoa email";
				return false;
			}
			
			
			return true;
		}else{
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para inserir pessoa";
			return false;
		}
	}
	
	// altera a pessoa
	function changePessoa() {
		
		if(TRUE == $this->codPessoa && TRUE == $this->nomPessoa){
			
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $this->getCodPessoa();
			$arrayParametros[1][0] = ":nomPessoa";
			$arrayParametros[1][1] = $this->getNomPessoa();
			$arrayParametros[2][0] = ":codUsuario";
			$arrayParametros[2][1] = $this->codUsuario;
	
	
			$strSql = "UPDATE PESSOA 
					      SET NOM_PESSOA =:nomPessoa,
					          DTH_ULTIMA_ALTERACAO = SYSDATE,
					          COD_USUARIO = :codUsuario
					    WHERE COD_PESSOA =:codPessoa";
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar pessoa";
				return false;
			}
	
			$arrayParametrosEmail = array();
			$arrayParametrosEmail[0][0] = ":codPessoa";
			$arrayParametrosEmail[0][1] = $this->codPessoa;
			$arrayParametrosEmail[1][0] = ":dscEmail";
			$arrayParametrosEmail[1][1] = $this->dscEmail;
	
			$strSql = "UPDATE PESSOA_EMAIL 
					      SET DSC_EMAIL =:dscEmail 
					    WHERE COD_PESSOA =:codPessoa";
			if (!$objConexao->execute($strSql, $arrayParametrosEmail)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar pessoa email";
				return false;
			}

			return true;
		}else{
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para alterar pessoa";
			return false;
		}
	}
	
	public function delPessoa($codPessoa){
	
		if(true == $codPessoa){
			$objConexao = $this->database;
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $codPessoa;
	
			$strSql = 'DELETE 
					     FROM PESSOA 
					    WHERE COD_PESSOA = :codPessoa';	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar pessoa";
				return false;
			}else{
				return true;
			}
		}else{
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para deletar pessoa";
			return false;
		}
	}
	
	
	
	
		
	
}

?>