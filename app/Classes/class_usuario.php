<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */


Class usuario
{
	
	public $database;
	public $error;
	
	protected $codEmpresa;
	protected $codEstabelecimento;
	protected $codFilial;
	protected $codGrupoEstabelecimento;
	protected $codPapelUsuario;
	protected $codPerfilUsuario;
	protected $codTipoProcesso;
	protected $codTipoUsuario;
	protected $codToken;
	protected $codUsuario;
	protected $dscIdentificacaoUsuario;
	protected $dscSenhaAcesso;
	protected $indAtivo;
	protected $indLoginSite;
	protected $nomEmpresa;
	protected $nomPessoa;
	protected $codTipoDocumento;
	protected $indPerfilSistema;
	protected $codTipoNotificacao;
	
	function __construct($database){
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}

	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getDscIdentificacaoUsuario(){
		return $this->dscIdentificacaoUsuario;
	}
	
	public function setDscIdentificacaoUsuario($dscIdentificacaoUsuario){
		$this->dscIdentificacaoUsuario = $dscIdentificacaoUsuario;
	}
	
	public function getCodUsuario(){
		return $this->codUsuario;
	}
	
	public function setCodUsuario($codUsuario){
		$this->codUsuario = $codUsuario;
	}
	
	public function getDscSenhaAcesso(){
		return $this->dscSenhaAcesso;
	}
	
	public function setDscSenhaAcesso($dscSenhaAcesso){
		$this->dscSenhaAcesso = $dscSenhaAcesso;
	}
	
	public function getIndAtivo(){
		return $this->indAtivo;
	}
	
	public function setIndAtivo($indAtivo){
		$this->indAtivo = $indAtivo;
	}
	
	public function getCodToken(){
		return $this->codToken;
	}
	
	public function setCodToken($codToken){
		$this->codToken = $codToken;
	}
	
	public function getCodPapelUsuario(){
		return $this->codPapelUsuario;
	}
	
	public function setCodPapelUsuario($codPapelUsuario){
		$this->codPapelUsuario = $codPapelUsuario;
	}
	
	public function getCodTipoUsuario(){
		return $this->codTipoUsuario;
	}
	
	public function setCodTipoUsuario($codTipoUsuario){
		$this->codTipoUsuario = $codTipoUsuario;
	}
	
	public function getNomPessoa(){
		return $this->nomPessoa;
	}
	
	public function setNomPessoa($nomPessoa){
		$this->nomPessoa = $nomPessoa;
	}
	
	public function getIndLoginSite(){
		return $this->indLoginSite;
	}
	
	public function setIndLoginSite($indLoginSite){
		$this->indLoginSite = $indLoginSite;
	}
	
	public function getCodEmpresa(){
		return $this->codEmpresa;
	}
	
	public function setCodEmpresa($codEmpresa){
		$this->codEmpresa = $codEmpresa;
	}
	
	public function getNomEmpresa(){
		return $this->nomEmpresa;
	}
	
	public function setNomEmpresa($nomEmpresa){
		$this->nomEmpresa = $nomEmpresa;
	}

	public function getCodFilial(){
		return $this->codFilial;
	}
	
	public function getNomeFilial(){
		return $this->nomeFilial;
	}
	
	public function setCodFilial($codFilial){
		$this->codFilial = $codFilial;
	}
	
	public function getCodGrupoEstabelecimento(){
		return $this->codGrupoEstabelecimento;
	}
	
	public function setCodGrupoEstabelecimento($codGrupoEstabelecimento){
		$this->codGrupoEstabelecimento = $codGrupoEstabelecimento;
	}

	public function getCodEstabelecimento(){
		return $this->codEstabelecimento;
	}
	
	public function setCodEstabelecimento($codEstabelecimento){
		$this->codEstabelecimento = $codEstabelecimento;
	}
	
	public function getCodTipoProcesso(){
		return $this->codTipoProcesso;
	}
	
	public function setCodTipoProcesso($codTipoProcesso){
		$this->codTipoProcesso = $codTipoProcesso;
	}
	
	public function encriptaSenha($senha){
		return md5(md5($senha));
	}
	
	// gera nova chave de token
	public function geraNovoToken($user, $pass){
		date_default_timezone_set("Brazil/East");
		$this->codToken =  sha1(sha1(date("dmY") . $user . $pass));
	}
	
	
	// verifica se o token passado é válido
	public function confereToken(){
		if (TRUE == $this->codToken){
			// instancia conexao
			$objConexao = $this->database;
			
			$strSql = "SELECT COUNT(COD_USUARIO) AS TOTAL 
					     FROM USUARIO 
					    WHERE COD_TOKEN = :codToken 
					      AND DTH_TOKEN = TRUNC(SYSDATE)";
				
			// parametros
			$arrayParametros = array();
			$arrayParametros[0][0]	= ":codToken";
			$arrayParametros[0][1]	= $this->codToken;

			$arrayUsuario	= $objConexao->executeFetch($strSql, $arrayParametros);
			
			if (TRUE == $arrayUsuario[0]["TOTAL"]){
				return true;
			}else{
				return false;
			}
			
		}else{
			return false;
		}
	}
	
	// verifica se o token passado acessa a imagem
	public function confereTokenDocumento($codDocumento){
		if (TRUE == $this->codToken && TRUE == $codDocumento){
			// instancia conexao
			$objConexao = $this->database;

			// VERIFICA SE O TOKEN PASSADO PODE ACESSAR O DOCUMENTO (POR FILIAL, GRUPO E POR ESTABEL)
			$strSql = "SELECT COUNT(D.COD_DOCUMENTO) AS TOTAL
			             FROM DOCUMENTO D
			            WHERE D.COD_DOCUMENTO = :codDocumento
					      AND D.IND_EXCLUIDO = 'N'
			              AND D.COD_ESTABELECIMENTO IN (SELECT E.COD_ESTABELECIMENTO
					                                      FROM USUARIO U
					                                      JOIN USUARIO_FILIAL UF ON (UF.COD_USUARIO = U.COD_USUARIO)
					                                      JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_FILIAL = UF.COD_FILIAL )
					                                      JOIN ESTABELECIMENTO E ON (E.COD_GRUPO_ESTABELECIMENTO = GE.COD_GRUPO_ESTABELECIMENTO )
					                                     WHERE U.COD_TOKEN = :codToken
					                                       AND U.DTH_TOKEN = TRUNC(SYSDATE)
														 UNION
					                                     SELECT E.COD_ESTABELECIMENTO
                                                           FROM USUARIO U
                                                           JOIN USUARIO_GRUPO_ESTABELECIMENTO UGE ON (UGE.COD_USUARIO = U.COD_USUARIO)
                                                           JOIN ESTABELECIMENTO E ON (E.COD_GRUPO_ESTABELECIMENTO = UGE.COD_GRUPO_ESTABELECIMENTO)
                                                          WHERE U.COD_TOKEN = :codToken
					                                        AND U.DTH_TOKEN = TRUNC(SYSDATE)
					                                     UNION
					                                    SELECT DISTINCT(UETP.COD_ESTABELECIMENTO) AS COD_ESTABELECIMENTO
					                                      FROM USUARIO U
					                                      JOIN USUARIO_ESTABELECIMENTO UETP ON (UETP.COD_USUARIO = U.COD_USUARIO)
					                                     WHERE U.COD_TOKEN = :codToken
					                                       AND U.DTH_TOKEN = TRUNC(SYSDATE))";
			
			// parametros
			$arrayParametros = array();
			$arrayParametros[0][0]	= ":codToken";
			$arrayParametros[0][1]	= $this->codToken;
			$arrayParametros[1][0]	= ":codDocumento";
			$arrayParametros[1][1]	= $codDocumento;
		
			$arrayUsuario	= $objConexao->executeFetch($strSql, $arrayParametros);
				
			if (TRUE == $arrayUsuario[0]["TOTAL"]){
				return true;
			}else{
				return false;
			}
				
		}else{
			return false;
		}
	}
	
	
	// verifica se o token acessa dados do estabelecimento
	public function confereTokenEstabelecimento(){
		if (TRUE == $this->codToken && TRUE == $this->codEstabelecimento){
			$objConexao = $this->database;
			
			$codUsuario = $this->getCodUsuarioToken();
			
			$strSql = "SELECT E.COD_ESTABELECIMENTO
  						 FROM USUARIO U
  						 JOIN USUARIO_FILIAL UF ON (UF.COD_USUARIO = U.COD_USUARIO)
  						 JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_FILIAL = UF.COD_FILIAL )
  						 JOIN ESTABELECIMENTO E ON (E.COD_GRUPO_ESTABELECIMENTO = GE.COD_GRUPO_ESTABELECIMENTO )
 						WHERE U.COD_USUARIO = :codUsuario
					    UNION
					   SELECT E.COD_ESTABELECIMENTO
                         FROM USUARIO U
                         JOIN USUARIO_GRUPO_ESTABELECIMENTO UGE ON (UGE.COD_USUARIO = U.COD_USUARIO)
                         JOIN ESTABELECIMENTO E ON (E.COD_GRUPO_ESTABELECIMENTO = UGE.COD_GRUPO_ESTABELECIMENTO)
					    WHERE U.COD_USUARIO = :codUsuario
 						UNION
					   SELECT DISTINCT(UETP.COD_ESTABELECIMENTO) AS COD_ESTABELECIMENTO
  						 FROM USUARIO U
  						 JOIN USUARIO_ESTABELECIMENTO UETP ON (UETP.COD_USUARIO = U.COD_USUARIO)
 						WHERE U.COD_USUARIO = :codUsuario";
			
			// parametros
			$arrayParametros = array();
			$arrayParametros[0][0]	= ":codUsuario";
			$arrayParametros[0][1]	= $codUsuario;
			
			$arrayDados	= $objConexao->executeFetch($strSql, $arrayParametros);
			
			$arrayEstabel = array();
			foreach ($arrayDados as $indice => $valor){
				$arrayEstabel[] = $valor['COD_ESTABELECIMENTO'];
			}
			
			if (in_array($this->codEstabelecimento, $arrayEstabel)){
				return true;
			}else{
				return false;
			}
		
		}else{
			return false;
		}
	}
	
	// pega o cod usuario pelo token
	public function getCodUsuarioToken(){
		if (TRUE == $this->codToken){
			// instancia conexao
			$objConexao = $this->database;
				
			$strSql = "SELECT COD_USUARIO FROM USUARIO WHERE COD_TOKEN = :codToken";
	
			// parametros
			$arrayParametros = array();
			$arrayParametros[0][0]	= ":codToken";
			$arrayParametros[0][1]	= $this->codToken;
	
			$arrayUsuario	= $objConexao->executeFetch($strSql, $arrayParametros);
				
			if (TRUE == $arrayUsuario[0]["COD_USUARIO"]){
				return $arrayUsuario[0]["COD_USUARIO"];
			}else{
				return false;
			}
				
		}else{
			return false;
		}
	}
	
	// update token
	public function changeToken(){
		if (TRUE == $this->codUsuario && TRUE == $this->codToken){
			// instancia conexao
			$objConexao = $this->database;
			
			$strSql = "UPDATE USUARIO SET COD_TOKEN = :codToken, 
					                      DTH_TOKEN = TRUNC(SYSDATE)
					    WHERE COD_USUARIO = :codUsuario";
			
			// parametros
			$arrayParametros = array();
			$arrayParametros[0][0]	= ":codToken";
			$arrayParametros[0][1]	= $this->codToken;
			$arrayParametros[1][0]	= ":codUsuario";
			$arrayParametros[1][1]	= $this->codUsuario;
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				return false;
			}
		
			return true;
		}else{
			return false;
		}
	}
	
	public function getUsuario($codUsuario){
		$objConexao = $this->database;
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ':codUsuario';
		$arrayParametros[0][1] = $codUsuario;
		
		$strSql = "SELECT U.DSC_IDENTIFICACAO_USUARIO, U.IND_ATIVO, U.COD_PAPEL_USUARIO, U.COD_TIPO_USUARIO,
                          E.COD_EMPRESA, E.NOM_EMPRESA, P.NOM_PESSOA, U.IND_LOGIN_SITE, U.COD_USUARIO,
                          (SELECT LISTAGG(PU.COD_PERFIL_USUARIO, ',') WITHIN GROUP (ORDER BY PU.COD_PERFIL_USUARIO) AS COD_PERFIL_USUARIO
                             FROM USUARIO_PERFIL_USUARIO PUP
                             JOIN PERFIL_USUARIO PU ON (PU.COD_PERFIL_USUARIO = PUP.COD_PERFIL_USUARIO)
                            WHERE COD_USUARIO = :codUsuario) AS COD_PERFIL_USUARIO, 
                         (SELECT LISTAGG(PU.DSC_PERFIL_USUARIO, ', ') WITHIN GROUP (ORDER BY PU.COD_PERFIL_USUARIO) AS DSC_PERFIL_USUARIO
                             FROM USUARIO_PERFIL_USUARIO PUP
                             JOIN PERFIL_USUARIO PU ON (PU.COD_PERFIL_USUARIO = PUP.COD_PERFIL_USUARIO)
                              WHERE COD_USUARIO = :codUsuario) AS DSC_PERFIL_USUARIO, 
                         (SELECT DECODE(COUNT(PU.IND_PERFIL_SISTEMA) ,0 , 'N', 'S') AS IND_PEFIL_SISTEMA
                            FROM USUARIO_PERFIL_USUARIO UPU
                            JOIN PERFIL_USUARIO PU ON (PU.COD_PERFIL_USUARIO = UPU.COD_PERFIL_USUARIO)
                           WHERE UPU.COD_USUARIO = :codUsuario
                             AND PU.IND_PERFIL_SISTEMA = 'S') AS COD_PERFIL_SISTEMA
                     FROM USUARIO U
                     JOIN PESSOA P ON (P.COD_PESSOA = U.COD_USUARIO)
                     JOIN PESSOA_FISICA PF ON (PF.COD_PESSOA = P.COD_PESSOA)
                     JOIN EMPRESA E ON (E.COD_EMPRESA = PF.COD_EMPRESA)
                    WHERE U.COD_USUARIO = :codUsuario";
		
		
		//$strSql = "SELECT U.DSC_IDENTIFICACAO_USUARIO, U.IND_ATIVO, U.COD_PAPEL_USUARIO, U.COD_TIPO_USUARIO,
		//		          E.COD_EMPRESA, E.NOM_EMPRESA, P.NOM_PESSOA, U.IND_LOGIN_SITE, U.COD_USUARIO,
		//		          UPU.COD_PERFIL_USUARIO, PU.DSC_PERFIL_USUARIO, PU.IND_PERFIL_SISTEMA
        //             FROM USUARIO U
		//		     JOIN PESSOA P ON (P.COD_PESSOA = U.COD_USUARIO)
  		//			 JOIN PESSOA_FISICA PF ON (PF.COD_PESSOA = P.COD_PESSOA)
  		//			 JOIN EMPRESA E ON (E.COD_EMPRESA = PF.COD_EMPRESA)
	  	//		     JOIN USUARIO_PERFIL_USUARIO UPU ON (UPU.COD_USUARIO = U.COD_USUARIO)
        //             JOIN PERFIL_USUARIO PU ON (PU.COD_PERFIL_USUARIO = UPU.COD_PERFIL_USUARIO)
		//		    WHERE U.COD_USUARIO = :codUsuario";
		
		$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
		
		$this->codUsuario		= $arrayUsuario[0]["COD_USUARIO"];
		$this->dscIdentificacaoUsuario = $arrayUsuario[0]["DSC_IDENTIFICACAO_USUARIO"];
		$this->indAtivo 		= $arrayUsuario[0]["IND_ATIVO"];
		$this->codPapelUsuario 	= $arrayUsuario[0]["COD_PAPEL_USUARIO"];
		$this->codTipoUsuario	= $arrayUsuario[0]["COD_TIPO_USUARIO"];
		$this->codEmpresa		= $arrayUsuario[0]["COD_EMPRESA"];
		$this->nomEmpresa		= $arrayUsuario[0]["NOM_EMPRESA"];
		$this->nomPessoa		= $arrayUsuario[0]["NOM_PESSOA"];
		$this->indLoginSite		= $arrayUsuario[0]["IND_LOGIN_SITE"];
		$this->codPerfilUsuario	= $arrayUsuario[0]["COD_PERFIL_USUARIO"];
		$this->indPerfilSistema	= $arrayUsuario[0]["IND_PERFIL_SISTEMA"];
		
		// filial
		if ($this->codTipoUsuario == 'F'){
			$arrayParametros = array();
			$arrayParametros[0][0] = ':codUsuario';
			$arrayParametros[0][1] = $codUsuario;
			
			$strSql = "SELECT COD_FILIAL
                        FROM USUARIO_FILIAL
				       WHERE COD_USUARIO = :codUsuario";
			
			$arrayFilial = $objConexao->executeFetch($strSql, $arrayParametros);
			
			$this->codFilial 				= $arrayFilial;
			$this->codGrupoEstabelecimento 	= 0;
			$this->codEstabelecimento 		= 0;
			
		// estabelecimento
		}elseif ($this->codTipoUsuario == 'G'){	
			// pega os grupos
			$arrayParametros = array();
			$arrayParametros[0][0] = ':codUsuario';
			$arrayParametros[0][1] = $codUsuario;
				
			$strSql = "SELECT UGE.COD_GRUPO_ESTABELECIMENTO, GE.COD_FILIAL 
                         FROM USUARIO_GRUPO_ESTABELECIMENTO UGE
                         JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_GRUPO_ESTABELECIMENTO = UGE.COD_GRUPO_ESTABELECIMENTO)
				        WHERE UGE.COD_USUARIO = :codUsuario";
				
			$arrayGrupo = $objConexao->executeFetch($strSql, $arrayParametros);
				
			$this->codFilial 				= $arrayGrupo[0]["COD_FILIAL"];
			$this->codGrupoEstabelecimento 	= $arrayGrupo;
			$this->codEstabelecimento 		= 0;
			
			
		// estabelecimento
		}elseif ($this->codTipoUsuario == 'E'){
			$arrayParametros = array();
			$arrayParametros[0][0] = ':codUsuario';
			$arrayParametros[0][1] = $codUsuario;
			
			$strSql = "SELECT DISTINCT(UETP.COD_ESTABELECIMENTO) AS COD_ESTABELECIMENTO, GE.COD_GRUPO_ESTABELECIMENTO, F.COD_FILIAL
                         FROM USUARIO_ESTABELECIMENTO UETP
  						 JOIN ESTABELECIMENTO E ON (E.COD_ESTABELECIMENTO = UETP.COD_ESTABELECIMENTO)
  						 JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_GRUPO_ESTABELECIMENTO = E.COD_GRUPO_ESTABELECIMENTO)
  						 JOIN FILIAL F ON (F.COD_FILIAL = GE.COD_FILIAL)
 						WHERE UETP.COD_USUARIO = :codUsuario";
				
			$arrayEstabel = $objConexao->executeFetch($strSql, $arrayParametros);
			
			$this->codFilial = $arrayEstabel[0]["COD_FILIAL"];
			$this->codGrupoEstabelecimento = $arrayEstabel[0]["COD_GRUPO_ESTABELECIMENTO"];
			$this->codEstabelecimento = $arrayEstabel[0]["COD_ESTABELECIMENTO"];
			
						
			if ($this->indPerfilSistema == "S"){
				 $strSql = "SELECT COD_TIPO_PROCESSO
                             FROM TIPO_PROCESSO TP
                             WHERE TP.COD_EMPRESA = :COD_EMPRESA;";
				
				 $arrayParametros = array();
				 $arrayParametros[0][0] = ':COD_EMPRESA';
				 $arrayParametros[0][1] = $this->codEmpresa;
				
			}else{
				$strSql = "SELECT COD_TIPO_PROCESSO
				             FROM USUARIO_PERFIL_USUARIO UPU
				             JOIN PERFIL_USUARIO PU ON (PU.COD_PERFIL_USUARIO = UPU.COD_PERFIL_USUARIO )
				             JOIN PERFIL_USUARIO_TIPO_PROCESSO PUTP ON (PUTP.COD_PERFIL_USUARIO = PU.COD_PERFIL_USUARIO)
				            WHERE UPU.COD_USUARIO = :COD_USUARIO";
				 $arrayParametros = array();
				 $arrayParametros[0][0] = ':COD_USUARIO';
				 $arrayParametros[0][1] = $codUsuario;
			}
			
			$arrayProcessos = $objConexao->executeFetch($strSql, $arrayParametros);
			$this->codTipoProcesso = $arrayProcessos;
		
		// se for admin
		}else{
			$this->codFilial = 0;
			$this->codGrupoEstabelecimento = 0;
			$this->codEstabelecimento = 0;
		}		
		
		return true;
	}
	
	public function insUsuario(){
		$objConexao = $this->database;
		//$objConexao->beginTrans();
		
		// cria usuario
		$strSql = "INSERT INTO USUARIO
					           (COD_USUARIO, DSC_IDENTIFICACAO_USUARIO, DSC_SENHA_ACESSO, IND_ATIVO, COD_TIPO_USUARIO, IND_LOGIN_SITE)
					   VALUES (:codUsuario, :dscIdentificacao_usuario, :dscSenhaAcesso, :indAtivo, :codTipoUsuario, :indLoginSite)";
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codUsuario";
		$arrayParametros[0][1] = $this->codUsuario;
		$arrayParametros[1][0] = ":dscIdentificacao_usuario";
		$arrayParametros[1][1] = $this->dscIdentificacaoUsuario;
		$arrayParametros[2][0] = ":dscSenhaAcesso";
		$arrayParametros[2][1] = $this->encriptaSenha(strtoupper($this->dscIdentificacaoUsuario));
		$arrayParametros[3][0] = ":indAtivo";
		$arrayParametros[3][1] = $this->indAtivo;
		$arrayParametros[4][0] = ":codTipoUsuario";
		$arrayParametros[4][1] = $this->codTipoUsuario;
		$arrayParametros[5][0] = ":indLoginSite";
		$arrayParametros[5][1] = $this->indLoginSite;
		
		if (!$objConexao->execute($strSql, $arrayParametros)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro ao inserir usuario";
			//$objConexao->rollbackTrans();
			return false;
		}
		
		
		foreach ($this->codPerfilUsuario as $codPerfilUsuario){
			// cria perfil
			$strSql = "INSERT INTO USUARIO_PERFIL_USUARIO
						           (COD_USUARIO, COD_PERFIL_USUARIO)
						   VALUES (:codUsuario, :codPerfilUsuario)";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $this->codUsuario;
			$arrayParametros[1][0] = ":codPerfilUsuario";
			$arrayParametros[1][1] = $codPerfilUsuario;
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao inserir perfil do usuario";
				//$objConexao->rollbackTrans();
				return false;
			}
		}
		//cria usuario administrador
		//if ($this->codTipoUsuario == 'A'){
			// nada por enquanto
		//}
		
		//cria usuario filial
		if ($this->codTipoUsuario == 'F'){
			
			foreach ($this->codFilial as $codFilial){
				$strSql = "INSERT INTO USUARIO_FILIAL
						           (COD_USUARIO, COD_FILIAL)
						   VALUES (:codUsuario, :codFilial)";
				
				$arrayParametros = array();
				$arrayParametros[0][0] = ":codUsuario";
				$arrayParametros[0][1] = $this->codUsuario;
				$arrayParametros[1][0] = ":codFilial";
				$arrayParametros[1][1] = $codFilial;
				
				if (!$objConexao->execute($strSql, $arrayParametros)){
					$this->error['code'] 	= "";
					$this->error['message'] = "Erro ao inserir usuario filial";
					//$objConexao->rollbackTrans();
					return false;
				}
			}
		}
		
		// cria grupo de estabelecimento
		elseif ($this->codTipoUsuario == 'G'){
			
			foreach ($this->codGrupoEstabelecimento as $codGrupoEstabelecimento){
				
				$strSql = "INSERT INTO USUARIO_GRUPO_ESTABELECIMENTO (COD_USUARIO, COD_GRUPO_ESTABELECIMENTO)
						   VALUES (:codUsuario, :codGrupoEstabelecimento)";
				
				$arrayParametros = array();
				$arrayParametros[0][0] = ":codUsuario";
				$arrayParametros[0][1] = $this->codUsuario;
				$arrayParametros[1][0] = ":codGrupoEstabelecimento";
				$arrayParametros[1][1] = $codGrupoEstabelecimento;
					
				if (!$objConexao->execute($strSql, $arrayParametros)){
					$this->error['code'] 	= "";
					$this->error['message'] = "Erro ao incluir grupo de estabelecimento do usuário";
					//$objConexao->rollbackTrans();
					return false;
				}
			}

		}
		
		// cria usuario estabelecimento
		elseif ($this->codTipoUsuario == 'E'){
					
			//INCLUI O USUARIO NA TABELA USUARIO ESTABELECIMENTO
			$strSql = "INSERT INTO USUARIO_ESTABELECIMENTO (COD_USUARIO, COD_ESTABELECIMENTO)
							                        VALUES (:codUsuario, :codEstabelecimento)";
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $this->codUsuario;
			$arrayParametros[1][0] = ":codEstabelecimento";
			$arrayParametros[1][1] = $this->codEstabelecimento;
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao incluir usuario estabelecimento";
				//$objConexao->rollbackTrans();
				return false;
			}
			
			
		}else{
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro ao verificar tipo de usuário";
			//$objConexao->rollbackTrans();
			return false;
		}
		
		//$objConexao->commitTrans();
		return true;
	}
	
	public function changeUsuario(){
		if(TRUE == $this->codUsuario){
			
			$objConexao = $this->database;
			$objConexao->beginTrans();
			
			// altera usuario (zera o token por segurança, obriga a relogar no ws após alteração)
			$strSql = "UPDATE USUARIO 
					      SET DSC_IDENTIFICACAO_USUARIO = :dscIdentificacao_usuario, 
					          IND_ATIVO = :indAtivo,
							  COD_TIPO_USUARIO = :codTipoUsuario,
					          COD_TOKEN = ''
					    WHERE COD_USUARIO = :codUsuario";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $this->codUsuario;
			$arrayParametros[1][0] = ":dscIdentificacao_usuario";
			$arrayParametros[1][1] = $this->dscIdentificacaoUsuario;
			$arrayParametros[2][0] = ":indAtivo";
			$arrayParametros[2][1] = $this->indAtivo;
			$arrayParametros[3][0] = ":codTipoUsuario";
			$arrayParametros[3][1] = $this->codTipoUsuario;
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar usuario";
				$objConexao->rollbackTrans();
				return false;
			}
			
					
			// deleta filial
			$strSql = "DELETE
						 FROM USUARIO_FILIAL
					    WHERE COD_USUARIO = :codUsuario";
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $this->codUsuario;
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar usuario filial na alteração";
				$objConexao->rollbackTrans();
				return false;
			}
			
			// deleta usuario grupo estabelecimento
			$strSql = "DELETE
						 FROM USUARIO_GRUPO_ESTABELECIMENTO
					    WHERE COD_USUARIO = :codUsuario";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $this->codUsuario;
				
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar usuario grupo estabelecimento na alteração";
				$objConexao->rollbackTrans();
				return false;
			}
			
			// deleta perfil usuario
			$strSql = "DELETE
						 FROM USUARIO_PERFIL_USUARIO
					    WHERE COD_USUARIO = :codUsuario";
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $this->codUsuario;
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar perfil do usuario na alteração";
				$objConexao->rollbackTrans();
				return false;
			}
			
			//DELETA O USUARIO ESTABELECIMENTO
			$strSql = "DELETE
					    FROM USUARIO_ESTABELECIMENTO
					   WHERE COD_USUARIO = :codUsuario";
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $this->codUsuario;
				
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar os estabelecimentos do usuário na alteração";
				$objConexao->rollbackTrans();
				return false;
			}
			
			//cria usuario administrador
			if ($this->codTipoUsuario == 'A'){
				// nada por enquanto
			}
			
			//cria usuario filial
			elseif ($this->codTipoUsuario == 'F'){
					
				foreach ($this->codFilial as $codFilial){
					$strSql = "INSERT INTO USUARIO_FILIAL
						           (COD_USUARIO, COD_FILIAL)
						   VALUES (:codUsuario, :codFilial)";
			
					$arrayParametros = array();
					$arrayParametros[0][0] = ":codUsuario";
					$arrayParametros[0][1] = $this->codUsuario;
					$arrayParametros[1][0] = ":codFilial";
					$arrayParametros[1][1] = $codFilial;
			
					if (!$objConexao->execute($strSql, $arrayParametros)){
						$this->error['code'] 	= "";
						$this->error['message'] = "Erro ao alterar usuario filial";
						$objConexao->rollbackTrans();
						return false;
					}
				}
			}
			
			// cria grupo de estabelecimento
			elseif ($this->codTipoUsuario == 'G'){
					
				foreach ($this->codGrupoEstabelecimento as $codGrupoEstabelecimento){
			
					$strSql = "INSERT INTO USUARIO_GRUPO_ESTABELECIMENTO (COD_USUARIO, COD_GRUPO_ESTABELECIMENTO)
						   VALUES (:codUsuario, :codGrupoEstabelecimento)";
			
					$arrayParametros = array();
					$arrayParametros[0][0] = ":codUsuario";
					$arrayParametros[0][1] = $this->codUsuario;
					$arrayParametros[1][0] = ":codGrupoEstabelecimento";
					$arrayParametros[1][1] = $codGrupoEstabelecimento;
						
					if (!$objConexao->execute($strSql, $arrayParametros)){
						$this->error['code'] 	= "";
						$this->error['message'] = "Erro ao alterar grupo de estabelecimento do usuário";
						$objConexao->rollbackTrans();
						return false;
					}
				}
			
			}
			
			// cria usuario estabelecimento
			elseif ($this->codTipoUsuario == 'E'){
								
				//INCLUI O USUARIO NA TABELA USUARIO ESTABELECIMENTO
				$strSql = "INSERT INTO USUARIO_ESTABELECIMENTO (COD_USUARIO, COD_ESTABELECIMENTO)
							                        VALUES (:codUsuario, :codEstabelecimento)";
				$arrayParametros = array();
				$arrayParametros[0][0] = ":codUsuario";
				$arrayParametros[0][1] = $this->codUsuario;
				$arrayParametros[1][0] = ":codEstabelecimento";
				$arrayParametros[1][1] = $this->codEstabelecimento;
					
				if (!$objConexao->execute($strSql, $arrayParametros)){
					$this->error['code'] 	= "";
					$this->error['message'] = "Erro ao incluir usuario estabelecimento";
					$objConexao->rollbackTrans();
					return false;
				}
				
			}
			
			foreach ($this->codPerfilUsuario as $codPerfilUsuario){
			
				// cria perfil usuario
				$strSql = "INSERT INTO USUARIO_PERFIL_USUARIO (COD_USUARIO, COD_PERFIL_USUARIO)
						   VALUES (:codUsuario, :codPerfilUsuario)";
					
				$arrayParametros = array();
				$arrayParametros[0][0] = ":codUsuario";
				$arrayParametros[0][1] = $this->codUsuario;
				$arrayParametros[1][0] = ":codPerfilUsuario";
				$arrayParametros[1][1] = $codPerfilUsuario;
				
				if (!$objConexao->execute($strSql, $arrayParametros)){
					$this->error['code'] 	= "";
					$this->error['message'] = "Erro ao incluir perfil do usuário na alteração";
					$objConexao->rollbackTrans();
					return false;
				}
			}
			$objConexao->commitTrans();
			return true;
		}else{
			return false;
		}
	}
	
	public function delUsuario($codUsuario){
		
		if (TRUE == $codUsuario) {
			$objConexao = $this->database;
			$objConexao->beginTrans();
		
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $codUsuario;
								
			// deleta ligação de usuário grupo estabelecimento
			$strSql = "DELETE
					     FROM USUARIO_GRUPO_ESTABELECIMENTO
					    WHERE COD_USUARIO = :codUsuario";
				
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar grupo de estabelecimento do usuário";
				$objConexao->rollbackTrans();
				return false;
			}
			
			// deleta ligacao de usuário filial
			$strSql = "DELETE
					     FROM USUARIO_FILIAL
					    WHERE COD_USUARIO = :codUsuario";
		
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar usuário filial";
				$objConexao->rollbackTrans();
				return false;
			}
			
			// deleta ligacao de perfil
			$strSql = "DELETE
					     FROM USUARIO_PERFIL_USUARIO
					    WHERE COD_USUARIO = :codUsuario";
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar perfil do usuário";
				$objConexao->rollbackTrans();
				return false;
			}
			
			// deleta usuario
			$strSql = "DELETE
					     FROM USUARIO
					    WHERE COD_USUARIO = :codUsuario";
				
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar usuário";
				$objConexao->rollbackTrans();
				return false;
			}
		
			$objConexao->commitTrans();
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "variável não setada";
			return false;
		}
		
	}
	
	public function ativaUsuario($codUsuario){
		$objConexao = $this->database;
		
		$strSql = "UPDATE USUARIO SET IND_ATIVO = 'S' 
				    WHERE COD_USUARIO = :codUsuario";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codUsuario";
		$arrayParametros[0][1] = $codUsuario;
		
		if (!$objConexao->execute($strSql, $arrayParametros)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro ao ativar usuário";
			$objConexao->rollbackTrans();
			return false;
		}
		
		return true;
	}
	
	public function desativaUsuario($codUsuario){
		$objConexao = $this->database;
		
		$strSql = "UPDATE USUARIO SET IND_ATIVO = 'N'
				    WHERE COD_USUARIO = :codUsuario";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codUsuario";
		$arrayParametros[0][1] = $codUsuario;
		
		if (!$objConexao->execute($strSql, $arrayParametros)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro ao desativar usuário";
			$objConexao->rollbackTrans();
			return false;
		}
		
		return true;
	}
	
	public function totalUsuario(){
		$objConexao = $this->database;
		
		$strSql = "SELECT COUNT(COD_USUARIO) AS TOTAL FROM USUARIO";
		$arrayDados = $objConexao->executeFetch($strSql);
		return $arrayDados[0]["TOTAL"];
	}
	
	public function totalUsuarioEmpresa(){
		$objConexao = $this->database;
	
		$strSql = "SELECT COUNT(U.COD_USUARIO) AS TOTAL 
				     FROM USUARIO U
				     JOIN PESSOA_FISICA PF ON (PF.COD_PESSOA = U.COD_USUARIO)
				    WHERE PF.COD_EMPRESA = ".$_SESSION['codEmpresa'];
		$arrayDados = $objConexao->executeFetch($strSql);
		return $arrayDados[0]["TOTAL"];
	}
	
	public function changePerfilUsuario($codUsuario, $arrCodPerfil){
	
		if ($codUsuario) {
			$objConexao = $this->database;
				
			$objConexao->beginTrans();
	
			// deleta perfis do usuário
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $codUsuario;
	
			$strSql = "DELETE
            		     FROM USUARIO_PERFIL_USUARIO
            		    WHERE COD_USUARIO =:codUsuario";
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$objConexao->rollbackTrans();
				return false;
			}
				
			// insere novos perfis do usuario
				
			foreach ($arrCodPerfil as $codPerfil){
	
				$arrayParametros = array();
				$arrayParametros[0][0] = ":codUsuario";
				$arrayParametros[0][1] = $codUsuario;
				$arrayParametros[1][0] = ":codPerfil";
				$arrayParametros[1][1] = $codPerfil;
	
				$strSql = "INSERT INTO USUARIO_PERFIL_USUARIO (COD_PERFIL_USUARIO, COD_USUARIO)
						                               VALUES (:codPerfil, :codUsuario)";
	
				if (!$objConexao->execute($strSql, $arrayParametros)){
					$objConexao->rollbackTrans();
					return false;
				}
			}
				
				
			$objConexao->commitTrans();
	
			return true;
		} else {
			return false;
		}
	}
	
	public function loginExists(){
		$objConexao = $this->database;
	
		$strSql = "SELECT COUNT(COD_USUARIO) AS TOTAL
					 FROM USUARIO
	                WHERE DSC_IDENTIFICACAO_USUARIO = :dscIdentificacaoUsuario";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ':dscIdentificacaoUsuario';
		$arrayParametros[0][1] = $this->dscIdentificacaoUsuario;
	
		$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
		return $arrayUsuario[0]['TOTAL'];
	}
	
	/**
	 * Retorna o codigo do usuário ou false se não encontrar
	 * @return boolean, integer
	 */
	public function getCodUsuarioByLogin(){
		if ($this->dscIdentificacaoUsuario) {
			$objConexao = $this->database;
			
			$strSql = "SELECT COD_USUARIO
					     FROM USUARIO
	                    WHERE DSC_IDENTIFICACAO_USUARIO = :dscIdentificacaoUsuario";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ':dscIdentificacaoUsuario';
			$arrayParametros[0][1] = $this->dscIdentificacaoUsuario;
			
			$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
			return $arrayUsuario[0]['COD_USUARIO'];
			
		}else{
			return false;
		}
	}
	
	public function changeSenha($senhaAtual, $novaSenha){
		if (!$senhaAtual || !$novaSenha) {
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro ao receber variáveis";
			return false;
		}
		
		$objConexao = $this->database;
		
		// verifica se a senha atual esta correta
		$strSql = "SELECT COUNT(*) AS TOTAL
				    FROM USUARIO
				   WHERE COD_USUARIO = :codUsuario
				     AND DSC_SENHA_ACESSO = :dscSenhaAcesso";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ':codUsuario';
		$arrayParametros[0][1] = $_SESSION['codUsuario'];
		$arrayParametros[1][0] = ':dscSenhaAcesso';
		$arrayParametros[1][1] = $this->encriptaSenha($senhaAtual);
		
		$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);

		if ($arrayUsuario[0]['TOTAL'] == 0){
			$this->error['code'] 	= "";
			$this->error['message'] = "Senha atual inválida";
			return false;
		}
		
		// verifica se o usuario pode fazer login no site
		$strSql = "SELECT COUNT(*) AS TOTAL
				    FROM USUARIO
				   WHERE COD_USUARIO = :codUsuario
				     AND IND_LOGIN_SITE = :indLoginSite";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ':codUsuario';
		$arrayParametros[0][1] = $_SESSION['codUsuario'];
		$arrayParametros[1][0] = ':indLoginSite';
		$arrayParametros[1][1] = 'S';
		
		$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
		
		if ($arrayUsuario[0]['TOTAL'] == 0){
			$this->error['code'] 	= "";
			$this->error['message'] = "Seu usuário não possui permissão para alterar a senha";
			return false;
		}
		
		// altera a senha
		$strSql = "UPDATE USUARIO
				      SET DSC_SENHA_ACESSO = :dscSenhaAcesso
				    WHERE COD_USUARIO = :codUsuario";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ':codUsuario';
		$arrayParametros[0][1] = $_SESSION['codUsuario'];
		$arrayParametros[1][0] = ':dscSenhaAcesso';
		$arrayParametros[1][1] = $this->encriptaSenha($novaSenha);
		
		if (!$objConexao->execute($strSql, $arrayParametros)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Ocorreu um erro ao alterar a senha do usuário";
			return false;
		}else{
			return true;
		}
		
	}
	
	public function reiniciaSenha($codUsuario){
		if (!$codUsuario) {
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro ao receber variáveis";
			return false;
		}
	
		$objConexao = $this->database;
		
		// verifica se o usuario pode fazer login no site
		$strSql = "SELECT COUNT(*) AS TOTAL
				    FROM USUARIO
				   WHERE COD_USUARIO = :codUsuario
				     AND IND_LOGIN_SITE = :indLoginSite";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ':codUsuario';
		$arrayParametros[0][1] = $codUsuario;
		$arrayParametros[1][0] = ':indLoginSite';
		$arrayParametros[1][1] = 'S';
		
		$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
		
		if ($arrayUsuario[0]['TOTAL'] == 0){
			$this->error['code'] 	= "";
			$this->error['message'] = "Este usuário não pode alterar a senha (Login automático)";
			return false;
		}
	
		// pega o login do usuario
		$strSql = "SELECT UPPER(DSC_IDENTIFICACAO_USUARIO) AS DSC_IDENTIFICACAO_USUARIO 
				    FROM USUARIO
				   WHERE COD_USUARIO = :codUsuario";
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ':codUsuario';
		$arrayParametros[0][1] = $codUsuario;
	
		$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
	
		// altera a senha
		$strSql = "UPDATE USUARIO
				      SET DSC_SENHA_ACESSO = :dscSenhaAcesso
				    WHERE COD_USUARIO = :codUsuario";
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ':codUsuario';
		$arrayParametros[0][1] = $codUsuario;
		$arrayParametros[1][0] = ':dscSenhaAcesso';
		$arrayParametros[1][1] = $this->encriptaSenha($arrayUsuario[0]['DSC_IDENTIFICACAO_USUARIO']);
	
		if (!$objConexao->execute($strSql, $arrayParametros)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Ocorreu um erro ao reiniciar a senha do usuário";
			return false;
		}else{
			return true;
		}
	
	}
	
	/**
	 * Retorna os dados dos Usuarios encontrados pelo Nome
	 * @return boolean, integer
	 */
	public function buscaUsuarioNomePessoa(){
		if ($this->nomPessoa && $this->codEmpresa ) {
			$objConexao = $this->database;
				
			$strSql = "SELECT U.COD_USUARIO, U.DSC_IDENTIFICACAO_USUARIO, P.NOM_PESSOA, PF.NUM_CPF 
					     FROM USUARIO U
					     JOIN PESSOA P ON (P.COD_PESSOA = U.COD_USUARIO)
					     JOIN PESSOA_FISICA PF ON (PF.COD_PESSOA = U.COD_USUARIO)
	                    WHERE P.NOM_PESSOA LIKE '%'|| :NOM_PESSOA ||'%'
					      AND PF.COD_EMPRESA = :COD_EMPRESA";
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ':NOM_PESSOA';
			$arrayParametros[0][1] = $this->nomPessoa;
			$arrayParametros[1][0] = ':COD_EMPRESA';
			$arrayParametros[1][1] = $this->codEmpresa;
		
			$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
			return $arrayUsuario;
				
		}else{
			return false;
		}
	}
	
	/**
	 * Retorna os dados do Usuario pelo DSC_IDENTIFICACAO_USUARIO
	 * @return Dados dos usuarios encontrados
	 */
	public function buscaUsuarioDscIdentificacao(){
		if ($this->dscIdentificacaoUsuario && $this->codEmpresa ) {
			$objConexao = $this->database;
	
			$strSql = "SELECT U.COD_USUARIO, U.DSC_IDENTIFICACAO_USUARIO, P.NOM_PESSOA
					     FROM USUARIO U
					     JOIN PESSOA P ON (P.COD_PESSOA = U.COD_USUARIO)
					     JOIN PESSOA_FISICA PF ON (PF.COD_PESSOA = U.COD_USUARIO)
	                    WHERE U.DSC_IDENTIFICACAO_USUARIO = '%'|| :DSC_IDENTIFICACAO_USUARIO ||'%'
					      AND PF.COD_EMPRESA = :COD_EMPRESA";
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ':DSC_IDENTIFICACAO_USUARIO';
			$arrayParametros[0][1] = $this->dscIdentificacaoUsuario;
			$arrayParametros[1][0] = ':COD_EMPRESA';
			$arrayParametros[1][1] = $this->codEmpresa;
	
			$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
			return $arrayUsuario;
	
		}else{
			return false;
		}
	}
	
	/**
	 * Retorna os dados dos Usuarios encontrados pelo Nome ou Identificação
	 * @return boolean, integer
	 */
	public function buscaUsuarioNomePessoaIdentificacao(){
		if ($this->nomPessoa && $this->codEmpresa ) {
			$objConexao = $this->database;
	
			$strSql = "SELECT U.COD_USUARIO, U.DSC_IDENTIFICACAO_USUARIO, P.NOM_PESSOA, PF.NUM_CPF
					     FROM USUARIO U
					     JOIN PESSOA P ON (P.COD_PESSOA = U.COD_USUARIO)
					     JOIN PESSOA_FISICA PF ON (PF.COD_PESSOA = U.COD_USUARIO)
	                    WHERE (P.NOM_PESSOA LIKE '%'|| :NOM_PESSOA ||'%'
					       OR U.DSC_IDENTIFICACAO_USUARIO LIKE '%'|| :NOM_PESSOA ||'%')
					      AND PF.COD_EMPRESA = :COD_EMPRESA";
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ':NOM_PESSOA';
			$arrayParametros[0][1] = $this->nomPessoa;
			$arrayParametros[1][0] = ':COD_EMPRESA';
			$arrayParametros[1][1] = $this->codEmpresa;
	
			$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
			return $arrayUsuario;
	
		}else{
			return false;
		}
	}
	
	/** INSERE NOTIFICACAO PARA O USUARIO DE UM GRUPO DE ESTABELECIMENTO
	 * @param integer codUsuario
	 * @param integer codGrupoEstabelecimento
	 * @return boolean
	 */
	public function insereNotificacaoGrest(){
		if (TRUE == $this->codUsuario && TRUE == $this->codGrupoEstabelecimento ) {
	
			$objConexao = $this->database;
				
			// insere processo_campo
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $this->codUsuario;
			$arrayParametros[1][0] = ":codGrupoEstabelecimento";
			$arrayParametros[1][1] = $this->codGrupoEstabelecimento;
			
	
			$strSql = "INSERT INTO NOTIFICACAO_GREST
						          (COD_USUARIO, COD_GRUPO_ESTABELECIMENTO)
						   VALUES (:codUsuario, :codGrupoEstabelecimento)";
				
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$returnedError = $objConexao->__get('error');
				$this->error['code'] 	= $returnedError['code'];
				$this->error['message'] = "Erro ao inserir notificação por grupo de estabelecimento\n".$returnedError['message'];
				return false;
			}
	
			return true;
		} else {
				
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados obrigatórios não foram informados";
			return false;
		}
	}
	
	/** LISTA OS USUARIO CADASTRADOS PARA RECEBER NOTIFICACAO GRUPO DE ESTABELECIMENTO
	 * @param integer codEmpresa
	 * @param string nomPessoa
	 * @return array
	 */
	public function listaUsuariosNotificacaoGrest(){
		if ($this->codEmpresa ) {
			
			$objConexao = $this->database;
			
			$arrayParametros = array();
			
			if (trim($this->nomPessoa) == ""){
				$strSql = "SELECT U.COD_USUARIO, U.DSC_IDENTIFICACAO_USUARIO, P.NOM_PESSOA, NG.COD_GRUPO_ESTABELECIMENTO, GE.NOM_GRUPO_ESTABELECIMENTO
						     FROM NOTIFICACAO_GREST NG
						     JOIN USUARIO U ON (U.COD_USUARIO = NG.COD_USUARIO)
						     JOIN PESSOA P ON (P.COD_PESSOA = U.COD_USUARIO)
						     JOIN PESSOA_FISICA PF ON (PF.COD_PESSOA = U.COD_USUARIO)
							 JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_GRUPO_ESTABELECIMENTO = NG.COD_GRUPO_ESTABELECIMENTO)
		                    WHERE PF.COD_EMPRESA = :COD_EMPRESA
						ORDER BY GE.NOM_GRUPO_ESTABELECIMENTO, P.NOM_PESSOA, U.DSC_IDENTIFICACAO_USUARIO";
				
			}else{
				$strSql = "SELECT U.COD_USUARIO, U.DSC_IDENTIFICACAO_USUARIO, P.NOM_PESSOA, NG.COD_GRUPO_ESTABELECIMENTO, GE.NOM_GRUPO_ESTABELECIMENTO
						     FROM NOTIFICACAO_GREST NG
						     JOIN USUARIO U ON (U.COD_USUARIO = NG.COD_USUARIO)
						     JOIN PESSOA P ON (P.COD_PESSOA = U.COD_USUARIO)
						     JOIN PESSOA_FISICA PF ON (PF.COD_PESSOA = U.COD_USUARIO)
						     JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_GRUPO_ESTABELECIMENTO = NG.COD_GRUPO_ESTABELECIMENTO)
		                    WHERE (P.NOM_PESSOA LIKE '%'|| :NOM_PESSOA ||'%'
						       OR U.DSC_IDENTIFICACAO_USUARIO LIKE '%'|| :NOM_PESSOA ||'%')
						      AND PF.COD_EMPRESA = :COD_EMPRESA
						    ORDER BY GE.NOM_GRUPO_ESTABELECIMENTO, P.NOM_PESSOA, U.DSC_IDENTIFICACAO_USUARIO";
				
				$arrayParametros[1][0] = ':NOM_PESSOA';
				$arrayParametros[1][1] = $this->nomPessoa;

			}
			
			$arrayParametros[0][0] = ':COD_EMPRESA';
			$arrayParametros[0][1] = $this->codEmpresa;
			$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
			return $arrayUsuario;
		}else{
			return false;
		}
	}
	
	/** DELETE USUARIO CADASTRADO PARA RECEBER NOTIFICACAO GRUPO DE ESTABELECIMENTO
	 * @param integer codGrupoEstabelecimento
	 * @param integer codUsuario
	 * @return boolean
	 */
	public function deletaUsuarioNotificacaoGrest(){
		
		if (TRUE == $this->codUsuario && TRUE == $this->codGrupoEstabelecimento ) {
			$objConexao = $this->database;
			
			$strSql = "DELETE FROM NOTIFICACAO_GREST 
					    WHERE COD_USUARIO = :codUsuario
					          AND COD_GRUPO_ESTABELECIMENTO = :codGrupoEstabelecimento";
		
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $this->codUsuario;
			$arrayParametros[1][0] = ":codGrupoEstabelecimento";
			$arrayParametros[1][1] = $this->codGrupoEstabelecimento;
		
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao excluir usuário";
				$objConexao->rollbackTrans();
				return false;
			}
		
			return true;
		} else {
		
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados obrigatórios não foram informados";
			return false;
		}
	}
	
	/** INSERE NOTIFICACAO PARA O USUARIO DE UM ESTABELECIMENTO TIPO PROCESSO TIPO DOCUMENTO
	 * @param integer codUsuario
	 * @param integer codEstabelecimento
	 * @param integer codTipoProcesso
	 * @param integer codTipoDocumento
	 * @return boolean
	 */
	public function insereNotificacaoEstbTpDoc(){
		if (TRUE == $this->codUsuario && TRUE == $this->codEstabelecimento && TRUE == $this->codTipoProcesso && TRUE == $this->codTipoDocumento  ) {
	
			$objConexao = $this->database;
	
			// insere processo_campo
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $this->codUsuario;
			$arrayParametros[1][0] = ":codEstabelecimento";
			$arrayParametros[1][1] = $this->codEstabelecimento;
			$arrayParametros[2][0] = ":codTipoProcesso";
			$arrayParametros[2][1] = $this->codTipoProcesso;
			$arrayParametros[3][0] = ":codTipoDocumento";
			$arrayParametros[3][1] = $this->codTipoDocumento;
				
	
			$strSql = "INSERT INTO NOTIFICACAO_ESTABEL_TPRTD
						          (COD_USUARIO, COD_ESTABELECIMENTO, COD_TIPO_PROCESSO, COD_TIPO_DOCUMENTO)
						   VALUES (:codUsuario, :codEstabelecimento, :codTipoProcesso, :codTipoDocumento)";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$returnedError = $objConexao->__get('error');
				$this->error['code'] 	= $returnedError['code'];
				$this->error['message'] = "Erro ao inserir notificação por estabelecimento tipo processo tipo documento\n".$returnedError['message'];
				return false;
			}
	
			return true;
		} else {
	
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados obrigatórios não foram informados";
			return false;
		}
	}
	
	/** LISTA OS USUARIO CADASTRADOS PARA RECEBER NOTIFICACAO GRUPO DE ESTABELECIMENTO
	 * @param integer codEmpresa
	 * @param string nomPessoa
	 * @return array
	 */
	public function listaUsuariosNotificacaoEstabTpDoc(){
		if ($this->codEmpresa ) {
				
			$objConexao = $this->database;
				
			$arrayParametros = array();
				
			if (trim($this->nomPessoa) == ""){
				$strSql = " SELECT U.COD_USUARIO, U.DSC_IDENTIFICACAO_USUARIO, P.NOM_PESSOA, NE.COD_ESTABELECIMENTO, 
                                   NVL(PJE.NOM_FANTASIA, PFE.DSC_APELIDO) AS NOM_ESTABELECIMENTO,
                                   NE.COD_TIPO_DOCUMENTO, TD.DSC_TIPO_DOCUMENTO, NE.COD_TIPO_PROCESSO, TP.DSC_TIPO_PROCESSO
                              FROM NOTIFICACAO_ESTABEL_TPRTD NE
                              JOIN USUARIO U ON (U.COD_USUARIO = NE.COD_USUARIO)
                              JOIN PESSOA P ON (P.COD_PESSOA = U.COD_USUARIO)
                              JOIN PESSOA_FISICA PF ON (PF.COD_PESSOA = U.COD_USUARIO)
                              JOIN ESTABELECIMENTO E ON (E.COD_ESTABELECIMENTO = NE.COD_ESTABELECIMENTO)
                              JOIN PESSOA PE ON (PE.COD_PESSOA = E.COD_ESTABELECIMENTO)
                   LEFT OUTER JOIN PESSOA_JURIDICA PJE ON (PJE.COD_PESSOA = NE.COD_ESTABELECIMENTO)
                   LEFT OUTER JOIN PESSOA_FISICA PFE ON (PFE.COD_PESSOA = NE.COD_ESTABELECIMENTO)
                              JOIN TIPO_PROCESSO TP ON (TP.COD_TIPO_PROCESSO = NE.COD_TIPO_PROCESSO)
                              JOIN TIPO_DOCUMENTO TD ON (TD.COD_TIPO_DOCUMENTO = NE.COD_TIPO_DOCUMENTO)
                        WHERE PF.COD_EMPRESA = :COD_EMPRESA
						ORDER BY P.NOM_PESSOA, U.DSC_IDENTIFICACAO_USUARIO, NOM_ESTABELECIMENTO, TP.DSC_TIPO_PROCESSO, TD.DSC_TIPO_DOCUMENTO  ";
	
			}else{
				$strSql = " SELECT U.COD_USUARIO, U.DSC_IDENTIFICACAO_USUARIO, P.NOM_PESSOA, NE.COD_ESTABELECIMENTO, 
                                   NVL(PJE.NOM_FANTASIA, PFE.DSC_APELIDO) AS NOM_ESTABELECIMENTO,
                                   NE.COD_TIPO_DOCUMENTO, TD.DSC_TIPO_DOCUMENTO, NE.COD_TIPO_PROCESSO, TP.DSC_TIPO_PROCESSO
                              FROM NOTIFICACAO_ESTABEL_TPRTD NE
                              JOIN USUARIO U ON (U.COD_USUARIO = NE.COD_USUARIO)
                              JOIN PESSOA P ON (P.COD_PESSOA = U.COD_USUARIO)
                              JOIN PESSOA_FISICA PF ON (PF.COD_PESSOA = U.COD_USUARIO)
                              JOIN ESTABELECIMENTO E ON (E.COD_ESTABELECIMENTO = NE.COD_ESTABELECIMENTO)
                              JOIN PESSOA PE ON (PE.COD_PESSOA = E.COD_ESTABELECIMENTO)
                   LEFT OUTER JOIN PESSOA_JURIDICA PJE ON (PJE.COD_PESSOA = NE.COD_ESTABELECIMENTO)
                   LEFT OUTER JOIN PESSOA_FISICA PFE ON (PFE.COD_PESSOA = NE.COD_ESTABELECIMENTO)
                              JOIN TIPO_PROCESSO TP ON (TP.COD_TIPO_PROCESSO = NE.COD_TIPO_PROCESSO)
                              JOIN TIPO_DOCUMENTO TD ON (TD.COD_TIPO_DOCUMENTO = NE.COD_TIPO_DOCUMENTO)
                       WHERE (P.NOM_PESSOA LIKE '%'|| :NOM_PESSOA ||'%'
                              OR U.DSC_IDENTIFICACAO_USUARIO LIKE '%'|| :NOM_PESSOA ||'%')
                          AND PF.COD_EMPRESA = :COD_EMPRESA
				          ORDER BY P.NOM_PESSOA, U.DSC_IDENTIFICACAO_USUARIO, NOM_ESTABELECIMENTO, TP.DSC_TIPO_PROCESSO, TD.DSC_TIPO_DOCUMENTO ";
	
				$arrayParametros[1][0] = ':NOM_PESSOA';
				$arrayParametros[1][1] = $this->nomPessoa;
	
			}
				
			$arrayParametros[0][0] = ':COD_EMPRESA';
			$arrayParametros[0][1] = $this->codEmpresa;
			$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
			return $arrayUsuario;
		}else{
			return false;
		}
	}
	
	/** DELETE USUARIO CADASTRADO PARA RECEBER NOTIFICACAO GRUPO DE ESTABELECIMENTO
	 * @param integer codGrupoEstabelecimento
	 * @param integer codUsuario
	 * @return boolean
	 */
	public function deletaUsuarioNotificacaoEstabTpDoc(){
	
		if (TRUE == $this->codUsuario && TRUE == $this->codEstabelecimento  && TRUE == $this->codTipoProcesso && TRUE == $this->codTipoDocumento) {
			$objConexao = $this->database;
				
			$strSql = "DELETE FROM NOTIFICACAO_ESTABEL_TPRTD
					    WHERE COD_USUARIO = :codUsuario
					          AND COD_ESTABELECIMENTO = :codEstabelecimento
					          AND COD_TIPO_PROCESSO = :codTipoProcesso
					          AND COD_TIPO_DOCUMENTO = :codTipoDocumento";
	 
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $this->codUsuario;
			$arrayParametros[1][0] = ":codEstabelecimento";
			$arrayParametros[1][1] = $this->codEstabelecimento;
			$arrayParametros[2][0] = ":codTipoProcesso";
			$arrayParametros[2][1] = $this->codTipoProcesso;
			$arrayParametros[3][0] = ":codTipoDocumento";
			$arrayParametros[3][1] = $this->codTipoDocumento;
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao excluir usuário";
				$objConexao->rollbackTrans();
				return false;
			}
	
			return true;
		} else {
	
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados obrigatórios não foram informados";
			return false;
		}
	}
	
	/** LISTA OS USUARIO CADASTRADOS PARA RECEBER NOTIFICACAO DE PROCESSO
	 * @param integer codEmpresa
	 * @param string nomPessoa
	 * @return array
	 */
	public function listaUsuariosNotificacaoTpPro(){
		if ($this->codEmpresa ) {
	
			$objConexao = $this->database;
	
			$arrayParametros = array();
	
		
		     $strSql = " SELECT U.COD_USUARIO, U.DSC_IDENTIFICACAO_USUARIO, P.NOM_PESSOA,
			 		            NOTP.COD_TIPO_PROCESSO, TP.DSC_TIPO_PROCESSO, NOTP.COD_TIPO_NOTIFICACAO, 
                                DECODE(NOTP.COD_TIPO_NOTIFICACAO, 'A', 'ABERTURA', 'C','CANCELAMENTO',
						               'E','EXPIRAÇÃO','F','FECHAMENTO','PENDÊNCIA') AS DSC_TIPO_NOTIFICACAO
                           FROM NOTIFICACAO_TPPRO NOTP
                           JOIN USUARIO U ON (U.COD_USUARIO = NOTP.COD_USUARIO)
                           JOIN PESSOA P ON (P.COD_PESSOA = U.COD_USUARIO)
                           JOIN PESSOA_FISICA PF ON (PF.COD_PESSOA = U.COD_USUARIO)
                           JOIN TIPO_PROCESSO TP ON (TP.COD_TIPO_PROCESSO = NOTP.COD_TIPO_PROCESSO)
                          WHERE PF.COD_EMPRESA = :COD_EMPRESA
                          ORDER BY P.NOM_PESSOA, U.DSC_IDENTIFICACAO_USUARIO, TP.DSC_TIPO_PROCESSO, DSC_TIPO_NOTIFICACAO";
	
			
	
			$arrayParametros[0][0] = ':COD_EMPRESA';
			$arrayParametros[0][1] = $this->codEmpresa;
			$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
			return $arrayUsuario;
		}else{
			return false;
		}
	}
	
	/** DELETE USUARIO CADASTRADO PARA RECEBER NOTIFICACAO DE PROCESSO
	 * @param string codTipoNotificacao
	 * @param integer codUsuario
	 * @param codTipoProcesso
	 * @return boolean
	 */
	public function deletaUsuarioNotificacaoTpPro(){
	
		if (TRUE == $this->codUsuario && TRUE == $this->codTipoNotificacao  && TRUE == $this->codTipoProcesso) {
			$objConexao = $this->database;
	
			$strSql = "DELETE FROM NOTIFICACAO_TPPRO
					    WHERE COD_USUARIO = :codUsuario
					      AND COD_TIPO_PROCESSO = :codTipoProcesso
					      AND COD_TIPO_NOTIFICACAO = :codTipoNotificacao";
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $this->codUsuario;
			$arrayParametros[1][0] = ":codTipoProcesso";
			$arrayParametros[1][1] = $this->codTipoProcesso;
			$arrayParametros[2][0] = ":codTipoNotificacao";
			$arrayParametros[2][1] = $this->codTipoNotificacao;
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao excluir usuário";
				$objConexao->rollbackTrans();
				return false;
			}
	
			return true;
		} else {
	
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados obrigatórios não foram informados";
			return false;
		}
	}
	
	/** INSERE NOTIFICACAO PARA O USUARIO PARA O PROCESSO
	 * @param integer codUsuario
	 * @param string codTipoNotificacao
	 * @param integer codTipoProcesso
	 * @return boolean
	 */
	public function insereNotificacaoTpPro(){
		if (TRUE == $this->codUsuario && TRUE == $this->codTipoNotificacao && TRUE == $this->codTipoProcesso ) {
	
			$objConexao = $this->database;
	
			// insere processo_campo
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codUsuario";
			$arrayParametros[0][1] = $this->codUsuario;
			$arrayParametros[1][0] = ":codTipoNotificacao";
			$arrayParametros[1][1] = $this->codTipoNotificacao;
			$arrayParametros[2][0] = ":codTipoProcesso";
			$arrayParametros[2][1] = $this->codTipoProcesso;
	
	
			$strSql = "INSERT INTO NOTIFICACAO_TPPRO
						          (COD_USUARIO, COD_TIPO_NOTIFICACAO, COD_TIPO_PROCESSO)
						   VALUES (:codUsuario, :codTipoNotificacao, :codTipoProcesso)";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$returnedError = $objConexao->__get('error');
				$this->error['code'] 	= $returnedError['code'];
				$this->error['message'] = "Erro ao inserir notificação\n".$returnedError['message'];
				return false;
			}
	
			return true;
		} else {
	
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados obrigatórios não foram informados";
			return false;
		}
	}
}

?>