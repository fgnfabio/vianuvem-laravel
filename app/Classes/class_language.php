<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class language
{
	
	public function __construct(){
		return true;	
	}
	
	public function __destruct(){
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	/**
	 * Pega o subdominio
	 * @access public
	 * @var string
	 */
	public function getLanguageCookie(){
		
		if (isset($_COOKIE['current_language'])){
			$language = $_COOKIE['current_language'];
		}else{
			$language = $_SESSION['config_ini']['language']['default'];
		}
		
		return $language;
	}
	
	// 
	/**
	 * Pega o texto a ser exibido na linguagem correta
	 * $strPagina - pagina xml a ser lida
	 * $strTexto - indice do xml
	 * @access public
	 * @var string
	 */
	public function loadXML($strPagina, $strTexto){
		
		$lang = $this->getLanguageCookie();	
			
		if (file_exists('languages/'.$strPagina.'.xml')) {
			$xml = simplexml_load_file('languages/'.$strPagina.'.xml');
			return $xml->$lang->$strTexto;
		}elseif (file_exists('../languages/'.$strPagina.'.xml')){
			$xml = simplexml_load_file('../languages/'.$strPagina.'.xml');
			return $xml->$lang->$strTexto;
		}elseif (file_exists('../../languages/'.$strPagina.'.xml')){
			$xml = simplexml_load_file('../../languages/'.$strPagina.'.xml');
			return $xml->$lang->$strTexto;
		}
		
	}
	
	
}