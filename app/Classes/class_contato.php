<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class contato {

	public  $database;
	public  $error;
    private $codContato;
    private $dscNome;
    private $dscEmail;
    private $dscArea;
    private $dscLogin;
    private $dscPais;
    private $dscEstado;
    private $dscCidade;
    private $numDDD;
    private $numTelefone;
    private $dscAssunto;
    private $dscMensagem;
 	private $codUsuario;
 	private $indNovo;
 	private $dthInclusao;
 	private $dthLeitura;

    function __construct($database) {
    	$this->database = $database;
    	$this->error['code'] 	= "";
    	$this->error['message'] = "";
        return true;
    }

    public function __get($propriedade) {
    	return $this->$propriedade;
    }
    
    public function __set($propriedade, $valor) {
    	$this->$propriedade = $valor;
    }
    
    //**********************************************************************************************//
    
    public function getContato($codContato){
    	if (TRUE == $codContato) {
    		$objConexao = $this->database;
    		
    		$strSql = "SELECT DSC_NOME, DSC_EMAIL, DSC_AREA, DSC_LOGIN, DSC_PAIS, DSC_ESTADO, DSC_CIDADE, NUM_DDD, 
    				          NUM_TELEFONE, DSC_ASSUNTO, DSC_MENSAGEM, DTH_INCLUSAO, IND_NOVO, COD_USUARIO, DTH_LEITURA
            		     FROM CONTATO
    				    WHERE COD_CONTATO = :codContato";
    		
    		$arrayParametros = array();
    		$arrayParametros[0][0] = ":codContato";
    		$arrayParametros[0][1] = $codContato;
    		
    		$arrayContato = $objConexao->executeFetch($strSql, $arrayParametros);
    		 
    		$this->dscNome		= $arrayContato[0]["DSC_NOME"];
    		$this->dscEmail		= $arrayContato[0]["DSC_EMAIL"];
    		$this->dscArea		= $arrayContato[0]["DSC_AREA"];
    		$this->dscLogin		= $arrayContato[0]["DSC_LOGIN"];
    		$this->dscPais		= $arrayContato[0]["DSC_PAIS"];
    		$this->dscEstado	= $arrayContato[0]["DSC_ESTADO"];
    		$this->dscCidade	= $arrayContato[0]["DSC_CIDADE"];
    		$this->numDDD		= $arrayContato[0]["NUM_DDD"];
    		$this->numTelefone	= $arrayContato[0]["NUM_TELEFONE"];
    		$this->dscAssunto	= $arrayContato[0]["DSC_ASSUNTO"];
    		$this->dscMensagem	= $arrayContato[0]["DSC_MENSAGEM"]->load();
    		$this->dthInclusao	= $arrayContato[0]["DTH_INCLUSAO"];
    		$this->indNovo		= $arrayContato[0]["IND_NOVO"];
    		$this->codUsuario	= $arrayContato[0]["COD_USUARIO"];
    		$this->dthLeitura	= $arrayContato[0]["DTH_LEITURA"];
    		
    		return true;
    		
    	}else{
    		return false;
    	}
    }
    
    public function insContato() {
        if (TRUE == $this->dscNome && TRUE == $this->dscEmail) {
        	$objConexao = $this->database;
        	
            $strSql = "SELECT SQ_CONTA_01.NEXTVAL AS COD_CONTATO FROM DUAL";
            $arrayContato = $objConexao->executeFetch($strSql);

            $strSql = "INSERT INTO CONTATO
                                   (COD_CONTATO, DSC_NOME, DSC_EMAIL, DSC_AREA, DSC_LOGIN, DSC_PAIS, DSC_ESTADO, DSC_CIDADE, 
            		               NUM_DDD, NUM_TELEFONE, DSC_ASSUNTO, DSC_MENSAGEM, DTH_INCLUSAO, IND_NOVO)
            		        VALUES (:codContato, :dscNome, :dscEmail, :dscArea, :dscLogin, :dscPais, :dscEstado, :dscCidade, 
            		                :numDDD, :numTelefone, :dscAssunto, :dscMensagem, SYSDATE, 'S')";
            
            $arrayParametros = array();
            $arrayParametros[0][0] = ":codContato";
            $arrayParametros[0][1] = $arrayContato[0]["COD_CONTATO"];
            $arrayParametros[1][0] = ":dscNome";
            $arrayParametros[1][1] = $this->dscNome;
            $arrayParametros[2][0] = ":dscEmail";
            $arrayParametros[2][1] = $this->dscEmail;
            $arrayParametros[3][0] = ":dscArea";
            $arrayParametros[3][1] = $this->dscArea;
            $arrayParametros[4][0] = ":dscLogin";
            $arrayParametros[4][1] = $this->dscLogin;
            $arrayParametros[5][0] = ":dscPais";
            $arrayParametros[5][1] = $this->dscPais;
            $arrayParametros[6][0] = ":dscEstado";
            $arrayParametros[6][1] = $this->dscEstado;
            $arrayParametros[7][0] = ":dscCidade";
            $arrayParametros[7][1] = $this->dscCidade;
            $arrayParametros[8][0] = ":numDDD";
            $arrayParametros[8][1] = $this->numDDD;
            $arrayParametros[9][0] = ":numTelefone";
            $arrayParametros[9][1] = $this->numTelefone;
            $arrayParametros[10][0] = ":dscAssunto";
            $arrayParametros[10][1] = $this->dscAssunto;
            $arrayParametros[11][0] = ":dscMensagem";
            $arrayParametros[11][1] = $this->dscMensagem;

            if (!$objConexao->execute($strSql, $arrayParametros)){
            	return false;
            }

            return true;
        } else {
            return false;
        }
    }
    
    public function totalContatos(){
    	$objConexao = $this->database;
    	 
    	$strSql = "SELECT COUNT(COD_CONTATO) AS TOTAL FROM CONTATO";
    	$arrayContato = $objConexao->executeFetch($strSql);
    	 
    	return $arrayContato[0]["TOTAL"];
    	 
    }
    
    public function totalNovosContatos(){
    	$objConexao = $this->database;
    	
    	$strSql = "SELECT COUNT(COD_CONTATO) AS TOTAL FROM CONTATO WHERE IND_NOVO = 'S'";
    	$arrayContato = $objConexao->executeFetch($strSql);
    	
    	return $arrayContato[0]["TOTAL"];
    	
    }
    
    public function marcaLido($codContato){
    	$objConexao = $this->database;
    	 
    	$strSql = "UPDATE CONTATO
    			      SET IND_NOVO = :indNovo,
    			          COD_USUARIO = :codUduario,
    					  DTH_LEITURA = SYSDATE
    			    WHERE COD_CONTATO = :codContato";
    	
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ":indNovo";
    	$arrayParametros[0][1] = null;
    	$arrayParametros[1][0] = ":codUduario";
    	$arrayParametros[1][1] = $_SESSION['codUsuario'];
    	$arrayParametros[2][0] = ":codContato";
    	$arrayParametros[2][1] = $codContato;
    	
    	if (!$objConexao->execute($strSql, $arrayParametros)){
    		return false;
    	}
    	
    	return true;
    }

}

