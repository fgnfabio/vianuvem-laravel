; Arquivo de configuração

;[geral]
;url_site 			= "app.vianuvem.com.br"
;path				= "https://app.vianuvem.com.br/"
;path_ssl			= "https://app.vianuvem.com.br/"
;favicon				= "favicon.ico"
;timezone			= "America/Sao_Paulo"
;server_ip			= "10.0.0.6"
;local_dir			= ""
;manutencao          = "N"

[header]
charset			= "utf-8"
keywords			= ""
description 			= ""

[language]
default			= "pt-br"
languages			= "pt-br|portugues, en|ingles, fr|frances, es|espanhol" ; sigla|nome_do_campo_no_arquivo_de_linguagem

[files_regex]
liberados			= "(\.|\/)(gif|jpe?g|png|pdf|doc|xls|)"

;[webservice]
;auditoria			= "N" ; S=sim, N=nao

;[sandbox]
;auditoria			= "N" ; S=sim, N=nao

[imagem]
dpi					= "100"
bits				= "100" ; qualidade
max_largura			= "1024" 
max_altura			= "768"
max_kbyte			= "500" ; 500 kb
qualidade_padrao	= "70"  ; 0-100

;[dynamsoft]
;productKey			= "06E06AADA658226601461A9ACD10C5A03F4AA7EE5E43F40B5DE06B5EC5C999311BED93F6EBC3043C8EE9D7654CB8C390996D681BE9ED93AB5DF1024749FAC6EE30000000" ; full version never expire
;host				= "app.vianuvem.com.br"
;local_dir			= ""

;-----------DESEV   comentar tudo antes de subir
[geral]
url_site 			= "localhost/"
path				= "http://localhost/"
path_ssl			= "https://localhost/"
favicon			= "favicon.ico"
timezone			= "America/Sao_Paulo"
server_ip			= "192.168.1.90"
local_dir			= ""
manutencao          = "N"
[webservice]
auditoria			= "S" ; S=sim, N=nao

[sandbox]
auditoria			= "S" ; S=sim, N=nao

[dynamsoft]
productKey			= "06E06AADA658226601461A9ACD10C5A03F4AA7EE5E43F40B5DE06B5EC5C999311BED93F6EBC3043C8EE9D7654CB8C390996D681BE9ED93AB5DF1024749FAC6EE30000000" ; full version never expire
host				= "vianuvemlocal"
local_dir			= ""
;-----------FIM DESENV