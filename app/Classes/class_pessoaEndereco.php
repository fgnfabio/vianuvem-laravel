<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class pessoaEndereco
{
	public    $database;
	public    $error;
	private   $codPessoa;
	protected $codTipoEndereco = 1;
	protected $numCep;
	protected $dscEndereco;
	protected $numEndereco;
	protected $dscComplemento;
	protected $nomBairro;
	protected $nomLocalidade;
	protected $sglUF;
	protected $dscPontoReferencia;
	protected $indEnderecoECT;
	protected $codLocalidade;
	
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getCodPessoa() {
		return $this->codPessoa;
	}
	
	public function setCodPessoa($codPessoa) {
		$this->codPessoa = $codPessoa;
		return $this;
	}
	
	public function getCodTipoEndereco() {
		return $this->codTipoEndereco;
	}
	
	public function setCodTipoEndereco($codTipoEndereco) {
		$this->codTipoEndereco = $codTipoEndereco;
		return $this;
	}
	
	public function getDscEndereco() {
		return $this->dscEndereco;
	}
	
	public function setDscEndereco($dscEndereco) {
		$this->dscEndereco = $dscEndereco;
		return $this;
	}
	
	
	public function getNumCep() {
		return $this->numCep;
	}
	
	public function setNumCep($numCep) {
		$this->numCep = $numCep;
		return $this;
	}
	
	public function getNumEndereco() {
		return $this->numEndereco;
	}
	
	public function setNumEndereco($numEndereco) {
		$this->numEndereco = $numEndereco;
		return $this;
	}
	
	public function getDscComplemento() {
		return $this->dscComplemento;
	}
	
	public function setDscComplemento($dscComplemento) {
		$this->dscComplemento = $dscComplemento;
		return $this;
	}
	
	public function getNomBairro() {
		return $this->nomBairro;
	}
	
	public function setNomBairro($nomBairro) {
		$this->nomBairro = $nomBairro;
		return $this;
	}
	
	public function getNomLocalidade() {
		return $this->nomLocalidade;
	}
	
	public function setNomLocalidade($nomLocalidade) {
		$this->nomLocalidade = $nomLocalidade;
	}
	
	public function getSglUF() {
		return $this->sglUF;
	}
	
	public function setSglUF($sglUF) {
		$this->sglUF = $sglUF;
		return $this;
	}
	
	public function getDscPontoReferencia() {
		return $this->dscPontoReferencia;
	}
	
	public function setDscPontoReferencia($dscPontoReferencia) {
		$this->dscPontoReferencia = $dscPontoReferencia;
		return $this;
	}
	
	public function getIndEnderecoECT() {
		return $this->indEnderecoECT;
	}
	
	public function setIndEnderecoECT($indEnderecoECT) {
		$this->indEnderecoECT = $indEnderecoECT;
		return $this;
	}
	
	public function getCodLocalidade() {
		return $this->codLocalidade;
	
	}
	
	public function setCodLocalidade($codLocalidade) {
		$this->codLocalidade = $codLocalidade;
		return $this;
	}
	
	public function getCodFilial() {
		return $this->codFilial;
	}
	
	public function setCodFilial($codFilial) {
		$this->codFilial = $codFilial;
		return $this;
	}

	public function getPessoaEndereco($codPessoa){
		if($codPessoa){
			$objConexao = $this->database;
		
			$strSql = "SELECT COD_PESSOA, COD_TIPO_ENDERECO, NUM_CEP, DSC_ENDERECO, NUM_ENDERECO, DSC_COMPLEMENTO, NOM_BAIRRO,
                              NOM_LOCALIDADE, SGL_UF, DSC_PONTO_REFERENCIA, IND_ENDERECO_ECT, COD_LOCALIDADE
                         FROM PESSOA_ENDERECO
                        WHERE COD_PESSOA = :codPessoa";
		
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $codPessoa;
		
			$arrayEndereco = $objConexao->executeFetch($strSql, $arrayParametros);
		
			$this->codPessoa = $arrayEndereco[0]["COD_PESSOA"];
			$this->codTipoEndereco = $arrayEndereco[0]["COD_TIPO_ENDERECO"];
			$this->numCep = $arrayEndereco[0]["NUM_CEP"];
			$this->dscEndereco = $arrayEndereco[0]["DSC_ENDERECO"];
			$this->numEndereco = $arrayEndereco[0]["NUM_ENDERECO"];
			$this->dscComplemento = $arrayEndereco[0]["DSC_COMPLEMENTO"];
			$this->nomBairro = $arrayEndereco[0]["NOM_BAIRRO"];
			$this->nomLocalidade = $arrayEndereco[0]["NOM_LOCALIDADE"];
			$this->sglUF = $arrayEndereco[0]["SGL_UF"];
			$this->dscPontoReferencia = $arrayEndereco[0]["DSC_PONTO_REFERENCIA"];
			$this->indEnderecoECT = $arrayEndereco[0]["IND_ENDERECO_ECT"];
			$this->codLocalidade = $arrayEndereco[0]["COD_LOCALIDADE"];
			
		}
		return true;
		
	}
	
	
	function insPessoaEndereco() {
	
		if (TRUE == $this->codPessoa && TRUE == $this->codTipoEndereco) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $this->codPessoa;
			$arrayParametros[1][0] = ":codTipoEndereco";
			$arrayParametros[1][1] = $this->codTipoEndereco;
			$arrayParametros[2][0] = ":numCep";
			$arrayParametros[2][1] = $this->numCep;
			$arrayParametros[3][0] = ":dscEndereco";
			$arrayParametros[3][1] = $this->dscEndereco;
			$arrayParametros[4][0] = ":numEndereco";
			$arrayParametros[4][1] = $this->numEndereco;
			$arrayParametros[5][0] = ":dscComplemento";
			$arrayParametros[5][1] = $this->dscComplemento;
			$arrayParametros[6][0] = ":nomBairro";
			$arrayParametros[6][1] = $this->nomBairro;
			$arrayParametros[7][0] = ":nomLocalidade";
			$arrayParametros[7][1] = $this->nomLocalidade;
			$arrayParametros[8][0] = ":sglUF";
			$arrayParametros[8][1] = $this->sglUF;
			$arrayParametros[9][0] = ":dscPontoReferencia";
			$arrayParametros[9][1] = $this->dscPontoReferencia;
			$arrayParametros[10][0] = ":indEnderecoECT";
			$arrayParametros[10][1] = $this->indEnderecoECT;
			$arrayParametros[11][0] = ":codLocalidade";
			$arrayParametros[11][1] = $this->codLocalidade;
	
			$strSql = "INSERT INTO PESSOA_ENDERECO 
					          (COD_PESSOA, COD_TIPO_ENDERECO, NUM_CEP, DSC_ENDERECO, NUM_ENDERECO, DSC_COMPLEMENTO, NOM_BAIRRO, 
					           NOM_LOCALIDADE, SGL_UF, DSC_PONTO_REFERENCIA, IND_ENDERECO_ECT, COD_LOCALIDADE)
                       VALUES (:codPessoa, :codTipoEndereco, :numCep, :dscEndereco, :numEndereco, :dscComplemento, :nomBairro, 
					           :nomLocalidade, :sglUF, :dscPontoReferencia, :indEnderecoECT, :codLocalidade)";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao inserir pessoa endereço";
				return false;
			}
	
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para inserir pessoa endereço";
			return false;
		}
	}
	
	function changePessoaEndereco() {
		
		if (TRUE == $this->getCodPessoa()) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $this->codPessoa;
			$arrayParametros[1][0] = ":codTipoEndereco";
			$arrayParametros[1][1] = $this->codTipoEndereco;
			$arrayParametros[2][0] = ":numCep";
			$arrayParametros[2][1] = $this->numCep;
			$arrayParametros[3][0] = ":dscEndereco";
			$arrayParametros[3][1] = $this->dscEndereco;
			$arrayParametros[4][0] = ":numEndereco";
			$arrayParametros[4][1] = $this->numEndereco;
			$arrayParametros[5][0] = ":dscComplemento";
			$arrayParametros[5][1] = $this->dscComplemento;
			$arrayParametros[6][0] = ":nomBairro";
			$arrayParametros[6][1] = $this->nomBairro;
			$arrayParametros[7][0] = ":nomLocalidade";
			$arrayParametros[7][1] = $this->nomLocalidade;
			$arrayParametros[8][0] = ":sglUF";
			$arrayParametros[8][1] = $this->sglUF;
			$arrayParametros[9][0] = ":dscPontoReferencia";
			$arrayParametros[9][1] = $this->dscPontoReferencia;
			$arrayParametros[10][0] = ":indEnderecoECT";
			$arrayParametros[10][1] = $this->indEnderecoECT;
			$arrayParametros[11][0] = ":codLocalidade";
			$arrayParametros[11][1] = $this->codLocalidade;
	
			$strSql = "UPDATE PESSOA_ENDERECO 
					      SET COD_TIPO_ENDERECO = :codTipoEndereco, 
					 		  NUM_CEP = :numCep, 
							  DSC_ENDERECO = :dscEndereco, 
							  NUM_ENDERECO = :numEndereco, 
							  DSC_COMPLEMENTO = :dscComplemento, 
							  NOM_BAIRRO = :nomBairro, 
					          NOM_LOCALIDADE = :nomLocalidade, 
							  SGL_UF = :sglUF, 
							  DSC_PONTO_REFERENCIA = :dscPontoReferencia, 
							  IND_ENDERECO_ECT = :indEnderecoECT, 
							  COD_LOCALIDADE = :codLocalidade 
					    WHERE COD_PESSOA = :codPessoa";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar pessoa endereço";
				return false;
			}
	
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para alterar pessoa endereço";
			return false;
		}
	}
	
	function delPessoaEndereco($codPessoa){
		
		if (TRUE == $codPessoa) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $codPessoa;
	
			$strSql = "DELETE 
					     FROM PESSOA_ENDERECO 
					    WHERE COD_PESSOA =:codPessoa";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar pessoa endereço";
				return false;
			}
	
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para deletar pessoa endereço";
			return false;
		}
	
	}
}