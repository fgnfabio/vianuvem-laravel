<?php

namespace App\Classes;

require_once('class_documento.php');

class gerarDocumentoPdfForm{
	
	public $database;
	public $error;
	

	/************************************************************************************************************/
	// MINHAS ALTERACOES
	protected function handle_form_data($file, $index, $arquivo_gerado) {
		//**********************************************************************************************/
		// Handle form data, e.g. $_REQUEST['description'][$index]
	
		$objDocumento = new documento($this->database);
	
		//obs: utilizo o nome web no fim da variavel devido a um bug no envio do formulario, q sobreescrevia os dados enviados via GET
		//     com os dados do form, se fosse enviado mais que um arquivo ele utilizava os dados do ultimo inserido
		 
		$objDocumento->setCodTipoProcesso(@$_REQUEST['codTipoProcessoWeb']);
		 
		if ( !isset($_REQUEST['codProcessoWeb']) || empty($_REQUEST['codProcessoWeb']) ){
			$objDocumento->setCodProcesso("");
		}else{
			$objDocumento->setCodProcesso(@$_REQUEST['codProcessoWeb']);
		}
		 
		$objDocumento->setCodTipoDocumento(@$_REQUEST['codTipoDocumentoWeb']);
		$objDocumento->setCodEstabelecimento(@$_REQUEST['codEstabelecimentoWeb']);
		$objDocumento->setCodUsuario($_SESSION['codUsuario']);
		$objDocumento->setCodEmpresa($_SESSION['codEmpresa']);
	
		$ip = $_SERVER['REMOTE_ADDR'];
	
		$objDocumento->setCodIpEstacao($ip);
	
		// via web browser será sempre file disk (mudara em breve)
		$codTipoDispositivo = 'F';
		$objDocumento->setCodTipoDispositivo($codTipoDispositivo);
		$objDocumento->setBlbArquivo(file_get_contents($arquivo_gerado));
		 
		// pega o mime type (comentado e alterado devido a bug na producao que nao pegava o type corretamente)
		//$finfo = new finfo(FILEINFO_MIME_TYPE);
		//$dscType = $finfo->buffer(file_get_contents($uploaded_file));
		$dscType = $file->type;
	
		$codMimeType = $objDocumento->getCodMimeByDscMime($dscType);
		 
		 
		 
		// precisa cadastrar o novo mime type
		if ($codMimeType == ""){
			$codMimeType = $objDocumento->insMimeType($dscType);
		}
		 
		$_ext = substr($file->name,strripos($file->name, ".") );
		 
		if ( $_ext == ".pdf" || $_ext == ".PDF" ){
			$codMimeType = 2;
		}
		 
		$objDocumento->setCodMimeType($codMimeType);
		 
		$arrayCampos = array();
		$arr = explode("|#|,", $_REQUEST['arrayCampos']);
		$numOrdem = null;
		foreach ($arr as $campos){
			//$campo =  preg_replace("/;/", "|#|;", $campos, 1);
			$arr2 = explode(';', $campos,2);
			$arr2[1] = str_replace("|#|", '', $arr2[1]);
			//pega a ordem do campo
			$objDocumento->__set("nomCampo",$arr2[0]);
	
			$numOrdem = $objDocumento->numOrdemCampo();
	
			$objDocumento->__set("numOrdem",$numOrdem);
	
			$codTipoCampo = $objDocumento->tipoCampo();
			if ($codTipoCampo == "V" || $codTipoCampo == "I"){
				$arrayCampos[$numOrdem] = str_replace(".","",$arr2[1]);
			}else{
				 
				$arrayCampos[$numOrdem] = $arr2[1];
			}
		}
		 
		$indexador = array();
		for ($i = 1; $i <= 6; $i++) {
			$indexador[$i] = @$arrayCampos[$i];
		}
		 
		$objDocumento->__set('arrayCampos',$indexador);
		if (!$objDocumento->insDocumento()){
			@unlink($uploaded_file);
	
			$this->error = $objDocumento->error['message'];
			return false;
		}
	
		return true;
	
	}

}