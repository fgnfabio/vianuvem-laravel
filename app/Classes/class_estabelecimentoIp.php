<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class estabelecimentoIp
{
	public    $database;
	public    $error;
	
	protected $codEstabelecimento;
	protected $datInicial;
	protected $datInicialAntiga;
	protected $datFinal;
	protected $dscMascaraIp;
	
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getCodEstabelecimento() {
		return $this->codEstabelecimento;
	}
	
	public function setCodEstabelecimento($codEstabelecimento) {
		$this->codEstabelecimento = $codEstabelecimento;
	}
	
	public function getDatInicial() {
		return $this->datInicial;
	}
	
	public function setDatInicial($datInicial) {
		$this->datInicial = $datInicial;
	}
	
	public function setDatInicialAntiga($datInicialAntiga) {
		$this->datInicialAntiga = $datInicialAntiga;
	}
	
	public function getDatFinal() {
		return $this->datFinal;
	}
	
	public function setDatFinal($datFinal) {
		$this->datFinal = $datFinal;
	}
	
	public function getDscMascaraIp() {
		return $this->dscMascaraIp;
	}
	
	public function setdscMascaraIp($dscMascaraIp) {
		$this->dscMascaraIp = $dscMascaraIp;
	}
	
	
	public function getEstabelecimentoIp($codEstabelecimento){
		
		if(TRUE == $codEstabelecimento){
			$objConexao = $this->database;

			$strSql = "SELECT EI.DAT_INICIAL, EI.DAT_FINAL, EI.DSC_MASCARA_IP
  						 FROM ESTABELECIMENTO_IP EI
 						WHERE COD_ESTABELECIMENTO = :codEstabelecimento";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codEstabelecimento";
			$arrayParametros[0][1] = $codEstabelecimento;
		
			$arrayEstabelecimento = $objConexao->executeFetch($strSql, $arrayParametros);
		
			$this->codEstabelecimento 	= $codEstabelecimento;
			$this->datInicial 			= $arrayEstabelecimento[0]["DAT_INICIAL"];
			$this->datFinal				= $arrayEstabelecimento[0]["DAT_FINAL"];
			$this->dscMascaraIp			= $arrayEstabelecimento[0]["DSC_MASCARA_IP"];
		}
		
		return true;
	}
	
	public function getEstabelecimentoIpVigente($codEstabelecimento){
		if(TRUE == $codEstabelecimento){
			$objConexao = $this->database;
		
			
			$strSql = "SELECT EI.DAT_INICIAL, EI.DAT_FINAL, EI.DSC_MASCARA_IP
  						 FROM ESTABELECIMENTO_IP EI
 						WHERE COD_ESTABELECIMENTO = :codEstabelecimento
					      AND TRUNC(SYSDATE) BETWEEN EI.DAT_INICIAL AND EI.DAT_FINAL";
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codEstabelecimento";
			$arrayParametros[0][1] = $codEstabelecimento;
		
			$arrayEstabelecimento = $objConexao->executeFetch($strSql, $arrayParametros);
		
			$this->codEstabelecimento 	= $codEstabelecimento;
			$this->datInicial 			= $arrayEstabelecimento[0]["DAT_INICIAL"];
			$this->datFinal				= $arrayEstabelecimento[0]["DAT_FINAL"];
			$this->dscMascaraIp			= $arrayEstabelecimento[0]["DSC_MASCARA_IP"];
		}
		
		return true;
	}
	
	public function insEstabelecimentoIp() {
		$objConexao = $this->database;
		
		// cria estabelecimentoIp
		if (TRUE == $this->codEstabelecimento) {
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codEstabelecimento";
			$arrayParametros[0][1] = $this->codEstabelecimento;
			$arrayParametros[1][0] = ":datInicial";
			$arrayParametros[1][1] = $this->datInicial;
			$arrayParametros[2][0] = ":datFinal";
			$arrayParametros[2][1] = $this->datFinal;
			$arrayParametros[3][0] = ":dscMascaraIp";
			$arrayParametros[3][1] = $this->dscMascaraIp;
	
			$strSql = "INSERT INTO ESTABELECIMENTO_IP 
					          (COD_ESTABELECIMENTO, DAT_INICIAL, DAT_FINAL, DSC_MASCARA_IP)
					   VALUES (:codEstabelecimento, :datInicial, :datFinal, :dscMascaraIp)";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao inserir IP de acesso";
				return false;
			}
			
			return true;
		}
	}
	
	public function changeEstabelecimentoIp() {
		$objConexao = $this->database;
		
		if (TRUE == $this->codEstabelecimento && TRUE == $this->datInicialAntiga){
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codEstabelecimento";
			$arrayParametros[0][1] = $this->codEstabelecimento;
			$arrayParametros[1][0] = ":datInicial";
			$arrayParametros[1][1] = $this->datInicial;
			$arrayParametros[2][0] = ":datFinal";
			$arrayParametros[2][1] = $this->datFinal;
			$arrayParametros[3][0] = ":dscMascaraIp";
			$arrayParametros[3][1] = $this->dscMascaraIp;
			$arrayParametros[4][0] = ":datInicialAntiga";
			$arrayParametros[4][1] = $this->datInicialAntiga;
	
			// altera estabelecimento
			$strSql = "UPDATE ESTABELECIMENTO_IP
					      SET DAT_INICIAL = :datInicial, 
					          DAT_FINAL = :datFinal, 
					          DSC_MASCARA_IP = :dscMascaraIp
					    WHERE COD_ESTABELECIMENTO = :codEstabelecimento
					      AND DAT_INICIAL = :datInicialAntiga";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar IP de acesso";
				return false;
			}
			
			return true;
			
		}else{
			return false;
		}
	}
	
	
	
	public function delEstabelecimentoIp($codEstabelecimento, $datInicial){
		
		if (TRUE == $codEstabelecimento && TRUE == $datInicial) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codEstabelecimento";
			$arrayParametros[0][1] = $codEstabelecimento;
			$arrayParametros[1][0] = ":datInicial";
			$arrayParametros[1][1] = $datInicial;
	
			$strSql = "DELETE
					     FROM ESTABELECIMENTO_IP
					    WHERE COD_ESTABELECIMENTO =:codEstabelecimento
					      AND DAT_INICIAL = :datInicial";
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar IP de acesso";
				return false;
			}
			
			return true;
		} else {
			return false;
		}
	
	}
	
	public function verificaIp($codEstabelecimento, $ip){
		
		if (TRUE == $codEstabelecimento && TRUE == $ip) {
			$objConexao = $this->database;
			
			$this->getEstabelecimentoIpVigente($codEstabelecimento);
			
			if (TRUE == $this->dscMascaraIp){
				$libera = false;
				$msk = explode(".", $this->dscMascaraIp);
				$arrayIp = explode(".", $ip);
				$total = count($msk);
				
				
				// primeira parte do IP
				if ($total >= 1){
					// coringa libera tudo
					if ($msk[0] == '*'){
						return true;
					}else{
						if ($msk[0] != $arrayIp[0]){
							return false;
						}
					}
				}
				
				// segunda parte do IP
				if ($total >= 2){
					// coringa libera tudo
					if ($msk[1] == '*'){
						return true;
					}else{
						if ($msk[1] != $arrayIp[1]){
							return false;
						}
					}
				}
				
				// terceira parte do IP
				if ($total >= 3){
					// coringa libera tudo
					if ($msk[2] == '*'){
						return true;
					}else{
						if ($msk[2] != $arrayIp[2]){
							return false;
						}
					}
				}
				
				// quarta parte do IP
				if ($total >= 4){
					// coringa libera tudo
					if ($msk[3] == '*'){
						return true;
					}else{
						if ($msk[3] != $arrayIp[3]){
							return false;
						}
					}
				}
				
				return true;
				
			}else{
				// se nao achou uma mascara é que não existe bloqueio vigente para o estabelecimento
				return true;
			}

		} else {
			return false;
		}
	}
}