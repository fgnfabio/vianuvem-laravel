<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class parametro
{
	public    $database;
	public    $error;
	
	protected $codEmpresa;
	protected $codContextoParametro;
	protected $codParametro;
	protected $dscParametro;
	protected $dscTituloParametro;
	protected $indParametroSistema;
	protected $codTipoAtributo;
	protected $dscValorParametro;
	
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getCodEmpresa() {
		return $this->codEmpresa;
	}
	
	public function setCodEmpresa($codEmpresa) {
		$this->codEmpresa = $codEmpresa;
	}
	
	public function getCodContextoParametro() {
		return $this->codContextoParametro;
	}
	
	public function setCodContextoParametro($codContextoParametro) {
		$this->codContextoParametro = $codContextoParametro;
	}
	
	public function getCodParametro() {
		return $this->codParametro;
	}
	
	public function setCodParametro($codParametro) {
		$this->codParametro = $codParametro;
	}
	
	public function getDscParametro() {
		return $this->dscParametro;
	}
	
	public function setDscParametro($dscParametro) {
		$this->dscParametro = $dscParametro;
	}
	
	public function getDscTituloParametro() {
		return $this->dscTituloParametro;
	}
	
	public function setDscTituloParametro($dscTituloParametro) {
		$this->dscTituloParametro = $dscTituloParametro;
	}
	
	public function getIndParametroSistema() {
		return $this->indParametroSistema;
	}
	
	public function setIndParametroSistema($indParametroSistema) {
		$this->indParametroSistema = $indParametroSistema;
	}
	
	public function getCodTipoAtributo() {
		return $this->codTipoAtributo;
	}
	
	public function setCodTipoAtributo($codTipoAtributo) {
		$this->codTipoAtributo = $codTipoAtributo;
	}
	
	public function getDscValorParametro() {
		return $this->dscValorParametro;
	}
	
	public function setDscValorParametro($dscValorParametro) {
		$this->dscValorParametro = $dscValorParametro;
	}
	
	// pega codigo do parametro pelo dsc_parametro
	public function getCodParametroByDescricao($dscParametro){
		$objConexao = $this->database;
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":dscParametro";
		$arrayParametros[0][1] = $dscParametro;
		
		// deleta os parametros com empresa e contexto passados
		$strSql = "SELECT COD_PARAMETRO
				     FROM PARAMETRO
				    WHERE DSC_PARAMETRO = :dscParametro";
		
		$arrayParametro = $objConexao->executeFetch($strSql, $arrayParametros);
		return $arrayParametro[0]["COD_PARAMETRO"];
		
	}
	
	// pega o valor do parametro pelo dsc_parametro
	public function getDscValorParametroByDescricao($dscParametro, $codEmpresa){
		$objConexao = $this->database;
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ":dscParametro";
		$arrayParametros[0][1] = $dscParametro;
		$arrayParametros[1][0] = ":codEmpresa";
		$arrayParametros[1][1] = $codEmpresa;
	
		// deleta os parametros com empresa e contexto passados
		$strSql = "SELECT PV.DSC_VALOR_PARAMETRO
                     FROM PARAMETRO P
                     LEFT JOIN PARAMETRO_VALOR PV ON (PV.COD_PARAMETRO = P.COD_PARAMETRO AND PV.COD_EMPRESA = :codEmpresa)
                    WHERE DSC_PARAMETRO = :dscParametro";
	
		$arrayParametro = $objConexao->executeFetch($strSql, $arrayParametros);
		return $arrayParametro[0]["DSC_VALOR_PARAMETRO"];
	
	}
	
	// cadastra parametros
	function insParametro() {
		if (!$this->codEmpresa || !$this->codParametro || !$this->dscValorParametro){
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para inserir parametro";
			return false;
		}
		
		$objConexao = $this->database;

		$arrayParametros = array();
		$arrayParametros[0][0] = ":codEmpresa";
		$arrayParametros[0][1] = $this->codEmpresa;
		$arrayParametros[1][0] = ":codParametro";
		$arrayParametros[1][1] = $this->codParametro;

		// deleta os parametros com empresa e contexto passados
		$strSql = "DELETE 
				     FROM PARAMETRO_VALOR
				    WHERE COD_EMPRESA = :codEmpresa
				      AND COD_PARAMETRO = :codParametro";
		
		if (!$objConexao->execute($strSql, $arrayParametros)) {
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro ao deletar parametro";
			return false;
		}
		
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codEmpresa";
		$arrayParametros[0][1] = $this->codEmpresa;
		$arrayParametros[1][0] = ":codParametro";
		$arrayParametros[1][1] = $this->codParametro;
		$arrayParametros[2][0] = ":dscValorParametro";
		$arrayParametros[2][1] = $this->dscValorParametro;
		
		$strSql = "INSERT INTO PARAMETRO_VALOR 
				          (COD_PARAMETRO, COD_EMPRESA, DSC_VALOR_PARAMETRO) 
				   VALUES (:codParametro, :codEmpresa, :dscValorParametro)";
	
		if (!$objConexao->execute($strSql, $arrayParametros)) {
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro ao inserir pessoa";
			return false;
		}
	
		
		
		return true;

	}

	
	
	
	
		
	
}

?>