<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class recurso {
	
	public  $database;
	public  $error;
	
	private  $codRecurso;
	private  $dscRecurso;
	private  $dscPosicaoMenu;
		
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getCodRecurso(){
		return $this->codRecurso;
	}
	
	public function setCodRecurso($codRecurso){
		$this->codRecurso = $codRecurso;
	}
	
	public function getDscRecurso(){
		return $this->dscRecurso;
	}
	
	public function setDscRecurso($dscRecurso){
		$this->dscRecurso = $dscRecurso;
	}
	
	public function getDscPosicaoMenu(){
		return $this->dscPosicaoMenu;
	}
	
	public function setDscPosicaoMenu($dscPosicaoMenu){
		$this->dscPosicaoMenu = $dscPosicaoMenu;
	}
	
	public function getRecurso($codRecurso){
		$objConexao = $this->database;
		 
		$strSql = "SELECT COD_RECURSO, DSC_RECURSO, DSC_POSICAO_MENU 
				     FROM RECURSO 
					WHERE COD_RECURSO = :codRecurso";
		 
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codRecurso";
		$arrayParametros[0][1] = $codRecurso;
		 
		$arrayRecurso = $objConexao->executeFetch($strSql, $arrayParametros);
		 
		$this->codRecurso = $arrayRecurso[0]["COD_RECURSO"];
		$this->dscRecurso = $arrayRecurso[0]["DSC_RECURSO"];
		$this->dscPosicaoMenu = $arrayRecurso[0]["DSC_POSICAO_MENU"];
		
	}
	
	public function insRecurso(){
		$objConexao = $this->database;
		
		$strSql = "SELECT SQ_RECUR_01.NEXTVAL AS COD_RECURSO FROM DUAL";
		$arrayRecurso = $objConexao->executeFetch($strSql);
		$this->codRecurso = $arrayRecurso[0]["COD_RECURSO"];
		
		if (!$this->codRecurso){
			return false;
		}
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codRecurso";
		$arrayParametros[0][1] = $this->codRecurso;
		$arrayParametros[1][0] = ":dscRecurso";
		$arrayParametros[1][1] = $this->dscRecurso;
		$arrayParametros[2][0] = ":dscPosicaoMenu";
		$arrayParametros[2][1] = $this->dscPosicaoMenu;
		
		$strSql = "INSERT INTO RECURSO 
				          (COD_RECURSO, DSC_RECURSO, DSC_POSICAO_MENU) 
				   VALUES (:codRecurso, :dscRecurso, :dscPosicaoMenu)";
		
		if (!$objConexao->execute($strSql, $arrayParametros)){
			return false;
		}
		
		return true;
	}
	
	public function changeRecurso($codRecurso) {
		if ($codRecurso && TRUE == $this->dscRecurso ){
			$objConexao = $this->database;
			 
			$strSql = "UPDATE RECURSO
            		      SET DSC_RECURSO = :dscRecurso,
					          DSC_POSICAO_MENU = :dscPosicaoMenu
            		    WHERE COD_RECURSO = :codRecurso";
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codRecurso";
			$arrayParametros[0][1] = $codRecurso;
			$arrayParametros[1][0] = ":dscRecurso";
			$arrayParametros[1][1] = $this->dscRecurso;
			$arrayParametros[2][0] = ":dscPosicaoMenu";
			$arrayParametros[2][1] = $this->dscPosicaoMenu;
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				return false;
			}
	
			return true;
		} else {
			return false;
		}
	}
	
	public function delRecurso($codRecurso) {
	
		if ($codRecurso) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codRecurso";
			$arrayParametros[0][1] = $codRecurso;
	
			$strSql = "DELETE
            		     FROM RECURSO
            		    WHERE COD_RECURSO =:codRecurso";
			if (!$objConexao->execute($strSql, $arrayParametros)){
				return false;
			}
	
			return true;
		} else {
			return false;
		}
	}
	
	public function changeRecursoPerfil($codPerfil, $arrCodRecursos){
		
		if ($codPerfil) {
			$objConexao = $this->database;
			
			$objConexao->beginTrans();
		
			// deleta recursos do perfil
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPerfil";
			$arrayParametros[0][1] = $codPerfil;
		
			$strSql = "DELETE
            		     FROM PERFIL_USUARIO_RECURSO
            		    WHERE COD_PERFIL_USUARIO =:codPerfil";
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$objConexao->rollbackTrans();
				return false;
			}
			
			// insere novos recursos do perfil
			
			foreach ($arrCodRecursos as $codRecurso){
				
				$arrayParametros = array();
				$arrayParametros[0][0] = ":codPerfil";
				$arrayParametros[0][1] = $codPerfil;
				$arrayParametros[1][0] = ":codRecurso";
				$arrayParametros[1][1] = $codRecurso;
				
				$strSql = "INSERT INTO PERFIL_USUARIO_RECURSO (COD_PERFIL_USUARIO, COD_RECURSO)
						                               VALUES (:codPerfil, :codRecurso)";
				
				if (!$objConexao->execute($strSql, $arrayParametros)){
					$objConexao->rollbackTrans();
					return false;
				}
			}
			
			
			$objConexao->commitTrans();
		
			return true;
		} else {
			return false;
		}
	}
	
	
}