<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class empresa {

	public  $database;
	public  $error;
	
	private $ajusteDocumento;
    private $codEmpresa;
    private $codFilialConectividade;
    private $codTokenEmpresa;
    private $codTpProcessoConectividade;
    private $codUsuario;
    private $dscFraseSecreta;
    private $indAjusteDocumento;
    private $indUtilizaCalendario;
    private $dscLabelFilial;
    private $dscLabelGrupoEstabel;
    private $dscLabelEstabelecimento;
    private $nomEmpresa;
    private $codPerfilConectividade;
    
    function __construct($database) {
    	$this->database = $database;
    	$this->error['code'] 	= "";
    	$this->error['message'] = "";
        return true;
    }

    public function __get($propriedade) {
    	return $this->$propriedade;
    }
    
    public function __set($propriedade, $valor) {
    	$this->$propriedade = $valor;
    }
    
    //**********************************************************************************************//
    
    public function getCodEmpresa() {
        return $this->codEmpresa;
    }

    public function setCodEmpresa($codEmpresa) {
        $this->codEmpresa = $codEmpresa;
    }

    public function getNomEmpresa() {
        return $this->nomEmpresa;
    }

    public function setNomEmpresa($nomEmpresa) {
        $this->nomEmpresa = $nomEmpresa;
    }
    
    public function getDscFraseSecreta() {
    	return $this->dscFraseSecreta;
    }
    
    public function setDscFraseSecreta($dscFraseSecreta) {
    	$this->dscFraseSecreta = $dscFraseSecreta;
    }
    
    public function getCodTokenEmpresa() {
    	return $this->codTokenEmpresa;
    }
    
    public function setCodTokenEmpresa($codTokenEmpresa) {
    	$this->codTokenEmpresa = $codTokenEmpresa;
    }
    
    public function getCodFilialConectividade() {
    	return $this->codFilialConectividade;
    }
    
    public function setCodFilialConectividade($codFilialConectividade) {
    	$this->codFilialConectividade = $codFilialConectividade;
    }
    
    public function getCodTpProcessoConectividade() {
    	return $this->codTpProcessoConectividade;
    }
    
    public function setCodTpProcessoConectividade($codTpProcessoConectividade) {
    	$this->codTpProcessoConectividade = $codTpProcessoConectividade;
    }
    
    public function getCodUsuario() {
    	return $this->codUsuario;
    }
    
    public function getEmpresa($codEmpresa){
    	$objConexao = $this->database;
    	
    	$strSql = "SELECT COD_EMPRESA, NOM_EMPRESA, DSC_FRASE_SECRETA, COD_FILIAL_CONECTIVIDADE, 
    			          COD_TP_PROCESSO_CONECTIVIDADE, IND_AJUSTE_DOCUMENTO,IND_UTILIZA_CALENDARIO, DSC_LABEL_FILIAL,
    					  DSC_LABEL_GRUPO_ESTABEL, DSC_LABEL_ESTABELECIMENTO, COD_PERFIL_CONECTIVIDADE
    			     FROM EMPRESA 
    			    WHERE COD_EMPRESA = :codEmpresa";
    	
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ":codEmpresa";
    	$arrayParametros[0][1] = $codEmpresa;
    	
    	$arrayEmpresa = $objConexao->executeFetch($strSql, $arrayParametros);
    	
    	$this->codEmpresa 					= $arrayEmpresa[0]["COD_EMPRESA"];
    	$this->nomEmpresa 					= $arrayEmpresa[0]["NOM_EMPRESA"];
    	$this->dscFraseSecreta				= $arrayEmpresa[0]["DSC_FRASE_SECRETA"];
    	$this->codFilialConectividade		= $arrayEmpresa[0]["COD_FILIAL_CONECTIVIDADE"];
    	$this->codTpProcessoConectividade	= $arrayEmpresa[0]["COD_TP_PROCESSO_CONECTIVIDADE"];
    	$this->indAjusteDocumento			= $arrayEmpresa[0]["IND_AJUSTE_DOCUMENTO"];
    	$this->dscLabelFilial               = $arrayEmpresa[0]["DSC_LABEL_FILIAL"];
    	$this->dscLabelGrupoEstabel         = $arrayEmpresa[0]["DSC_LABEL_GRUPO_ESTABEL"];
    	$this->dscLabelEstabelecimento      = $arrayEmpresa[0]["DSC_LABEL_ESTABELECIMENTO"];
    	$this->indUtilizaCalendario			= $arrayEmpresa[0]["IND_UTILIZA_CALENDARIO"];
    	$this->codPerfilConectividade		= $arrayEmpresa[0]["COD_PERFIL_CONECTIVIDADE"];
    	
    }
    
    public function insEmpresa() {
        if (TRUE == $this->nomEmpresa) {
        	$objConexao = $this->database;
        	
            $strSql = "SELECT SQ_EMPRE_01.NEXTVAL AS COD_EMPRESA FROM DUAL";
            $arrayEmpresa = $objConexao->executeFetch($strSql);

            $strSql = "INSERT INTO EMPRESA
            		          (COD_EMPRESA, NOM_EMPRESA, DSC_FRASE_SECRETA, 
            		           COD_FILIAL_CONECTIVIDADE, COD_TP_PROCESSO_CONECTIVIDADE, 
            		           IND_AJUSTE_DOCUMENTO, IND_UTILIZA_CALENDARIO,
            		           DSC_LABEL_FILIAL, DSC_LABEL_GRUPO_ESTABEL, DSC_LABEL_ESTABELECIMENTO,
            				   COD_PERFIL_CONECTIVIDADE
    	                       )
            		   VALUES (:codEmpresa,:nomEmpresa, :dscFraseSecreta, 
            		           :codFilialConectividade, :codTpProcessoConectividade, 
            		           :indAjusteDocumento, :indUtilizaCalendario,
            		           :dscLabelFilial,:dscLabelGrupoEstabelecimento,:dscLabelEstabelecimento,
    	                       :codPerfilConectividade)";
            
            $arrayParametros = array();
            $arrayParametros[0][0] = ":codEmpresa";
            $arrayParametros[0][1] = $arrayEmpresa[0]["COD_EMPRESA"];
            $arrayParametros[1][0] = ":nomEmpresa";
            $arrayParametros[1][1] = $this->nomEmpresa;
            $arrayParametros[2][0] = ":dscFraseSecreta";
            $arrayParametros[2][1] = $this->dscFraseSecreta;
            $arrayParametros[3][0] = ":codFilialConectividade";
            $arrayParametros[3][1] = $this->codFilialConectividade;
            $arrayParametros[4][0] = ":codTpProcessoConectividade";
            $arrayParametros[4][1] = $this->codTpProcessoConectividade;
            $arrayParametros[5][0] = ":indAjusteDocumento";
            $arrayParametros[5][1] = $this->indAjusteDocumento;
            $arrayParametros[6][0] = ":dscLabelFilial";
            $arrayParametros[6][1] = $this->dscLabelFilial;
            $arrayParametros[7][0] = ":dscLabelGrupoEstabelecimento";
            $arrayParametros[7][1] = $this->dscLabelGrupoEstabelecimento;
            $arrayParametros[8][0] = ":dscLabelEstabelecimento";
            $arrayParametros[8][1] = $this->dscLabelEstabelecimento;
            $arrayParametros[9][0] = ":indUtilizaCalendario";
            $arrayParametros[9][1] = $this->indUtilizaCalendario;
            $arrayParametros[10][0] = ":codPerfilConectividade";
            $arrayParametros[10][1] = $this->codPerfilConectividade;
            

            if (!$objConexao->execute($strSql, $arrayParametros)){
            	$this->error['code'] 	= "";
            	$this->error['message'] = "Erro ao inserir empresa";
            	return false;
            }

            return true;
        } else {
        	$this->error['code'] 	= "";
        	$this->error['message'] = "Variáveis necessárias não informadas";
            return false;
        }
    }

    public function changeEmpresa($codEmpresa) {
        if ($codEmpresa && TRUE == $this->nomEmpresa) {
        	$objConexao = $this->database;
        	
            $strSql = "UPDATE EMPRESA 
            		      SET NOM_EMPRESA       			   = :nomEmpresa,
            		          DSC_FRASE_SECRETA 			   = :dscFraseSecreta,
            		          COD_FILIAL_CONECTIVIDADE 		   = :codFilialConectividade,
            		          COD_TP_PROCESSO_CONECTIVIDADE	   = :codTpProcessoConectividade,
            				  IND_AJUSTE_DOCUMENTO			   = :indAjusteDocumento,
            		          IND_UTILIZA_CALENDARIO		   = :indUtilizaCalendario,
            				  DSC_LABEL_FILIAL                 = :dscLabelFilial,
            				  DSC_LABEL_GRUPO_ESTABEL          = :dscLabelGrupoEstabelecimento,
            				  DSC_LABEL_ESTABELECIMENTO        = :dscLabelEstabelecimento,
            		          COD_PERFIL_CONECTIVIDADE         = :codPerfilConectividade
            		    WHERE COD_EMPRESA       			   = :codEmpresa";
            
            $arrayParametros = array();
            $arrayParametros[0][0] = ":codEmpresa";
            $arrayParametros[0][1] = $codEmpresa;
            $arrayParametros[1][0] = ":nomEmpresa";
            $arrayParametros[1][1] = $this->nomEmpresa;
            $arrayParametros[2][0] = ":dscFraseSecreta";
            $arrayParametros[2][1] = $this->dscFraseSecreta;
            $arrayParametros[3][0] = ":codFilialConectividade";
            $arrayParametros[3][1] = $this->codFilialConectividade;
            $arrayParametros[4][0] = ":codTpProcessoConectividade";
            $arrayParametros[4][1] = $this->codTpProcessoConectividade;
            $arrayParametros[5][0] = ":indAjusteDocumento";
            $arrayParametros[5][1] = $this->indAjusteDocumento;
            $arrayParametros[6][0] = ":dscLabelFilial";
            $arrayParametros[6][1] = $this->dscLabelFilial;
            $arrayParametros[7][0] = ":dscLabelGrupoEstabelecimento";
            $arrayParametros[7][1] = $this->dscLabelGrupoEstabelecimento;
            $arrayParametros[8][0] = ":dscLabelEstabelecimento";
            $arrayParametros[8][1] = $this->dscLabelEstabelecimento;
            $arrayParametros[9][0] = ":indUtilizaCalendario";
            $arrayParametros[9][1] = $this->indUtilizaCalendario;
            $arrayParametros[10][0] = ":codPerfilConectividade";
            $arrayParametros[10][1] = $this->codPerfilConectividade;
           
            if (!$objConexao->execute($strSql, $arrayParametros)){
            	$this->error['code'] 	= "";
            	$this->error['message'] = "Erro ao alterar empresa";
            	return false;
            }

            return true;
        } else {
        	$this->error['code'] 	= "";
        	$this->error['message'] = "Variáveis necessárias não informadas";
            return false;
        }
    }

    public function delEmpresa($codEmpresa) {

        if ($codEmpresa) {
            $objConexao = $this->database;

            $arrayParametros = array();
            $arrayParametros[0][0] = ":codEmpresa";
            $arrayParametros[0][1] = $codEmpresa;

            $strSql = "DELETE 
            		     FROM EMPRESA 
            		    WHERE COD_EMPRESA =:codEmpresa";
            if (!$objConexao->execute($strSql, $arrayParametros)){
            	$this->error['code'] 	= "";
            	$this->error['message'] = "Erro ao deletar empresa";
            	return false;
            }

            return true;
        } else {
        	$this->error['code'] 	= "";
        	$this->error['message'] = "Variáveis necessárias não informadas";
            return false;
        }
    }
    
    public function totalEmpresa(){
    	$objConexao = $this->database;
    	
    	$strSql = "SELECT COUNT(COD_EMPRESA) AS TOTAL FROM EMPRESA";
    	$arrayDados = $objConexao->executeFetch($strSql);
    	return $arrayDados[0]["TOTAL"];
    }
    
    /**
     * Gera o token do primeiro acesso para ser comparado
     * @param integer $codEmpresa
     * @return string
     */
    public function geraTokenBoasVindas($codEmpresa){
    	
    	$this->getEmpresa($codEmpresa);
    	date_default_timezone_set("Brazil/East");
    	return md5(str_pad($codEmpresa, 6, "0", STR_PAD_LEFT) . date("dmY") . $this->dscFraseSecreta);
    } 
    
    /**
     * Gera token de acesso do webservice (cada código deve ser único)
     * @param integer $codEmpresa
     * @return string
     */
    public function geraTokenAcesso($codEmpresa){
    	$this->getEmpresa($codEmpresa);
    	date_default_timezone_set("Brazil/East");
    	return md5(md5(str_pad($codEmpresa, 6, "0", STR_PAD_LEFT) . date("dmYHisu") . mt_rand() . $this->dscFraseSecreta));
    }
    
    /**
     * Valida o token de primeiro contato da empresa e gera um token de uso
     * @param integer $codEmpresa
     * @param string $codToken
     * @return boolean
     */
    public function validaTokenBoasVindas($codEmpresa, $codTokenCliente, $codUsuario){
    	$objConexao = $this->database;
    	
    	// gera token do dia
    	$tokenBoasVindas = $this->geraTokenBoasVindas($codEmpresa);
    	
    	if ($codTokenCliente == $tokenBoasVindas){
    		
    		// se validou o token de primeiro contato
    		$codTokenEmpresa = $this->geraTokenAcesso($codEmpresa);
    		
    		$arrayParametros = array();
            $arrayParametros[0][0] = ":codEmpresa";
            $arrayParametros[0][1] = $codEmpresa;
            $arrayParametros[1][0] = ":codToken";
            $arrayParametros[1][1] = $codTokenEmpresa;
            $arrayParametros[2][0] = ":indDisponivel";
            $arrayParametros[2][1] = 'S';
            $arrayParametros[3][0] = ":codUsuario";
            $arrayParametros[3][1] = $codUsuario;

            // insere na tabela empresa_token
            $strSql = "INSERT INTO EMPRESA_TOKEN (COD_EMPRESA, COD_TOKEN, IND_DISPONIVEL, DAT_CRIACAO, COD_USUARIO)
            		   VALUES (:codEmpresa, :codToken, :indDisponivel, TRUNC(SYSDATE), :codUsuario)";
            		 
            if (!$objConexao->execute($strSql, $arrayParametros)){
            	$this->error['code'] 	= "";
            	$this->error['message'] = "Ocorreu um erro ao atualizar token no banco";
            	return false;
            }
            
            $this->codTokenEmpresa = $codTokenEmpresa;
            
    		return true;
    	}else{
	    	return false; 
    	}
    	
    }
    
    public function validaTokenAcesso($codTokenAcesso){
   		if (!$codTokenAcesso){
			$this->error['code'] 	= "";
			$this->error['message'] = "Variáveis necessárias não informadas";
			return false;
		}
		
    	$objConexao = $this->database;
    	
    	// procura token
    	$strSql = "SELECT ET.COD_EMPRESA, E.NOM_EMPRESA, ET.COD_USUARIO
    			     FROM EMPRESA_TOKEN ET
    			     JOIN EMPRESA E ON (E.COD_EMPRESA = ET.COD_EMPRESA)
    			    WHERE ET.COD_TOKEN = :codToken
    			      AND ET.IND_DISPONIVEL = :indDisponivel";
    	
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ":codToken";
    	$arrayParametros[0][1] = $codTokenAcesso;
    	$arrayParametros[1][0] = ":indDisponivel";
    	$arrayParametros[1][1] = 'S';
    	
    	$arrayEmpresa = $objConexao->executeFetch($strSql, $arrayParametros);
    	
    	if (!$arrayEmpresa){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Ocorreu um erro ao validar token";
    		return false;
    	}
    	
    	
    	// altera token para indisponibilizá-lo
    	$strSql = "UPDATE EMPRESA_TOKEN
    			      SET IND_DISPONIVEL = :indDisponivel
    			    WHERE COD_TOKEN = :codToken";
    	 
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ":codToken";
    	$arrayParametros[0][1] = $codTokenAcesso;
    	$arrayParametros[1][0] = ":indDisponivel";
    	$arrayParametros[1][1] = 'N';
    	 
    	if (!$objConexao->execute($strSql, $arrayParametros)){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Ocorreu um erro ao atualizar o token";
    		return false;
    	}
    	
    	$this->codEmpresa = $arrayEmpresa[0]['COD_EMPRESA'];
    	$this->nomEmpresa = $arrayEmpresa[0]['NOM_EMPRESA'];
    	$this->codUsuario = $arrayEmpresa[0]['COD_USUARIO'];
    	
    	return true;
    	
    }
    
    /**
     * Retorna o codigo da empresa de um usuario
     * @param integer $codUsuario
     * @return integer|boolean
     */
    public function getCodEmpresaByUsuario($codUsuario){
    	if (!$codUsuario){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Variáveis necessárias não informadas";
    		return false;
    	}
    	
    	$objConexao = $this->database;
    	
    	$strSql = "SELECT PF.COD_EMPRESA
					 FROM USUARIO U
					 JOIN PESSOA_FISICA PF ON (PF.COD_PESSOA = U.COD_USUARIO)
					WHERE U.IND_ATIVO = 'S'
					  AND U.COD_USUARIO = :codUsuario";
    	 
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ":codUsuario";
    	$arrayParametros[0][1] = $codUsuario;

    	 
    	$arrayUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
    	 
    	if($arrayUsuario){
    		return $arrayUsuario[0]['COD_EMPRESA'];	
    	}else{
    		return false;
    	}

    }
    
    /**
     * Retorna o codigo da empresa de um estabelecimento
     * @param integer $codUsuario
     * @return integer|boolean
     */
    public function getCodEmpresaByEstabelecimento($codEstabelecimento){
    	if (!$codEstabelecimento){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Variáveis necessárias não informadas";
    		return false;
    	}
    	 
    	$objConexao = $this->database;
    	 
    	$strSql = "SELECT NVL(PJ.COD_EMPRESA, PF.COD_EMPRESA) AS COD_EMPRESA
                     FROM PESSOA P
                     LEFT OUTER JOIN PESSOA_JURIDICA PJ ON (PJ.COD_PESSOA = P.COD_PESSOA)
                     LEFT OUTER JOIN PESSOA_FISICA PF ON (PF.COD_PESSOA = P.COD_PESSOA)                   
                    WHERE P.COD_PESSOA = :COD_ESTABELECIMENTO";
    
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ":COD_ESTABELECIMENTO";
    	$arrayParametros[0][1] = $codEstabelecimento;
    
    
    	$arrayDados = $objConexao->executeFetch($strSql, $arrayParametros);
    
    	if($arrayDados){
    		return $arrayDados[0]['COD_EMPRESA'];
    	}else{
    		return false;
    	}
    
    }
    
    public function replicaEmpresa($codEmpresa, $codEmpresaReplica){
    	if (!$codEmpresaReplica){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Variáveis necessárias não informadas";
    		return false;
    	}
    	
    	$objConexao = $this->database;
    	
    	$strSql = "BEGIN PRC_REPLICA_EMPRESA(:codEmpresa, :codEmpresaReplica); END;";
    	
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ":codEmpresa";
    	$arrayParametros[0][1] = $codEmpresa;
    	$arrayParametros[1][0] = ":codEmpresaReplica";
    	$arrayParametros[1][1] = $codEmpresaReplica;
    	
    	if (!$objConexao->executeProcedure($strSql, $arrayParametros)){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro ao replicar";
    		return false;
    	}else{
    		return true;
    	}

    }

    public function limpaEmpresa($codEmpresa){
    	if (!$codEmpresa){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Variáveis necessárias não informadas";
    		return false;
    	}
    	
    	$objConexao = $this->database;
    	
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ":codEmpresa";
    	$arrayParametros[0][1] = $codEmpresa;
    	
    	// VERIFICA SE JA TEM DOCUMENTOS
    	$strSql = "SELECT COUNT(*) AS TOTAL
                     FROM DOCUMENTO D
                     JOIN PESSOA_JURIDICA PJ ON (PJ.COD_PESSOA = D.COD_ESTABELECIMENTO)
                    WHERE PJ.COD_EMPRESA = :codEmpresa";
    	
    	$dados = $objConexao->executeFetch($strSql, $arrayParametros);
    	
    	if (!$dados){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro verificar quantidade de documentos";
    		return false;
    	}
    	
    	if ($dados[0]['TOTAL'] > 0){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Não é possível limpar tabelas, exitem documentos armazenados";
    		return false;
    	}
    	
    	
    	$objConexao->beginTrans();
    	
    	// apaga TIPO_PROCESSO_TPDOC
    	$strSql = "DELETE FROM TIPO_PROCESSO_TPDOC
    			    WHERE COD_TIPO_PROCESSO IN (SELECT COD_TIPO_PROCESSO
    	                                          FROM TIPO_PROCESSO
    	                                         WHERE COD_EMPRESA = :codEmpresa)";
    	
    	if (!$objConexao->execute($strSql, $arrayParametros)){
    		$objConexao->rollbackTrans();
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro ao limpar documentos x dispositivos";
    		return false;
    	}
    	
    	// apaga TIPO_PROCESSO_CAMPO
    	$strSql = "DELETE FROM TIPO_PROCESSO_CAMPO
    			    WHERE COD_TIPO_PROCESSO IN (SELECT COD_TIPO_PROCESSO
    	                                          FROM TIPO_PROCESSO
    	                                         WHERE COD_EMPRESA = :codEmpresa)";
    	
    	if (!$objConexao->execute($strSql, $arrayParametros)){
    		$objConexao->rollbackTrans();
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro ao limpar campos de documentos";
    		return false;
    	}
    	
    	// apaga TIPO_PROCESSO
    	$strSql = "DELETE FROM TIPO_PROCESSO
    			    WHERE COD_EMPRESA = :codEmpresa";
    	 
    	if (!$objConexao->execute($strSql, $arrayParametros)){
    		$objConexao->rollbackTrans();
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro ao limpar processos";
    		return false;
    	}
    	
    	// apaga TIPO_PROCESSO_GRUPO
    	$strSql = "DELETE FROM TIPO_PROCESSO_GRUPO
    			    WHERE COD_EMPRESA = :codEmpresa";
    	
    	if (!$objConexao->execute($strSql, $arrayParametros)){
    		$objConexao->rollbackTrans();
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro ao limpar grupo de processos";
    		return false;
    	}
    	
    	$objConexao->commitTrans();
    	return true;
    }
    
}

