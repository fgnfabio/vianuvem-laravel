<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

require_once ('class_pessoa.php');

Class pessoaFisica extends pessoa
{
	public    $database;
	public    $error;
	protected $dscApelido;
	protected $numCpf;
	protected $codEmpresa;
	protected $nomEmpresa;
	protected $codUsuario;
	
	function __construct($database) {
		$this->database = $database;
		parent::__construct($this->database);
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getDscApelido() {
		return $this->dscApelido;
	}
	
	public function setDscApelido($dscApelido) {
		$this->dscApelido = $dscApelido;
	}
	
	public function getNumCpf() {
		return $this->numCpf;
	}
	
	public function getNumCpfToView() {
		return $this->formatCpfToView($this->numCpf);
	}
	
	public function setNumCpf($numCpf) {
		$this->numCpf = $numCpf;
	}
	
	public function getCodEmpresa() {
		return $this->codEmpresa;
	}
	
	public function setCodEmpresa($codEmpresa) {
		$this->codEmpresa = $codEmpresa;
	}
	
	public function getNomEmpresa() {
		return $this->nomEmpresa;
	}
	
	public function setNomEmpresa($nomEmpresa) {
		$this->nomEmpresa = $nomEmpresa;
	}
	
	public function getCodUsuario() {
		return $this->codUsuario;
	}
	
	public function setCodUsuario($codUsuario) {
		$this->codUsuario = $codUsuario;
	}
	
	public function getPessoaFisica($codPessoa){
		if (TRUE == $codPessoa){
		
			$objConexao = $this->database;
			$strSql = "SELECT PF.DSC_APELIDO, PF.NUM_CPF, E.COD_EMPRESA, E.NOM_EMPRESA
					     FROM PESSOA_FISICA PF
					     JOIN EMPRESA E ON (E.COD_EMPRESA = PF.COD_EMPRESA)
					    WHERE PF.COD_PESSOA =:codPessoa";
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $codPessoa;
				
			$arrayPessoaFisica = $objConexao->executeFetch($strSql, $arrayParametros);
				
			$this->dscApelido  	= $arrayPessoaFisica[0]["DSC_APELIDO"];
			$this->numCpf		= $arrayPessoaFisica[0]["NUM_CPF"];
			$this->codEmpresa	= $arrayPessoaFisica[0]["COD_EMPRESA"];
			$this->nomEmpresa	= $arrayPessoaFisica[0]["NOM_EMPRESA"];
				
			parent::getPessoa($codPessoa);
			
			return true;
		}else{
			return false;
		}
	}
	
	
	public function insPessoaFisica(){
		
		$this->setCodTipoPessoa('F');
		
		if(TRUE == $this->nomPessoa && TRUE == $this->dscApelido){
				
			
			if (!$this->InsPessoa()){
				return false;
			}
			
			$objConexao = $this->database;
			
			$objConexao = $this->database;
			$strSql = "INSERT INTO PESSOA_FISICA 
					          (COD_PESSOA, DSC_APELIDO, NUM_CPF, COD_EMPRESA) 
					   VALUES (:codPessoa, :dscApelido, :numCpf, :codEmpresa)";
		
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $this->codPessoa;
			$arrayParametros[1][0] = ":dscApelido";
			$arrayParametros[1][1] = $this->dscApelido;
			$arrayParametros[2][0] = ":numCpf";
			$arrayParametros[2][1] = $this->formatCpfToDb($this->numCpf);
			$arrayParametros[3][0] = ":codEmpresa";
			$arrayParametros[3][1] = $this->codEmpresa;
		
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao inserir pessoa fisica";
				return false;
			}
		
			return true;
		}else{
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para insrir pessoa física";
			return false;
		}
	}
	
	public function changePessoaFisica(){
		if(TRUE == $this->codPessoa && TRUE == $this->dscApelido){
			if(!$this->changePessoa()){
				return false;
			}
			
			$objConexao = $this->database;
			
			$strSql = "UPDATE PESSOA_FISICA 
					      SET DSC_APELIDO =:dscApelido 
					    WHERE COD_PESSOA =:codPessoa";
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $this->codPessoa;
			$arrayParametros[1][0] = ":dscApelido";
			$arrayParametros[1][1] = $this->dscApelido;
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar pessoa física";
				return false;
			}
	
			return true;
	
		}else{
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para alterar pessoa física";
			return false;
		}
	}
	
	public function delPessoaFisica($codPessoa){
		if(true == $codPessoa){
			$objConexao = $this->database;
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $codPessoa;
		
			$strSql = 'DELETE
					     FROM PESSOA_EMAIL
					    WHERE COD_PESSOA = :codPessoa';
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar pessoa física email";
				return false;
			}

			$strSql = 'DELETE
					     FROM PESSOA_FISICA
					    WHERE COD_PESSOA = :codPessoa';
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar pessoa física";
				return false;
			}
		
			if (parent::delPessoa($codPessoa)){
				return true;
			}else{
				return false;
			}
		}
		return false;
	}
	
	public function CpfExists(){
		$objConexao = $this->database;
	
		$strSql = "SELECT NUM_CPF
				     FROM PESSOA_FISICA
				    WHERE NUM_CPF = '" . $this->formatCpfToDb($this->numCpf) . "'
				      AND COD_EMPRESA = :codEmpresa";
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codEmpresa";
		$arrayParametros[0][1] = $this->codEmpresa;
	
	
		$result = $objConexao->executeFetch($strSql, $arrayParametros);
	
		if(null ==  $result || $result == "")
			return false;
	
		return true;
	
	}
	
	protected function formatCpfToDb($numCpf){
	
		$numCpf = str_replace('.', '', $numCpf);
		$numCpf = str_replace('-', '', $numCpf);
		$numCpf = str_replace('/', '', $numCpf);
	
		return $numCpf;
	}
	
	protected function formatCpfToView($numCpf){
	
		//Se existia zeros à esquerda,estes não foram gravados no banco.
		$numCpf = (string)$numCpf;
	
		while(strlen($numCpf) < 11){
			$numCpf = "0" . $numCpf;
		}
	
		$part1 = substr($numCpf, 0, 3);
		$part2 = substr($numCpf, 3, 3);
		$part3 = substr($numCpf, 6, 3);
		$part4 = substr($numCpf, 9, 2);
	
		return $part1 . "." . $part2 . "." . $part3 . "-" . $part4;
	}
	
	
}