<?php

namespace App\Classes;

/**
 * Gera menu dinamicamente
 * 
 * @author Marcos Menezes
 * @version 1.0
 * 
 */
Class menu {

	public  $database;
	public  $language;
	public  $error;
	private $codUsuario;
	
	function __construct($database, $language) {
		$this->database = $database;
		$this->language = $language;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
		
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function setCodUsuario($codUsuario){
		$this->codUsuario = $codUsuario;
	}
	
	public function getCodUsuario(){
		return $this->codUsuario;
	}
	
	public function montaMenu(){
	
		if (!$this->codUsuario){
			return false;
		}
		$menu = "";
		$objConexao = $this->database;
		$strSql = "DECLARE DSC_MENU LONG; BEGIN PRC_MONTA_MENU(:COD_USUARIO, :DSC_MENU); END;";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":COD_USUARIO";
		$arrayParametros[0][1] = $this->codUsuario;
		$arrayParametros[1][0] = ":DSC_MENU";
		$arrayParametros[1][1] = $menu;
		$arrayParametros[1][2] = "100000";
		
		$retorno = $objConexao->executeProcedure($strSql, $arrayParametros);
	    		
		return $retorno[1][1];		
	}
}