<?php

namespace App\Classes;

/**
 * @author: Rogerio Caetano da Silva
 */

Class MailSender
{
	public    $error;
	protected $mailSender;
	protected $mailTo;
	protected $subject;
	protected $message;
	
	
	function __construct() {
		
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function enviarEmail(){
		require_once("../phpmailer/class.phpmailer.php");
		
		//define('GUSER', 'rogerio.silva@rovereti.com.br');	// <-- Insira aqui o seu GMail
		//define('GPWD', 'xxxxx');		                   // <-- Insira aqui a senha do seu GMail
		
		ini_set("SMTP","ssl://smtp.gmail.com");
		ini_set("smtp_port","587");
		
		$mail = new PHPMailer();
		
		//if($this->closeSmtp == true){
		//	$mail->SmtpClose();
		//	return true;
		//}
		
		$mail->SMTPKeepAlive = true;//$this->keepAlive;
		$mail->CharSet = 'UTF-8';
		$mail->IsSMTP();		        // Ativar SMTP
		$mail->SMTPDebug = 0;		    // Debugar: 1 = erros e mensagens, 2 = mensagens apenas
		$mail->SMTPAuth = true;		    // Autenticação ativada
		$mail->SMTPSecure = 'tls';	    // SSL REQUERIDO pelo GMail
		$mail->Host = 'smtp.gmail.com';	// SMTP utilizado
		$mail->Port = 587;  	        // A porta 587 deverá estar aberta em seu servidor
		$mail->Username = $this->mailSender;
		$mail->Password = $this->mailPwd;
		$mail->SetFrom($this->mailSender, "ViaNuvem");
		$mail->IsHTML(true);
		$mail->Subject = $this->subject;
		$mail->Body = $this->message;
		//$mail->AltBody = "Teste de envio de body alternativo";
		$mail->AddAddress($this->mailTo);
		if(!$mail->Send()) {
			$error = 'Mail error: '.$mail->ErrorInfo;
			$this->error = $error;
			
			return false;
		} else {
			$error = false;
			$this->error = $error;
			return true;
		}
		
	}
}
?>