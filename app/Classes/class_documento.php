<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

include('class_documentoAndamento.php');

Class documento {

	private $andamento;
	public  $database;
	public  $error;
    private $codDocumento;
    private $codProcesso;
    private $codTipoProcesso;
    private $codTipoDocumento;
    private $codEstabelecimento;
    private $codUsuario;
    private $codIpEstacao;
    private $codMacAddress;
    private $codTipoDispositivo;
    private $codMimeType;
    private $dscMimeType;
    private $dthCaptacao;
    private $blbArquivo;
    private $dthDataIni;
    private $dthDataFim;
    private $codToken;
    private $codIdentificacao;
    private $codTipoIdentificador;
    private $codEmpresa;
    private $codTipoUsuario;
    private $arrayCampos;
    private $dscConteudoCampo;
    private $nomCampo;
    private $numOrdem;
    private $indConfereDocumento;
    private $codSituacao;

    function __construct($database) {
    	$this->database = $database;
    	$this->error['code'] 	= "";
    	$this->error['message'] = "";
    	
    	$this->andamento = new documentoAndamento($this->database);
    	return true;
    }
    
    public function __get($propriedade) {
    	return $this->$propriedade;
    }
    
    public function __set($propriedade, $valor) {
    	$this->$propriedade = $valor;
    }
    
    //**********************************************************************************************//
    
    public function getCodDocumento() {
    	return $this->codDocumento;
    }
    
    public function setCodDocumento($codDocumento) {
    	$this->codDocumento = $codDocumento;
    }
    
    public function getCodProcesso() {
    	return $this->codProcesso;
    }
    
    public function setCodProcesso($codProcesso) {
    	$this->codProcesso = $codProcesso;
    }
    
    public function getCodTipoProcesso() {
    	return $this->codTipoProcesso;
    }
    
    public function setCodTipoProcesso($codTipoProcesso) {
    	$this->codTipoProcesso = $codTipoProcesso;
    }
    
    public function getCodTipoDocumento() {
    	return $this->codTipoDocumento;
    }
    
    public function setCodTipoDocumento($codTipoDocumento) {
    	$this->codTipoDocumento = $codTipoDocumento;
    }
    
    public function getCodEstabelecimento() {
    	return $this->codEstabelecimento;
    }
    
    public function setCodEstabelecimento($codEstabelecimento) {
    	$this->codEstabelecimento = $codEstabelecimento;
    }
    
    public function getCodUsuario() {
    	return $this->codUsuario;
    }
    
    public function setCodUsuario($codUsuario) {
    	$this->codUsuario = $codUsuario;
    }
    
    public function getCodIpEstacao() {
    	return $this->codIpEstacao;
    }
    
    public function setCodIpEstacao($codIpEstacao) {
    	$this->codIpEstacao = $codIpEstacao;
    }
    
    public function getCodMacAddress() {
    	return $this->codMacAddress;
    }
    
    public function setCodMacAddress($codMacAddress) {
    	$this->codMacAddress = $codMacAddress;
    }
    
    public function getCodToken() {
    	return $this->codToken;
    }
    
    public function setCodToken($codToken) {
    	$this->codToken = $codToken;
    }
    
    public function getCodTipoDispositivo() {
    	return $this->codTipoDispositivo;
    }
    
    public function setCodTipoDispositivo($codTipoDispositivo) {
    	$this->codTipoDispositivo = $codTipoDispositivo;
    }
    
    public function getDthDataIni() {
    	return $this->dthDataIni;
    }
    
    public function setDthDataIni($dthDataIni) {
    	$this->dthDataIni = $dthDataIni;
    }
    
    public function getDthDataFim() {
    	return $this->dthDataFim;
    }
    
    public function setDthDataFim($dthDataFim) {
    	$this->dthDataFim = $dthDataFim;
    }
    
    public function getDthCaptacao() {
    	return $this->dthCaptacao;
    }
    
    public function setDthCaptacao($dthCaptacao) {
    	$this->dthCaptacao = $dthCaptacao;
    }
    
    public function getCodMimeType() {
    	return $this->codMimeType;
    }
    
    public function setCodMimeType($codMimeType) {
    	$this->codMimeType = $codMimeType;
    }
    
    public function getCodIdentificacao() {
    	return $this->codIdentificacao;
    }
    
    public function setCodIdentificacao($codIdentificacao) {
    	$this->codIdentificacao = $codIdentificacao;
    }
    
    public function getCodTipoIdentificador() {
    	return $this->codTipoIdentificador;
    }
    
    public function setCodTipoIdentificador($codTipoIdentificador) {
    	$this->codTipoIdentificador = $codTipoIdentificador;
    }
    
    public function getCodEmpresa() {
    	return $this->codEmpresa;
    }
    
    public function setCodEmpresa($codEmpresa) {
    	$this->codEmpresa = $codEmpresa;
    }
    
    public function getCodTipoUsuario() {
    	return $this->codTipoUsuario;
    }
    
    public function setCodTipoUsuario($codTipoUsuario) {
    	$this->codTipoUsuario = $codTipoUsuario;
    }
    
    
    // pega o tipo mime do documento
    public function getDscMimeType($codDocumento) {
    	$objConexao = $this->database;
    	
    	$strSql = "SELECT M.DSC_MIME_TYPE
        		     FROM MIME_TYPE M
                     JOIN DOCUMENTO D ON (D.COD_MIME_TYPE = M.COD_MIME_TYPE)
        		    WHERE D.COD_DOCUMENTO = :codDocumento";
    	
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ':codDocumento';
    	$arrayParametros[0][1] = $codDocumento;
    	
    	$arrayDocumento = $objConexao->executeFetch($strSql, $arrayParametros);
    	
    	$this->dscMimeType = $arrayDocumento[0]["DSC_MIME_TYPE"];
    	
    	return $this->dscMimeType;
    }
    
    // pega o mime do banco
    public function getCodMimeByDscMime($dscMimeType){
    	$objConexao = $this->database;
    	 
    	$strSql = "SELECT M.COD_MIME_TYPE
        		     FROM MIME_TYPE M
        		    WHERE M.DSC_MIME_TYPE = :dscMimeType";
    	 
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ':dscMimeType';
    	$arrayParametros[0][1] = $dscMimeType;
    	 
    	$arrayDocumento = $objConexao->executeFetch($strSql, $arrayParametros);
    	
    	if (!$arrayDocumento){
    		return false;
    	}else{
    		return $arrayDocumento[0]["COD_MIME_TYPE"];
    	}
    	
    }
    
    public function getBlbArquivo() {
    	if (!$this->codDocumento){
    		return false;
    	}
    	
    	$objConexao = $this->database;
    	 
    	$stmt = oci_parse($objConexao->DBobj, "SELECT IMG_DOCUMENTO FROM DOCUMENTO_IMAGEM WHERE COD_DOCUMENTO = :codDocumento");
    	oci_bind_by_name($stmt, ":codDocumento", $this->codDocumento, -1);
    	oci_execute($stmt);
    	
    	if (oci_fetch($stmt)){
    		$this->blbArquivo = ociresult($stmt, "IMG_DOCUMENTO");
    		return $this->blbArquivo->load();
    	}
    	
    	/*** método antigo com PDO ***/    	
    	//$stmt = $objConexao->DBobj->prepare("SELECT IMG_DOCUMENTO FROM DOCUMENTO_IMAGEM WHERE COD_DOCUMENTO = ?");
    	//$stmt->execute(array($this->codDocumento));
    	//$stmt->bindColumn(1, $this->blbArquivo, PDO::PARAM_LOB);
    	//$stmt->fetch(PDO::FETCH_BOUND);
    	//return $this->blbArquivo;
    }
    
    public function setBlbArquivo($blbArquivo) {
    	$this->blbArquivo = $blbArquivo;
    }
    
    public function getDocumento($codDocumento){
     	$objConexao = $this->database;
     	
        $strSql = "SELECT D.COD_DOCUMENTO, D.COD_TIPO_DOCUMENTO, D.COD_ESTABELECIMENTO, D.COD_TIPO_DISPOSITIVO, D.COD_USUARIO,
        		          D.COD_PROCESSO, D.DTH_CAPTACAO, D.COD_IP_ESTACAO, D.COD_MAC_ADRESS, D.COD_MIME_TYPE, D.COD_SITUACAO,
        		          M.DSC_MIME_TYPE, TP.IND_CONFERE_DOCUMENTO
        		     FROM DOCUMENTO D
        		     JOIN MIME_TYPE M ON (M.COD_MIME_TYPE = D.COD_MIME_TYPE)
        		     LEFT OUTER JOIN TIPO_PROCESSO TP ON (TP.COD_TIPO_PROCESSO = D.COD_TIPO_PROCESSO)
        		    WHERE COD_DOCUMENTO = :codDocumento
                      AND D.IND_EXCLUIDO ='N' ";

        $arrayParametros = array();
        $arrayParametros[0][0] = ':codDocumento';
        $arrayParametros[0][1] = $codDocumento;

        $arrayDocumento = $objConexao->executeFetch($strSql, $arrayParametros);
        
        $this->codDocumento 		= $arrayDocumento[0]["COD_DOCUMENTO"];
        $this->codTipoDocumento 	= $arrayDocumento[0]["COD_TIPO_DOCUMENTO"];
        $this->codEstabelecimento 	= $arrayDocumento[0]["COD_ESTABELECIMENTO"];
        $this->codTipoDispositivo 	= $arrayDocumento[0]["COD_TIPO_DISPOSITIVO"];
        $this->codUsuario 			= $arrayDocumento[0]["COD_USUARIO"];
        $this->codProcesso 			= $arrayDocumento[0]["COD_PROCESSO"];
        $this->dthCaptacao 			= $arrayDocumento[0]["DTH_CAPTACAO"];
        $this->codIpEstacao 		= $arrayDocumento[0]["COD_IP_ESTACAO"];
        $this->codMacAddress 		= $arrayDocumento[0]["COD_MAC_ADRESS"];
        $this->codMimeType 			= $arrayDocumento[0]["COD_MIME_TYPE"];
        $this->dscMimeType 			= $arrayDocumento[0]["DSC_MIME_TYPE"];
        $this->indConfereDocumento  = $arrayDocumento[0]["IND_CONFERE_DOCUMENTO"];
        $this->codSituacao          = $arrayDocumento[0]["COD_SITUACAO"];

        return true;
    }
    
    public function getDocumentoFiltros(){
    	$objConexao = $this->database;
    	
    	$where = "";
    	$i = 0;
    	
    	$arrayParametros = array();
    	
    	//$where .= " AND D.COD_EMPRESA = :codEmpresa ";
    	$arrayParametros[$i][0] = ':codEmpresa';
    	$arrayParametros[$i][1] = $this->codEmpresa;
    	$i++;
    	   	
    	// procura pelo documento especifico
    	if ($this->codDocumento){
    		$where .= " AND D.COD_DOCUMENTO = :codDocumento ";
    		$arrayParametros[$i][0] = ':codDocumento';
    		$arrayParametros[$i][1] = $this->codDocumento;
    		$i++;
    	
    	// procura usando filtros
    	}else{ 	
    			
    		$where .= " AND D.COD_TIPO_PROCESSO = :codTipoProcesso ";
    		$arrayParametros[$i][0] = ':codTipoProcesso';
    		$arrayParametros[$i][1] = $this->codTipoProcesso;
    		$i++;
    		
    		
    		//$where .= " AND PC.COD_TIPO_IDENTIFICADOR = :codTipoIdentificador ";
    		//$arrayParametros[$i][0] = ':codTipoIdentificador';
    		//$arrayParametros[$i][1] = $this->codTipoIdentificador;
    		//$i++;

    		// busca por período
    		if ($this->dthDataIni && $this->dthDataFim ){
    			
    			// verifica se as datas tem hora, senao coloca inicio e fim do dia
    			$aux = explode(" ", $this->dthDataIni);
    			if (count($aux) == 1){
	    			$this->dthDataIni = $this->dthDataIni." 00:00:00";
    			}
    			
    			$aux = explode(" ", $this->dthDataFim);
    			if (count($aux) == 1){
    				$this->dthDataFim = $this->dthDataFim." 23:59:59";
    			}
    			
    			$where .= "AND D.DTH_CAPTACAO BETWEEN TO_DATE(:datIni, 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE(:datFim, 'DD/MM/YYYY HH24:MI:SS') ";
    			$arrayParametros[$i][0] = ':datIni';
    			$arrayParametros[$i][1] = $this->dthDataIni;
    			$i++;
    			$arrayParametros[$i][0] = ':datFim';
    			$arrayParametros[$i][1] = $this->dthDataFim;
    			$i++;
    		// busca por dia
    		}else{
    			$where .= " AND TRUNC(D.DTH_CAPTACAO) = :data ";
    			$arrayParametros[$i][0] = ':data';
    			$arrayParametros[$i][1] = $this->dthDataIni;
    			$i++;
    		}
    		
    		if ($this->codTipoDocumento){
    			$where .= " AND D.COD_TIPO_DOCUMENTO = :codTipoDocumento ";
    			$arrayParametros[$i][0] = ':codTipoDocumento';
    			$arrayParametros[$i][1] = $this->codTipoDocumento;
    			$i++;
    		}
    		 
    		if ($this->codEstabelecimento){
    			$where .= " AND D.COD_ESTABELECIMENTO = :codEstabelecimento ";
    			$arrayParametros[$i][0] = ':codEstabelecimento';
    			$arrayParametros[$i][1] = $this->codEstabelecimento;
    			$i++;
    		}
    		 
    		if ($this->codIdentificacao){
    			//$where .= " AND PC.DSC_CONTEUDO_CAMPO = :codIdentificacao ";
    			$where .= " AND D.INDEXADOR1 = :codIdentificacao ";
    			$arrayParametros[$i][0] = ':codIdentificacao';
    			$arrayParametros[$i][1] = $this->codIdentificacao;
    			$i++;
    		}
    	
    	}
    	    	    	
    	$strSql = "SELECT D.COD_DOCUMENTO, D.COD_TIPO_DOCUMENTO, TD.DSC_TIPO_DOCUMENTO, D.COD_TIPO_DISPOSITIVO,
                          D.COD_USUARIO, U.DSC_IDENTIFICACAO_USUARIO, D.COD_MIME_TYPE, MT.DSC_MIME_TYPE, D.COD_ESTABELECIMENTO, PJE.NOM_FANTASIA AS NOM_ESTABELECIMENTO, PJE.NUM_CNPJ,
                          D.DTH_CAPTACAO, D.COD_PROCESSO, D.COD_TIPO_PROCESSO, TP.DSC_TIPO_PROCESSO, NULL AS COD_TIPO_IDENTIFICADOR,  NULL AS DSC_TIPO_IDENTIFICADOR, 
                          D.INDEXADOR1 AS COD_IDENTIFICACAO
					 FROM DOCUMENTO             D
    	             JOIN TIPO_DOCUMENTO        TD  ON (TD.COD_TIPO_DOCUMENTO = D.COD_TIPO_DOCUMENTO)
    	             JOIN USUARIO               U   ON (U.COD_USUARIO = D.COD_USUARIO)
    	             JOIN MIME_TYPE             MT  ON (MT.COD_MIME_TYPE = D.COD_MIME_TYPE)
    	             JOIN PESSOA_JURIDICA       PJE ON (PJE.COD_PESSOA = D.COD_ESTABELECIMENTO)
    	             JOIN TIPO_PROCESSO         TP  ON (TP.COD_TIPO_PROCESSO = D.COD_TIPO_PROCESSO)
    	             --JOIN TIPO_IDENTIFICADOR  TI  ON (TI.COD_TIPO_IDENTIFICADOR = :codTipoIdentificador )
    	             JOIN ESTABELECIMENTO       E   ON (E.COD_ESTABELECIMENTO = D.COD_ESTABELECIMENTO)
					WHERE D.COD_EMPRESA         = :codEmpresa
    			      AND D.IND_EXCLUIDO = 'N'
    				  --AND D.COD_TIPO_PROCESSO = :codTipoProcesso
    				      ".$where."
    				ORDER BY D.COD_DOCUMENTO DESC ";
    	
    	
    	$arrayDocumento = $objConexao->executeFetch($strSql, $arrayParametros);
    	
    	return $arrayDocumento;
    }
    
    /**
     * verifica se o documento pertente a empresa
     * @param integer $codDocumento
     * @param integer $codEmpresa
     * @return boolean
     */
    public function verificaDocumentoEmpresa($codDocumento, $codEmpresa){
    	$objConexao = $this->database;
    	
    	$strSql =  "SELECT D.COD_DOCUMENTO
					  FROM DOCUMENTO D
					 WHERE D.COD_EMPRESA = :codEmpresa
					   AND D.COD_DOCUMENTO = :codDocumento";
    	
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ':codDocumento';
    	$arrayParametros[0][1] = $codDocumento;
    	$arrayParametros[1][0] = ':codEmpresa';
    	$arrayParametros[1][1] = $codEmpresa;
    	
    	$dados = $objConexao->executeFetch($strSql, $arrayParametros);
    	
    	return ($dados[0]["COD_DOCUMENTO"]) ? true : false;
    }
    
    /**
     * verifica se o documento pertente a filial
     * @param integer $codDocumento
     * @param integer $codFilial
     * @return boolean
     */
    public function verificaDocumentoFilial($codDocumento, $codFilial){
    	$objConexao = $this->database;
    	
    	$strSql =  "SELECT D.COD_DOCUMENTO
					  FROM DOCUMENTO D
					  JOIN PROCESSO P ON (P.COD_PROCESSO = D.COD_PROCESSO)
					  JOIN ESTABELECIMENTO E ON (E.COD_ESTABELECIMENTO = P.COD_ESTABELECIMENTO)
					  JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_GRUPO_ESTABELECIMENTO = E.COD_GRUPO_ESTABELECIMENTO)
					  JOIN FILIAL F ON (F.COD_FILIAL = GE.COD_FILIAL)
					 WHERE F.COD_FILIAL = :codFilial
					   AND D.COD_DOCUMENTO = :codDocumento";
    	
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ':codDocumento';
    	$arrayParametros[0][1] = $codDocumento;
    	$arrayParametros[1][0] = ':codFilial';
    	$arrayParametros[1][1] = $codFilial;
    	
    	$dados = $objConexao->executeFetch($strSql, $arrayParametros);
    	 
    	return ($dados[0]["COD_DOCUMENTO"]) ? true : false;
    }
    
    /**
     * verifica se o documento pertente ao grupo de estabelecimento
     * @param integer $codDocumento
     * @param integer $codGrupoEstabel
     * @return boolean
     */
    public function verificaDocumentoGrupo($codDocumento, $codGrupoEstabel){
    	$objConexao = $this->database;
    	
    	$strSql =  "SELECT D.COD_DOCUMENTO
					  FROM DOCUMENTO D
					  JOIN PROCESSO P ON (P.COD_PROCESSO = D.COD_PROCESSO)
					  JOIN ESTABELECIMENTO E ON (E.COD_ESTABELECIMENTO = P.COD_ESTABELECIMENTO)
					  JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_GRUPO_ESTABELECIMENTO = E.COD_GRUPO_ESTABELECIMENTO)
					 WHERE GE.COD_GRUPO_ESTABELECIMENTO = :codGrupoEstabelecimento
					   AND D.COD_DOCUMENTO = :codDocumento";
    	
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ':codDocumento';
    	$arrayParametros[0][1] = $codDocumento;
    	$arrayParametros[1][0] = ':codGrupoEstabelecimento';
    	$arrayParametros[1][1] = $codGrupoEstabel;
    	
    	$dados = $objConexao->executeFetch($strSql, $arrayParametros);
    	 
    	return ($dados[0]["COD_DOCUMENTO"]) ? true : false;
    }
    
    /**
     * verifica se o documento pertence ao estabelecimento
     * @param integer $codDocumento
     * @param integer $codEstabelecimento
     * @return boolean
     */
    public function verificaDocumentoEstabelecimento($codDocumento, $codEstabelecimento){
    	$objConexao = $this->database;
    	
    	$strSql =  "SELECT D.COD_DOCUMENTO
					  FROM DOCUMENTO D
					  JOIN PROCESSO P ON (P.COD_PROCESSO = D.COD_PROCESSO)
					  JOIN ESTABELECIMENTO E ON (E.COD_ESTABELECIMENTO = P.COD_ESTABELECIMENTO)
					 WHERE E.COD_ESTABELECIMENTO = :codEstabelecimento
					   AND D.COD_DOCUMENTO = :codDocumento";
    	
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ':codDocumento';
    	$arrayParametros[0][1] = $codDocumento;
    	$arrayParametros[1][0] = ':codEstabelecimento';
    	$arrayParametros[1][1] = $codEstabelecimento;
    	
    	$dados = $objConexao->executeFetch($strSql, $arrayParametros);
    	 
    	return ($dados[0]["COD_DOCUMENTO"]) ? true : false;
    }
    
    // insere novo documento
    public function insDocumento(){
    	$objConexao = $this->database;
    	
    	if (!$this->codTipoDocumento or !$this->codEstabelecimento or !$this->codUsuario or !$this->codTipoDispositivo){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro ao receber informações para gravação";
    		$objConexao->rollbackTrans();
    		return false;
    	}
    	
    	$codSituacao = "N";
    	$countRecurso[0]['IND_CONFERE'] = 0;
    	
    	if ( !is_numeric($this->codProcesso) ){
    		$this->codProcesso = null;
    	}
    	
    	//VERIFICA SE O TIPO DE PROCESSO EXIGE CONFERENCIA DOS DOCUMENTOS
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ':COD_TIPO_PROCESSO';
    	$arrayParametros[0][1] = $this->codTipoProcesso;
    	//verifica se existe o recurso de Conferencia de Documento para o usuario
    	$strSql = "SELECT TP.IND_CONFERE_DOCUMENTO
  					 FROM TIPO_PROCESSO TP
                    WHERE TP.COD_TIPO_PROCESSO = :COD_TIPO_PROCESSO";
    	
    	$indConfereDocumento = $objConexao->executeFetch($strSql,$arrayParametros );
    	
    	if ($indConfereDocumento[0]["IND_CONFERE_DOCUMENTO"] == 'S'){
	    	
    		$arrayParametros = array();
	    	$arrayParametros[0][0] = ':COD_USUARIO';
	    	$arrayParametros[0][1] = $this->codUsuario;
	    	//verifica se existe o recurso de Conferencia de Documento para o usuario
	    	$strSql = "SELECT COUNT(PUR.COD_RECURSO) AS IND_CONFERE
	  					 FROM USUARIO U
	                     JOIN USUARIO_PERFIL_USUARIO USU ON (USU.COD_USUARIO = U.COD_USUARIO)
	                     JOIN PERFIL_USUARIO_RECURSO PUR ON (PUR.COD_PERFIL_USUARIO = USU.COD_PERFIL_USUARIO AND PUR.COD_RECURSO = 52 )
	                    WHERE U.COD_USUARIO = :COD_USUARIO";
	    	$countRecurso = $objConexao->executeFetch($strSql,$arrayParametros );
    	}
    	
    	$strSql = "SELECT SQ_DOCUM_01.NEXTVAL AS COD_DOCUMENTO FROM DUAL";
    	$proximoDocumento = $objConexao->executeFetch($strSql);
    	
    	if (!$proximoDocumento){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro ao gerar ID";
    		$objConexao->rollbackTrans();
    		return false;
    	}
    	
    	$objConexao->beginTrans();
    	               
        $this->codDocumento = $proximoDocumento[0]["COD_DOCUMENTO"];
               
        if($countRecurso[0]['IND_CONFERE'] > 0){
        	$codSituacao = "C";
        }
        // insere documento
        $strSql = "INSERT INTO DOCUMENTO 
        		          (COD_DOCUMENTO, COD_TIPO_DOCUMENTO, COD_ESTABELECIMENTO, COD_PROCESSO, COD_MIME_TYPE,
        		           COD_USUARIO, COD_IP_ESTACAO, COD_MAC_ADRESS, DTH_CAPTACAO, COD_TIPO_DISPOSITIVO, COD_EMPRESA, 
        		           COD_TIPO_PROCESSO, INDEXADOR1, INDEXADOR2, INDEXADOR3, INDEXADOR4, INDEXADOR5, INDEXADOR6, COD_SITUACAO) 
        		   VALUES (:codDocumento, :codTipoDocumento, :codEstabelecimento, :codProcesso, :codMimeType, 
        		           :codUsuario, :codIpEstacao, :codMacAddress, SYSDATE, :codTipoDispositivo, :codEmpresa,
    	                   :codTipoProcesso, :indexador1, :indexador2, :indexador3, :indexador4, :indexador5, :indexador6, :codSituacao)";
        
        $arrayParametros = array();
        $arrayParametros[0][0] = ':codDocumento';
        $arrayParametros[0][1] = $this->codDocumento;
        $arrayParametros[1][0] = ':codTipoDocumento';
        $arrayParametros[1][1] = $this->codTipoDocumento;
        $arrayParametros[2][0] = ':codEstabelecimento';
        $arrayParametros[2][1] = $this->codEstabelecimento;
        $arrayParametros[3][0] = ':codProcesso';
        $arrayParametros[3][1] = $this->codProcesso;
        $arrayParametros[4][0] = ':codMimeType'; 
        $arrayParametros[4][1] = $this->codMimeType;
        $arrayParametros[5][0] = ':codUsuario';
        $arrayParametros[5][1] = $this->codUsuario;
        $arrayParametros[6][0] = ':codIpEstacao';
        $arrayParametros[6][1] = $this->codIpEstacao;
        $arrayParametros[7][0] = ':codMacAddress';
        $arrayParametros[7][1] = $this->codMacAddress;
        $arrayParametros[8][0] = ':codTipoDispositivo';
        $arrayParametros[8][1] = $this->codTipoDispositivo;
        $arrayParametros[9][0] = ':codEmpresa';
        $arrayParametros[9][1] = $this->codEmpresa;
        $arrayParametros[10][0] = ':codTipoProcesso';
        $arrayParametros[10][1] = $this->codTipoProcesso;
        $arrayParametros[11][0] = ':indexador1';
        $arrayParametros[11][1] = $this->arrayCampos[1];
        $arrayParametros[12][0] = ':indexador2';
        $arrayParametros[12][1] = $this->arrayCampos[2];
        $arrayParametros[13][0] = ':indexador3';
        $arrayParametros[13][1] = $this->arrayCampos[3];
        $arrayParametros[14][0] = ':indexador4';
        $arrayParametros[14][1] = $this->arrayCampos[4];
        $arrayParametros[15][0] = ':indexador5';
        $arrayParametros[15][1] = $this->arrayCampos[5];
        $arrayParametros[16][0] = ':indexador6';
        $arrayParametros[16][1] = $this->arrayCampos[6];
        $arrayParametros[17][0] = ':codSituacao';
        $arrayParametros[17][1] = $codSituacao;
                
        if (!$objConexao->execute($strSql, $arrayParametros)){
        	$this->error['code'] 	= "";
        	$this->error['message'] = "Erro ao inserir documento ";
        	$objConexao->rollbackTrans();
        	return false;
        }
           
        // insere documento imagem
        $strSql = "INSERT INTO DOCUMENTO_IMAGEM
        		          (COD_DOCUMENTO, IMG_DOCUMENTO)
        		   VALUES ('".$this->codDocumento."', EMPTY_BLOB()) RETURNING IMG_DOCUMENTO INTO :IMG_DOCUMENTO";
        
        //$conexaoLob = $objConexao->conectaLOB();
        $lob = oci_new_descriptor($this->database->DBobj, OCI_D_LOB);
        $stmt = oci_parse($this->database->DBobj, $strSql);
        oci_bind_by_name($stmt, ':IMG_DOCUMENTO', $lob, -1, OCI_B_BLOB);
        $r = oci_execute($stmt, OCI_NO_AUTO_COMMIT);
        
        if (!$r) {
        	$e = oci_error($stmt);
        	trigger_error(htmlentities($e['message']), E_USER_ERROR);
        
        	$this->error['code'] 	= "";
        	$this->error['message'] = "Erro ao inserir imagem";
        
        	$objConexao->rollbackTrans();
        	return false;
        }
        
        $lob->write($this->blbArquivo);
        
        if (!$r) {
        	$e = oci_error($this->database->DBobj);
        	trigger_error(htmlentities($e['message']), E_USER_ERROR);
        
        	$this->error['code'] 	= "";
        	$this->error['message'] = "Erro ao comitar imagem";
        
        	$objConexao->rollbackTrans();
        	return false;
        }
        $objConexao->commitTrans();
        
        //VERIFICA SE UTILIZA PROCESSO E SE FOI FEITO UPLOAD DOS DOCUMENTOS OBRIGATORIOS
        $strSql = "SELECT IND_UTILIZA_PROCESSO FROM TIPO_PROCESSO WHERE COD_TIPO_PROCESSO = :CodTipoProcesso";
        	$arrayParametros = array();
        	$arrayParametros[0][0] = ":codTipoProcesso";
        	$arrayParametros[0][1] = $this->codTipoProcesso;
        	
        $UtilizaProcesso = $objConexao->executeFetch($strSql, $arrayParametros);
     
        if ($UtilizaProcesso[0]['IND_UTILIZA_PROCESSO'] == 'S'){
        	//UTILIZA PROCESSO, FAZ UM LOOP NOS DOCUMENTOS OBRIGATORIOS
        	$strSql = "SELECT COUNT(COD_TIPO_DOCUMENTO) AS TOTAL FROM TIPO_PROCESSO_TPDOC
        				WHERE COD_TIPO_PROCESSO = :CodTipoProcesso
        				AND IND_OBRIGATORIO = 'S'";
        
        	$arrayParametros = array();
        	$arrayParametros[0][0] = ":CodTipoProcesso";
        	$arrayParametros[0][1] = $this->codTipoProcesso;
        
        	$TotalDocumentosObrigatorios = $objConexao->executeFetch($strSql, $arrayParametros);
        	$TotalDocumentosObrigatorios = $TotalDocumentosObrigatorios[0]['TOTAL'];
        	 
        	//VERIFICA SE FOI FEITO UPLOAD DE TODOS OS DOCUMENTOS OBRIGATORIOS
        	$strSql = "SELECT COUNT (DISTINCT COD_TIPO_DOCUMENTO) AS TOTAL
        				FROM DOCUMENTO
        				WHERE COD_TIPO_DOCUMENTO IN (SELECT COD_TIPO_DOCUMENTO
        												FROM TIPO_PROCESSO_TPDOC
        												WHERE COD_TIPO_PROCESSO = :CodTipoProcesso
        												AND IND_OBRIGATORIO = 'S')
        				AND COD_TIPO_PROCESSO = :CodTipoProcesso
						AND COD_PROCESSO = :CodProcesso";
        	$arrayParametros = array();
        	$arrayParametros[0][0] = ":CodTipoProcesso";
        	$arrayParametros[0][1] = $this->codTipoProcesso;
        	$arrayParametros[1][0] = ":CodProcesso";
        	$arrayParametros[1][1] = $this->codProcesso;
        	 
        	$TotalDocumentosEnviados = $objConexao->executeFetch($strSql, $arrayParametros);
        	$TotalDocumentosEnviados = $TotalDocumentosEnviados[0]['TOTAL'];
        	 
        	if ($TotalDocumentosEnviados >= $TotalDocumentosObrigatorios){
        		//ROTINA PARA SETAR O STATUS DO PROCESSO
	        		//CONSULTA QUAL A SITUACAO DEPOIS DE ADICIONAR OS DOCUMENTOS OBRIGATORIOS
	        		$strSql = "SELECT COD_SITUACAO FROM PROCESSO_SITUACAO 
	        					WHERE COD_TIPO_PROCESSO = :codTipoProcesso
	        					AND IND_SITUACAO_DOC_OBRIGATORIO = 'S'";
	        		$arrayParametros = array();
	        		$arrayParametros[0][0] = ":codTipoProcesso";
	        		$arrayParametros[0][1] = $this->codTipoProcesso;
	        		
	        		$SituacaoDocObrigatorio = $objConexao->executeFetch($strSql, $arrayParametros);
	        		$SituacaoDocObrigatorio = $SituacaoDocObrigatorio[0]['COD_SITUACAO'];
	        		
	        		//FAZ O UPDATE DO REGISTRO PARA A SITUACAO CORRETA
	        		$strSql = "UPDATE PROCESSO SET COD_SITUACAO = :CodSituacao WHERE COD_PROCESSO = :codProcesso";
	        		$arrayParametros = array();
	        		$arrayParametros[0][0] = ":CodSituacao";
	        		$arrayParametros[0][1] = $SituacaoDocObrigatorio;
	        		$arrayParametros[1][0] = ":codProcesso";
	        		$arrayParametros[1][1] = $this->codProcesso;
	        		
	        		if (!$objConexao->execute($strSql, $arrayParametros)){
	        			$this->error['code'] 	= "";
	        			$this->error['message'] = "Erro ao atualizar situação do processo!";
	        			return false;
	        		}		
        	}
        }
        
        $this->andamento->setCodDocumento($this->codDocumento);
        $this->andamento->setCodAndamento('1'); // sigla (Captura do documento)
        $this->andamento->setCodUsuario($this->codUsuario);
        $this->andamento->insAndamento();
        
        if($countRecurso[0]['IND_CONFERE'] > 0){
        	$this->andamento->setCodAndamento(14);
        	$this->andamento->setCodUsuario($this->codUsuario);
        	$this->andamento->setCodDocumento($this->codDocumento);
        	$this->andamento->insAndamentoAux();
        }
        
        return true;
    }
    
    public function delDocumento($codDocumento){
    	$objConexao = $this->database;
    	
    	// marca como excluido
    	$strSql = "UPDATE DOCUMENTO SET IND_EXCLUIDO = 'S' WHERE COD_DOCUMENTO = :codDocumento";
    	
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ':codDocumento';
    	$arrayParametros[0][1] = $codDocumento;
    	    	 
    	if (!$objConexao->execute($strSql, $arrayParametros)){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro ao deletar documento";
    		return false;
    	}
  
    	return true;
    }
    
    // deleta imagem
    public function delDocumentoImagem($codDocumento){
    	$objConexao = $this->database;
  
    	$strSql = "DELETE
    			     FROM DOCUMENTO_IMAGEM
    			    WHERE COD_DOCUMENTO = :codDocumento";
    	 
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ':codDocumento';
    	$arrayParametros[0][1] = $codDocumento;
    	 
    	if (!$objConexao->execute($strSql, $arrayParametros)){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro ao deletar imagem";
    		return false;
    	}
    	 
    	return true;
    }
	
    public function totalMbTabelaImagem(){
    	$objConexao = $this->database;
    	 
    	$strSql = "SELECT ROUND(SUM(length(IMG_DOCUMENTO))/1024/1024,2) AS SIZE_MB 
    			     FROM DOCUMENTO_IMAGEM";
    	$arrayDados = $objConexao->executeFetch($strSql);
    	return $arrayDados[0]["SIZE_MB"];
    }
    
    public function totalMbTabelaImagemEmpresa(){
    	$objConexao = $this->database;
    
    	$strSql = "SELECT ROUND(SUM(length(DI.IMG_DOCUMENTO))/1024/1024,2) AS SIZE_MB 
				     FROM DOCUMENTO_IMAGEM DI
					 JOIN DOCUMENTO D ON (D.COD_DOCUMENTO = DI.COD_DOCUMENTO)
					 JOIN ESTABELECIMENTO E ON (E.COD_ESTABELECIMENTO = D.COD_ESTABELECIMENTO)
					 JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_GRUPO_ESTABELECIMENTO = E.COD_GRUPO_ESTABELECIMENTO)
					 JOIN FILIAL F ON (F.COD_FILIAL = GE.COD_FILIAL)
				    WHERE F.COD_EMPRESA = ".$_SESSION['codEmpresa'];
    	$arrayDados = $objConexao->executeFetch($strSql);
    	return $arrayDados[0]["SIZE_MB"];
    }
    
    // FIXME
    public function existeDocumento($codTipoIdentificador, $codIdentificacao, $codEstabelecimento, $codTipoDocumento = null, $datInclusao = null){
    	
    	if (!$codTipoIdentificador || !$codIdentificacao || !$codEstabelecimento){
    		return false;
    	}

    	$objConexao = $this->database;
    		
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ':codTipoIdentificador';
    	$arrayParametros[0][1] = $codTipoIdentificador;
    	$arrayParametros[1][0] = ':codIdentificacao';
    	$arrayParametros[1][1] = $codIdentificacao;
    	$arrayParametros[2][0] = ':codEstabelecimento';
    	$arrayParametros[2][1] = $codEstabelecimento;
    		
    	$i = 3;
    	$where = "";
		if ($codTipoDocumento){
			
			$where .= "  AND D.COD_TIPO_DOCUMENTO = :codTipoDocumento";
			$arrayParametros[$i][0] = ':codTipoDocumento';
			$arrayParametros[$i][1] = $codTipoDocumento;
			$i++;
		}
		
		if ($datInclusao){
			
			$where .= " AND TRUNC(D.DTH_CAPTACAO) = :datInclusao";
			$arrayParametros[$i][0] = ':datInclusao';
			$arrayParametros[$i][1] = $datInclusao;
			$i++;
		}
		
   		$strSql =  "SELECT COUNT(D.COD_DOCUMENTO) AS TOTAL
					  FROM DOCUMENTO D
					  JOIN PROCESSO P ON (P.COD_PROCESSO = D.COD_PROCESSO)
					  JOIN PROCESSO_CAMPO PC ON (PC.COD_PROCESSO = P.COD_PROCESSO )
					 WHERE D.COD_ESTABELECIMENTO = :codEstabelecimento
					   AND PC.COD_TIPO_IDENTIFICADOR = :codTipoIdentificador
					   AND PC.DSC_CONTEUDO_CAMPO = :codIdentificacao
   	 			           ".$where;
    		
    	$arrayDados = $objConexao->executeFetch($strSql, $arrayParametros);
    	
    	return $arrayDados[0]['TOTAL'];
    }
    
    public function insMimeType($dscMimeType){
    	$objConexao = $this->database;
    	
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ':dscMimeType';
    	$arrayParametros[0][1] = $dscMimeType;
    	
    	$strSql = "INSERT INTO MIME_TYPE 
    			          (COD_MIME_TYPE, DSC_MIME_TYPE)
    			   VALUES ((SELECT MAX(COD_MIME_TYPE)+1 FROM MIME_TYPE), :dscMimeType)";
    	
    	if (!$objConexao->execute($strSql, $arrayParametros)){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro ao inserir mime type";
    		return false;
    	}else{
    		
    		$strSql = "SELECT COD_MIME_TYPE FROM MIME_TYPE WHERE DSC_MIME_TYPE = :dscMimeType";
    		
    		$arrayDados = $objConexao->executeFetch($strSql, $arrayParametros);
    		return $arrayDados[0]["COD_MIME_TYPE"];
    		
    	}
    }
    

    public function totalDocumentos(){
    	$objConexao = $this->database;
    
    	$strSql = "SELECT COUNT(COD_DOCUMENTO) AS TOTAL
  					 FROM DOCUMENTO
 					WHERE IND_EXCLUIDO = 'N'";
    	$arrayDados = $objConexao->executeFetch($strSql);
    	return $arrayDados[0]["TOTAL"];
    }
    
    public function totalDocumentosExcluidos(){
    	$objConexao = $this->database;
    
    	$strSql = "SELECT COUNT(COD_DOCUMENTO) AS TOTAL
  					 FROM DOCUMENTO
 					WHERE IND_EXCLUIDO = 'S'";
    	$arrayDados = $objConexao->executeFetch($strSql);
    	return $arrayDados[0]["TOTAL"];
    }
    
    public function totalDocumentosEmpresa(){
    	$objConexao = $this->database;
    
    	$strSql = "SELECT COUNT(D.COD_DOCUMENTO) AS TOTAL
				     FROM DOCUMENTO D 
				     JOIN ESTABELECIMENTO E ON (E.COD_ESTABELECIMENTO = D.COD_ESTABELECIMENTO)
				     JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_GRUPO_ESTABELECIMENTO = E.COD_GRUPO_ESTABELECIMENTO)
				     JOIN FILIAL F ON (GE.COD_FILIAL = F.COD_FILIAL)
				    WHERE F.COD_EMPRESA = ".$_SESSION['codEmpresa']."
				      AND IND_EXCLUIDO = 'N'";
    	$arrayDados = $objConexao->executeFetch($strSql);
    	return $arrayDados[0]["TOTAL"];
    }
    
    public function totalDocumentosExcluidosEmpresa(){
    	$objConexao = $this->database;
    
    	$strSql = "SELECT COUNT(D.COD_DOCUMENTO) AS TOTAL
				     FROM DOCUMENTO D
				     JOIN ESTABELECIMENTO E ON (E.COD_ESTABELECIMENTO = D.COD_ESTABELECIMENTO)
				     JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_GRUPO_ESTABELECIMENTO = E.COD_GRUPO_ESTABELECIMENTO)
				     JOIN FILIAL F ON (GE.COD_FILIAL = F.COD_FILIAL)
				    WHERE F.COD_EMPRESA = ".$_SESSION['codEmpresa']."
				      AND IND_EXCLUIDO = 'S'";
    	$arrayDados = $objConexao->executeFetch($strSql);
    	return $arrayDados[0]["TOTAL"];
    }
    
    public function listaCamposEdicaoDocumento(){
    	$objConexao = $this->database;
    	
    	$strSql = "SELECT AUX.*, (CASE AUX.NUM_ORDEM
                                    WHEN 1 THEN D.INDEXADOR1
                                    WHEN 2 THEN D.INDEXADOR2
                                    WHEN 3 THEN D.INDEXADOR3
                                    WHEN 4 THEN D.INDEXADOR4
                                    WHEN 5 THEN D.INDEXADOR5
                                    WHEN 6 THEN D.INDEXADOR6
                                   END) AS DSC_CONTEUDO_CAMPO
                    FROM DOCUMENTO D
                    JOIN AUX_TIPO_PROCESSO AUX ON (AUX.COD_TIPO_PROCESSO = D.COD_TIPO_PROCESSO AND AUX.COD_TIPO_DOCUMENTO = D.COD_TIPO_DOCUMENTO )
                   WHERE D.COD_DOCUMENTO = :codDocumento
    			   ORDER BY AUX.NUM_ORDEM";
    	
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ":codDocumento";
    	$arrayParametros[0][1] = $this->codDocumento;
    	
    	return $objConexao->executeFetch($strSql, $arrayParametros);
    }
    
    public function updateIndexador(){
    	$objConexao = $this->database;
    
    	//foreach ($this->arrayCampos as $nomCampo => $dscConteudoCampo){
    		 
    		//$dadosCampo = $this->codIdentificadorCampo($nomCampo);
    		//$codTipoIdentificador = $dadosCampo[0]['COD_TIPO_IDENTIFICADOR'];
    		//$codTipoLista = $dadosCampo[0]['COD_TIPO_LISTA'];
    		 
    		// insere processo_campo
    		$strSql = "UPDATE DOCUMENTO SET
					          INDEXADOR1 = :INDEXADOR1,
    				          INDEXADOR2 = :INDEXADOR2, 
    				          INDEXADOR3 = :INDEXADOR3,
    				          INDEXADOR4 = :INDEXADOR4, 
    				          INDEXADOR5 = :INDEXADOR5, 
    				          INDEXADOR6 = :INDEXADOR6
    				    WHERE COD_DOCUMENTO = :COD_DOCUMENTO";
    
    		$arrayParametros = array();
    		$arrayParametros[0][0] = ":COD_DOCUMENTO";
    		$arrayParametros[0][1] = $this->codDocumento;
    		$arrayParametros[1][0] = ":INDEXADOR1";
    		$arrayParametros[1][1] = $this->arrayCampos[1];
    		$arrayParametros[2][0] = ":INDEXADOR2";
    		$arrayParametros[2][1] = $this->arrayCampos[2];
    		$arrayParametros[3][0] = ":INDEXADOR3";
    		$arrayParametros[3][1] = $this->arrayCampos[3];
    		$arrayParametros[4][0] = ":INDEXADOR4";
    		$arrayParametros[4][1] = $this->arrayCampos[4];
    		$arrayParametros[5][0] = ":INDEXADOR5";
    		$arrayParametros[5][1] = $this->arrayCampos[5];
    		$arrayParametros[6][0] = ":INDEXADOR6";
    		$arrayParametros[6][1] = $this->arrayCampos[6];
    		 
    		if (!$objConexao->execute($strSql, $arrayParametros)){
    			$this->error['code'] 	= "";
    			$this->error['message'] = "Erro ao editar valores do documento";
    			return false;
    		}
    	//}
    	return true;
    }
    
    public function codIdentificadorCampo($nomCampo){
    	$objConexao = $this->database;
    	$strSql = "SELECT COD_TIPO_IDENTIFICADOR AS COD_TIPO_IDENTIFICADOR, COD_TIPO_LISTA
                     FROM TIPO_PROCESSO_CAMPO
                    WHERE COD_TIPO_PROCESSO = :codTipoProcesso
                      AND NOM_CAMPO = :nomCampo
                    UNION
                   SELECT COD_TIPO_IDENTIFICADOR AS COD_TIPO_IDENTIFICADOR, COD_TIPO_LISTA
                     FROM TIPO_PROCESSO_TPDOC_CAMPO
                    WHERE COD_TIPO_PROCESSO = :codTipoProcesso
                      AND NOM_CAMPO = :nomCampo";
    
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ":codTipoProcesso";
    	$arrayParametros[0][1] = $this->codTipoProcesso;
    	$arrayParametros[1][0] = ":nomCampo";
    	$arrayParametros[1][1] = $nomCampo;
    
    	$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
    
    	if (!$retorno){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro ao pegar dados de identificador";
    		$objConexao->rollbackTrans();
    		return false;
    	}
    
    	return $retorno;
    }
    
    public function numOrdemCampo(){
    	$objConexao = $this->database;
    
    	$strSql = "SELECT NUM_ORDEM
				     FROM TIPO_PROCESSO_CAMPO TPC
				    WHERE TPC.COD_TIPO_PROCESSO = :codTipoProcesso
    		   	      AND TPC.NOM_CAMPO = :nomCampo
    			    UNION 
    			   SELECT NUM_ORDEM
				     FROM TIPO_PROCESSO_TPDOC_CAMPO TPDC
				    WHERE TPDC.COD_TIPO_PROCESSO = :codTipoProcesso
    			      AND TPDC.COD_TIPO_DOCUMENTO = :codTipoDocumento
    		   	      AND TPDC.NOM_CAMPO = :nomCampo";
    	
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ":codTipoProcesso";
    	$arrayParametros[0][1] = $this->codTipoProcesso;
    	$arrayParametros[1][0] = ":nomCampo";
    	$arrayParametros[1][1] = $this->nomCampo;
    	$arrayParametros[2][0] = ":codTipoDocumento";
    	$arrayParametros[2][1] = $this->codTipoDocumento;
    	
    	$arrayDados = $objConexao->executeFetch($strSql,$arrayParametros);
    	return $arrayDados[0]["NUM_ORDEM"];
    }
    
    public function tipoCampo(){
    	$objConexao = $this->database;
    
    	$strSql = "SELECT ATP.COD_TIPO_CAMPO
				     FROM AUX_TIPO_PROCESSO ATP
				    WHERE ATP.COD_TIPO_PROCESSO = :codTipoProcesso
    			      AND ATP.COD_TIPO_DOCUMENTO = :codTipoDocumento
    		   	      AND ATP.NUM_ORDEM = :numOrdem";
    	 
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ":codTipoProcesso";
    	$arrayParametros[0][1] = $this->codTipoProcesso;
    	$arrayParametros[1][0] = ":codTipoDocumento";
    	$arrayParametros[1][1] = $this->codTipoDocumento;
    	$arrayParametros[2][0] = ":numOrdem";
    	$arrayParametros[2][1] = $this->numOrdem;
    	 
    	$arrayDados = $objConexao->executeFetch($strSql,$arrayParametros);
    	return $arrayDados[0]["COD_TIPO_CAMPO"];
    }
    
    
    /**
     * Marca o documento como conferido
     * @param integer $codDocumento
     * @return boolean
     */
    public function confereDocumento(){
    	$objConexao = $this->database;

    	// insere processo_campo
    	$strSql = "UPDATE DOCUMENTO 
    			      SET COD_SITUACAO = 'C'
    				WHERE COD_DOCUMENTO = :COD_DOCUMENTO";
    
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ":COD_DOCUMENTO";
    	$arrayParametros[0][1] = $this->codDocumento;
    	    	 
    	if (!$objConexao->execute($strSql, $arrayParametros)){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro ao marcar Documento como conferido";
    		return false;
    	}
    	return true;
    }
    
    /**
     * Marca o documento como pendente
     * @param integer $codDocumento
     * @return boolean
     */
    public function insPendenciaDocumento(){
    	$objConexao = $this->database;
    
    	// insere processo_campo
    	$strSql = "UPDATE DOCUMENTO
    			      SET COD_SITUACAO = 'P'
    				WHERE COD_DOCUMENTO = :COD_DOCUMENTO";
    
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ":COD_DOCUMENTO";
    	$arrayParametros[0][1] = $this->codDocumento;
    	 
    	if (!$objConexao->execute($strSql, $arrayParametros)){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro ao marcar Documento como pendênte";
    		return false;
    	}
    	return true;
    }
    
    public function _getDocumentoFiltros(){
    	$objConexao = $this->database;
    	 
    	$where = "";
    	$i = 0;
    	 
    	$arrayParametros = array();
    	 
    	//$where .= " AND D.COD_EMPRESA = :codEmpresa ";
    	$arrayParametros[$i][0] = ':codEmpresa';
    	$arrayParametros[$i][1] = $this->codEmpresa;
    	$i++;
    		
    	// procura pelo documento especifico
    	if ($this->codDocumento){
    		$where .= " AND D.COD_DOCUMENTO = :codDocumento ";
    		$arrayParametros[$i][0] = ':codDocumento';
    		$arrayParametros[$i][1] = $this->codDocumento;
    		$i++;
    		 
    		// procura usando filtros
    	}else{
    		 
    		$where .= " AND D.COD_TIPO_PROCESSO = :codTipoProcesso ";
    		$arrayParametros[$i][0] = ':codTipoProcesso';
    		$arrayParametros[$i][1] = $this->codTipoProcesso;
    		$i++;
    
    
    		//$where .= " AND PC.COD_TIPO_IDENTIFICADOR = :codTipoIdentificador ";
    		//$arrayParametros[$i][0] = ':codTipoIdentificador';
    		//$arrayParametros[$i][1] = $this->codTipoIdentificador;
    		//$i++;
    
    		// busca por período
    		if ($this->dthDataIni && $this->dthDataFim ){
    			 
    			// verifica se as datas tem hora, senao coloca inicio e fim do dia
    			$aux = explode(" ", $this->dthDataIni);
    			if (count($aux) == 1){
    				$this->dthDataIni = $this->dthDataIni." 00:00:00";
    			}
    			 
    			$aux = explode(" ", $this->dthDataFim);
    			if (count($aux) == 1){
    				$this->dthDataFim = $this->dthDataFim." 23:59:59";
    			}
    			 
    			$where .= "AND D.DTH_CAPTACAO BETWEEN TO_DATE(:datIni, 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE(:datFim, 'DD/MM/YYYY HH24:MI:SS') ";
    			$arrayParametros[$i][0] = ':datIni';
    			$arrayParametros[$i][1] = $this->dthDataIni;
    			$i++;
    			$arrayParametros[$i][0] = ':datFim';
    			$arrayParametros[$i][1] = $this->dthDataFim;
    			$i++;
    			// busca por dia
    		}else{
    			$where .= " AND TRUNC(D.DTH_CAPTACAO) = :data ";
    			$arrayParametros[$i][0] = ':data';
    			$arrayParametros[$i][1] = $this->dthDataIni;
    			$i++;
    		}
    
    		if ($this->codTipoDocumento){
    			$where .= " AND D.COD_TIPO_DOCUMENTO = :codTipoDocumento ";
    			$arrayParametros[$i][0] = ':codTipoDocumento';
    			$arrayParametros[$i][1] = $this->codTipoDocumento;
    			$i++;
    		}
    		 
    		if ($this->codEstabelecimento){
    			$where .= " AND D.COD_ESTABELECIMENTO = :codEstabelecimento ";
    			$arrayParametros[$i][0] = ':codEstabelecimento';
    			$arrayParametros[$i][1] = $this->codEstabelecimento;
    			$i++;
    		}
    		 
    		//if ($this->codIdentificacao){
    		//$where .= " AND PC.DSC_CONTEUDO_CAMPO = :codIdentificacao ";
    		//	$where .= " AND D.INDEXADOR1 = :codIdentificacao ";
    		//	$arrayParametros[$i][0] = ':codIdentificacao';
    		//	$arrayParametros[$i][1] = $this->codIdentificacao;
    		//	$i++;
    		//}
    
    		$arrayCampos = array();
    		if(@$_REQUEST['arrayCampos'] != ""){
    			$arr = explode("|#|,", @$_REQUEST['arrayCampos']);
    			foreach ($arr as $campos){
    				//$campo =  preg_replace("/;/", "|#|;", $campos, 1);
    				$arr2 = explode('|#|;', $campos);
    				$arr2[1] = str_replace("|#|", '', $arr2[1]);
    
    				//pega a ordem do campo
    				//$arr2[0] = NOME DO CAMPO
    				//$arr2[1] = TIPO DO CAMPO ( "S" - STRING | "L" - LISTA | "V" - VALOR | "I" - INTEIRO | "D" - DATA
    				//$arr2[2] = VALOR DO PRIMEIRO CAMPO
    				//$arr2[3] = VALOR DO SEGUNDO CAMPO QUANDO O OPERADOR É DE INTERVALO EX. 'ENTRE(BETWEEN)'
    				//$arr2[4] = TIPO DE OPERADOR ("I" - Igual | "E" - Entre | "D" - Diferente | "MAQ" - Maior que | "MAI" - Maior ou igual | "MEQ" - Menor que | "MEI" - Menor ou igual | "INI" - Iniciando por | "C" - Contendo
    				$this->nomCampo = $arr2[0];
    				$numOrdem = $this->numOrdemCampo();
    				//$arrayCampos[$numOrdem][0] = str_replace("|#|", '', $arr2[4]); //Operador
    				//$arrayCampos[$numOrdem][1] = $arr2[1]; //Tipo do Campo
    				//if($arr2[1]== "V" || $arr2[1] == "I"){
    				//	$arrayCampos[$numOrdem][2] = str_replace(".","",$arr2[2]); //Campo1
    				//	$arrayCampos[$numOrdem][3] = str_replace(".","",$arr2[3]); //Campo2
    				//}else{
    					$arrayCampos[$numOrdem][0] = $arr2[1]; //Campo1
    					//$arrayCampos[$numOrdem][3] = $arr2[3]; //Campo2
    				//}
    
    			}
    			$andCampos = "";
    			foreach ($arrayCampos as $key => $value){
    				$where .= " AND UPPER(D.INDEXADOR".$key. ") = UPPER(:INDEXADOR".$key.") ";
    				$arrayParametros[$i][0] = ":INDEXADOR".$key;
    				$arrayParametros[$i][1] = $value[0];
    				$i++;
    			}
    		}
    	}
    	//FNC_LISTA_INDEXADORES(D.COD_TIPO_PROCESSO, D.COD_TIPO_DOCUMENTO, D.INDEXADOR1, D.INDEXADOR2, D.INDEXADOR3, D.INDEXADOR4, D.INDEXADOR5, D.INDEXADOR6) AS DSC_INDEXADORES
    	$strSql = "SELECT D.COD_DOCUMENTO, D.COD_TIPO_DOCUMENTO, TD.DSC_TIPO_DOCUMENTO, D.COD_TIPO_DISPOSITIVO,
                          D.COD_USUARIO, U.DSC_IDENTIFICACAO_USUARIO, D.COD_MIME_TYPE, MT.DSC_MIME_TYPE, D.COD_ESTABELECIMENTO, PJE.NOM_FANTASIA AS NOM_ESTABELECIMENTO, PJE.NUM_CNPJ,
                          D.DTH_CAPTACAO, D.COD_PROCESSO, D.COD_TIPO_PROCESSO, TP.DSC_TIPO_PROCESSO, NULL AS COD_TIPO_IDENTIFICADOR,  NULL AS DSC_TIPO_IDENTIFICADOR,
                          D.INDEXADOR1 AS COD_IDENTIFICACAO, D.COD_SITUACAO
					 FROM DOCUMENTO             D
    	             JOIN TIPO_DOCUMENTO        TD  ON (TD.COD_TIPO_DOCUMENTO = D.COD_TIPO_DOCUMENTO)
    	             JOIN USUARIO               U   ON (U.COD_USUARIO = D.COD_USUARIO)
    	             JOIN MIME_TYPE             MT  ON (MT.COD_MIME_TYPE = D.COD_MIME_TYPE)
    	             JOIN PESSOA_JURIDICA       PJE ON (PJE.COD_PESSOA = D.COD_ESTABELECIMENTO)
    	             JOIN TIPO_PROCESSO         TP  ON (TP.COD_TIPO_PROCESSO = D.COD_TIPO_PROCESSO)
    	             --JOIN TIPO_IDENTIFICADOR  TI  ON (TI.COD_TIPO_IDENTIFICADOR = :codTipoIdentificador )
    	             JOIN ESTABELECIMENTO       E   ON (E.COD_ESTABELECIMENTO = D.COD_ESTABELECIMENTO)
					WHERE D.COD_EMPRESA         = :codEmpresa
    			      AND D.IND_EXCLUIDO = 'N'
    				      ".$where."
    				ORDER BY D.COD_DOCUMENTO DESC ";
    	 
    	 
    	$arrayDocumento = $objConexao->executeFetch($strSql, $arrayParametros);
    	 
    	return $arrayDocumento;
    }
    
}