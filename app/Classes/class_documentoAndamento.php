<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class documentoAndamento {

	public  $database;
	public  $error;
    private $codDocumento;
    private $dthAndamento;
    private $codAndamento;
    private $codUsuario;
    private $dscObservacao;
   

    function __construct($database) {
    	$this->database = $database;
    	$this->error['code'] 	= "";
    	$this->error['message'] = "";
    	return true;
    }
      
    public function __get($propriedade) {
    	return $this->$propriedade;
    }
    
    public function __set($propriedade, $valor) {
    	$this->$propriedade = $valor;
    }
    
    //**********************************************************************************************//
    
    public function setCodDocumento($codDocumento) {
    	$this->codDocumento = $codDocumento;
    }
    
    public function setDthAndamento($dthAndamento) {
    	$this->dthAndamento = $dthAndamento;
    }
    
    public function setCodAndamento($codAndamento) {
    	$this->codAndamento = $codAndamento;
    }
    
    public function setCodUsuario($codUsuario) {
    	$this->codUsuario = $codUsuario;
    }
    
    public function setDscObservacao($dscObservacao) {
    	$this->dscObservacao = $dscObservacao;
    }
    
  
    public function getAndamento($codDocumento){
     	$objConexao = $this->database;
     	
        $strSql = "SELECT DA.COD_DOCUMENTO, DA.DTH_ANDAMENTO, DA.COD_ANDAMENTO, DA.COD_USUARIO, DA.DSC_OBSERVACAO, U.DSC_IDENTIFICACAO_USUARIO
  					 FROM DOCUMENTO_ANDAMENTO DA
  					 JOIN SIGLA S ON (S.COD_SIGLA = DA.COD_ANDAMENTO AND S.COD_TIPO_SIGLA = :codTipoSigla)
  					 JOIN USUARIO U ON (U.COD_USUARIO = DA.COD_USUARIO)
 					WHERE DA.COD_DOCUMENTO = :codDocumento";

        $arrayParametros = array();
        $arrayParametros[0][0] = ':codDocumento';
        $arrayParametros[0][1] = $codDocumento;
        $arrayParametros[1][0] = ':codTipoSigla';
        $arrayParametros[1][1] = 1;

        $arrayAndamentos = $objConexao->executeFetch($strSql, $arrayParametros);
        
        return $arrayAndamentos;    
    }
    
    // insere novo andamento
    public function insAndamento(){
    	$objConexao = $this->database;

        // insere andamento do documento
        $strSql = "INSERT INTO DOCUMENTO_ANDAMENTO
        		          (COD_DOCUMENTO, DTH_ANDAMENTO, COD_ANDAMENTO, COD_USUARIO, DSC_OBSERVACAO) 
        		   VALUES (:codDocumento, SYSDATE, :codAndamento, :codUsuario, :dscObservacao)";
        
        $arrayParametros = array();
        $arrayParametros[0][0] = ':codDocumento';
        $arrayParametros[0][1] = $this->codDocumento;
        $arrayParametros[1][0] = ':codAndamento';
        $arrayParametros[1][1] = $this->codAndamento;
        $arrayParametros[2][0] = ':codUsuario';
        $arrayParametros[2][1] = $this->codUsuario;
        $arrayParametros[3][0] = ':dscObservacao';
        $arrayParametros[3][1] =  $this->dscObservacao;
       
        
        if (!$objConexao->execute($strSql, $arrayParametros)){
        	$this->error['code'] 	= "";
        	$this->error['message'] = "Erro ao inserir andamento";
        	return false;
        }

        return true;
    }
    // insere novo andamento
    public function insAndamentoAux(){
    	$objConexao = $this->database;
    
    	// insere andamento do documento
    	$strSql = "INSERT INTO DOCUMENTO_ANDAMENTO
        		          (COD_DOCUMENTO, DTH_ANDAMENTO, COD_ANDAMENTO, COD_USUARIO, DSC_OBSERVACAO)
        		   VALUES (:codDocumento,  SYSDATE + 0.00001, :codAndamento, :codUsuario, NULL)";
    
    	$arrayParametros = array();
    	$arrayParametros[0][0] = ':codDocumento';
    	$arrayParametros[0][1] = $this->codDocumento;
    	$arrayParametros[1][0] = ':codAndamento';
    	$arrayParametros[1][1] = $this->codAndamento;
    	$arrayParametros[2][0] = ':codUsuario';
    	$arrayParametros[2][1] = $this->codUsuario;
    	//$arrayParametros[3][0] = ':dscObservacao';
    	//$arrayParametros[3][1] =  $this->dscObservacao;
    	 
    
    	if (!$objConexao->execute($strSql, $arrayParametros)){
    		$this->error['code'] 	= "";
    		$this->error['message'] = "Erro ao inserir andamento";
    		return false;
    	}
    
    	return true;
    }
}