<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class tipoProcessoGrupo
{
	public    $database;
	public    $error;
	protected $codGrupo;
	protected $dscGrupo;
	protected $codGrupoPai;
	protected $numSequencia;
	protected $codEmpresa;

	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//

	
	public function getTipoProcessoGrupo($codGrupo){
		$objConexao = $this->database;
		
		$strSql = "SELECT COD_GRUPO, DSC_GRUPO, COD_GRUPO_PAI, NUM_SEQUENCIA, COD_EMPRESA
				     FROM TIPO_PROCESSO_GRUPO
				    WHERE COD_GRUPO = :codGrupo
				      AND COD_EMPRESA = :codEmpresa";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codGrupo";
		$arrayParametros[0][1] = $codGrupo;
		$arrayParametros[1][0] = ":codEmpresa";
		$arrayParametros[1][1] = $_SESSION['codEmpresa'];
		
		$arrayGrupo = $objConexao->executeFetch($strSql, $arrayParametros);
		
		$this->codGrupo 		= $arrayGrupo[0]["COD_GRUPO"];
		$this->dscGrupo 		= $arrayGrupo[0]["DSC_GRUPO"];
		$this->codGrupoPai		= $arrayGrupo[0]["COD_GRUPO_PAI"];
		$this->numSequencia		= $arrayGrupo[0]["NUM_SEQUENCIA"];
		$this->codEmpresa		= $arrayGrupo[0]["COD_EMPRESA"];
		
		return true;
	}
	
	public function insTipoProcessoGrupo() {
	
		if (TRUE == $this->dscGrupo) {
			$objConexao = $this->database;
			
			// pega novo id
			$strSql = "SELECT SQ_TPROG_01.NEXTVAL AS COD_GRUPO FROM DUAL";
			$proximoGrupo = $objConexao->executeFetch($strSql);
			
			if (!$proximoGrupo){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao gerar ID";
				return false;
			}
			 
			if (!$this->numSequencia){
				$this->numSequencia = 1;
			}
			
			if ($this->codGrupoPai == 0){
				$this->codGrupoPai = null;
			}
			
			$codTipoProcessoGrupo = $proximoGrupo[0]["COD_GRUPO"];
	
			// insere processo grupo
			$strSql = "INSERT INTO TIPO_PROCESSO_GRUPO
					          (COD_GRUPO, DSC_GRUPO, COD_GRUPO_PAI, NUM_SEQUENCIA, COD_EMPRESA)
					   VALUES (:codGrupo, :dscGrupo, :codGrupoPai, :numSequencia, :codEmpresa)";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codGrupo";
			$arrayParametros[0][1] = $codTipoProcessoGrupo;
			$arrayParametros[1][0] = ":dscGrupo";
			$arrayParametros[1][1] = $this->dscGrupo;
			$arrayParametros[2][0] = ":codGrupoPai";
			$arrayParametros[2][1] = $this->codGrupoPai;
			$arrayParametros[3][0] = ":numSequencia";
			$arrayParametros[3][1] = $this->numSequencia;
			$arrayParametros[4][0] = ":codEmpresa";
			$arrayParametros[4][1] = $_SESSION['codEmpresa'];
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao inserir grupo de processo";
				return false;
			}
			
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "variável não setada";
			return false;
		}
	}
	
	public function changeTipoProcessoGrupo() {
		if (TRUE == $this->dscGrupo) {
			$objConexao = $this->database;
			
			if ($this->codGrupoPai == 0){
				$this->codGrupoPai = null;
			}
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codGrupo";
			$arrayParametros[0][1] = $this->codGrupo;
			$arrayParametros[1][0] = ":dscGrupo";
			$arrayParametros[1][1] = $this->dscGrupo;
			$arrayParametros[2][0] = ":codGrupoPai";
			$arrayParametros[2][1] = $this->codGrupoPai;
			$arrayParametros[3][0] = ":numSequencia";
			$arrayParametros[3][1] = $this->numSequencia;
			$arrayParametros[4][0] = ":codEmpresa";
			$arrayParametros[4][1] = $_SESSION['codEmpresa'];
	
			$strSql = "UPDATE TIPO_PROCESSO_GRUPO
					      SET DSC_GRUPO = :dscGrupo,
							  COD_GRUPO_PAI = :codGrupoPai,
							  NUM_SEQUENCIA = :numSequencia
					    WHERE COD_GRUPO = :codGrupo
					      AND COD_EMPRESA = :codEmpresa";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar grupo de processo";
				return false;
			}
	
			return true;
		} else {
			return false;
		}
	}
	
	public function delTipoProcessoGrupo($codGrupo){
		
		if (TRUE == $codGrupo) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codGrupo";
			$arrayParametros[0][1] = $codGrupo;
	
			
			// Deleta campos do processo
			$strSql = "DELETE
					     FROM TIPO_PROCESSO_GRUPO
					    WHERE COD_GRUPO = :codGrupo";
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar grupo de processo";
				return false;
			}
	
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "variável não setada";
			return false;
		}
	
	}
	

	


	

}