<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

require_once ('class_pessoa.php');
require_once ('class_validate.php');

Class pessoaJuridica extends pessoa
{
	public    $database;
	public    $error;
	protected $numCnpj;
	protected $nomFantasia;
	protected $codEmpresa;
	protected $nomEmpresa;
	
	function __construct($database) {
		$this->database = $database;
		parent::__construct($this->database);
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getNumCnpj() {
		return $this->numCnpj;
	}
	
	public function getNumCnpjToView() {
		return $this->formatCnpjToView($this->numCnpj);
	}
	
	public function setNumCnpj($numCnpj) {
		$this->numCnpj = $numCnpj;
	}
	
	public function getNomFantasia() {
		return $this->nomFantasia;
	}
	
	public function setNomFantasia($nomFantasia) {
		$this->nomFantasia = $nomFantasia;
	}
	
	public function getCodEmpresa() {
		return $this->codEmpresa;
	}
	
	public function setCodEmpresa($codEmpresa) {
		$this->codEmpresa = $codEmpresa;
	}
	
	public function getNomEmpresa() {
		return $this->nomEmpresa;
	}
	
	public function setNomEmpresa($nomEmpresa) {
		$this->nomEmpresa = $nomEmpresa;
	}

	public function getPessoaJuridica($codPessoa){
		if (TRUE == $codPessoa){
			
			$objConexao = $this->database;
			$strSql = "SELECT PJ.NUM_CNPJ, PJ.NOM_FANTASIA, E.COD_EMPRESA, E.NOM_EMPRESA
					     FROM PESSOA_JURIDICA PJ
					     JOIN EMPRESA E ON (E.COD_EMPRESA = PJ.COD_EMPRESA)
					    WHERE PJ.COD_PESSOA =:codPessoa";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $codPessoa;
			
			$arrayPessoaJuridica = $objConexao->executeFetch($strSql, $arrayParametros);
			
			$this->numCnpj     	= $arrayPessoaJuridica[0]["NUM_CNPJ"];
			$this->nomFantasia 	= $arrayPessoaJuridica[0]["NOM_FANTASIA"];
			$this->codEmpresa  	= $arrayPessoaJuridica[0]["COD_EMPRESA"];
			$this->nomEmpresa	= $arrayPessoaJuridica[0]["NOM_EMPRESA"];
			
			parent::getPessoa($codPessoa);
			
			return true;
		}else{
			return false;
		}
	}
	
	public function insPessoaJuridica(){
	
		$this->setCodTipoPessoa('J');
		
		$objValidate = new validate();
		if(!$objValidate->validaCNPJ($this->numCnpj)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro ao validar CNPJ";
			return false;
		}
	
		if($this->CnpjExists()){
			$this->error['code'] 	= "";
			$this->error['message'] = "CNPJ já está cadastrado no sistema";
			return false;
		}
	
		if(TRUE == $this->numCnpj && TRUE == $this->nomFantasia)
		{
			if (!$this->InsPessoa()){
				return false;
			}
			
			$objConexao = $this->database;
			$strSql = "INSERT INTO PESSOA_JURIDICA 
					          (COD_PESSOA, NUM_CNPJ, NOM_FANTASIA, COD_EMPRESA) 
					   VALUES (:codPessoa,:numCnpj, :nomFantasia, :codEmpresa)";
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $this->getCodPessoa();
			$arrayParametros[1][0] = ":numCnpj";
			$arrayParametros[1][1] = $this->formatCnpjToDb($this->numCnpj);
			$arrayParametros[2][0] = ":nomFantasia";
			$arrayParametros[2][1] = $this->nomFantasia;
			$arrayParametros[3][0] = ":codEmpresa";
			$arrayParametros[3][1] = $this->codEmpresa;
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao inserir pessoa jurídica";
				return false;
			}
	
			return true;
		}else{
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para insrir pessoa jurídica";
			return false;
		}
	
	}
	
	public function changePessoaJuridica(){
		if(TRUE == $this->numCnpj && TRUE == $this->nomFantasia && TRUE == $this->getCodPessoa()){
			if (!$this->changePessoa()){
				return false;
			}
	
			$objConexao = $this->database;
	
			$strSql = "UPDATE PESSOA_JURIDICA 
					      SET NUM_CNPJ =:numCnpj, 
					          NOM_FANTASIA = :nomFantasia 
					    WHERE COD_PESSOA =:codPessoa";
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $this->getCodPessoa();
			$arrayParametros[1][0] = ":numCnpj";
			$arrayParametros[1][1] = $this->formatCnpjToDb($this->numCnpj);
			$arrayParametros[2][0] = ":nomFantasia";
			$arrayParametros[2][1] = $this->nomFantasia;
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar pessoa jurídica";
				return false;
			}
	
			return true;
		}else{
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados faltando para alterar pessoa jurídica";
			return false;
		}
	}
	
	public function delPessoaJuridica($codPessoa){
		if(true == $codPessoa){
			$objConexao = $this->database;
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPessoa";
			$arrayParametros[0][1] = $codPessoa;
	
			$strSql = 'DELETE 
					     FROM PESSOA_EMAIL 
					    WHERE COD_PESSOA = :codPessoa';
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar pessoa jurídica email";
				return false;
			}
			
			$strSql = 'DELETE 
					     FROM PESSOA_JURIDICA 
					    WHERE COD_PESSOA = :codPessoa';
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao deletar pessoa jurídica";
				return false;
			}
	
			if (parent::delPessoa($codPessoa)){
				return true;
			}else{
				return false;
			}
		}
		return false;
	}
	
	protected function formatCnpjToDb($numCnpj){
	
		$numCnpj = str_replace('.', '', $numCnpj);
		$numCnpj = str_replace('-', '', $numCnpj);
		$numCnpj = str_replace('/', '', $numCnpj);
	
		return $numCnpj;
	}
	
	protected function formatCnpjToView($numCnpj){
	
		//Se existia zeros à esquerda,estes não foram gravados no banco.
		$numCnpj = (string)$numCnpj;
	
		while(strlen($numCnpj) < 14){
			$numCnpj = "0" . $numCnpj;
		}
	
		$part1 = substr($numCnpj, 0, 2);
		$part2 = substr($numCnpj, 2, 3);
		$part3 = substr($numCnpj, 5, 3);
		$part4 = substr($numCnpj, 8, 4);
		$part5 = substr($numCnpj, 12, 2);
	
		return $part1 . "." . $part2 . "." . $part3 . "/" . $part4 . "-" . $part5;
	}
	
	public function CnpjExists(){
		$objConexao = $this->database;
	
		$strSql = "SELECT NUM_CNPJ 
				     FROM PESSOA_JURIDICA 
				    WHERE NUM_CNPJ = '" . $this->formatCnpjToDb($this->getNumCnpj()) . "'
				      AND COD_EMPRESA = :codEmpresa";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codEmpresa";
		$arrayParametros[0][1] = $this->codEmpresa;
		
		
		$result = $objConexao->executeFetch($strSql, $arrayParametros);
	
		if(null ==  $result || $result == "")
			return false;
	
		return true;
	
	}
}