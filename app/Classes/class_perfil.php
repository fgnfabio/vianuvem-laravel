<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class perfil {
	
	public  $database;
	public  $error;
	
	private  $codPerfilUsuario;
	private  $dscPerfilUsuario;
	private  $indPerfilSistema;
    private  $arrayTipoProcesso;
	private  $codEmpresa;
    
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getCodPerfilUsuario(){
		return $this->codPerfilUsuario;
	}
	
	public function setCodPerfilUsuario($codPerfilUsuario){
		$this->codPerfilUsuario = $codPerfilUsuario;
	}
	
	public function getDscPerfilUsuario(){
		return $this->dscPerfilUsuario;
	}
	
	public function getIndPerfilSistema(){
		return $this->indPerfilSistema;
	}
	
	public function setDscPerfilUsuario($dscPerfilUsuario){
		$this->dscPerfilUsuario = $dscPerfilUsuario;
	}
	
	public function setArrayTipoProcesso($arrayTipoProcesso) {
		$this->arrayTipoProcesso = $arrayTipoProcesso;
	}
	
	public function getPerfil($codPerfilUsuario){
		$objConexao = $this->database;
		 
		$strSql = "SELECT COD_PERFIL_USUARIO, DSC_PERFIL_USUARIO, IND_PERFIL_SISTEMA
				     FROM PERFIL_USUARIO 
					WHERE COD_PERFIL_USUARIO = :codPerfilUsuario";
		 
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codPerfilUsuario";
		$arrayParametros[0][1] = $codPerfilUsuario;
		 
		$arrayPerfil = $objConexao->executeFetch($strSql, $arrayParametros);
		 
		$this->codPerfilUsuario = $arrayPerfil[0]["COD_PERFIL_USUARIO"];
		$this->dscPerfilUsuario = $arrayPerfil[0]["DSC_PERFIL_USUARIO"];
		$this->indPerfilSistema = $arrayPerfil[0]["IND_PERFIL_SISTEMA"];
		
	}
	
	public function insPerfil(){
		
		$objConexao = $this->database;
		$objConexao->beginTrans();
		
		$strSql = "SELECT SQ_PERFI_01.NEXTVAL AS COD_PERFIL_USUARIO FROM DUAL";
		$arrayPerfil = $objConexao->executeFetch($strSql);
		$this->codPerfilUsuario = $arrayPerfil[0]["COD_PERFIL_USUARIO"];
		
		if (!$this->codPerfilUsuario){
			return false;
		}
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codPerfilUsuario";
		$arrayParametros[0][1] = $this->codPerfilUsuario;
		$arrayParametros[1][0] = ":dscPerfilUsuario";
		$arrayParametros[1][1] = $this->dscPerfilUsuario;
		
		$arrayParametros[2][0] = ":indPerfilSistema";
		$arrayParametros[2][1] = $this->indPerfilSistema;
		
		$arrayParametros[3][0] = ":codEmpresa";
		$arrayParametros[3][1] = $this->codEmpresa;
		
		$strSql = "INSERT INTO PERFIL_USUARIO 
				          (COD_PERFIL_USUARIO, DSC_PERFIL_USUARIO, IND_PERFIL_SISTEMA, COD_EMPRESA) 
				   VALUES (:codPerfilUsuario, :dscPerfilUsuario, :indPerfilSistema, :codEmpresa)";
		
		if (!$objConexao->execute($strSql, $arrayParametros)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro ao inserir perfil";
			$objConexao->rollbackTrans();
			return false;
		}

	   if ($this->indPerfilSistema == "N" && $this->arrayTipoProcesso != ""){
	       foreach ($this->arrayTipoProcesso as $codTipoProcesso) {
				$strSql = 'INSERT INTO PERFIL_USUARIO_TIPO_PROCESSO 
						               (COD_PERFIL_USUARIO, COD_TIPO_PROCESSO)
					             VALUES(:codPerfilUsuario, :codTipoProcesso)';
				
				$arrayParametros = array();
				$arrayParametros[0][0] = ":codPerfilUsuario";
				$arrayParametros[0][1] = $this->codPerfilUsuario;
				$arrayParametros[1][0] = ":codTipoProcesso";
				$arrayParametros[1][1] = $codTipoProcesso;
				
				if (!$objConexao->execute($strSql, $arrayParametros)){
					$this->error['code'] 	= "";
					$this->error['message'] = "Erro ao inserir processo para o perfil";
					$objConexao->rollbackTrans();
					return false;
				}
			}
	   }
	   
		$objConexao->commitTrans();
		return true;
	}
	
	public function changePerfil($codPerfilUsuario) {
		if ($codPerfilUsuario && TRUE == $this->dscPerfilUsuario ){
			
			$objConexao = $this->database;
			$objConexao->beginTrans();
			 
			$strSql = "UPDATE PERFIL_USUARIO
            		      SET DSC_PERFIL_USUARIO = :dscPerfilUsuario,
					          IND_PERFIL_SISTEMA = :indPerfilSistema,
					          COD_EMPRESA        = :codEmpresa 
            		    WHERE COD_PERFIL_USUARIO = :codPerfilUsuario";
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":dscPerfilUsuario";
			$arrayParametros[0][1] = $this->dscPerfilUsuario;
			$arrayParametros[1][0] = ":codPerfilUsuario";
			$arrayParametros[1][1] = $codPerfilUsuario;
			$arrayParametros[2][0] = ":indPerfilSistema";
			$arrayParametros[2][1] = $this->indPerfilSistema;
			$arrayParametros[3][0] = ":codEmpresa";
			$arrayParametros[3][1] = $this->codEmpresa;
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar o perfil";
				$objConexao->rollbackTrans();
				return false;
			}
			
			//EXCLUI ANTES DE RECRIAR A ASSOCIACAO
			$strSql = "DELETE 
            		     FROM PERFIL_USUARIO_TIPO_PROCESSO
	           		    WHERE COD_PERFIL_USUARIO = :codPerfilUsuario";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPerfilUsuario";
			$arrayParametros[0][1] = $codPerfilUsuario;
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar o perfil";
				$objConexao->rollbackTrans();
				return false;
			}
			
			if ($this->indPerfilSistema == "N" && $this->arrayTipoProcesso != ""){
				foreach ($this->arrayTipoProcesso as $codTipoProcesso) {
					$strSql = 'INSERT INTO PERFIL_USUARIO_TIPO_PROCESSO
						               (COD_PERFIL_USUARIO, COD_TIPO_PROCESSO)
					             VALUES(:codPerfilUsuario, :codTipoProcesso)';
			
					$arrayParametros = array();
					$arrayParametros[0][0] = ":codPerfilUsuario";
					$arrayParametros[0][1] = $codPerfilUsuario;
					$arrayParametros[1][0] = ":codTipoProcesso";
					$arrayParametros[1][1] = $codTipoProcesso;
			
					if (!$objConexao->execute($strSql, $arrayParametros)){
						$this->error['code'] 	= "";
						$this->error['message'] = "Erro ao inserir processo para o perfil";
						$objConexao->rollbackTrans();
						return false;
					}
				}
			}
			
	
			$objConexao->commitTrans();
			return true;
		} else {
			return false;
		}
	}
	
	public function delPerfil($codPerfilUsuario) {
	
		if ($codPerfilUsuario) {
			$objConexao = $this->database;
			$objConexao->beginTrans();
						
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codPerfilUsuario";
			$arrayParametros[0][1] = $codPerfilUsuario;
				
			$strSql = "DELETE FROM PERFIL_USUARIO_TIPO_PROCESSO
			            WHERE COD_PERFIL_USUARIO = :codPerfilUsuario";
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao excluir associação do perfil de usuário com os tipos processos";
				$objConexao->rollbackTrans();
				return false;
			}
			
			$strSql = "DELETE
            		     FROM PERFIL_USUARIO
            		    WHERE COD_PERFIL_USUARIO =:codPerfilUsuario";
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao excluir perfil (Este perfil esta associado a usuários do Sistema)";
				$objConexao->rollbackTrans();
				return false;
			}
			
			$objConexao->commitTrans();
			return true;
		
		} else {
			return false;
		}
	}
	
	public function changeRecursoPerfil($codRecurso, $arrCodPerfil){
	
		if ($codRecurso) {
			$objConexao = $this->database;
				
			$objConexao->beginTrans();
	
			// deleta os perfils do recurso
			$arrayParametros = array();
			$arrayParametros[0][0] = ":COD_RECURSO";
			$arrayParametros[0][1] = $codRecurso;
	
			$strSql = "DELETE
            		     FROM PERFIL_USUARIO_RECURSO
            		    WHERE COD_RECURSO =:COD_RECURSO";
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$objConexao->rollbackTrans();
				return false;
			}
				
			// insere novos recursos do perfil
			foreach ($arrCodPerfil as $codPerfil){
	
				$arrayParametros = array();
				$arrayParametros[0][0] = ":codPerfil";
				$arrayParametros[0][1] = $codPerfil;
				$arrayParametros[1][0] = ":codRecurso";
				$arrayParametros[1][1] = $codRecurso;
	
				$strSql = "INSERT INTO PERFIL_USUARIO_RECURSO (COD_PERFIL_USUARIO, COD_RECURSO)
						                               VALUES (:codPerfil, :codRecurso)";
	
				if (!$objConexao->execute($strSql, $arrayParametros)){
					$objConexao->rollbackTrans();
					return false;
				}
			}
							
			$objConexao->commitTrans();
	
			return true;
		} else {
			return false;
		}
	}
	
}