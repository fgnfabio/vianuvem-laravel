<?php

namespace App\Classes;

/**
 * Verifica a existência de dependências necessárias para o funcionamento do sistema.
 * 
 * @author Marcos Menezes
 * @version 1.0
 * 
 */
Class dependencias {

	/**
	 * Método constutor que verifica se as extensões previamente cadastradas foram lidas.
	 * 
	 * @return boolean
	 */	
	function __construct() {
		
		//get_loaded_extensions();
		$array_extensoes = array("oci8", "gd", "fileinfo", "xml", "SimpleXML", "dom", "session");
		$array_faltando = array();
		
		foreach ($array_extensoes as $extensao){
			
			if (!extension_loaded($extensao)){
				$array_faltando[] = $extensao;
			}
		}
		
		if (count($array_faltando) > 0){
			$this->encerra($array_faltando);
			die();
		}else{
			return true;
		}
		
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	/**
	 * Recebe um array com os nomes das extensões que estão faltando e exibe mensagem informativa.
	 * 
	 * @param array $array_faltando
	 * @return void
	 */
	private function encerra($array_faltando){
		if (!headers_sent()) {
			header('Content-type: text/html; charset=UTF8');
		}
		
		$nome = "";
		
		foreach ($array_faltando as $indice => $extensao){
			$nome .= ($indice + 1)." - ".strtoupper($extensao)."<br>";
		}
		echo ("Ocorreu um erro, as seguintes extensões do PHP não foram instaladas:<br> <b>$nome</b>");
		
	} 
	

}