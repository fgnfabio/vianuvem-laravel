<?php

namespace App\Classes;

/**********************************************************************************************************
  Data de criação : 18/08/2009
  Autor           : Daniel Flores Bastos
  Proposta        : Validar CPF e CNPJ. Em caso de erro
                    retorna em qual digito verificador ocorreu
                    o erro.
************************************************************************************************************/
  class CpfCnpjValidate
  {
  
    public  $erroCPF    = 'CPF Inválido';
    public  $erroCNPJ   = 'CNPJ Inválido';

    private $pArray_cpf = array(10, 9, 8, 7, 6, 5, 4, 3, 2);
    private $sArray_cpf = array(11, 10, 9, 8, 7, 6, 5, 4, 3, 2);
    private $pArray_cnpj = array(5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
    private $sArray_cpj = array(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
    
    

    public function validaCPF($valor){
    	$valor = str_replace('.', '', $valor);
		$valor = str_replace('-', '', $valor);
		$valor = str_replace('/', '', $valor);
		
      	$somador_cpf = 0;
      	
      	for($i = 0; $i < (strlen($valor) - 2); $i++){
        	$somador_cpf = $somador_cpf + ($valor[$i] * $this->pArray_cpf[$i]);
      	}
      
      	$auxiliar = $somador_cpf % 11;
      	$p_digito_verificador = 11 - $auxiliar;

      	if($p_digito_verificador < 2)
        	$p_digito_verificador = 0;
        
      	if($p_digito_verificador == $valor[9]){
      
	        $somador_cpf = 0;
	        for($i = 0; $i < (strlen($valor) - 1); $i++){
	        	$somador_cpf = $somador_cpf + ($valor[$i] * $this->sArray_cpf[$i]);
	        }
	
	        $auxiliar = $somador_cpf % 11;
	        $s_digito_verificador = 11 - $auxiliar;
	
	        if($s_digito_verificador < 2)
	        	$s_digito_verificador = 0;
	        
	        if($s_digito_verificador == $valor[10]){
	        	return true;
	        }else{
	        	$this->erroCPF = 2; // Erro no segundo digito verificador
	        	return false;
	        }
      	}else{
        	$this->erroCPF= 1; // Erro no primeiro digito verificador
        	return false;
      	}
    }
    
    public function validaCNPJ($valor)
    {
      $somador_cnpj = 0;
      //$pArray_cnpj = array(5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);

      for($i = 0; $i < (strlen($valor) - 2); $i++)
      {
        $somador_cnpj = $somador_cnpj + ($valor[$i] * $this->pArray_cnpj[$i]);
      }

      $auxiliar = $somador_cnpj % 11;
      $p_digito_verificador = 11 - $auxiliar;
      
      if($p_digito_verificador < 2)
        $p_digito_verificador = 0;

      if($p_digito_verificador == $valor[12])
      {

        //$sArray_cpj = array(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
        $somador_cnpj = 0;

        for($i = 0; $i < (strlen($valor) - 1); $i++)
        {
          $somador_cnpj = $somador_cnpj + ($valor[$i] * $this->sArray_cpj[$i]);
        }

        $auxiliar = $somador_cnpj % 11;
        $s_digito_verificador = 11 - $auxiliar;
        
        if($s_digito_verificador < 2)
          $s_digito_verificador = 0;

        if($s_digito_verificador == $valor[13])
        {
          return true;
        }
        else
        {
          $this->erroCNPJ = 2; // Erro no segundo digito verificador
          return false;
        }
      }
      else
      {
          $this->erroCNPJ = 1; // Erro no primeiro digito verificador
          return false;
      }


    }
    
    public function validarCNPJ($cnpj) { 
        
        
        if (strlen($cnpj) <> 18) return 0; 
            $soma1 = ($cnpj[0] * 5) + 

            ($cnpj[1] * 4) + 
            ($cnpj[3] * 3) + 
            ($cnpj[4] * 2) + 
            ($cnpj[5] * 9) + 
            ($cnpj[7] * 8) + 
            ($cnpj[8] * 7) + 
            ($cnpj[9] * 6) + 
            ($cnpj[11] * 5) + 
            ($cnpj[12] * 4) + 
            ($cnpj[13] * 3) + 
            ($cnpj[14] * 2); 
            $resto = $soma1 % 11; 
            $digito1 = $resto < 2 ? 0 : 11 - $resto; 
            $soma2 = ($cnpj[0] * 6) + 

            ($cnpj[1] * 5) + 
            ($cnpj[3] * 4) + 
            ($cnpj[4] * 3) + 
            ($cnpj[5] * 2) + 
            ($cnpj[7] * 9) + 
            ($cnpj[8] * 8) + 
            ($cnpj[9] * 7) + 
            ($cnpj[11] * 6) + 
            ($cnpj[12] * 5) + 
            ($cnpj[13] * 4) + 
            ($cnpj[14] * 3) + 
            ($cnpj[16] * 2); 
            $resto = $soma2 % 11; 
            $digito2 = $resto < 2 ? 0 : 11 - $resto; 
            return (($cnpj[16] == $digito1) && ($cnpj[17] == $digito2)); 
        }


  }

?>