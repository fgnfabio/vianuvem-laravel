<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class grupoEstabelecimento
{
	public  $database;
	public  $error;
	private $codGrupoEstabelecimento;
	private $nomGrupoEstabelecimento;
	private $codEmpresa;
	private $codFilial;
	private $codUsuario;
		
	function __construct($database) {
		$this->database = $database;
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getCodGrupoEstabelecimento() {
		return $this->codGrupoEstabelecimento;
	}
	
	public function setCodGrupoEstabelecimento($codGrupoEstabelecimento) {
		$this->codGrupoEstabelecimento = $codGrupoEstabelecimento;
	}
	
	public function getNomGrupoEstabelecimento() {
		return $this->nomGrupoEstabelecimento;
	}
	
	public function setNomGrupoEstabelecimento($nomGrupoEstabelecimento) {
		$this->nomGrupoEstabelecimento = $nomGrupoEstabelecimento;
	}
	
	public function getCodEmpresa() {
		return $this->codEmpresa;
	}
	
	public function setCodEmpresa($codEmpresa) {
		$this->codEmpresa = $codEmpresa;
	}
	
	public function getCodFilial() {
		return $this->codFilial;
	}
	
	public function setCodFilial($codFilial) {
		$this->codFilial = $codFilial;
	}
	
	public function setCodUsuario($codUsuario) {
		$this->codUsuario = $codUsuario;
	}
	
	public function getGrupoEstabelecimento($codGrupoEstabelecimento){
		$objConexao = $this->database;
		
		$strSql = "SELECT F.COD_EMPRESA, G.COD_FILIAL, G.COD_GRUPO_ESTABELECIMENTO, G.NOM_GRUPO_ESTABELECIMENTO 
                     FROM GRUPO_ESTABELECIMENTO G
                     JOIN FILIAL F ON (F.COD_FILIAL = G.COD_FILIAL)
                    WHERE G.COD_GRUPO_ESTABELECIMENTO = :codGrupoEstabelecimento";
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codGrupoEstabelecimento";
		$arrayParametros[0][1] = $codGrupoEstabelecimento;
	
		$arrayGrupo = $objConexao->executeFetch($strSql, $arrayParametros);
	
		$this->codGrupoEstabelecimento 	= $arrayGrupo[0]["COD_GRUPO_ESTABELECIMENTO"];
		$this->nomGrupoEstabelecimento 	= $arrayGrupo[0]["NOM_GRUPO_ESTABELECIMENTO"];
		$this->codFilial				= $arrayGrupo[0]["COD_FILIAL"];
		$this->codEmpresa				= $arrayGrupo[0]["COD_EMPRESA"];
	
		return true;
	}
	
	
	function insGrupoEstabelecimento(){
		
		if(TRUE == $this->nomGrupoEstabelecimento && TRUE == $this->codFilial){
			$objConexao = $this->database;
			
				
			
			$strSql = "SELECT SQ_GREST_01.NEXTVAL AS COD_GRUPO_ESTABELECIMENTO FROM DUAL";
			$arrayGrupoEstabelecimento = $objConexao->executeFetch($strSql);
	
			$strSql = "INSERT INTO GRUPO_ESTABELECIMENTO
					          (COD_FILIAL, COD_GRUPO_ESTABELECIMENTO, NOM_GRUPO_ESTABELECIMENTO) 
					   VALUES (:codFilial, :codGrupoEstabelecimento, :nomGrupoEstabelecimento)";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codFilial";
			$arrayParametros[0][1] = $this->codFilial;
			$arrayParametros[1][0] = ":codGrupoEstabelecimento";
			$arrayParametros[1][1] = $arrayGrupoEstabelecimento[0]['COD_GRUPO_ESTABELECIMENTO']+1;
			$arrayParametros[2][0] = ":nomGrupoEstabelecimento";
			$arrayParametros[2][1] = $this->nomGrupoEstabelecimento;
			
			if (!$objConexao->execute($strSql, $arrayParametros)){
				return false;
			}
	
			return true;
		}else{
			return false;
		}
	}
	
	public function changeGrupoEstabelecimento(){
		if(TRUE == $this->codGrupoEstabelecimento && TRUE == $this->nomGrupoEstabelecimento){
			$objConexao = $this->database;
			
			$strSql = "UPDATE GRUPO_ESTABELECIMENTO 
					      SET NOM_GRUPO_ESTABELECIMENTO = :nomGrupoEstabelecimento,
					          COD_FILIAL = :codFilial 
					    WHERE COD_GRUPO_ESTABELECIMENTO = :codGrupoEstabelecimento";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codGrupoEstabelecimento";
			$arrayParametros[0][1] = $this->codGrupoEstabelecimento;
			$arrayParametros[1][0] = ":nomGrupoEstabelecimento";
			$arrayParametros[1][1] = $this->nomGrupoEstabelecimento;
			$arrayParametros[2][0] = ":codFilial";
			$arrayParametros[2][1] = $this->codFilial;
			
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error = $objConexao->errorInfo();
				return false;
			}
	
			return true;
		}else{
			return false;
		}
	}
	
	function delGrupoEstabelecimento($codGrupoEstabelecimento) {
	
		if (TRUE == $codGrupoEstabelecimento) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codGrupoEstabelecimento";
			$arrayParametros[0][1] = $codGrupoEstabelecimento;
	
			$strSql = "DELETE 
					     FROM GRUPO_ESTABELECIMENTO 
					    WHERE COD_GRUPO_ESTABELECIMENTO =:codGrupoEstabelecimento";
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error = $objConexao->errorInfo();
				return false;
			}
	
			return true;
		} else {
			return false;
		}
	}
	
	public function totalGrupoEstabelecimento(){
		$objConexao = $this->database;
		
		$strSql = "SELECT COUNT(COD_GRUPO_ESTABELECIMENTO) AS TOTAL
                     FROM GRUPO_ESTABELECIMENTO";
		$arrayDados = $objConexao->executeFetch($strSql);
		return $arrayDados[0]["TOTAL"];
	}
	
	public function totalGrupoEstabelecimentoEmpresa(){
		$objConexao = $this->database;
		
		$strSql = "SELECT COUNT(GE.COD_GRUPO_ESTABELECIMENTO) AS TOTAL
                     FROM GRUPO_ESTABELECIMENTO GE
                     JOIN FILIAL F ON (GE.COD_FILIAL = F.COD_FILIAL)
                    WHERE F.COD_EMPRESA = ".$_SESSION['codEmpresa'];
		$arrayDados = $objConexao->executeFetch($strSql);
		return $arrayDados[0]["TOTAL"];
	}
	
	public function listaGrupoEstabelecimentoUsuario(){
		$objConexao = $this->database;
	
		// chama procedure para autenticar e fazer inserções necessarias
		$strSql = "BEGIN PKG_USUARIO.PRC_LISTA_GRUPO_ESTABEL(:COD_USUARIO, :COD_FILIAL, :refc); END;";
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ":COD_USUARIO";
		$arrayParametros[0][1] = $this->codUsuario;
		$arrayParametros[1][0] = ":COD_FILIAL";
		$arrayParametros[1][1] = $this->codFilial;
	
		$retorno = $objConexao->executeProcedureRefCursor($strSql, $arrayParametros);
		return $retorno;
	}
}