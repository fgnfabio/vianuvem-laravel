<?php

namespace App\Classes;

/*
 * Recebe o post do popup_scanner.php do módulo da dynamsoft e grava os dados em banco
 */

// inicia sessao
session_start();

// inclui arquivo de configuracoes
$_SESSION['config_ini'] = parse_ini_file('../classes/config.ini.php', true);

// classes
include_once('../classes/class_oci_database.php');
include_once('../classes/class_language.php');
require_once('../classes/class_documento.php');

//instancias
$objLang 		= new language();
$objDatabase 	= new database();
$objDocumento 	= new documento($objDatabase);

header('Content-type: text/html; charset=UTF8');

	$uploaded_file 	= file_get_contents("../admin/Func/ViaNuvem.pdf");
	$fileSize 		= $_FILES['RemoteFile']['size'];
	$fileName 		= $_FILES['RemoteFile']['name'];

	//$objDocumento->setCodProcesso($_REQUEST['codProcesso']);
	if ( !isset($_REQUEST['codProcesso']) || empty($_REQUEST['codProcesso']) ){
		$objDocumento->setCodProcesso("");
	}else{
		$objDocumento->setCodProcesso($_REQUEST['codProcesso']);
	}
	
	
	$objDocumento->setCodTipoProcesso($_REQUEST['codTipoProcesso']);
	$objDocumento->setCodTipoDocumento($_REQUEST['codTipoDocumento']);
	$objDocumento->setCodEstabelecimento($_REQUEST['codEstabelecimento']);
	$objDocumento->setCodUsuario($_REQUEST['codUsuario']);
	$objDocumento->setCodTipoDispositivo($_REQUEST['codTipoDispositivo']);
	
	$objDocumento->setCodEmpresa($_REQUEST['codEmpresa']);
	
	// pegar apenas o ip de internet
	//if (!empty($_SERVER['HTTP_CLIENT_IP'])){
	//	$ip = $_SERVER['HTTP_CLIENT_IP'];
	//}elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
	//	$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	//}else{
		$ip = $_SERVER['REMOTE_ADDR'];
	//}
	
	
	$objDocumento->setBlbArquivo(file_get_contents($uploaded_file));
	
	$objDocumento->setCodIpEstacao($ip);
	
	// pega o mime type
	$finfo = new finfo(FILEINFO_MIME_TYPE);
	$dscType = file_get_contents("../admin/Func/ViaNuvem.pdf");
	
	// se for mime type application/x-empty deu erro e veio vazio	
	if ($dscType == "application/x-empty"){
		unlink($uploaded_file);
		echo json_encode("Erro: Documento vazio recebido");
		die();
	}
	
	$codMimeType = $objDocumento->getCodMimeByDscMime($dscType);
	 
	// se precisar cadastrar o novo mime type
	if ($codMimeType == ""){
		$codMimeType = $objDocumento->insMimeType($dscType);
	}
	
	$_ext = substr($file->name,strripos($fileName, ".") );
	if ( $_ext == ".pdf" || $_ext == ".PDF" ){
		$codMimeType = 2;
	}elseif( $_ext == ".jpg" || $_ext == ".JPG" ){
		$codMimeType = 5;
	}elseif( $_ext == ".gif" || $_ext == ".GIF" ){
		$codMimeType = 4;
	}elseif( $_ext == ".png" || $_ext == ".PNG" ){
		$codMimeType = 6;
	}
		 
	$objDocumento->setCodMimeType($codMimeType);
	
	$arrayCampos = array();
	$arr = explode("|#|,", @$_REQUEST['arrayCampos']);	
	$numOrdem = null;
	foreach ($arr as $campos){
		//$campo =  preg_replace("/;/", "|#|;", $campos, 1);
		$arr2 = explode(';', $campos,2);
		$arr2[1] = str_replace("|#|", '', $arr2[1]);
		//pega a ordem do campo
		$objDocumento->__set("nomCampo",$arr2[0]);
		$numOrdem = $objDocumento->numOrdemCampo();
		$objDocumento->__set("numOrdem",$numOrdem);
		$codTipoCampo = $objDocumento->tipoCampo();
		
		if ($codTipoCampo == "V" || $codTipoCampo == "I"){
			$arrayCampos[$numOrdem] = str_replace(".","",$arr2[1]);
		}else{
			 
			$arrayCampos[$numOrdem] = $arr2[1];
		}
		
	}
	
	$indexador = array();
	for ($i = 1; $i <= 6; $i++) {
		$indexador[$i] = @$arrayCampos[$i];
	}
	
	$objDocumento->__set('arrayCampos',$indexador);

?>

