<?php

namespace App\Classes;

/**
 * @author Marcos Menezes
 * @date 23/12/2013
 * 
 * Classe criada para substituir a classe de banco PDO, os nomes das funções e logica de execução foram mantidas para
 * não ser necessário a alteração em todo o projeto.
 */

class database {
	
	public  $DBobj;
	public  $error;
	private $SERVIDOR;
	private $BANCO;
	private $USUARIO;
	private $SENHA;
	private $DESCRIPTION;
	private $PORTA;
	private $CHARSET;
	private $transacao;
	
	public $numLinhasAfetadas;

	#--------------------------------------------------------------------------------------------
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	
	/**
	 * Função construtora que inicializa o banco de dados
	 * @return boolean
	 */
	public function __construct() {
		// seta dados pro servidor
		if ($_SERVER['SERVER_ADDR'] == '10.0.0.6'){
			// PRODUCAO
			$this->SERVIDOR		= '10.0.0.4';
			$this->BANCO		= 'SUPREMMA';
			$this->USUARIO		= 'mydocs_adm';
			$this->SENHA		= 'mydocs_adm';
			$this->PORTA		= '1521';
		}else{
			// DESENV
			$this->SERVIDOR		= '23.97.103.79';
			$this->BANCO		= 'SUPREMMA';
			$this->USUARIO		= 'mydocs_adm';
			$this->SENHA		= 'mydocs_adm';
			$this->PORTA		= '22521';
		}
		
		$this->DESCRIPTION		= "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = ".$this->SERVIDOR.")(PORT = ".$this->PORTA."))(CONNECT_DATA = (SERVER = dedicated)(SERVICE_NAME = ".$this->BANCO.")))";
		$this->CHARSET			= 'AL32UTF8';
		
		try {
			// cria conexao
			$this->DBobj = oci_connect($this->USUARIO, $this->SENHA, $this->DESCRIPTION, $this->CHARSET);
			
			if (!$this->DBobj){
				$e = oci_error();
				throw new Exception("Erro de acesso ao banco de dados ".$e['message']);
			}
			
			// altera dados de sessão do oracle
			oci_execute(oci_parse($this->DBobj, "ALTER SESSION SET NLS_DATE_FORMAT='DD/MM/YYYY HH24:MI:SS'")); // altera formato da data e hora
			oci_execute(oci_parse($this->DBobj, "ALTER SESSION SET NLS_SORT='BINARY_AI'")); // altera a ordenação (1,10,2,9,A,á,a,Z,z) (case insensitive e accent incensitive)
			oci_execute(oci_parse($this->DBobj, "ALTER SESSION SET NLS_COMP='LINGUISTIC'")); // comparação linguística (ignora acentos, caracteres maiúsculos e minúsculos)
			

		} catch (Exception $e) {
			die("Error - ".$this->DESCRIPTION." | " . $e->getMessage());
		}

		// seta variavel auxiliar de transação para falso
		$this->transacao = false;
		
		return true;
	}

	#--------------------------------------------------------------------------------------------
	/**
	 * Função de execução de query sem retorno de dados
	 * @param String $strSql Query em sql
	 * @param Array $arrayParametros Array bidimensional [x][0] = nome do parser, [x][1] = valor do parser
	 * @return boolean
	 */
	public function execute($strSql, $arrayParametros = FALSE) {
		// se não enviar query finaliza
		if (!$strSql){
			return false;
		}

		// inicia o parse
		$stmt = oci_parse($this->DBobj, $strSql);

		// faz o bind das variaveis da query
		if ($arrayParametros != FALSE){
			for ($i = 0; $i < count($arrayParametros); $i++){
				oci_bind_by_name($stmt, $arrayParametros[$i][0], $arrayParametros[$i][1], -1);
			}
		}

		// verifica se é transacional ou nao
		if ($this->transacao){
			$mode = OCI_NO_AUTO_COMMIT;
		}else{
			$mode = OCI_COMMIT_ON_SUCCESS;
		}

		// executa query
		oci_execute($stmt, $mode);
		$e = oci_error($stmt);  // For oci_execute errors pass the statement handle
		
		// informa quantas linhas foram afetadas
		$this->numLinhasAfetadas = oci_num_rows($stmt);
		
		// verifica erro
		if ($e){
			$this->error = $e;
			oci_free_statement($stmt);
			return false;
		}else{
			oci_free_statement($stmt);
			return true;
		}

	}

	#--------------------------------------------------------------------------------------------
	/**
	 * Função de execução de query COM retorno de dados
	 * @param String $strSql Query em sql
	 * @param Array $arrayParametros Array bidimensional [x][0] = nome do parser, [x][1] = valor do parser
	 * @return boolean
	 */
	public function executeFetch($strSql, $arrayParametros = FALSE) {
		// se não enviar query finaliza
		if (!$strSql){
			return false;
		}
		
		// inicia o parse
		$stmt = oci_parse($this->DBobj, $strSql);

		// faz o bind das variaveis da query
		if ($arrayParametros != FALSE){
			for ($i = 0; $i < count($arrayParametros); $i++){
				oci_bind_by_name($stmt, $arrayParametros[$i][0], $arrayParametros[$i][1], -1);
			}
		}

		// verifica se é transacional ou nao
		if ($this->transacao){
			$mode = OCI_NO_AUTO_COMMIT;
		}else{
			$mode = OCI_COMMIT_ON_SUCCESS;
		}

		// executa query
		oci_execute($stmt, $mode);

		// verifica erro
		if ($this->error(oci_error($stmt))){
			oci_free_statement($stmt);
			return false;
		}else{
			
			// pega os dados e joga no formato compatível com o retorno da versão em PDO
			while($row = oci_fetch_array($stmt, OCI_ASSOC+OCI_RETURN_NULLS)) {
			   $result[] = $row;
			}
			
			oci_free_statement($stmt);
			
			return @$result;
		}
	}
	
	#--------------------------------------------------------------------------------------------
	/**
	 * Função de execução de procedures COM retorno de dados
	
	 */
	public function executeProcedure($strSql, $arrayParametros = FALSE) {
		// se não enviar query finaliza
		if (!$strSql){
			return false;
		}
	
		// inicia o parse
		$stmt = oci_parse($this->DBobj, $strSql);
	
		// faz o bind das variaveis da query
		if ($arrayParametros != FALSE){
			for ($i = 0; $i < count($arrayParametros); $i++){
				if (isset($arrayParametros[$i][2])){
					oci_bind_by_name($stmt, $arrayParametros[$i][0], $arrayParametros[$i][1], $arrayParametros[$i][2]);
				}else{
					oci_bind_by_name($stmt, $arrayParametros[$i][0], $arrayParametros[$i][1], -1);
				}
			}
		}
	
		// verifica se é transacional ou nao
		if ($this->transacao){
			$mode = OCI_NO_AUTO_COMMIT;
		}else{
			$mode = OCI_COMMIT_ON_SUCCESS;
		}
	
		// executa query
		oci_execute($stmt, $mode);
		
		// verifica erro
		if ($this->error(oci_error($stmt))){
			$this->error = oci_error($stmt);
			oci_free_statement($stmt);
			return false;
		}else{
			
			oci_free_statement($stmt);

			if ($arrayParametros != FALSE){
				return $arrayParametros;
			}else{
				return true;
			}
		}
	}
	
	public function executeProcedureRefCursor($strSql, $arrayParametros = FALSE) {
		// se não enviar query finaliza
		if (!$strSql){
			return false;
		}
		$cursor = oci_new_cursor ( $this->DBobj );
		
		// inicia o parse
		$stmt = oci_parse($this->DBobj, $strSql);
	
		// faz o bind das variaveis da query
		if ($arrayParametros != FALSE){
			for ($i = 0; $i < count($arrayParametros); $i++){
				if (isset($arrayParametros[$i][2])){
					oci_bind_by_name($stmt, $arrayParametros[$i][0], $arrayParametros[$i][1], $arrayParametros[$i][2]);
				}else{
					oci_bind_by_name($stmt, $arrayParametros[$i][0], $arrayParametros[$i][1], -1);
				}
			}
		}
		oci_bind_by_name ( $stmt, 'refc', $cursor, - 1, OCI_B_CURSOR );
		
		// verifica se é transacional ou nao
		if ($this->transacao){
			$mode = OCI_NO_AUTO_COMMIT;
		}else{
			$mode = OCI_COMMIT_ON_SUCCESS;
		}
	
		// executa query
		oci_execute($stmt, $mode);
		oci_execute ($cursor, $mode);
		
	
		// verifica erro
		if ($this->error(oci_error($stmt)) || $this->error(oci_error($cursor)) ){
			oci_free_statement($stmt);
			oci_free_statement($cursor);
			return false;
		}else{
			while($row = oci_fetch_array($cursor, OCI_ASSOC+OCI_RETURN_NULLS)) {
				$result[] = $row;
			}
			
			oci_free_statement($stmt);
			oci_free_statement($cursor);
			
			return @$result;

		}
	}

	#--------------------------------------------------------------------------------------------
	/**
	 * Função para simular inicio de transação
	 * @return boolean
	 */
	public function beginTrans(){
		$this->transacao = true;
		return true;
	}

	#--------------------------------------------------------------------------------------------
	/**
	 * Função para comitar transação
	 * @return boolean
	 */
	public function commitTrans(){
		$this->transacao = false;
		return oci_commit($this->DBobj);
	}

	#--------------------------------------------------------------------------------------------
	/**
	 * Função para desfazer alteraçõs no banco
	 * @return boolean
	 */
	public function rollbackTrans(){
		$this->transacao = false;
		return oci_rollback($this->DBobj);
	}
	
	#--------------------------------------------------------------------------------------------
	/**
	 * Função para detectar erros
	 * @param Array $erro
	 * @return boolean
	 */
	public function error($erro){
	
		if (is_array($erro)){ // tem erro
			return true;
		}else{
			return false;
		}
	}
	
	public function getError(){
		$e = oci_error($this->DBobj);
		return $e['message'];
	}
	
	#--------------------------------------------------------------------------------------------
	/**
	 * Função para fechar o banco
	 * @return boolean
	 */
	public function closeDB(){
		$this->transacao = false;
		return @oci_close($this->DBobj);
	}

	#--------------------------------------------------------------------------------------------
	/**
	 * Função destruidora que fecha o banco
	 * @return boolean
	 */
	function __destruct(){
		return $this->closeDB();
	}
}