<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class processo
{
	public  $database;
	public  $error;
	private $codProcesso;
	private $codTipoProcesso;
	private $codEstabelecimento;
	private $codUsuario;
	private $codIpEstacao;
	private $codMacAdress;
	private $dthCricacao;	
	private $arrayCampos;
	private $codDocumento; //codigo da imagem;
	private $indexador1;
	private $indexador2;
	private $indexador3;
	private $indexador4;
	private $indexador5;
	private $indexador6;
	private $processoAberto;
	private $qtdDocumentosProcesso;
	private $codTipoAndamento;
	private $codTipoNotificacao;
	private $codSituacaoProcesso;
	private $dscObservacao;
	private $dthReferencia;
	private $SituacaoInicial;
	
	
	
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getCodProcesso() {
		return $this->codProcesso;
	}
	
	public function processoAberto() {
		return $this->processoAberto;
	}
	
	public function setCodProcesso($codProcesso) {
		$this->codProcesso = $codProcesso;
	}
	
	public function getCodTipoProcesso() {
		return $this->codTipoProcesso;
	}
	
	public function setCodTipoProcesso($codTipoProcesso) {
		$this->codTipoProcesso = $codTipoProcesso;
	}
	
	public function getCodEstabelecimento() {
		return $this->codEstabelecimento;
	}
	
	public function setCodEstabelecimento($codEstabelecimento) {
		$this->codEstabelecimento = $codEstabelecimento;
	}
	
	public function getCodUsuario() {
		return $this->codUsuario;
	}
	
	public function setCodUsuario($codUsuario) {
		$this->codUsuario = $codUsuario;
	}
	
	public function getCodIpEstacao() {
		return $this->codIpEstacao;
	}
	
	public function setCodIpEstacao($codIpEstacao) {
		$this->codIpEstacao = $codIpEstacao;
	}
	
	public function getCodMacAdress() {
		return $this->codMacAdress;
	}
	
	public function setCodMacAdress($codMacAdress) {
		$this->codMacAdress = $codMacAdress;
	}
	
	public function getDthCricacao() {
		return $this->dthCricacao;
	}
	
	public function setDthCricacao($dthCricacao) {
		$this->dthCricacao = $dthCricacao;
	}
	
	public function getArrayCampos() {
		return $this->arrayCampos;
	}
	
	public function setArrayCampos($arrayCampos) {
		$this->arrayCampos = $arrayCampos;
	}
	
	
	public function getProcesso($codProcesso){
		$objConexao = $this->database;
		return true;
	}
	
	public function insProcesso() {
	
		if (TRUE == $this->codTipoProcesso &&  TRUE == $this->codUsuario) {
			$objConexao = $this->database;
			$objConexao->beginTrans();
			
			//VALIDA SE EXISTE ALGUM PROCESSO COM OS MESMO INDEXADORES EM ABERTO OU PENDENTE	
			$indexador1 = isset($this->indexador1) ? ' AND INDEXADOR1 = :indexador1 ' : ' AND INDEXADOR1 IS NULL ';
			$indexador2 = isset($this->indexador2) ? ' AND INDEXADOR2 = :indexador2 ' : ' AND INDEXADOR2 IS NULL ';
			$indexador3 = isset($this->indexador3) ? ' AND INDEXADOR3 = :indexador3 ' : ' AND INDEXADOR3 IS NULL ';
			$indexador4 = isset($this->indexador4) ? ' AND INDEXADOR4 = :indexador4 ' : ' AND INDEXADOR4 IS NULL ';
			$indexador5 = isset($this->indexador5) ? ' AND INDEXADOR5 = :indexador5 ' : ' AND INDEXADOR5 IS NULL ';
			$indexador6 = isset($this->indexador6) ? ' AND INDEXADOR6 = :indexador6 ' : ' AND INDEXADOR6 IS NULL ';
					
			$strSqlPesquisaProcesso = "SELECT COD_PROCESSO
					                     FROM PROCESSO
					                    WHERE COD_TIPO_PROCESSO = :codTipoProcesso
					                      AND COD_EMPRESA = :codEmpresa".
					                      $indexador1.
					                      $indexador2.
					                      $indexador3.
					                      $indexador4.
					                      $indexador5.
					                      $indexador6.
					                    " AND COD_SITUACAO IN ('A','P')";
				
			$arrayParametrosPesquisa = array();
			$arrayParametrosPesquisa[0][0] = ":codTipoProcesso";
			$arrayParametrosPesquisa[0][1] = $this->codTipoProcesso;
			$arrayParametrosPesquisa[1][0] = ":codEmpresa";
			$arrayParametrosPesquisa[1][1] = $this->codEmpresa;
			$arrayParametrosPesquisa[2][0] = ":indexador1";
			$arrayParametrosPesquisa[2][1] = $this->indexador1;
			$arrayParametrosPesquisa[3][0] = ":indexador2";
			$arrayParametrosPesquisa[3][1] = $this->indexador2;
			$arrayParametrosPesquisa[4][0] = ":indexador3";
			$arrayParametrosPesquisa[4][1] = $this->indexador3;
			$arrayParametrosPesquisa[5][0] = ":indexador4";
			$arrayParametrosPesquisa[5][1] = $this->indexador4;
			$arrayParametrosPesquisa[6][0] = ":indexador5";
			$arrayParametrosPesquisa[6][1] = $this->indexador5;
			$arrayParametrosPesquisa[7][0] = ":indexador6";
			$arrayParametrosPesquisa[7][1] = $this->indexador6;
			
			$this->processoAberto = $objConexao->executeFetch($strSqlPesquisaProcesso, $arrayParametrosPesquisa);	
			
			if (count($this->processoAberto) > 1 ){
				$this->error['code'] 	= "";
				$this->error['message'] = "Foram encontratos mais de 1 processo em Aberto ou Pendente para esse tipo de processo!";
				$objConexao->rollbackTrans();
				return false;
			}elseif(count($this->processoAberto) == 1){
				$this->codProcesso = $this->processoAberto[0]['COD_PROCESSO'];
				
				$strSqlQtdDoc = "SELECT COUNT(COD_DOCUMENTO) AS QTD_DOCUMENTOS
					                   FROM DOCUMENTO
					                  WHERE COD_PROCESSO = :codProcesso";
					
				$arrayParametrosQtdDoc = array();
				$arrayParametrosQtdDoc[0][0] = ":codProcesso";
				$arrayParametrosQtdDoc[0][1] = $this->codProcesso;
				
				$retornoQtd = $objConexao->executeFetch($strSqlQtdDoc, $arrayParametrosQtdDoc);
					
				if (!$retornoQtd){
					$this->error['code'] 	= "";
					$this->error['message'] = "Erro ao iniciar processo";
					$objConexao->rollbackTrans();
					return false;
				}else{
					$this->qtdDocumentosProcesso = $retornoQtd[0]['QTD_DOCUMENTOS'];
				}
				
				
			}else{
				$strSql = "SELECT SQ_PROCE_01.NEXTVAL AS COD_PROCESSO FROM DUAL";
				$proximoProcesso = $objConexao->executeFetch($strSql);
					
				if (!$proximoProcesso){
					$this->error['code'] 	= "";
					$this->error['message'] = "Erro ao gerar código para o novo processo!";
					$objConexao->rollbackTrans();
					return false;
				}else{
					//PEGA A SITUAÇAO INCIAL DO PROCESSO
					$strSqlSitInicial = "SELECT COD_SITUACAO FROM PROCESSO_SITUACAO WHERE COD_TIPO_PROCESSO = :codTipoProcesso AND IND_SITUACAO_INICIAL = 'S'";
						$arrayParametrosSitInicial = array();
						$arrayParametrosSitInicial[0][0] = ":codTipoProcesso";
						$arrayParametrosSitInicial[0][1] = $this->codTipoProcesso;
						$retornoSitInicial = $objConexao->executeFetch($strSqlSitInicial, $arrayParametrosSitInicial);
						
							if (!$retornoSitInicial){
								$this->error['code']	= "";
								$this->error['message'] = "Erro ao pegar situação inicial do Processo";
								$objConexao->rollbackTrans();
								return false;
							}else{
								$this->SituacaoInicial = $retornoSitInicial[0]['COD_SITUACAO'];
							}

					$this->codProcesso = $proximoProcesso[0]["COD_PROCESSO"];
					$this->qtdDocumentosProcesso = 0;
					// insere processo
					$strSql = "INSERT INTO PROCESSO
					          (COD_PROCESSO, COD_TIPO_PROCESSO, COD_EMPRESA, INDEXADOR1, INDEXADOR2, INDEXADOR3, INDEXADOR4, INDEXADOR5, INDEXADOR6, COD_SITUACAO, DTH_ABERTURA)
					   VALUES (:codProcesso, :codTipoProcesso, :codEmpresa, :indexador1, :indexador2, :indexador3, :indexador4, :indexador5, :indexador6, :SituacaoInicial, SYSDATE)";
						
					$arrayParametros = array();
					$arrayParametros[0][0] = ":codProcesso";
					$arrayParametros[0][1] = $this->codProcesso;
					$arrayParametros[1][0] = ":codTipoProcesso";
					$arrayParametros[1][1] = $this->codTipoProcesso;
					$arrayParametros[2][0] = ":codEmpresa";
					$arrayParametros[2][1] = $this->codEmpresa;
					$arrayParametros[3][0] = ":indexador1";
					$arrayParametros[3][1] = $this->indexador1;
					$arrayParametros[4][0] = ":indexador2";
					$arrayParametros[4][1] = $this->indexador2;
					$arrayParametros[5][0] = ":indexador3";
					$arrayParametros[5][1] = $this->indexador3;
					$arrayParametros[6][0] = ":indexador4";
					$arrayParametros[6][1] = $this->indexador4;
					$arrayParametros[7][0] = ":indexador5";
					$arrayParametros[7][1] = $this->indexador5;
					$arrayParametros[8][0] = ":indexador6";
					$arrayParametros[8][1] = $this->indexador6;
					$arrayParametros[9][0] = ":SituacaoInicial";
					$arrayParametros[9][1] = $this->SituacaoInicial;
						
					if (!$objConexao->execute($strSql, $arrayParametros)){
						$this->error['code'] 	= "";
						$this->error['message'] = "Erro ao inserir processo";
						$objConexao->rollbackTrans();
						return false;
					}
									
					$this->codTipoAndamento = 16; //abertura do processo
					if (!$this->insAndamentoProcesso()){
						$this->error['code'] 	= "";
						$this->error['message'] = "Erro ao inserir andamento de abertura do processo";
						$objConexao->rollbackTrans();
						return false;
					}
									
					
					$this->codTipoNotificacao = 'A';
					if (!$this->enviaNotificacaoProcesso() ){
						$this->error['code'] 	= "";
						$this->error['message'] = "Erro ao enviar notificação de abertura do processo";
						$objConexao->rollbackTrans();
						return false;
					}
					
				}
			}
	
			$objConexao->commitTrans();
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "variável não setada";
			return false;
		}
	}
	
	public function insAndamentoProcesso() {
		$objConexao = $this->database;
		
		
		$strDthRef = "SELECT SYSDATE FROM DUAL";			
		$retornoDthReferencia = $objConexao->executeFetch($strDthRef);
		$this->dthReferencia = $retornoDthReferencia[0]['SYSDATE'];		
		// insere processo
		$strSqlAndamento = "INSERT INTO PROCESSO_ANDAMENTO
					                   (COD_PROCESSO, DTH_ANDAMENTO, COD_TIPO_ANDAMENTO, COD_USUARIO, DSC_OBSERVACAO)
					            VALUES (:codProcesso, :dthReferencia, :codTipoAndamento, :codUsuario, :dscObservacao)";
		$arrayParametrosAndamento = array();
		$arrayParametrosAndamento[0][0] = ":codProcesso";
		$arrayParametrosAndamento[0][1] = $this->codProcesso;
		$arrayParametrosAndamento[1][0] = ":codUsuario";
		$arrayParametrosAndamento[1][1] = $this->codUsuario;
		$arrayParametrosAndamento[2][0] = ":codTipoAndamento";
		$arrayParametrosAndamento[2][1] = $this->codTipoAndamento;
		$arrayParametrosAndamento[3][0] = ":dscObservacao";
		$arrayParametrosAndamento[3][1] = $this->dscObservacao;
		$arrayParametrosAndamento[4][0] = ":dthReferencia";
		$arrayParametrosAndamento[4][1] = $this->dthReferencia;
				
		return $objConexao->execute($strSqlAndamento, $arrayParametrosAndamento);
			
	
	}
	
	public function enviaNotificacaoProcesso() {
		$objConexao = $this->database;
		//VALIDA O ENVIO DO E-MAIL DE NOTIFICACAO DE ABERTURA
		$strSqlNotificacao = "BEGIN PRC_ENVIA_NOTIFICACAO_PROCESSO(:COD_TIPO_PROCESSO, :COD_PROCESSO, :COD_TIPO_NOTIFICACAO, :DTH_REFERENCIA); END;";
		
		$arrayParametrosNotificacao = array();
		$arrayParametrosNotificacao[0][0] = ":COD_TIPO_PROCESSO";
		$arrayParametrosNotificacao[0][1] = $this->codTipoProcesso;
		$arrayParametrosNotificacao[1][0] = ":COD_PROCESSO";
		$arrayParametrosNotificacao[1][1] = $this->codProcesso;
		$arrayParametrosNotificacao[2][0] = ":COD_TIPO_NOTIFICACAO";
		$arrayParametrosNotificacao[2][1] = $this->codTipoNotificacao;
		$arrayParametrosNotificacao[3][0] = ":DTH_REFERENCIA";
		$arrayParametrosNotificacao[3][1] = $this->dthReferencia;
		
		return $objConexao->execute($strSqlNotificacao, $arrayParametrosNotificacao);
	
	}
	
	public function updateProcesso(){
		$objConexao = $this->database;
		// insere processo
		$strSqlUpdate = "UPDATE PROCESSO SET COD_SITUACAO = :COD_SITUACAO
				             WHERE COD_PROCESSO = :COD_PROCESSO";
		$arrayParametrosUpdate = array();
		$arrayParametrosUpdate[0][0] = ":COD_PROCESSO";
		$arrayParametrosUpdate[0][1] = $this->codProcesso;
		$arrayParametrosUpdate[1][0] = ":COD_SITUACAO";
		$arrayParametrosUpdate[1][1] = $this->codSituacaoProcesso;
		
		return $objConexao->execute($strSqlUpdate, $arrayParametrosUpdate);
		
		
	}
	public function validaReabertura(){
		//VALIDA SE EXISTE ALGUM PROCESSO COM OS MESMO INDEXADORES EM ABERTO OU PENDENTE
		$objConexao = $this->database;
			
		$strSqlPesquisaProcesso = "SELECT COUNT(P.COD_PROCESSO) AS QTD_PROCESSO
                                     FROM PROCESSO P
                                    WHERE P.COD_PROCESSO =  :codProcesso 
                           AND EXISTS( SELECT PX.COD_PROCESSO
                                             FROM PROCESSO PX
                                            WHERE PX.COD_PROCESSO <> :codProcesso  
                                              AND PX.COD_TIPO_PROCESSO = P.COD_TIPO_PROCESSO
                                              AND PX.COD_SITUACAO IN ('A','P')
                                              AND ((PX.INDEXADOR1 = P.INDEXADOR1) OR (PX.INDEXADOR1 IS NULL AND P.INDEXADOR1 IS NULL ))
                                              AND ((PX.INDEXADOR2 = P.INDEXADOR2) OR (PX.INDEXADOR2 IS NULL AND P.INDEXADOR2 IS NULL ))
                                              AND ((PX.INDEXADOR3 = P.INDEXADOR3) OR (PX.INDEXADOR3 IS NULL AND P.INDEXADOR3 IS NULL ))
                                              AND ((PX.INDEXADOR4 = P.INDEXADOR4) OR (PX.INDEXADOR4 IS NULL AND P.INDEXADOR4 IS NULL ))
                                              AND ((PX.INDEXADOR5 = P.INDEXADOR5) OR (PX.INDEXADOR5 IS NULL AND P.INDEXADOR5 IS NULL ))
                                              AND ((PX.INDEXADOR6 = P.INDEXADOR6) OR (PX.INDEXADOR6 IS NULL AND P.INDEXADOR6 IS NULL ))
                                           )";
		
		$arrayParametrosPesquisa = array();
		$arrayParametrosPesquisa[0][0] = ":codProcesso";
		$arrayParametrosPesquisa[0][1] = $this->codProcesso;
		return $objConexao->executeFetch($strSqlPesquisaProcesso, $arrayParametrosPesquisa);
		
	}
	
}