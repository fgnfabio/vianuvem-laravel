<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class estabelecimento
{
	public    $database;
	public    $error;
	protected $codEmpresa;
	protected $codFilial;
	protected $codGrupoEstabelecimento;
	protected $codEstabelecimento;
	protected $arrayTipoProcesso;
	protected $indExibeHistorico;
	
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getCodEmpresa() {
		return $this->codEmpresa;
	}
	
	public function setCodEmpresa($codEmpresa) {
		$this->codEmpresa = $codEmpresa;
	}
	
	public function getCodFilial() {
		return $this->codFilial;
	}
	
	public function setCodFilial($codFilial) {
		$this->codFilial = $codFilial;
	}
	
	public function getCodGrupoEstabelecimento() {
		return $this->codGrupoEstabelecimento;
	}
	
	public function setCodGrupoEstabelecimento($codGrupoEstabelecimento) {
		$this->codGrupoEstabelecimento = $codGrupoEstabelecimento;
	}
	
	public function getCodEstabelecimento() {
		return $this->codEstabelecimento;
	}
	
	public function setCodEstabelecimento($codEstabelecimento) {
		$this->codEstabelecimento = $codEstabelecimento;
	}

	public function getArrayTipoProcesso() {
		return $this->arrayTipoProcesso;
	}
	
	public function setArrayTipoProcesso($arrayTipoProcesso) {
		$this->arrayTipoProcesso = $arrayTipoProcesso;
	}
	
	public function getIndExibeHistorico(){
		return $this->indExibeHistorico;
	}
	
	public function setIndExibeHistorico($indExibeHistorico){
		$this->indExibeHistorico = $indExibeHistorico;
	}
	
	public function getEstabelecimento($codEstabelecimento){
		
		if(TRUE == $codEstabelecimento){
			$objConexao = $this->database;

			// pega dados do estabelecimento e filial
			$strSql = "SELECT E.COD_ESTABELECIMENTO, E.COD_GRUPO_ESTABELECIMENTO, F.COD_FILIAL, NVL(E.IND_EXIBE_HISTORICO, 'N') AS IND_EXIBE_HISTORICO
  						 FROM ESTABELECIMENTO E
  						 JOIN GRUPO_ESTABELECIMENTO G ON (G.COD_GRUPO_ESTABELECIMENTO = E.COD_GRUPO_ESTABELECIMENTO )
  						 JOIN FILIAL F ON (F.COD_FILIAL = G.COD_FILIAL)
 						WHERE COD_ESTABELECIMENTO = :codEstabelecimento";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codEstabelecimento";
			$arrayParametros[0][1] = $codEstabelecimento;
		
			$arrayEstabelecimento = $objConexao->executeFetch($strSql, $arrayParametros);
		
			$this->codEstabelecimento 		= $arrayEstabelecimento[0]["COD_ESTABELECIMENTO"];
			$this->codGrupoEstabelecimento 	= $arrayEstabelecimento[0]["COD_GRUPO_ESTABELECIMENTO"];
			$this->codFilial				= $arrayEstabelecimento[0]["COD_FILIAL"];
			$this->indExibeHistorico		= $arrayEstabelecimento[0]["IND_EXIBE_HISTORICO"];
			
			// pega os dados de tipo de processos
			$this->getEstabelecimentoTipoProcesso($codEstabelecimento);
		}
		
		return true;
	}
	
	public function getEstabelecimentoCnpj($codEmpresa, $numCnpj){
	
		if(TRUE == $numCnpj && TRUE == $codEmpresa){
			$objConexao = $this->database;
	
			// pega dados do estabelecimento e filial
			$strSql =  "SELECT PJ.COD_EMPRESA, F.COD_FILIAL, GE.COD_GRUPO_ESTABELECIMENTO, PJ.COD_PESSOA AS COD_ESTABELECIMENTO
						  FROM PESSOA_JURIDICA PJ
						  JOIN ESTABELECIMENTO E ON (E.COD_ESTABELECIMENTO = PJ.COD_PESSOA)
						  JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_GRUPO_ESTABELECIMENTO = E.COD_GRUPO_ESTABELECIMENTO)
						  JOIN FILIAL F ON (F.COD_FILIAL = GE.COD_FILIAL)
						 WHERE PJ.NUM_CNPJ = :numCnpj
					       AND PJ.COD_EMPRESA = :codEmpresa";
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ":numCnpj";
			$arrayParametros[0][1] = $numCnpj;
			$arrayParametros[1][0] = ":codEmpresa";
			$arrayParametros[1][1] = $codEmpresa;
	
			$arrayEstabelecimento = $objConexao->executeFetch($strSql, $arrayParametros);
	
			$this->codEstabelecimento 		= $arrayEstabelecimento[0]["COD_ESTABELECIMENTO"];
			$this->codGrupoEstabelecimento 	= $arrayEstabelecimento[0]["COD_GRUPO_ESTABELECIMENTO"];
			$this->codFilial				= $arrayEstabelecimento[0]["COD_FILIAL"];
			$this->codEmpresa				= $arrayEstabelecimento[0]["COD_EMPRESA"];
				
			// pega os dados de tipo de processos
			$this->getEstabelecimentoTipoProcesso($this->codEstabelecimento);
			
			return true;
		}else{
			return false;
		}
	
		
	}
	
	public function getEstabelecimentoTipoProcesso($codEstabelecimento){
		if ($codEstabelecimento){
			$objConexao = $this->database;
		
			$strSql = 'SELECT COD_TIPO_PROCESSO 
					     FROM ESTABELECIMENTO_TIPO_PROCESSO
					    WHERE COD_ESTABELECIMENTO = :codEstabelecimento';
		
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codEstabelecimento";
			$arrayParametros[0][1] = $this->codEstabelecimento;
		
			$this->arrayTipoProcesso = $objConexao->executeFetch($strSql, $arrayParametros);
		}
		return true;
	}
	
	public function insEstabelecimento() {
		$objConexao = $this->database;
		$objConexao->beginTrans();	
		
		// cria estabelecimento
		if (TRUE == $this->codGrupoEstabelecimento && TRUE == $this->codEstabelecimento ) {
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codEstabelecimento";
			$arrayParametros[0][1] = $this->codEstabelecimento;
			$arrayParametros[1][0] = ":codGrupoEstabelecimento";
			$arrayParametros[1][1] = $this->codGrupoEstabelecimento;
			$arrayParametros[2][0] = ":indExibeHistorico";
			$arrayParametros[2][1] = $this->indExibeHistorico;
	
			$strSql = "INSERT INTO ESTABELECIMENTO 
					          (COD_ESTABELECIMENTO, COD_GRUPO_ESTABELECIMENTO, IND_EXIBE_HISTORICO)
					   VALUES (:codEstabelecimento, :codGrupoEstabelecimento, :indExibeHistorico)";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao inserir estabelecimento";
				$objConexao->rollbackTrans();
				return false;
			}

			$objConexao->commitTrans();
			
			return true;
		}else{
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para inserir estabelecimento";
			$objConexao->rollbackTrans();
			return false;
		}
	}
	
		
	public function changeEstabelecimento() {
		$objConexao = $this->database;
		$objConexao->beginTrans();
		
		if (TRUE == $this->codEstabelecimento && TRUE == $this->codGrupoEstabelecimento){
						
						
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codEstabelecimento";
			$arrayParametros[0][1] = $this->codEstabelecimento;
			$arrayParametros[1][0] = ":codGrupoEstabelecimento";
			$arrayParametros[1][1] = $this->codGrupoEstabelecimento;
			$arrayParametros[2][0] = ":indExibeHistorico";
			$arrayParametros[2][1] = $this->indExibeHistorico;
	
			// altera estabelecimento
			$strSql = "UPDATE ESTABELECIMENTO 
					      SET COD_GRUPO_ESTABELECIMENTO = :codGrupoEstabelecimento,
					          IND_EXIBE_HISTORICO = :indExibeHistorico
					    WHERE COD_ESTABELECIMENTO = :codEstabelecimento";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao alterar estabelecimento";
				$objConexao->rollbackTrans();
				return false;
			}
			
			
			
			$objConexao->commitTrans();
			return true;
			
		}else{
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para alterar estabelecimento";
			$objConexao->rollbackTrans();
			return false;
		}
	}
	
	public function delEstabelecimento($codEstabelecimento){
		
		if (TRUE == $codEstabelecimento) {
			$objConexao = $this->database;
			$objConexao->beginTrans();
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codEstabelecimento";
			$arrayParametros[0][1] = $codEstabelecimento;
						
			// deleta estabelecimento
			$strSql = "DELETE 
					     FROM ESTABELECIMENTO 
					    WHERE COD_ESTABELECIMENTO =:codEstabelecimento";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				$this->error['code'] 	= "";
				$this->error['message'] = "Erro ao excluir estabelecimento";
				$objConexao->rollbackTrans();
				return false;
			}
	
			$objConexao->commitTrans();
			
			return true;
		} else {
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando para excluir estabelecimento";
			return false;
		}
	}
	
	public function totalEstabelecimento(){
		$objConexao = $this->database;
	
		$strSql = "SELECT COUNT(COD_ESTABELECIMENTO) AS TOTAL
                     FROM ESTABELECIMENTO";
		$arrayDados = $objConexao->executeFetch($strSql);
		return $arrayDados[0]["TOTAL"];
	}
	
	public function totalEstabelecimentoEmpresa(){
		$objConexao = $this->database;
	
		$strSql = "SELECT COUNT(E.COD_ESTABELECIMENTO) AS TOTAL
				     FROM ESTABELECIMENTO E
				     JOIN GRUPO_ESTABELECIMENTO GE ON (GE.COD_GRUPO_ESTABELECIMENTO = E.COD_GRUPO_ESTABELECIMENTO)
				     JOIN FILIAL F ON (GE.COD_FILIAL = F.COD_FILIAL)
				    WHERE F.COD_EMPRESA = ".$_SESSION['codEmpresa'];
		$arrayDados = $objConexao->executeFetch($strSql);
		return $arrayDados[0]["TOTAL"];
	}
	
	public function listaEstabelecimentosUsuario(){
		$objConexao = $this->database;
	
		// chama procedure para autenticar e fazer inserções necessarias
		$strSql = "BEGIN PKG_USUARIO.PRC_LISTA_ESTABELECIMENTO(:COD_USUARIO, :COD_GRUPO_ESTABEL, :refc); END;";
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ":COD_USUARIO";
		$arrayParametros[0][1] = $this->codUsuario;
		$arrayParametros[1][0] = ":COD_GRUPO_ESTABEL";
		$arrayParametros[1][1] = $this->codGrupoEstabelecimento;
	
		$retorno = $objConexao->executeProcedureRefCursor($strSql, $arrayParametros);
		return $retorno;
	}
	
	public function listaEstabelecimentosUsuarioMobile(){
		$objConexao = $this->database;
		
		//Verifica o tipo de usuário 
			$strSql = "SELECT COD_TIPO_USUARIO FROM USUARIO WHERE COD_USUARIO = :COD_USUARIO";
			
			//parametros
			$arrayParametros = array();
			$arrayParametros[0][0] = "COD_USUARIO";
			$arrayParametros[0][1] = $this->codUsuario;
			
			$arrayPerfilUsuario = $objConexao->executeFetch($strSql, $arrayParametros);
			$indTipoUsuario = $arrayPerfilUsuario[0]['COD_TIPO_USUARIO'];
	
		if($indTipoUsuario == 'E'){
			// pega dados do estabelecimento e filial
			$strSql =  "SELECT EST.COD_GRUPO_ESTABELECIMENTO, EST.COD_ESTABELECIMENTO,
                   NVL (PJ.NOM_FANTASIA, PF.DSC_APELIDO)
                      AS NOM_ESTABELECIMENTO
              FROM USUARIO_ESTABELECIMENTO UE
                   JOIN ESTABELECIMENTO EST
                      ON (EST.COD_ESTABELECIMENTO = UE.COD_ESTABELECIMENTO)
                   LEFT OUTER JOIN PESSOA_FISICA PF
                      ON (PF.COD_PESSOA = EST.COD_ESTABELECIMENTO)
                   LEFT OUTER JOIN PESSOA_JURIDICA PJ
                      ON (PJ.COD_PESSOA = EST.COD_ESTABELECIMENTO)
             WHERE     UE.COD_USUARIO = :COD_USUARIO";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":COD_USUARIO";
			$arrayParametros[0][1] = $this->codUsuario;
			
			$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
			return $retorno;
		}elseif ($indTipoUsuario == 'F'){
		// pega dados do estabelecimento e filial
			$strSql =  "SELECT EST.COD_GRUPO_ESTABELECIMENTO, EST.COD_ESTABELECIMENTO,
                   NVL (PJ.NOM_FANTASIA, PF.DSC_APELIDO)
                      AS NOM_ESTABELECIMENTO, EST.COD_GRUPO_ESTABELECIMENTO AS COD_GRUPO_ESTABELECIMENTO
              FROM ESTABELECIMENTO EST
                   LEFT OUTER JOIN PESSOA_FISICA PF
                      ON (PF.COD_PESSOA = EST.COD_ESTABELECIMENTO)
                   LEFT OUTER JOIN PESSOA_JURIDICA PJ
                      ON (PJ.COD_PESSOA = EST.COD_ESTABELECIMENTO)
             WHERE EST.COD_GRUPO_ESTABELECIMENTO IN (SELECT GE.COD_GRUPO_ESTABELECIMENTO
              FROM USUARIO_FILIAL UF
                   JOIN GRUPO_ESTABELECIMENTO GE
                      ON (GE.COD_FILIAL = UF.COD_FILIAL)
             WHERE     UF.COD_USUARIO = :COD_USUARIO
                   AND UF.COD_FILIAL = :COD_FILIAL)";
				
			$arrayParametros = array();
			$arrayParametros[0][0] = ":COD_USUARIO";
			$arrayParametros[0][1] = $this->codUsuario;
			$arrayParametros[1][0] = ":COD_FILIAL";
			$arrayParametros[1][1] = $this->codFilial;
	
		$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
		return $retorno;
		
		}elseif ($indTipoUsuario == 'G'){
		//pega dados do estabelecimento que tem acesso
			$strSql = "SELECT EST.COD_GRUPO_ESTABELECIMENTO, EST.COD_ESTABELECIMENTO,
                   NVL (PJ.NOM_FANTASIA, PF.DSC_APELIDO)
                      AS NOM_ESTABELECIMENTO
              FROM USUARIO_GRUPO_ESTABELECIMENTO UGE
                   JOIN GRUPO_ESTABELECIMENTO GE
                      ON (GE.COD_GRUPO_ESTABELECIMENTO =
                             UGE.COD_GRUPO_ESTABELECIMENTO)
                   JOIN ESTABELECIMENTO EST
                      ON (EST.COD_GRUPO_ESTABELECIMENTO =
                             GE.COD_GRUPO_ESTABELECIMENTO)
                   LEFT OUTER JOIN PESSOA_FISICA PF
                      ON (PF.COD_PESSOA = EST.COD_ESTABELECIMENTO)
                   LEFT OUTER JOIN PESSOA_JURIDICA PJ
                      ON (PJ.COD_PESSOA = EST.COD_ESTABELECIMENTO)
             WHERE     UGE.COD_USUARIO = :COD_USUARIO";
			
			$arrayParametros = array();
			$arrayParametros[0][0] = ":COD_USUARIO";
			$arrayParametros[0][1] = $this->codUsuario;
			$arrayParametros[1][0] = ":COD_FILIAL";
			$arrayParametros[1][1] = $this->codFilial;
			
			$retorno = $objConexao->executeFetch($strSql, $arrayParametros);
			return $retorno;
		}
	}
}