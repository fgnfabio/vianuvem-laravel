<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class imagem {
	public  $error;
	
	private $largura; // width
	private $altura; // height
	private $imageType;
	private $attributoHtml;
	private $canal; // 3 para imagens RGB e 4 para imagens CMYK.
	private $bits; // número de bits para cada cor
	private $mimeType;
	
		
	function __construct() {
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//

	private function getImageType($codType){
		
		if ($codType > 16){
			$codType = 0;
		}
		
		$arrType[0] = 'UNKNOWN TYPE';
		$arrType[1] = 'GIF';
		$arrType[2] = 'JPG';
		$arrType[3] = 'PNG';
		$arrType[4] = 'SWF';
		$arrType[5] = 'PSD';
		$arrType[6] = 'BMP';
		$arrType[7] = 'TIFF(intel byte order)';
		$arrType[8] = 'TIFF(motorola byte order)';
		$arrType[9] = 'JPC';
		$arrType[10] = 'JP2';
		$arrType[11] = 'JPX';
		$arrType[12] = 'JB2';
		$arrType[13] = 'SWC';
		$arrType[14] = 'IFF';
		$arrType[15] = 'WBMP';
		$arrType[16] = 'XBM';
		
		return $arrType[$codType];
	}
	
	public function getImageInfo($imagem){
		$size = getimagesize($imagem);
		
		if (!$size){
			return false;
		}
		
		$this->largura			= $size[0];
		$this->altura			= $size[1];
		$this->imageType 		= $this->getImageType($size[2]);
		$this->attributoHtml	= $size[3];
		$this->canal			= $size['channels'];
		$this->bits				= $size['bits'];
		$this->mimeType			= $size['mime'];
		return true;
	}
	
	private function createImage($imagem){
		
		$this->getImageInfo($imagem);
		
		switch ($this->imageType) {
			case 'GIF':
				$im = imagecreatefromgif($imagem);
				break;
			case 'JPG':
				$im = imagecreatefromjpeg($imagem);
				break;
			case 'PNG':
				$im = imagecreatefrompng($imagem);
				break;
			default:
				$im = false;
				break;
		}
		return $im;
	}
	
	public function compressImage($imagem, $destino, $qualidade){

		ob_start();
		$im = $this->createImage($imagem);
		imagejpeg($im, $destino, $qualidade);
		$novaImagem = ob_get_contents();
		
		ImageDestroy($im);
		ob_end_clean();

		// retorna stream
		if ($destino == null){
			return $novaImagem;	
		}else{
			return true;
		}
		
	}
}