<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */


Class tipoIdentificador
{
	public  $database;
	public  $error;
	private $codTipoIdentificador;
	private $dscTipoIdentificador;
	private $dscClasseValidacao;
	
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//
	
	public function getDscClasseValidacao() {
		return $this->dscClasseValidacao;
	}
	
	public function setDscClasseValidacao($dscClasseValidacao) {
		$this->dscClasseValidacao = $dscClasseValidacao;
	}
	
	public function getCodTipoIdentificador() {
		return $this->codTipoIdentificador;
	}
	
	public function setCodTipoIdentificador($codTipoIdentificador) {
		$this->codTipoIdentificador = $codTipoIdentificador;
	}
	
	public function getDscTipoIdentificador() {
		return $this->dscTipoIdentificador;
	}
	
	public function setDscTipoIdentificador($dscTipoIdentificador) {
		$this->dscTipoIdentificador = $dscTipoIdentificador;
	}
	
	public function getTipoIdentificador($codTipoIdentificador){
		$objConexao = $this->database;
		
		//Seleciona descrição do tipo identificador
		$strSql = "SELECT COD_TIPO_IDENTIFICADOR, DSC_TIPO_IDENTIFICADOR, DSC_CLASSE_VALIDACAO
				     FROM TIPO_IDENTIFICADOR 
				    WHERE COD_TIPO_IDENTIFICADOR = :codTipoIdentificador";
		 
		$arrayParametros = array();
		$arrayParametros[0][0] = ':codTipoIdentificador';
		$arrayParametros[0][1] = $codTipoIdentificador;
		
		$arrayTipoIdentificador = $objConexao->executeFetch($strSql, $arrayParametros);
		
		$this->codTipoIdentificador = $arrayTipoIdentificador[0]["COD_TIPO_IDENTIFICADOR"];
		$this->dscTipoIdentificador = $arrayTipoIdentificador[0]["DSC_TIPO_IDENTIFICADOR"];
		$this->dscClasseValidacao	= $arrayTipoIdentificador[0]["DSC_CLASSE_VALIDACAO"];
		
		return true;
	}
	
	function insTipoIdentificador() {
		
		if (TRUE == $this->getDscTipoIdentificador()) {
			$objConexao = $this->database;
			
			$strSql = "SELECT COD_TIPO_IDENTIFICADOR 
					     FROM TIPO_IDENTIFICADOR 
					    WHERE ROWNUM = 1 
					    ORDER BY COD_TIPO_IDENTIFICADOR DESC";
			$arrayTipoIdentificador = $objConexao->executeFetch($strSql);
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoIdentificador";
			$arrayParametros[0][1] = $arrayTipoIdentificador[0]["COD_TIPO_IDENTIFICADOR"] + 1;
			$arrayParametros[1][0] = ":dscTipoIdentificador";
			$arrayParametros[1][1] = $this->dscTipoIdentificador;
			$arrayParametros[2][0] = ":dscClasseValidacao";
			$arrayParametros[2][1] = $this->dscClasseValidacao;
	
			$strSql = "INSERT INTO TIPO_IDENTIFICADOR 
					               (COD_TIPO_IDENTIFICADOR, DSC_TIPO_IDENTIFICADOR, DSC_CLASSE_VALIDACAO) 
					        VALUES (:codTipoIdentificador, :dscTipoIdentificador, :dscClasseValidacao)";
	
			if (!$objConexao->execute($strSql, $arrayParametros)){
				return false;
			}
	
			return true;
		}else{
			return false;
		}
	}
	
	function changeTipoIdentificador() {
	
		if (TRUE == $this->getCodTipoIdentificador() && TRUE == $this->dscTipoIdentificador) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoIdentificador";
			$arrayParametros[0][1] = $this->getCodTipoIdentificador();
			$arrayParametros[1][0] = ":dscTipoIdentificador";
			$arrayParametros[1][1] = $this->dscTipoIdentificador;
			$arrayParametros[2][0] = ":dscClasseValidacao";
			$arrayParametros[2][1] = $this->dscClasseValidacao;
	
			$strSql = "UPDATE TIPO_IDENTIFICADOR 
					      SET DSC_TIPO_IDENTIFICADOR =:dscTipoIdentificador,
					          DSC_CLASSE_VALIDACAO   =:dscClasseValidacao 
					    WHERE COD_TIPO_IDENTIFICADOR =:codTipoIdentificador";
	
			if(!$objConexao->execute($strSql, $arrayParametros)){
				return false;
			}
	
			return true;
		} else {
			return false;
		}
	}
	
	function delTipoIdentificador($codTipoIdentificador) {
	
		if (TRUE == $codTipoIdentificador) {
			$objConexao = $this->database;
	
			$arrayParametros = array();
			$arrayParametros[0][0] = ":codTipoIdentificador";
			$arrayParametros[0][1] = $codTipoIdentificador;
	
			$strSql = "DELETE TIPO_IDENTIFICADOR 
					    WHERE COD_TIPO_IDENTIFICADOR =:codTipoIdentificador";
			
			if(!$objConexao->execute($strSql, $arrayParametros)){
				return false;
			}
	
			return true;
		} else {
			return false;
		}
	}
	
	public function getAllTipoIdentificador(){
		$objConexao = $this->database;
	
		//Seleciona descrição do tipo identificador
		$strSql = "SELECT COD_TIPO_IDENTIFICADOR, DSC_TIPO_IDENTIFICADOR, DSC_CLASSE_VALIDACAO
				     FROM TIPO_IDENTIFICADOR 
				    ORDER BY COD_TIPO_IDENTIFICADOR";
	
		$arrayTipoIdentificador = $objConexao->executeFetch($strSql);
	
		return $arrayTipoIdentificador;
	}
	
	public function validaTipoIdentificador($codTipoIdentificador, $codIdentificacao){
		if (!$codTipoIdentificador || !$codIdentificacao) {
			return false;
		}
		
		$objConexao = $this->database;
		
		$strSql = "SELECT DSC_CLASSE_VALIDACAO
				     FROM TIPO_IDENTIFICADOR
				    WHERE COD_TIPO_IDENTIFICADOR = :codTipoIdentificador";
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codTipoIdentificador";
		$arrayParametros[0][1] = $codTipoIdentificador;
		
		$arrayTipoIdentificador = $objConexao->executeFetch($strSql, $arrayParametros);
		
		$classValidacao = $arrayTipoIdentificador[0]['DSC_CLASSE_VALIDACAO'];
		
		// verifica se a identificacao é valida
		if (is_string($classValidacao) && strlen($classValidacao) > 0){
		
			require_once("class_validate.php");
			$validateObj = new validate();
			
			if ($validateObj->$classValidacao($codIdentificacao)){
				return true;
			}else{
				return false;
			}
			 
		}else{ // se nao tem instrucao de validacao passa como verdadeiro
			return true;
		}
	}
	
}