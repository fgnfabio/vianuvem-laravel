<?php

namespace App\Classes;

/**
 * @author: Marcos Menezes
 */

Class auditoria {
	
	public  $database;
	public  $error;
	
	private $codAuditoria;
	private $codEmpresa;
	private $codRecurso;
	private $codUsuario;
	private $dscOperacao;
	private $dthOperacao;
		
	function __construct($database) {
		$this->database = $database;
		$this->error['code'] 	= "";
		$this->error['message'] = "";
		return true;
	}
	
	public function __get($propriedade) {
		return $this->$propriedade;
	}
	
	public function __set($propriedade, $valor) {
		$this->$propriedade = $valor;
	}
	
	//**********************************************************************************************//

	public function getAuditoria(){
		$objConexao = $this->database;

	}
	
	public function insAuditoria(){
		if (!$this->codEmpresa || !$this->codUsuario || !$this->codRecurso){
			$this->error['code'] 	= "";
			$this->error['message'] = "Dados necessários faltando";
			return false;
		}
		
		$objConexao = $this->database;
		
		$strSql = "SELECT SQ_AUDIT_01.NEXTVAL AS COD_AUDITORIA FROM DUAL";
		$arrayAuditoria = $objConexao->executeFetch($strSql);
		$this->codAuditoria = $arrayAuditoria[0]["COD_AUDITORIA"];
		
		if (!$this->codAuditoria ){
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro ao gerar código para auditoria";
			return false;
		}
		
		$arrayParametros = array();
		$arrayParametros[0][0] = ":codAuditoria";
		$arrayParametros[0][1] = $this->codAuditoria;
		$arrayParametros[1][0] = ":codUsuario";
		$arrayParametros[1][1] = $this->codUsuario;
		$arrayParametros[2][0] = ":codEmpresa";
		$arrayParametros[2][1] = $this->codEmpresa;
		$arrayParametros[3][0] = ":dscOperacao";
		$arrayParametros[3][1] = $this->dscOperacao;
		$arrayParametros[4][0] = ":codRecurso";
		$arrayParametros[4][1] = $this->codRecurso;
		
		$strSql = "INSERT INTO AUDITORIA 
				          (COD_AUDITORIA, COD_USUARIO, DTH_OPERACAO, COD_RECURSO, DSC_OPERACAO, COD_EMPRESA) 
				   VALUES (:codAuditoria, :codusuario, SYSDATE, :codRecurso, :dscOperacao, :codEmpresa)";
		
		if (!$objConexao->execute($strSql, $arrayParametros)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro ao inserir auditoria";
			return false;
		}
		
		return true;
	}
	
	public function insAuditoriaWebservice(){
		$objConexao = $this->database;

		if ($_SERVER['SERVER_PORT'] == 443){
			$server_protocol = $_SERVER['SERVER_PROTOCOL'].' (SSL)';
		}else{
			$server_protocol = $_SERVER['SERVER_PROTOCOL'];
		}
	
		$arrayParametros = array();
		$arrayParametros[0][0] = ":dscQueryString";
		$arrayParametros[0][1] = $_SERVER['QUERY_STRING'];
		$arrayParametros[1][0] = ":dscScript";
		$arrayParametros[1][1] = $_SERVER['SCRIPT_NAME'];
		$arrayParametros[2][0] = ":dscRequestMethod";
		$arrayParametros[2][1] = $_SERVER['REQUEST_METHOD'];
		$arrayParametros[3][0] = ":dscRemoteAddr";
		$arrayParametros[3][1] = $_SERVER['REMOTE_ADDR'];
		$arrayParametros[4][0] = ":dscProtocol";
		$arrayParametros[4][1] = $server_protocol;
	
		$strSql = "INSERT INTO AUDITORIA_WEBSERVICE
				          (DTH_INCLUSAO, DSC_QUERY_STRING, DSC_SCRIPT, DSC_REQUEST_METHOD, DSC_REMOTE_ADDR, DSC_PROTOCOL)
				   VALUES (SYSDATE, :dscQueryString, :dscScript, :dscRequestMethod, :dscRemoteAddr, :dscProtocol)";
	
		if (!$objConexao->execute($strSql, $arrayParametros)){
			$this->error['code'] 	= "";
			$this->error['message'] = "Erro ao inserir auditoria de webservice";
			return false;
		}
	
		return true;
	}
}