<?php

namespace App\Http\Middleware;

use Closure;
use App\Classes\database;
use App\Classes\seguranca;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $objSeguranca = new seguranca(new database());

        if ($objSeguranca->verificaSessao() == false) {

            dd($_SESSION);

            return redirect('/login');
        }

        return $next($request);
    }
}
