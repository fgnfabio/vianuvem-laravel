<?php 

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use App\Classes\database;
use App\Classes\usuario;
use App\Classes\seguranca;

class ControllerPaginas extends controller{

	public function captura()
	{
        $nomeUsuario = $_SESSION['nomPessoa'];
        $nomeEmpresa = $_SESSION['nomEmpresa'];

		return view('captura', compact('nomeUsuario', 'nomeEmpresa'));
	}

	public function busca()
	{
		return view('busca');	
	}

	public function logout()
	{
        session_start();
        session_destroy();
        header("Location:index.php");
	}

    public function autentica()
    {
        $login = base64_decode(request()->get('v1'));
        $senha = base64_decode(request()->get('v2'));

        $objSeguranca = new seguranca(new database());

        $auth = $objSeguranca->autentica($login, $senha);

        $redirect = '/login';

        if ($auth) {
            $redirect = 'captura';
        }

        return redirect($redirect);
    }
}

?>