@extends('estrutura')
	
            
	 @section('header-content')
			<div id="actions">
                <section id="page-header" class="section">
                    <div class="card-title">Nova Captura</div>
                    <h6 class="valign-wrapper left"><i class="material-icons circle">folder</i><span class="valign"> Pasta 1 > Processo 1</span></h6>
                    <a class="waves-effect waves-light btn start"><i class="material-icons left">cloud</i>salvar proposta</a>
                </section>
                <section id="form-proposta" class="section">
                    <div class="input-field white-text">
                      <input placeholder="somente números" id="cod_proposta" type="text" class="validate">
                      <label for="cod_proposta">Código da Proposta</label>
                    </div>
                    <div class="input-field">
                      <i class="material-icons prefix" id="work">work</i>
                      <input id="icon_prefix" type="text" class="validate">
                      <label for="icon_prefix">nome da empresa</label>
                    </div>
                    <div class="input-field">
                      <i class="material-icons prefix">assignment</i>
                      <input id="icon_prefix" type="text" class="validate">
                      <label for="icon_prefix">CNPJ</label>
                    </div>
                    
                    <!--- INÍCIO DO TEMPLATE DO DROPZONE -->
                    <div class="section">
                        <button class="btn-large fileinput-button dz-clickable waves-effect waves-light input-field col s6 l6 m6 left">
                                Escolher imagem
                                <i class="material-icons right">note_add</i>
                        </button>
                        <button class="btn-large waves-effect waves-light input-field col s6 l6 m6 right">
                                Iniciar captura
                                <i class="material-icons right">open_in_browser</i>
                        </button>

                        <button class="btn cancel waves-effect waves-light input-field col s12 l12 m12" type="reset" name="action">
                                Cancelar
                                <i class="material-icons right">not_interested</i>
                        </button>
                    </div>
                </section>
            </div>
	@stop
                  

        
    @section('action-content')
        
        <section id="previews" class="files col s3 m3 l3">
        
				<div id="template" class="col s12 m12 l6">            
                    <div class="card small">
                      <div class="card-image">
                        <img data-dz-thumbnail />
                        <span class="card-title" data-dz-name></span>
                      </div>
                      <div class="card-content">
                      	<p class="size" data-dz-size></p>
                        <div id="progress-bar" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
	                        <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                        </div>
                        <!--
                        <button class="btn-floating btn-large waves-effect waves-light start">
                          <i class="material-icons right">send</i>
                        </button>
                        -->
                        <button data-dz-remove class="btn-floating btn-large waves-effect waves-light cancel">
                          <i class="material-icons right">not_interested</i>
                        </button>
                      </div>
                    </div>
                </div>
        </section>               
        
        
        <section id="seletores-uploads" class="col s3 m3 l3">
            <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
              <li class="">
                <div class="collapsible-header">Documento 1</div>
                <div id="teste-drag" class="collapsible-body" style="display: none;"><p>Arraste para cá a sua imagem</p></div>
              </li>
              <li class="active">
                <div class="collapsible-header active">Documento 2</div>
                <div id="receptor" class="collapsible-body" style="display: block;"><p>Arraste para cá a sua imagem</p></div>
              </li>
              <li class="">
                <div class="collapsible-header">Documento 3</div>
                <div class="collapsible-body" style="display: none;"><p>Arraste para cá a sua imagem</p></div>
              </li>
            </ul>
        </section>
                
	@stop