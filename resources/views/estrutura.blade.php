<!DOCTYPE html>
<html>
<head>
    <title>teste</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/app.css">
    <link rel="stylesheet" type="text/css" href="basic.css">
    <link rel="stylesheet" type="text/css" href="dropzone.css">
    <link rel="stylesheet" type="text/css" href="dragula.css">
</head>


<body class="grey lighten-3">

<!-- barra superior + menu -->
<header id="header" class="page-topbar">
    <nav class="white">
        <div class="nav-wrapper">
            <ul>
                <li data-activates="left-sidebar-nav" class="main-menu"><a href="#" ><i class="material-icons cinza-claro">reorder</i></a></li>
                <li>
                    <a href="#" class="brand-logo">
                        <img src="vianuvem-logo.png">
                    </a>
                </li>
            </ul>
            <a id="user-id" class="dropdown-button right cinza-claro" href="#" data-activates="user-options"><i class="material-icons cinza-claro">perm_identity</i> {{ $nomeUsuario  }}</a>
            <ul id="user-options" class="dropdown-content">
                <li><a href="#!">Confgurações</a></li>
                <li class="divider"></li>
                <li><a href="/logout">Logoff</a></li>
            </ul>
        </div>
    </nav>
    <nav>
        <ul id="nav-mobile" class="left">
            <li>
                <select name="" id="">
                    <option value="">opção 1</option>
                    <option value="">opção 2</option>
                    <option value="">opção 3</option>
                </select>
            </li>
            <li>
                <select name="" id="">
                    <option value="">opção 1</option>
                    <option value="">opção 2</option>
                    <option value="">opção 3</option>
                </select>
            </li>
            <li>
                <select name="" id="">
                    <option value="">opção 1</option>
                    <option value="">opção 2</option>
                    <option value="">opção 3</option>
                </select>
            </li>
        </ul>
    </nav>
</header>

<div class="wrapper">

    <!-- Menu geral lateral -->
    <aside id="barra-lateral" class="white">
        <ul id="left-sidebar-nav" class="cinza-escuro white side-nav fixed">
            <li class="no-padding">
                <ul class="collapsible collapsible-accordion">
                    <li>
                        <div class="collapsible-header">
                            <a class=""><i class="material-icons prefix icone-menu" id="work">work</i>Nova Captura</a>
                        </div>
                        <div class="collapsible-body">
                            <ul class="grey lighten-4">
                                <li class="collection-header avatar">
                                    <i class="material-icons circle">folder</i>Arquivo 1
                                </li>

                                <li class="collection-item">
                                    <a href="#!">
                                        <input name="grupo-processos" class="processo" data-cod-processo="2522" type="radio" id="processo-1" />
                                        <label for="processo-1">Processo 1</label>
                                    </a>
                                    <p>explicação do processo</p>
                                </li>
                                <li class="collection-item">
                                    <a href="#!">
                                        <input name="grupo-processos" class="processo" data-cod-processo="2523" type="radio" id="processo-2" />
                                        <label for="processo-2">Processo 2</label>
                                    </a>
                                    <p>explicação do processo</p>
                                </li>
                                <li class="collection-item">
                                    <a href="#!">
                                        <input name="grupo-processos" class="processo" data-cod-processo="2524" type="radio" id="processo-3" />
                                        <label for="processo-3">Processo 3</label>
                                    </a>
                                    <p>explicação do processo</p>
                                </li>

                            </ul>
                        </div>

                    </li>
                </ul>
            </li>
        </ul>
    </aside>

    <!-- content principal -->
    <div class="row">

        <div id="teste-altura" class="main-panel-container col s3 m3 l3 blue lighten-1">
                    <span class="white-text">
    	
			        @yield('header-content')
                    
                    </span>
        </div>

        @yield('action-content')

    </div>

</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/bin/materialize.js"></script>
<script type="text/javascript" src="js/dropzone.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
</body>
</html>
