@extends('estrutura')
	
    @section('conteudo')
        <div class="container">
            <div class="card blue-grey darken-1">
            	<section id="available-uploads" class="col s3 m3 l3">
                    <div id="card-2" class="col s12 m12 l6">            
                        <div class="card small">
                          <div class="card-image">
                            <img src="images/sample-1.jpg">
                            <span class="card-title">Cartão 1</span>
                          </div>
                          <div class="card-content">
                            <a href="#">Isto é um link</a>
                            <a href="#">Isto é um link</a>
                          </div>
                        </div>
                    </div>                
                    <div id="card-1" class="col s12 m12 l6">            
                        <div class="card small">
                          <div class="card-image">
                            <img src="images/sample-1.jpg">
                            <span class="card-title">Cartão 2</span>
                          </div>
                          <div class="card-content">
                            <a href="#">Isto é um link</a>
                            <a href="#">Isto é um link</a>
                          </div>
                        </div>
                    </div>
                    <div class="col s12 m12 l6">            
                        <div class="card small">
                          <div class="card-image" id="card-4">
                            <img src="images/sample-1.jpg">
                            <span class="card-title">Cartão 3</span>
                          </div>
                          <div class="card-content">
                            <a href="#">Isto é um link</a>
                            <a href="#">Isto é um link</a>
                          </div>
                        </div>
                    </div>                
				</section>
            </div>
        </div>
	@stop
        
