									var alturaBanner;
									var dragula = require('dragula');
									
									function arrumaBanner(){
											alturaBanner = $("#left-sidebar-nav").height() - $("#header").height();
											
											$("#teste-altura").css("height", alturaBanner );
										}
								// drag and drop de imagens uploadadas para as categorias
									dragula([document.querySelector('#previews'), document.querySelector('#receptor')]);
									
									
								// drag para upload de imagens
							
									// Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
									var previewNode = document.querySelector("#template");
									previewNode.id = "";
									var previewTemplate = previewNode.parentNode.innerHTML;
									previewNode.parentNode.removeChild(previewNode);
									
									var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
									  url: "http://23.97.101.196/captura", // Set the url
									  thumbnailWidth: 80,
									  thumbnailHeight: 80,
									  parallelUploads: 20,
									  previewTemplate: previewTemplate,
									  autoQueue: false, // Make sure the files aren't queued until manually added
									  previewsContainer: "#previews", // Define the container to display the previews
									  clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
									});
									
									myDropzone.on("addedfile", function(file) {
									  // Hookup the start button
									  file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
									});
									
									// Update the total progress bar
									myDropzone.on("totaluploadprogress", function(progress) {
									  document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
									});
									
									myDropzone.on("sending", function(file) {
									  // Show the total progress bar when upload starts
									  document.querySelector("#total-progress").style.opacity = "1";
									  // And disable the start button
									  file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
									});
									
									// Hide the total progress bar when nothing's uploading anymore
									myDropzone.on("queuecomplete", function(progress) {
									  document.querySelector("#total-progress").style.opacity = "0";
									});
									
									// Setup the buttons for all transfers
									// The "add files" button doesn't need to be setup because the config
									// `clickable` has already been specified.
									document.querySelector("#actions .start").onclick = function() {
									  myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
									};
									document.querySelector("#actions .cancel").onclick = function() {
									  myDropzone.removeAllFiles(true);
									};
									
									$('.dropdown-button').dropdown({
											  inDuration: 300,
											  outDuration: 225,
											  constrain_width: false, // Does not change width of dropdown to that of the activator
											  hover: true, // Activate on hover
											  gutter: 0, // Spacing from edge
											  belowOrigin: false, // Displays dropdown below the button
											  alignment: 'right' // Displays dropdown with edge aligned to the left of button
											}
										  );																	
									
							// Coisas que iniciam durante o carregamento da página
							
									$(document).ready(function(){
										
										arrumaBanner();
										
										$(".main-menu").sideNav({
											menuWidth: 240,
											edge: 'left',
										});
										
										$("#left-sidebar-nav").hover( function(){
												$(".main-menu").sideNav("show");
											}, function(){
												$("#left-sidebar-nav > li > ul > li > .collapsible-body").css("display","none");
												$(".main-menu").sideNav("hide");
											});
										
										
										
										$( window ).resize(function() {	
											$("#teste-altura").css("height", 0 );		
											arrumaBanner();
										});
									
									});
									

